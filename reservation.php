<?php require('./wp/wp-load.php'); ?>
<?php require_once('./lang/lang.php'); ?>
<?php require('./price_table.php'); ?>

<?php get_header(); ?>

<?php $calender = get_option('wp-simple-booking-calendar-options');?>

<?php if(lang()=='ja'): ?>
<script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine-ja.js"></script>
<?php endif; ?>
<?php if(lang()=='en'): ?>
<script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine-en.js"></script>
<?php endif; ?>
<?php if(lang()=='zh'): ?>
<script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine-zh_TW.js"></script>
<?php endif; ?>
<?php if(lang()=='th'): ?>
<script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine-th.js"></script>
<?php endif; ?>

<!-- カレンダー言語設定 -->
<?php $lang_cal = "&amp;hl=".lang(); ?>

<script>
    $(function() {
        $(document).on('click', '.open-cdw', function(event) {
            event.preventDefault();
            $('.modal-cdw').iziModal('open');
        });
        $('.modal-cdw').iziModal({
            headerColor: '#0a8bcf', //ヘッダー部分の色
            width: 600, //横幅
            overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
            fullscreen: true, //全画面表示
            transitionIn: 'fadeInUp', //表示される時のアニメーション
            transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
        });
    });

</script>

<script>
    $(function() {
        $(document).on('click', '.open-rap', function(event) {
            event.preventDefault();
            $('.modal-rap').iziModal('open');
        });
        $('.modal-rap').iziModal({
            headerColor: '#0a8bcf', //ヘッダー部分の色
            width: 600, //横幅
            overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
            fullscreen: true, //全画面表示
            transitionIn: 'fadeInUp', //表示される時のアニメーション
            transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
        });
    });

</script>

<script>
    jQuery(function() {
        $("#maxcheck1").change(function() {
            if ($("#maxcheck1").prop("checked") == true) {
                $("#maxcheck2").prop('checked', false);
            }
        });
        $("#maxcheck2").change(function() {
            if ($("#maxcheck2").prop("checked") == true) {
                $("#maxcheck1").prop('checked', false);
            }
        });
    });

</script>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/calcPrice.js?v=<?php echo date('YmdHis'); ?>"></script>
<script type="text/javascript">
    // wordpressからカレンダー情報を取得する
    var g_cal = <?php echo json_encode($calender); ?>;
    var g_cal_obj = (new Function("return " + g_cal.calendars[1].calendarJson))();

</script>
<div id="reserve" class="subpage">
    <?php include(TEMPLATEPATH.'/part-title.php'); ?>
    <?php include(TEMPLATEPATH.'/part-pan.php'); ?>
    <div id="price">
        <div id="contents_center">
            <section class="calender" id="c05">

                <h2 class="headline01"><?php echo $title_main; ?></h2>

                <ul class="cf">

                    <?php
            if( have_rows('車種',$carmodel_post_id)):
              while( have_rows('車種',$carmodel_post_id) ): the_row(); ?>
                    <li>
                        <h4 class="<?php echo get_sub_field('カラー'); ?>">
                            <?php echo ${get_sub_field('車種コード')}; ?>
                            <?php if(get_sub_field('ペット乗車可能')): ?><?php echo $common_msg18; ?><?php endif; ?>
                            <?php $car_cal_img = get_sub_field('画像_カレンダー上部'); ?>
                            <img src="<?php echo $car_cal_img['sizes']['car_photo_cal'];?>" alt="<?php echo ${get_sub_field('車種コード')};?>">
                        </h4>
                        <div class="ifrm-container">
                            <iframe class="ifrm" src="<?php echo get_sub_field('カレンダー'); ?><?php echo $lang_cal;?>" style="border: 0" frameborder="0" scrolling="no"></iframe>
                        </div>
                    </li>
                    <?php
              endwhile;
            endif;
            ?>
                </ul>
                <p class="text"><?php echo $title_msg; ?></p>
            </section>

            <section class="calc">

                <?php if(lang()=='ja'): ?>
                <h2 class="    headline01">レンタカーの料金</h2>
                <ul class="cf">
                    <li class="fead1"><img src="<?php bloginfo('template_url'); ?>/images/calc_kihon.png">
                        <p class="mt">レンタカー<br class="sp">基本料金</p>
                    </li>
                    <li class="fead2"><img src="<?php bloginfo('template_url'); ?>/images/calc_option.png">
                        <p>オプション<br>追加サービス料金</p>
                    </li>
                    <li class="fead3"><img src="<?php bloginfo('template_url'); ?>/images/calc_kei.png">
                        <p class="kei">ご利用総額</p>
                    </li>
                </ul>
                <?php endif; ?>

                <?php if(lang()=='en'): ?>
                <h2 class="    headline01">Car rental Price</h2>
                <ul class="cf">
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kihon.png">
                        <p class="mt">Car rental basic Price</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_option.png">
                        <p>Option additional<br>service price</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kei.png">
                        <p class="kei">Total price</p>
                    </li>
                </ul>
                <?php endif; ?>


                <?php if(lang()=='zh'): ?>
                <h2 class="    headline01">租車費用</h2>
                <ul class="cf">
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kihon.png">
                        <p class="mt">租車基本費用</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_option.png">
                        <p>加租用品<br>另加計服務費</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kei.png">
                        <p class="kei">總額</p>
                    </li>
                </ul>
                <?php endif; ?>


                <?php if(lang()=='th'): ?>
                <h2 class="    headline01">ราคาค่าเช่า</h2>
                <ul class="cf">
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kihon.png">
                        <p class="mt">ราคาค่าเช่าพื้นฐาน</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_option.png">
                        <p>ค่าอุปกรณ์เสริม</p>
                    </li>
                    <li><img src="<?php bloginfo('template_url'); ?>/images/calc_kei.png">
                        <p class="kei">จำนวนเงินสุทธิ</p>
                    </li>
                </ul>
                <?php endif; ?>


            </section>

            <section id="c06">


                <h2 class="    headline01"><?php echo $title; ?></h2>

                <p style="font-size:18px;color:#f00">※For other languages, please move to another webpage.</p>
                <?php if(lang()=='ja'): ?>
                <ul class="lang-btn cf">
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=en">English（英語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=zh">繁體中文（中国語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=th">ภาษาไทย（タイ語版）</a></p>
                    </li>
                </ul>

                <?php endif; ?>

                <?php if(lang()=='en'): ?>
                <ul class="lang-btn cf">
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=ja">Japanese（日本語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=zh">Chinese（中国語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=th">Thai（タイ語版）</a></p>
                    </li>
                </ul>

                <?php endif; ?>

                <?php if(lang()=='zh'): ?>
                <ul class="lang-btn cf">
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=ja">日本（日本語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=en">英語（英語版）</a></p>
                    </li>
                    <li>
                        <p class="form_en_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=th">泰語（タイ語版）</a></p>
                    </li>
                </ul>

                <?php endif; ?>


                <?php if(lang()=='ja'): ?>
                <div class="tal">
                    <p>ご予約の方、まずはお見積りをという方も、下記フォームよりご連絡ください。<br>
                        「<em>※</em>」は必須項目となります。<br>
                        まずは空車カレンダーで<a href="#c05" style="color: #33F;text-decoration: underline;">予約可能日</a>をご確認下さい<br>
                        ※「仮予約」の場合は空く可能性がございますので<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/" style="color: #33F;text-decoration: underline;">お問合せフォーム</a>より空車待ちも承ります。</p>
                    <p>&nbsp;</p>
                </div>
                <?php endif; ?>
                <?php if(lang()=='en'): ?>
                <div class="tal">
                    <p>Please contact us through this form for the reservation or estimation.<br>
                        “<em>※</em>” fields are requirement.<br>
                        First, please check the calendar for <a href="#c05" style="color: #33F;text-decoration: underline;">reservation availability</a>.<br>
                        ※If the selected date is “Pre-reserved”, we can put you on the waiting list. Please contact us by <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/" style="color: #33F;text-decoration: underline;">“CONTACT FORM”</a>.</p>
                    <p>&nbsp;</p>
                </div>
                <?php endif; ?>
                <?php if(lang()=='zh'): ?>
                <div class="tal">
                    <p>如欲詢價的客戶，請填寫下方表單。<br>
                        「<em>※</em>」為必填項目。<br>
                        請先確認預約現況行程表中<a href="#c05" style="color: #33F;text-decoration: underline;">可預約的日程</a><br>
                        ※「仮予約（暫定預約）」的部分，有可能還可以預約，如欲預約的話請於<a href="http://zh.nomad-r.jp/contact.html" target="_blank" style="color: #33F;text-decoration: underline;">聯繫我們</a>填寫「等待空車」。</p>
                    <p>&nbsp;</p>
                </div>
                <?php endif; ?>
                <?php if(lang()=='th'): ?>
                <div class="tal">
                    <p>ผู้ที่ต้องการเช่ารถกรุณากรอกแบบฟอร์มด้านล่างก่อน<br>
                        「<em>※</em>」จำเป็นต้องกรอก<br>
                        <a href="#c05" style="color: #33F;text-decoration: underline;">ก่อนอื่น กรุณาตรวจสอบวันที่รถว่างจากปฏิทินเช็ครถว่าง</a><br>
                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/" style="color: #33F;text-decoration: underline;">ในกรณีที่เป็นการจองชั่วคราว ยังถือว่าเป็นรถว่าง สามารถสอบถามผ่านอีเมล์ก็ได้</a></p>
                    <p>&nbsp;</p>
                </div>
                <?php endif; ?>


            </section>
        </div>
        <!-- contents_center -->
        <div id="main_contents" class="wrapper cf">
            <div id="contents">
                <section class="form">
                    <form method="post" id="form1" action="mail.php?lang=<?php echo lang(); ?>">
                        <table>
                            <tbody>
                                <tr>
                                    <th><?php echo $carModel; ?><em>※</em></th>
                                    <td>
                                        <ul class="carlist">

                                            <?php if( have_rows('車種',$carmodel_post_id)):
                                          while( have_rows('車種',$carmodel_post_id) ): the_row(); ?>
                                            <li class="cf">
                                                <div class="left">
                                                    <?php $car_form_img = get_sub_field('画像'); ?>
                                                    <img src="<?php echo $car_form_img['sizes']['car_photo_form'];?>" alt="<?php echo ${get_sub_field('車種コード')};?>">
                                                </div>
                                                <!-- left -->
                                                <div class="right">
                                                    <label>
                                                        <input type="radio" data-car="<?php echo get_sub_field('車種コード'); ?>" id="<?php echo ${get_sub_field('車種コード')};?>" name="car" value="<?php echo ${get_sub_field('車種コード').'_val'};?>" class="validate[required]" onclick="onloadCalcPrice()">
                                                        <?php if(get_sub_field('車種クラス')=="premium"): ?><?php echo $common_msg17; ?>
                                                        <?php elseif(get_sub_field('車種クラス')=="luxury"): ?>
                                                        <?php echo $common_msg30; ?>
                                                        <?php endif; ?>
                                                        <?php echo ${get_sub_field('車種コード')};?>
                                                    </label>

                                                    <?php if(lang()=='zh'): ?>
                                                    <span class="hd pc small">（<a href="http://zh.nomad-r.jp/car_type=<?php echo get_sub_field('車種タイプ'); ?>.html" target="_blank"><?php echo $detail1; ?></a>）</span>
                                                    <?php else: ?>
                                                    <span class="hd pc small">（<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=<?php echo get_sub_field('車種タイプ'); ?>" target="_blank"><?php echo $detail1; ?></a>）</span>
                                                    <?php endif; ?>
                                                    <?php if(get_sub_field('ペット乗車可能')): ?>
                                                    <p class="pet_comment">[<?php echo $common_msg15; ?>]</p>
                                                    <?php endif; ?>
                                                </div>
                                                <!-- right -->
                                            </li>
                                            <?php
                                          endwhile;
                                        endif;
                                        ?>
                                        </ul>
                                        <div style="border:2px solid #a00;color:#a00;font-weight:bold;padding:12px 2px;text-align:center;background:#fff;">
                                            コルドランディの予約は12月1日以降から承っております
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="many"><?php echo $departureDesiredTime;?><em>※</em></th>
                                    <td>
                                        <!--<input type="text" name="goDate" class="w25 validate[required]" id="datepicker1"  size="30" value="" placeholder="ご出発希望日をお選びください">-->
                                        <input type="text" name="goDate" class="w25" id="datepicker_1" size="30" placeholder="<?php echo $departureDesiredTimeDeital2; ?>" translate="no" readonly>

                                        <select name="goTime" id="goTime" class="w15 validate[required]">
                                            <option value=''><?php echo $common_msg4; ?></option>
                                            <option value="5:00">5:00</option>
                                            <option value="5:30">5:30</option>
                                            <option value="6:00">6:00</option>
                                            <option value="6:30">6:30</option>
                                            <option value="7:00">7:00</option>
                                            <option value="7:30">7:30</option>
                                            <option value="8:00">8:00</option>
                                            <option value="8:30">8:30</option>
                                            <option value="9:00">9:00</option>
                                            <option value="9:30">9:30</option>
                                            <option value="10:00">10:00</option>
                                            <option value="10:30">10:30</option>
                                            <option value="11:00">11:00</option>
                                            <option value="11:30">11:30</option>
                                            <option value="12:00">12:00</option>
                                            <option value="12:30">12:30</option>
                                            <option value="13:00">13:00</option>
                                            <option value="13:30">13:30</option>
                                            <option value="14:00">14:00</option>
                                            <option value="14:30">14:30</option>
                                            <option value="15:00">15:00</option>
                                            <option value="15:30">15:30</option>
                                            <option value="16:00">16:00</option>
                                            <option value="16:30">16:30</option>
                                            <option value="17:00">17:00</option>
                                            <option value="17:30">17:30</option>
                                            <option value="18:00">18:00</option>
                                            <option value="18:30">18:30</option>
                                            <option value="19:00">19:00</option>
                                            <option value="19:30">19:30</option>
                                            <option value="20:00">20:00</option>
                                            <option value="20:30">20:30</option>
                                            <option value="21:00">21:00</option>
                                            <option value="21:30">21:30</option>
                                            <option value="22:00">22:00</option>
                                        </select>
                                        <br>
                                        <span class="small">※<?php echo $departureDesiredTimeDeital1; ?></span></td>
                                </tr>
                                <tr>
                                    <th><?php echo $returnDesiredTime;?><em>※</em></th>
                                    <td><input type="text" name="backDate" class="w25" size="30" value="" placeholder="<?php echo $returnDesiredTimeDeital2; ?>" id="datepicker_2" translate="no" readonly>
                                        <select name="backTime" id="backTime" class="w15 validate[required]">
                                            <option value=''><?php echo $common_msg4; ?></option>
                                            <option value="5:00">5:00</option>
                                            <option value="5:30">5:30</option>
                                            <option value="6:00">6:00</option>
                                            <option value="6:30">6:30</option>
                                            <option value="7:00">7:00</option>
                                            <option value="7:30">7:30</option>
                                            <option value="8:00">8:00</option>
                                            <option value="8:30">8:30</option>
                                            <option value="9:00">9:00</option>
                                            <option value="9:30">9:30</option>
                                            <option value="10:00">10:00</option>
                                            <option value="10:30">10:30</option>
                                            <option value="11:00">11:00</option>
                                            <option value="11:30">11:30</option>
                                            <option value="12:00">12:00</option>
                                            <option value="12:30">12:30</option>
                                            <option value="13:00">13:00</option>
                                            <option value="13:30">13:30</option>
                                            <option value="14:00">14:00</option>
                                            <option value="14:30">14:30</option>
                                            <option value="15:00">15:00</option>
                                            <option value="15:30">15:30</option>
                                            <option value="16:00">16:00</option>
                                            <option value="16:30">16:30</option>
                                            <option value="17:00">17:00</option>
                                            <option value="17:30">17:30</option>
                                            <option value="18:00">18:00</option>
                                            <option value="18:30">18:30</option>
                                            <option value="19:00">19:00</option>
                                            <option value="19:30">19:30</option>
                                            <option value="20:00">20:00</option>
                                            <option value="20:30">20:30</option>
                                            <option value="21:00">21:00</option>
                                            <option value="21:30">21:30</option>
                                            <option value="22:00">22:00</option>
                                        </select>
                                        <br>
                                        <span class="small">※<?php echo $returnDesiredTimeDeital1; ?></span></td>
                                </tr>
                                <!-- ご出発又は送迎希望場所  -->
                                <tr>
                                    <th><?php echo $departureDesiredLocation; ?><em>※</em></th>
                                    <td class="pd1">
                                        <?php if(false): ?>
                                        <div class="title" style="color:#FF0000;display:block;font-size: 14px;font-weight: bold;">
                                            <?php echo getMultiLang(
                                                'As the Chitose office opens on July 1st, the location where to rent and return will change',
                                                '７月１日 千歳店オープンにつき、貸出返却場所が変わります',
                                                '７月１日 起千歲店開幕 租車還車地點變動',
                                                'เนื่องจากเปิดสาขาที่สนามบินชินจิโตเสะตั้งแต่วันที่1ก.ค. จึงมีการเปลี่ยนสถานที่ให้บริการเช่ารถ'
                                            ); ?>
                                        </div>
                                        <div class="chitose_openmae" id="st_chitose_openmae" sytle="visibility:hidden;">
                                            <div class="disable"></div>
                                            <div class="title-box">

                                                <?php echo getMultiLang(
                                                'Customers who will start rental until June 30th',
                                                'ご利用開始日が6月30日までのお客様はこちら',
                                                '租車開始日於6月30日之前的顧客，請看這裡',
                                                'ลูกค้าที่ใช้บริการถึงวันที่30มิ.ย.เชิญทางนี้'
                                                ); ?>
                                            </div>
                                            <ul class="padding">
                                                <!-- ご来店／駐車場有  -->
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect1 ?>" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect1 ?>
                                                    </label></li>
                                                <!-- 札幌駅北口及び札幌市中心部主要ホテルへの送迎  -->
                                                <li>
                                                    <label for="go04_btn-1">
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect2 ?>" id="go04_btn-1" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect2 ?></label>
                                                    <span class="hd small2">
                                                        <?php if(lang()=='en'): ?>
                                                        ※<?php echo $common_msg22; ?><?php echo $common_msg16; ?>
                                                        <?php else: ?>
                                                        <span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>※<?php echo $common_msg22; ?><span class="pc"><br>&nbsp;&nbsp;&nbsp;</span><?php echo $common_msg16; ?>
                                                        <?php endif; ?>
                                                    </span>

                                                </li>
                                                <!-- 新千歳空港への送迎  -->
                                                <li>
                                                    <label for="go01_btn-1">
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect3 ?>" id="go01_btn-1" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect3 ?>／</label>
                                                    <input type="text" name="place_go_text" id="go01" size="60" value="" class="w30 validate[required]" placeholder="<?php echo $departureDesiredLocationDetail1; ?>" disabled="">
                                                    <br>
                                                    <span class="small">
                                                        <?php if(lang()=='en'): ?>
                                                        ※5,000 JPY for <?php echo $common_msg23; ?><?php echo $common_msg16; ?>
                                                        <?php else: ?>
                                                        <span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        ※<?php echo $common_msg23; ?><span class="pc"><br>&nbsp;&nbsp;&nbsp;</span><?php echo $common_msg16; ?>
                                                        <?php endif; ?>
                                                    </span>
                                                </li>
                                                <!-- その他  -->
                                                <li>
                                                    <label for="go03_btn-1">
                                                        <input type="radio" name="place_go" value="<?php echo $depDesiredLocationSelect5 ?>" id="go03_btn-1" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect5 ?>／</label>
                                                    <input type="text" name="place_go_text" id="go03" size="60" value="" disabled="" class="w30 validate[required]">
                                                    <br>
                                                    <span class="hd small">
                                                        <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span><?php endif; ?>※<?php echo $common_msg3; ?>&nbsp;&nbsp;
                                                        <?php if(lang()=='zh'): ?>
                                                        <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php else: ?>
                                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php endif; ?>
                                                    </span></li>

                                            </ul>
                                        </div>
                                        <!-- chitose_openmae -->
                                        <?php endif; ?>
                                        <div class="chitose_opengo" id="st_chitose_opengo">
                                            <div class="disable"></div>
                                            <?php if(false): ?>
                                            <div class="title-box">
                                                <?php echo getMultiLang(
                                                'Customers who will start rental after July 1st',
                                                'ご利用開始日が7月1日以降のお客様はこちら',
                                                '租車開始日於7月1日之後的顧客，請看這裡',
                                                'ลูกค้าที่ใช้บริการตั้งแต่วันที่1ก.ค.เชิญทางนี้'
                                                ); ?>
                                            </div>
                                            <?php endif; ?>
                                            <h4 class="sub-title">

                                                <?php echo getMultiLang(
                                                'Customers who wish to pick up at Chitose office',
                                                '（1）千歳店での貸出をご希望のお客様',
                                                '希望於千歲店租車的顧客',
                                                'ลูกค้าที่ต้องการเช่ารถที่  สาขาสนามบินชินจิโตเสะ'
                                                ); ?>
                                            </h4>
                                            <ul class="padding outer-bx">
                                                <!--
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect1 ?>(<?php echo getMultiLang('chitose office','千歳店','chitose office','chitose office'); ?>)" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect1 ?>
                                                        （<?php echo $departureDesiredLocationDetail3; ?>）
                                                    </label>
                                                </li>
-->
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo getMultiLang(
                                                        'Transport from New Chitose Airport to Chitose office',
                                                        '新千歳空港から千歳店への送迎',
                                                        '新千歲機場至千歲店之接送',
                                                        'รับส่งจากสาขาสนามบินชินจิโตเสะไปยังสนามบินชินจิโตเสะ'
                                                        ); ?>" onchange="onloadCalcPrice()">

                                                        <?php echo getMultiLang(
                                                        'Transport from New Chitose Airport to Chitose office／',
                                                        '新千歳空港から千歳店への送迎／',
                                                        '新千歲機場至千歲店之接送／',
                                                        'รับส่งจากสาขาสนามบินชินจิโตเสะไปยังสนามบินชินจิโตเสะ'
                                                        ); ?>
                                                    </label>
                                                    <input type="text" name="place_go_text" id="go05" size="60" value="" placeholder="<?php echo $departureDesiredLocationDetail1; ?>" disabled="" class="w30 validate[required]">
                                                    <p class="small">
                                                        <?php echo getMultiLang(
                                                        '※Transport fee is FREE',
                                                        '※送迎料金は無料です',
                                                        '※免費接送',
                                                        '※บริการรับส่งฟรี'
                                                        ); ?>
                                                        <br>
                                                        <?php echo getMultiLang(
                                                        'Please take a taxi from New Chitose Airport to Chitose office. It takes about 10min from airport to the office. We will refund the taxi fare when you arrive at Chitose office. (Please show us receipt of Taxi when you arrive at Chitose office.)',                                               '新千歳空港から千歳店までお客様ご自身でタクシーにてお越しください。乗車時間は約10分です。 千歳店へのご到着時にタクシー代金の領収書またはレシートのご提示で、 新千歳空港から千歳店までのタクシー代金をご返金いたします。',
                                                        '從新千歲機場至千歲店的顧客，請自行搭乘計程車前往。到達千歲店時，請將計程車收據或是費用明細交給我們，我們將會負擔乘車費用。(請記得拿取收據)',
                                                        'คุณลูกค้ากรุณาใช้บริการแท็กซี่จากสนามบินชินจิโตเสะมาที่ร้านสาขาสนามบินจิโตเสะ เมื่อมาถึงที่ร้านกรุณายื่นใบเรียกเก็บเงินหรือใบเสร็จค่าแท็กซี่เพื่อรับเงินค่าเดินทางจากสนามบินมาที่ร้านคืน'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo getMultiLang(
                                                        'Transport from JR Sapporo station, north gate or main hotel in the centre of Sapporo to Chitose office',
                                                        '札幌駅北口及び札幌市中心部主要ホテルから千歳店への送迎',
                                                        '札幌車站北口或札幌市中心主要飯店至千歲店之接送',
                                                        'บริการรับส่งจากสถานีซัปโปโรทางออกด้านเหนือ โรงแรมใจเมืองซัปโปโรถึงร้านที่สนามบินชินจิโตเสะ'
                                                        ); ?>" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang(
                                                        'Transport from JR Sapporo station, north gate or main hotel in the centre of Sapporo to Chitose office',
                                                        '札幌駅北口及び札幌市中心部主要ホテルから千歳店への送迎',
                                                        '札幌車站北口或札幌市中心主要飯店至千歲店之接送',
                                                        'บริการรับส่งจากสถานีซัปโปโรทางออกด้านเหนือ โรงแรมใจเมืองซัปโปโรถึงร้านที่สนามบินชินจิโตเสะ'
                                                        ); ?> </label>
                                                    <p class="small">

                                                        <?php echo getMultiLang(
                                                        '※Extra 8,000 JPY as transport fee',
                                                        '※千歳店への専用ハイヤー送迎料が別途 8,000 円かかります',
                                                        '※前往千歲店的專車接送費為8000日幣(另計)',
                                                        '※บริการรถรับส่งไปยังสาขาสนามบินชินจิโตเสะมีค่าบริการต่างหาก8000เยน'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo getMultiLang(
                                                        'Transport from JR Chitose stationor JR Minami-Chitose station to Chitose office',
                                                        'JR千歳駅又はJR南千歳駅から千歳店への送迎',
                                                        'JR千歳車站或JR南千歳車站至千歳店之接送',
                                                        'บริการรับส่งจากสถานีJRจิโตเสะหรือสถานีJRมินามิจิโตเสะไปถึงร้านสาขาจิโตเสะ'
                                                        ); ?>" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang(
                                                        'Transport from JR Chitose stationor JR Minami-Chitose station to Chitose office',
                                                        'JR千歳駅又はJR南千歳駅から千歳店への送迎',
                                                        'JR千歳車站或JR南千歳車站至千歳店之接送',
                                                        'บริการรับส่งจากสถานีJRจิโตเสะหรือสถานีJRมินามิจิโตเสะไปถึงร้านสาขาจิโตเสะ'
                                                        ); ?>

                                                    </label>
                                                    <p class="small">
                                                        <?php echo getMultiLang(
                                                        '※Transport fee is FREE',
                                                        '※送迎料金は無料です',
                                                        '※免費接送',
                                                        '※ค่าบริการรับส่งฟรี'
                                                        ); ?>
                                                        <br>
                                                        <?php echo getMultiLang(
                                                        'Please take a taxi from JR Chitose station or JR Minami-Chitose station to Chitose office by yourself. It takes about 3min from these stations to the office. We will refund the taxi fare when you arrive at Chitose office. (Please show us receipt of Taxi when you arrive at Chitose office.)',
                                                        'JR千歳駅又はJR南千歳駅から千歳店まではお客様ご自身でタクシーにてお越しください。乗車時間は約３分です。千歳店へのご到着時にタクシー代金のレシートのご提示で、JR千歳駅又はJR南千歳駅から千歳店までのタクシー代金をご返金いたします。',
                                                        '從JR千歳車站或JR南千歳車站至千歳店的顧客，請自行搭乘計程車前往。車程約3分鐘。到達千歲店時，請將計程車收據或是費用明細交給我們，我們將會負擔乘車費用。(請記得拿取收據)',
                                                        'จากสถานีJRจิโตเสะหรือสถานีJRมินามิจิโตเสะไปถึงร้านสาขาจิโตเสะขอความกรุณาลูกค้านั่งรถแท็กซี่มาเองใช้เวลานั่งรถประมาณ3นาทีเมื่อมาถึงร้านสาขาจิโตเสะกรุณารับใบเสร็จจากแท็กซี่ทางเราจะมอบเงินค่าแท็กซี่ตามใบเสร็จระยะทางจากสถานีJRจิโตเสะหรือสถานีJRมินามิจิโตเสะถึงร้านสาขาจิโตเสะ'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <!--
                                                <li>
                                                    <label for="go04_btn-9">
                                                        <input type="radio" name="place_go" value="<?php echo $depDesiredLocationSelect5 ?>" id="go04_btn-9" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect5 ?>／</label>
                                                    <input type="text" name="place_go_text" id="go04" size="60" value="" disabled="" class="w30 validate[required]">
                                                    <br>
                                                    <span class="hd small">
                                                        <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span><?php endif; ?>※<?php echo $common_msg3; ?>&nbsp;&nbsp;
                                                        <?php if(lang()=='zh'): ?>
                                                        <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php else: ?>
                                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php endif; ?>
                                                    </span></li>
-->
                                                <li style="border:1px solid #a00;color:#a00;padding:10px;margin-top:10px;">
                                                    <p>
                                                        <span class="hd small">
                                                            <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc"></span><?php endif; ?>※<?php echo $common_msg3; ?>
                                                            <?php if(lang()=='zh'): ?>
                                                            <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                            <?php else: ?>
                                                            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                            <?php endif; ?>
                                                        </span>
                                                    </p>
                                                    <p><span class="hd small">
                                                            <?php echo getMultiLang('※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください'); ?>
                                                        </span></p>
                                                </li>

                                            </ul>
                                            <?php if(lang()=='ja'): ?>
                                            <h4 class="sub-title pt">（2）清田貸出ベースでの貸出をご希望のお客様</h4>
                                            <ul class="padding outer-bx">
                                                <!-- ご来店／駐車場有  -->
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect1 ?>(<?php echo getMultiLang('kiyota office','清田ベース','kiyota office','kiyota office'); ?>)" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect1 ?>
                                                        （<?php echo $departureDesiredLocationDetail3; ?>）
                                                        (<?php echo getMultiLang('Transport service is unavailable','送迎不要の方のみ','僅限不需要接送服務者','ลูกค้าที่ไม่ต้องการบริการรับส่ง'); ?>)
                                                    </label></li>
                                            </ul>
                                            <?php else: ?>
                                            <div style="visibility:hidden;">
                                                <input type="radio" name="place_go" class="validate[required]" value="<?php echo $depDesiredLocationSelect1 ?>(<?php echo getMultiLang('kiyota office','清田ベース','kiyota office','kiyota office'); ?>)" onchange="onloadCalcPrice()">
                                            </div>
                                            <?php endif; ?>

                                        </div>
                                        <!-- chitose_opengo -->
                                    </td>
                                </tr>
                                <!-- ご返却又は送迎希望場所  -->
                                <tr>
                                    <th><?php echo $returnDesiredLocation; ?><em>※</em></th>
                                    <td class="pd1">
                                        <?php if(false): ?>
                                        <div class="title" style="color:#FF0000;display:block;font-size: 14px;font-weight: bold;">
                                            <?php echo getMultiLang(
                                                'As the Chitose office opens on July 1st, the location where to rent and return will change',
                                                '７月１日 千歳店オープンにつき、貸出返却場所が変わります',
                                                '７月１日 起千歲店開幕 租車還車地點變動',
                                                'เนื่องจากเปิดสาขาที่สนามบินชินจิโตเสะตั้งแต่วันที่1ก.ค. จึงมีการเปลี่ยนสถานที่ให้บริการเช่ารถ'
                                            ); ?>
                                        </div>
                                        <div class="chitose_openmae" id="ed_chitose_openmae">
                                            <div class="disable"></div>
                                            <div class="title-box">
                                                <?php echo getMultiLang(
                                                'Customers who will drop off camping car until June 30th',
                                                'ご返却日が6月30日までのお客様はこちら',
                                                '還車日於6月30日之前的顧客，請看這裡',
                                                'ลูกค้าที่คืนรถภายในวันที่30มิ.ย. เชิญทางนี้'
                                                ); ?>
                                            </div>
                                            <ul class="padding">
                                                <!-- ご来店及び札幌市中心部主要ホテルへの送迎 -->
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_back" value="<?php echo $retDesiredLocationSelect1 ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $retDesiredLocationSelect1 ?></label>
                                                </li>
                                                <!-- 札幌駅北口 -->
                                                <li>
                                                    <label for="back04_btn-1">
                                                        <input type="radio" name="place_back" value="<?php echo $retDesiredLocationSelect2 ?>" id="back04_btn-1" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $retDesiredLocationSelect2 ?></label>
                                                    <span class="hd small2">
                                                        <?php if(lang()=='en'): ?>
                                                        ※<?php echo $common_msg24; ?><?php echo $common_msg16; ?>
                                                        <?php else: ?>
                                                        <span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>※<?php echo $common_msg24; ?><span class="pc"><br>&nbsp;&nbsp;&nbsp;</span><?php echo $common_msg16; ?>
                                                        <?php endif; ?>

                                                    </span>
                                                </li>
                                                <!-- 新千歳空港への送迎 -->
                                                <li>
                                                    <label for="back02_btn-1">
                                                        <input type="radio" name="place_back" value="<?php echo $retDesiredLocationSelect3 ?>" id="back02_btn-1" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $retDesiredLocationSelect3 ?></label>
                                                    <input type="text" name="place_back_text" id="back02" size="60" value="" placeholder="<?php echo $returnDesiredLocationDetail1; ?>" disabled="" class="w30 validate[required]">
                                                    <br>
                                                    <span class="small">
                                                        <?php if(lang()=='en'): ?>
                                                        ※5,000 JPY for <?php echo $common_msg25; ?><?php echo $common_msg16; ?>
                                                        <?php else: ?>
                                                        <span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        ※<?php echo $common_msg25; ?><span class="pc"><br>&nbsp;&nbsp;&nbsp;</span><?php echo $common_msg16; ?>
                                                        <?php endif; ?>

                                                    </span>
                                                </li>
                                                <!-- その他　-->
                                                <li>
                                                    <label for="back03_btn-1">
                                                        <input type="radio" name="place_back" value="<?php echo $retDesiredLocationSelect5 ?>" id="back03_btn-1" class="w25 validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $retDesiredLocationSelect5 ?>／</label>
                                                    <input type="text" name="place_back_text" id="back03" size="60" value="" disabled="" class="w30 validate[required]">
                                                    <br>
                                                    <span class="hd small">
                                                        <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span><?php endif; ?>※<?php echo $common_msg3; ?>&nbsp;&nbsp;
                                                        <?php if(lang()=='zh'): ?>
                                                        <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php else: ?>
                                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php endif; ?>
                                                    </span></li>
                                            </ul>
                                        </div>
                                        <?php endif; ?>
                                        <!-- chitose_openmae -->
                                        <div class="chitose_opengo" id="ed_chitose_opengo">
                                            <div class="disable"></div>
                                            <?php if(false): ?>
                                            <div class="title-box pt">
                                                <?php echo getMultiLang(
                                                'Customers who will drop off camping car after July 1st',
                                                'ご返却日が7月1日以降のお客様はこちら',
                                                '還車日於7月1日之後的顧客，請看這裡',
                                                'ลูกค้าที่คืนรถตั้งแต่วันที่1ก.ค. เชิญทางนี้'
                                                ); ?>
                                            </div>
                                            <?php endif; ?>
                                            <h4 class="sub-title">

                                                <?php echo getMultiLang(
                                                'Customers who wish to drop off at Chitose office',
                                                '（1）千歳店へご返却のお客様',
                                                '千歲店還車的顧客',
                                                'ลูกค้าที่รับมอบคืนรถที่ ร้านสาขาสนามบินชินจิโตเสะ'
                                                ); ?>
                                            </h4>
                                            <ul class="padding outer-bx">
                                                <!--
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_back" class="validate[required]" value="<?php echo getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน'); ?>(<?php echo getMultiLang('chitose office','千歳店','chitose office','chitose office'); ?>)" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน'); ?>
                                                    </label>
                                                </li>-->
                                                <li>
                                                    <label for="back05_btn-1">
                                                        <input type="radio" name="place_back" value="<?php echo getMultiLang(
                                                        'Transport from Chitose office to New Chitose Airport',
                                                        '千歳店より新千歳空港への送迎',
                                                        '千歲店至新千歲機場之接送',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงสนามบินชินจิโตเสะ'
                                                        ); ?>" id="back05_btn-1" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang(
                                                        'Transport from Chitose office to New Chitose Airport／',
                                                        '千歳店より新千歳空港への送迎／',
                                                        '千歲店至新千歲機場之接送／',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงสนามบินชินจิโตเสะ／'
                                                        ); ?>
                                                    </label>


                                                    <input type="text" name="place_back_text" id="back05" size="60" value="" placeholder="<?php echo $departureDesiredLocationDetail1; ?>" disabled="" class="w30 validate[required]"> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p class="small">
                                                        <?php echo getMultiLang(
                                                        '※Transport fee is FREE',
                                                        '※送迎料金は無料です',
                                                        '※免費接送',
                                                        '※ค่าบริการรับส่งฟรี'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_back" class="validate[required]" value="<?php echo getMultiLang(
                                                        'Transport from Chitose office to JR Sapporo station, north gate or main hotel in the centre of Sapporo',
                                                        '千歳店より札幌駅北口及び札幌市中心部主要ホテルへの送迎',
                                                        '從千歲店至札幌車站北口或札幌市中心主要飯店之接送',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงสถานีรถไฟซัปโปโรทางออกเหนือหรือโรงแรมใจกลางเมืองซัปโปโร'
                                                        ); ?>" onchange="onloadCalcPrice()">

                                                        <?php echo getMultiLang(
                                                        'Transport from Chitose office to JR Sapporo station, north gate or main hotel in the centre of Sapporo',
                                                        '千歳店より札幌駅北口及び札幌市中心部主要ホテルへの送迎',
                                                        '從千歲店至札幌車站北口或札幌市中心主要飯店之接送',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงสถานีรถไฟซัปโปโรทางออกเหนือหรือโรงแรมใจกลางเมืองซัปโปโร'
                                                        ); ?>

                                                    </label>
                                                    <p class="small">
                                                        <?php echo getMultiLang(
                                                        '※Extra 8,000 JPY as transport fee',
                                                        '※千歳店への専用ハイヤー送迎料が別途 8,000 円かかります',
                                                        '※前往千歲店的專車接送費為8000日幣(另計)',
                                                        '※บริการรถรับส่งไปยังสาขาสนามบินชินจิโตเสะมีค่าบริการต่างหาก8000เยน'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_back" class="validate[required]" value="<?php echo getMultiLang(
                                                        'Transport from Chitose office to JR Chitose station',
                                                        '千歳店よりJR千歳駅への送迎',
                                                        '千歲店至JR千歲車站之接送',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงรถไฟเจอาร์สถานีจิโตเสะ'
                                                        ); ?>" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang(
                                                        'Transport from Chitose office to JR Chitose station',
                                                        '千歳店よりJR千歳駅への送迎',
                                                        '千歲店至JR千歲車站之接送',
                                                        'รับส่งจากร้านสาขาสนามบินจิโตเสะถึงรถไฟเจอาร์สถานีจิโตเสะ'
                                                        ); ?>
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp; <p class="small">
                                                        <?php echo getMultiLang(
                                                        '※Transport fee is FREE',
                                                        '※送迎料金は無料です',
                                                        '※免費接送',
                                                        '※บริการรับส่งฟรี'
                                                        ); ?>
                                                    </p>
                                                    <br>
                                                    <p class="small">
                                                        <?php echo getMultiLang(
                                                        '※It&#39;s about 30minutes from JR Chitose Station to Sapporo Station',
                                                        '※JR千歳駅までは札幌駅から電車で30分です',
                                                        '※從札幌站到JR千歲站的電車車程約30分鐘',
                                                        '※เดินทางจากสถานีซัปโปโรถึงสถานีJRจิโตเสะด้วยรถไฟ30นาที'
                                                        ); ?>
                                                    </p>
                                                </li>
                                                <!--
                                                <li>
                                                    <label for="back06_btn-1">
                                                        <input type="radio" name="place_back" value="<?php echo $depDesiredLocationSelect5 ?>" id="back06_btn-1" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $depDesiredLocationSelect5 ?>／</label>
                                                    <input type="text" name="place_back_text" id="back06" size="60" value="" disabled="" class="w30 validate[required]">
                                                    <br>
                                                    <span class="hd small">
                                                        <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;</span><?php endif; ?>※<?php echo $common_msg3; ?>&nbsp;&nbsp;
                                                        <?php if(lang()=='zh'): ?>
                                                        <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php else: ?>
                                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                        <?php endif; ?>
                                                    </span></li>
-->
                                                <li style="border:1px solid #a00;color:#a00;padding:10px;margin-top:10px;">
                                                    <p>
                                                        <span class="hd small">
                                                            <?php if(lang()=='ja' || lang()=='zh'): ?><span class="pc"></span><?php endif; ?>※<?php echo $common_msg3; ?>
                                                            <?php if(lang()=='zh'): ?>
                                                            <a href="http://zh.nomad-r.jp/guide.html/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                            <?php else: ?>
                                                            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05" target="_blank"><?php echo $common_msg21; ?></a>
                                                            <?php endif; ?>
                                                        </span>
                                                    </p>
                                                    <p><span class="hd small">
                                                            <?php echo getMultiLang('※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください','※道内主要空港へ配車ご希望の方は別途ご連絡ください'); ?>
                                                        </span></p>
                                                </li>

                                            </ul>
                                            <?php if(lang()=='ja'): ?>
                                            <h4 class="sub-title pt">
                                                （2）清田貸出ベースへご返却のお客様
                                            </h4>
                                            <ul class="padding outer-bx">
                                                <!-- ご来店／駐車場有  -->
                                                <li>
                                                    <label>
                                                        <input type="radio" name="place_back" class="validate[required]" value="<?php echo getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน'); ?>(<?php echo getMultiLang('kiyota office','清田ベース','kiyota office','kiyota office'); ?>)" onchange="onloadCalcPrice()">
                                                        <?php echo getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน'); ?>
                                                        (<?php echo getMultiLang('Transport service is unavailable','送迎不要の方のみ','僅限不需要接送服務者','ลูกค้าที่ไม่ต้องการบริการรับส่ง'); ?>)
                                                    </label>
                                                </li>
                                            </ul>
                                            <?php else: ?>
                                            <div style="visibility:hidden;">
                                                <input type="radio" name="place_back" class="validate[required]" value="<?php echo getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน'); ?>(<?php echo getMultiLang('kiyota office','清田ベース','kiyota office','kiyota office'); ?>)" onchange="onloadCalcPrice()">
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <!-- chitose_opengo -->
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $applicant; ?><em>※</em></th>
                                    <td><input type="text" name="name" id="name" class="w30 validate[required]" size="60" value="" onkeyup="updateField();"></td>
                                </tr>
                                <?php if(isset($nameKana)): ?>
                                <tr>
                                    <th><?php echo $nameKana; ?> <em>※</em></th>
                                    <td><input type="text" name="name_furigana" class="w30 validate[required]" size="60" value=""></td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th><?php echo $tel; ?><em>※</em></th>
                                    <td><input type="text" name="tel" class="w30 validate[required]" size="60" value=""></td>
                                </tr>
                                <tr>
                                    <th><?php echo $email; ?><em>※</em></th>
                                    <td><input type="email" name="mailadd" size="60" value="" class="validate[required,custom[email]]"></td>
                                </tr>
                                <tr>
                                    <th><?php echo $address; ?><em>※</em></th>
                                    <td>
                                        <ul>
                                            <li><?php echo $nationality; ?>
                                                <input type="text" name="country" class="w30 validate[required]" size="60" value="">
                                            </li>
                                            <li><?php if(lang()=='ja'): ?>〒<?php endif; ?>
                                                <?php if(lang()=='zh'): ?>（POST CODE）<?php endif; ?>
                                                <input type="text" name="zip" class="w10 validate[required]" size="10" maxlength="10" value="" placeholder="0040812">

                                            </li>
                                            <li>
                                                <input type="text" name="address" class="w43 address validate[required]" size="60" value="">
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $birth; ?><em>※</em></th>
                                    <td><select name="birth[][/]" id="birth_year" style="width:5em;" onchange="onloadCalcPrice()">
                                            <option value="1900">1900</option>
                                            <option value="1901">1901</option>
                                            <option value="1902">1902</option>
                                            <option value="1903">1903</option>
                                            <option value="1904">1904</option>
                                            <option value="1905">1905</option>
                                            <option value="1906">1906</option>
                                            <option value="1907">1907</option>
                                            <option value="1908">1908</option>
                                            <option value="1909">1909</option>
                                            <option value="1910">1910</option>
                                            <option value="1911">1911</option>
                                            <option value="1912">1912</option>
                                            <option value="1913">1913</option>
                                            <option value="1914">1914</option>
                                            <option value="1915">1915</option>
                                            <option value="1916">1916</option>
                                            <option value="1917">1917</option>
                                            <option value="1918">1918</option>
                                            <option value="1919">1919</option>
                                            <option value="1920">1920</option>
                                            <option value="1921">1921</option>
                                            <option value="1922">1922</option>
                                            <option value="1923">1923</option>
                                            <option value="1924">1924</option>
                                            <option value="1925">1925</option>
                                            <option value="1926">1926</option>
                                            <option value="1927">1927</option>
                                            <option value="1928">1928</option>
                                            <option value="1929">1929</option>
                                            <option value="1930">1930</option>
                                            <option value="1931">1931</option>
                                            <option value="1932">1932</option>
                                            <option value="1933">1933</option>
                                            <option value="1934">1934</option>
                                            <option value="1935">1935</option>
                                            <option value="1936">1936</option>
                                            <option value="1937">1937</option>
                                            <option value="1938">1938</option>
                                            <option value="1939">1939</option>
                                            <option value="1940">1940</option>
                                            <option value="1941">1941</option>
                                            <option value="1942">1942</option>
                                            <option value="1943">1943</option>
                                            <option value="1944">1944</option>
                                            <option value="1945">1945</option>
                                            <option value="1946">1946</option>
                                            <option value="1947">1947</option>
                                            <option value="1948">1948</option>
                                            <option value="1949">1949</option>
                                            <option value="1950">1950</option>
                                            <option value="1951">1951</option>
                                            <option value="1952">1952</option>
                                            <option value="1953">1953</option>
                                            <option value="1954">1954</option>
                                            <option value="1955">1955</option>
                                            <option value="1956">1956</option>
                                            <option value="1957">1957</option>
                                            <option value="1958">1958</option>
                                            <option value="1959">1959</option>
                                            <option value="1960">1960</option>
                                            <option value="1961">1961</option>
                                            <option value="1962">1962</option>
                                            <option value="1963">1963</option>
                                            <option value="1964">1964</option>
                                            <option value="1965">1965</option>
                                            <option value="1966">1966</option>
                                            <option value="1967">1967</option>
                                            <option value="1968">1968</option>
                                            <option value="1969">1969</option>
                                            <option value="1970" selected="selected">1970</option>
                                            <option value="1971">1971</option>
                                            <option value="1972">1972</option>
                                            <option value="1973">1973</option>
                                            <option value="1974">1974</option>
                                            <option value="1975">1975</option>
                                            <option value="1976">1976</option>
                                            <option value="1977">1977</option>
                                            <option value="1978">1978</option>
                                            <option value="1979">1979</option>
                                            <option value="1980">1980</option>
                                            <option value="1981">1981</option>
                                            <option value="1982">1982</option>
                                            <option value="1983">1983</option>
                                            <option value="1984">1984</option>
                                            <option value="1985">1985</option>
                                            <option value="1986">1986</option>
                                            <option value="1987">1987</option>
                                            <option value="1988">1988</option>
                                            <option value="1989">1989</option>
                                            <option value="1990">1990</option>
                                            <option value="1991">1991</option>
                                            <option value="1992">1992</option>
                                            <option value="1993">1993</option>
                                            <option value="1994">1994</option>
                                            <option value="1995">1995</option>
                                            <option value="1996">1996</option>
                                            <option value="1997">1997</option>
                                            <option value="1998">1998</option>
                                            <option value="1999">1999</option>
                                            <option value="2000">2000</option>
                                            <option value="2001">2001</option>
                                            <option value="2002">2002</option>
                                            <option value="2003">2003</option>
                                            <option value="2004">2004</option>
                                            <option value="2005">2005</option>
                                            <option value="2006">2006</option>
                                            <option value="2007">2007</option>
                                            <option value="2008">2008</option>
                                            <option value="2009">2009</option>
                                            <option value="2010">2010</option>
                                            <option value="2011">2011</option>
                                            <option value="2012">2012</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                        <?php echo $y; ?>
                                        <select name="birth[][/]" id="birth_month" style="width:4em;" onchange="onloadCalcPrice()">
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                            <option value="05">5</option>
                                            <option value="06">6</option>
                                            <option value="07">7</option>
                                            <option value="08">8</option>
                                            <option value="09">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                        <?php echo $m; ?>
                                        <select name="birth[][]" id="birth_day" style="width:4em;" onchange="onloadCalcPrice()">
                                            <option value="01">1</option>
                                            <option value="02">2</option>
                                            <option value="03">3</option>
                                            <option value="04">4</option>
                                            <option value="05">5</option>
                                            <option value="06">6</option>
                                            <option value="07">7</option>
                                            <option value="08">8</option>
                                            <option value="09">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                        <?php echo $d; ?></td>
                                </tr>
                                <tr>
                                    <th><?php echo $emergency; ?><em>※</em></th>
                                    <td>
                                        <ul class="emergency">
                                            <li><?php echo $name; ?>
                                                <input type="text" id="driver_name" name="emagency[][／]" class="w25 validate[required]" size="60" value="">
                                            </li>
                                            <li><?php echo $relationship; ?>
                                                <input type="text" name="emagency[][／]" class="w5 validate[required]" size="60" value="">
                                                <br class="sp">
                                                <?php echo $tel; ?>
                                                <input type="text" name="emagency[][]" class="emagency_tel validate[required] w25" size="60" value="">
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $numOfpassengers; ?><em>※</em></th>
                                    <td class="pd1">
                                        <ul>
                                            <li><?php echo $adult ?>
                                                <select name="adult" id="sel_adult1" class="w5 validate[required]" onchange="onloadCalcPrice()">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                </select>
                                                <!--
                                                <select name="adult2" id="sel_adult2" style="display:none;" class="w5 validate[required]" onchange="onloadCalcPrice()">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                </select>-->
                                                <?php echo $common_msg10; ?>　　<br class="sp">
                                                <?php echo $child ?>
                                                <select name="child" class="w5" onchange="onloadCalcPrice()">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                <?php echo $common_msg10; ?><br>
                                                <span class="winter_msg" style="color:#FF0000;padding:2% 0;display:block;font-size: 13px;font-weight: bold;">
                                                    <?php echo $common_msg29; ?>
                                                </span>
                                            </li>
                                            <li>

                                                <?php if(lang()=='ja'): ?>

                                                <div id="pet_disp" style="display:none;">
                                                    <label>
                                                        <input type="checkbox" name="pet" id="pet" value="<?php echo $common_msg11; ?>" onclick="onloadCalcPrice()">
                                                        ペットの乗車
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    ペット数
                                                    <select name="pet_num" id="pet_num" class="w5" onchange="onloadCalcPrice()" disabled>
                                                        <option value="1">１匹</option>
                                                        <option value="2">２匹</option>
                                                        <option value="3">３匹以上</option>
                                                    </select>
                                                    <p style="background:#fff;padding:5px ;border:1px solid #ccc;margin-top:5px;">※清掃料 (1レンタルにつき)<br>　１匹　　 8,000円<br>　２匹　　 10,000円<br>　３匹以上 12,000円</p>
                                                </div>
                                                <?php endif; ?>

                                                <?php if(lang()=='en'): ?>

                                                <div id="pet_disp" style="display:none;">
                                                    <label>
                                                        <input type="checkbox" name="pet" id="pet" value="<?php echo $common_msg11; ?>" onclick="onloadCalcPrice()">
                                                        Boarding of pet
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    Number of pet
                                                    <select name="pet_num" id="pet_num" class="w5" onchange="onloadCalcPrice()" disabled>
                                                        <option value="1">For 1</option>
                                                        <option value="2">For 2</option>
                                                        <option value="3">For 3 or more than3</option>
                                                    </select>
                                                    <p style="background:#fff;padding:5px ;border:1px solid #ccc;margin-top:5px;">※Cleaning Fee(By 1 rental)<br>　For 1 　 8,000yen<br>　For 2　　 10,000yen<br>　For 3 or more than3 12,000yen<p>
                                                </div>
                                                <?php endif; ?>


                                                <?php if(lang()=='zh'): ?>

                                                <div id="pet_disp" style="display:none;">
                                                    <label>
                                                        <input type="checkbox" name="pet" id="pet" value="<?php echo $common_msg11; ?>" onclick="onloadCalcPrice()">
                                                        寵物共乘
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    寵物數量
                                                    <select name="pet_num" id="pet_num" class="w5" onchange="onloadCalcPrice()" disabled>
                                                        <option value="1">１隻</option>
                                                        <option value="2">２隻</option>
                                                        <option value="3">３隻以上</option>
                                                    </select>
                                                    <p style="background:#fff;padding:5px ;border:1px solid #ccc;margin-top:5px;">※清掃料 (以單次承租為計)<br>　１隻　　 8,000日幣<br>　２隻　　 10,000日幣<br>　３隻以上 12,000日幣<p>
                                                </div>
                                                <?php endif; ?>

                                                <?php if(lang()=='th'): ?>

                                                <div id="pet_disp" style="display:none;">
                                                    <label>
                                                        <input type="checkbox" name="pet" id="pet" value="<?php echo $common_msg11; ?>" onclick="onloadCalcPrice()">
                                                        สัตว์เลี้ยงสามารถขึ้นรถได้
                                                    </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                    จำนวนสัตว์เลี้ยง
                                                    <select name="pet_num" id="pet_num" class="w5" onchange="onloadCalcPrice()" disabled>
                                                        <option value="1">1ตัว</option>
                                                        <option value="2">2ตัว</option>
                                                        <option value="3">3ตัวขึ้นไป</option>
                                                    </select>
                                                    <p style="background:#fff;padding:5px ;border:1px solid #ccc;margin-top:5px;">*ค่าทำความสะอาด (ต่อการเช่า1ครั้ง)<br>　1ตัว　　 8000เยน<br>　2ตัว　　 10000เยน<br>　3ตัวขึ้นไป　　12000เยน</p>
                                                </div>
                                                <?php endif; ?>


                                            </li>
                                            <li><?php echo $NumberOfChildSeat; ?>
                                                <select name="child_seat" class="w5">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                &nbsp;&nbsp; <br class="sp">
                                                <?php echo $common_msg5; ?>
                                                <!-- <input type="text" name="child_seat_age" class="w7" size="60" value=""> -->
                                                <select name="child_seat_age" value="" class="w5">
                                                    <option value=""></option>
                                                    <option value="0">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                                <?php echo $common_msg8; ?></li>
                                            <li><?php echo $NumberOfJuniorSeat; ?>
                                                <select name="junior_seat" class="w5">
                                                    <option value=""></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                                &nbsp;&nbsp; <br class="sp">
                                                <?php echo $common_msg5; ?>
                                                <!--                                             <input type="text" name="junior_seat_age" class="w7" size="60" value=""> -->
                                                <select name="junior_seat_age" value="" class="w5">
                                                    <option value=""></option>
                                                    <option value="0">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                                <?php echo $common_msg8; ?></li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $driver; ?><em>※</em><span class="small">※<?php echo $driverDetail1 ?></span></th>
                                    <td class="pd1">
                                        <ul style="margin-bottom: 10px;">
                                            <li>
                                                <h4>■ <?php echo $driver1 ?><em>※</em>
                                                </h4>
                                                <p><?php echo $name ?>
                                                    <input type="text" name="driver1_name" id="driver1_name" class="w25 validate[required]" size="60" value="">
                                                    <?php if(is_mobile()): ?><br><?php endif; ?><?php echo $common_msg5 ?>
                                                    <select id="driver1_age" name="driver1_age" value="" class="w6">
                                                        <option value=""></option>
                                                        <?php
                                                        for ($i = 20 ; $i <= 90; $i++) {
                                                            echo "<option value=".$i.">".$i."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </p>

                                            </li>
                                            <li><?php echo $driversLicenseNo ?>
                                                <input type="text" name="driver1_no" class="w25 validate[required]" size="60" value="">
                                            </li>
                                            <li style="margin-top: 15px;">
                                                <hr>
                                                <h4>■ <?php echo $driver2 ?></h4>
                                                <p><?php echo $name ?>
                                                    <input type="text" name="driver2_name" class="w25" size="60" value="">
                                                    <?php if(is_mobile()): ?><br><?php endif; ?><?php echo $common_msg5 ?>
                                                    <select name="driver2_age" value="" class="w6">
                                                        <option value=""></option>
                                                        <?php
                                                        for ($i = 20 ; $i <= 90; $i++) {
                                                            echo "<option value=".$i.">".$i."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </p>
                                            </li>
                                            <li><?php echo $tel ?>
                                                <input type="text" name="driver2_tel" class="w25" size="60" value="">
                                            </li>
                                            <li><?php echo $driversLicenseNo ?>
                                                <input type="text" name="driver2_no" class="w25" size="60" value="">
                                            </li>
                                            <li style="margin-top: 15px;">
                                                <hr>
                                                <h4>■ <?php echo $driver3 ?></h4>
                                                <p><?php echo $name ?>
                                                    <input type="text" name="driver3_name" class="w25" size="60" value="">
                                                    <?php if(is_mobile()): ?><br><?php endif; ?><?php echo $common_msg5 ?>
                                                    <select name="driver3_age" value="" class="w6">
                                                        <option value=""></option>
                                                        <?php
                                                        for ($i = 20 ; $i <= 90; $i++) {
                                                            echo "<option value=".$i.">".$i."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </p>
                                            </li>
                                            <li><?php echo $tel ?>
                                                <input type="text" name="driver3_tel" class="w25" size="60" value="">
                                            </li>
                                            <li><?php echo $driversLicenseNo ?>
                                                <input type="text" name="driver3_no" class="w25" size="60" value="">
                                            </li>
                                            <li style="margin-top: 15px;">
                                                <hr>
                                                <h4>■ <?php echo $driver4 ?></h4>
                                                <p><?php echo $name ?>
                                                    <input type="text" name="driver4_name" class="w25" size="60" value="">
                                                    <?php if(is_mobile()): ?><br><?php endif; ?><?php echo $common_msg5 ?>
                                                    <select name="driver4_age" value="" class="w6">
                                                        <option value=""></option>
                                                        <?php
                                                        for ($i = 20 ; $i <= 90; $i++) {
                                                            echo "<option value=".$i.">".$i."</option>";
                                                        }
                                                    ?>
                                                    </select>
                                                </p>
                                            </li>
                                            <li><?php echo $tel ?>
                                                <input type="text" name="driver4_tel" class="w25" size="60" value="">
                                            </li>
                                            <li><?php echo $driversLicenseNo ?>
                                                <input type="text" name="driver4_no" class="w25" size="60" value="">
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $discountPlan; ?>
                                        <?php if(lang()=='zh'): ?>
                                        <a href="http://zh.nomad-r.jp/price#c03" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php else:?>
                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c03" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php endif;?>
                                    </th>
                                    <td>
                                        <label for="disc_repeater">
                                            <input type="checkbox" name="disc_repeater" value="<?php echo $repeatCustomDisc ?>" id="disc_repeater" onchange="onloadCalcPrice()">
                                            <?php echo $repeatCustomDisc ?><span class="small">&nbsp;<?php echo $repeatCustomDiscDetail1 ?></span></label>
                                        <?php if(lang()=='ja'): ?>
                                        <br>
                                        <label for="disc_doumin">
                                            <input type="checkbox" name="disc_doumin" value="道民割引" id="disc_doumin" onchange="onloadCalcPrice()">&nbsp;道民割引<span class="small">&nbsp;（運転される方が北海道在住の場合）</span></label>
                                        <?php endif; ?>
                                        <br>※<?php echo $repeatCustomDiscDetail2 ?>
                                    </td>
                                </tr>
                                <?php if(lang()!='ja'): ?>
                                <tr id="hep_disp" style="display:none;">
                                    <th>（Foreign Visitors to Japan Only）<br><?php echo $hep4 ?><br><br><span class="small"><?php echo $hep6; ?></span></th>
                                    <td>
                                        <label><input type="radio" name="hep" class="validate[required]" value="<?php echo $common_msg26 ?>" onchange="onloadCalcPrice()"><?php echo $common_msg26 ?></label>&nbsp;&nbsp;
                                        <label><input type="radio" name="hep" class="validate[required]" value="<?php echo $common_msg27 ?>" onchange="onloadCalcPrice()"><?php echo $common_msg27 ?></label>
                                        <p><?php echo $hep1 ?><br>
                                            <?php echo $hep2 ?><br>
                                            <span class="small2">※<?php echo $hep3 ?><br>
                                                <?php echo $hep5 ?></span></p>
                                        <a href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">(<?php echo $detail1; ?>)</a>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <tr>
                                    <th><?php echo $paymentMethods; ?><em>※</em></th>
                                    <td><select name="payout" class="validate[required]">
                                            <option value="" selected="selected"><?php echo $paymentMethodsSelect1; ?></option>
                                            <!--                                            <option value="<?php echo $paymentMethodsSelect2; ?>"><?php echo $paymentMethodsSelect2; ?></option>-->
                                            <option value="<?php echo $paymentMethodsSelect3; ?>"><?php echo $paymentMethodsSelect3; ?></option>
                                        </select>
                                        <br>
                                        <?php if(lang()=='zh'): ?>
                                        <a href="http://zh.nomad-r.jp/guide.html/#c08" target="_blank">(<?php echo $detail3; ?>)</a></td>
                                    <?php else: ?>
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c09" target="_blank">(<?php echo $detail3; ?>)</a></td>
                                    <?php endif; ?>

                                </tr>
                                <tr>
                                    <th>
                                        <p><?php echo $cdw1; ?><em>※</em>
                                            <br><span class="small"><?php echo $cdw2; ?></span></p>
                                        <br>
                                        <p class="cdw"><?php echo $cdw3; ?><a class="open-cdw" href="#"><?php echo do_shortcode('[wp-svg-icons icon="question" wrap="i"]'); ?></a></p>
                                        <div class="modal-cdw" data-izimodal-loop="" data-izimodal-title="<?php echo $cdw3; ?>？">
                                            <p><?php echo $cdw4; ?><br><br></p>
                                        </div>

                                        <p class="cdw"><?php echo $cdw5; ?><a class="open-rap" href="#"><?php echo do_shortcode('[wp-svg-icons icon="question" wrap="i"]'); ?></a></p>
                                        <div class="modal-rap" data-izimodal-loop="" data-izimodal-title="<?php echo $cdw5; ?>？">
                                            <p><?php echo $cdw6; ?></p>
                                        </div>
                                        <br>
                                        <?php if(lang()=='zh'): ?>
                                        <a href="http://zh.nomad-r.jp/guide.html/#c02" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php else: ?>
                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php endif; ?>
                                    </th>
                                    <td>
                                        <div class="japan">
                                            <!--                                             <h3><?php echo $cdwDetail1; ?></h3> -->
                                            <ul>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail11; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail2; ?></label></li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail12; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail3; ?></label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail13; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail4; ?></label>
                                                    <p style="text-indent: 0px; padding-left:0;"><span class="small2">※<?php echo $cdwDetail5; ?></span></p>
                                                    <?php if(lang()=='ja'): ?>
                                                    <p style="text-indent: 0px; padding-left:0;"><span class="small2">※ご自身の車の任意保険に付帯する「他車運転特約」をご利用いただく場合は加入の必要がございませんので、ご確認くださいませ。</span></p>
                                                    <?php endif; ?>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- <div class="abroad">
                                            <h3><?php echo $cdwDetail6; ?></h3>
                                            <ul class="padding">
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail11; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail7; ?></label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail12; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail8; ?></label>
                                                </li>
                                                <li>
                                                    <label>
                                                        <input type="radio" name="cdw" value="<?php echo $cdwDetail13; ?>" class="validate[required]" onchange="onloadCalcPrice()">
                                                        <?php echo $cdwDetail9; ?></label>
                                                </li>
                                            </ul>
                                        </div> -->
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $bedClean; ?></th>
                                    <td><?php echo $bedCleanDetail1; ?><br>
                                        <span class="small">※<?php echo $bedCleanDetail2; ?></span></td>
                                </tr>
                                <tr>
                                    <th><?php echo $rentalItem; ?><br>
                                        <?php if(lang()=='zh'): ?>
                                        <a href="http://zh.nomad-r.jp/rental.html" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php else: ?>
                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/" target="_blank">(<?php echo $detail2; ?>)</a>
                                        <?php endif; ?>

                                        <br><br><span class="hd small"><?php echo $rentalItemDetail1; ?><br>
                                            ※<?php echo $rentalItemDetail2; ?></span></th>
                                    <td>
                                        <ul class="goods">
                                            <?php
                                        if( have_rows('有料レンタルグッズ',$rental_post_id)):
                                          while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
                                            <?php if(!get_sub_field('非表示')): ?>
                                            <li>
                                                <label for="<?php echo get_sub_field('レンタルグッズコード'); ?>">
                                                    <input type="checkbox" <?php if(get_sub_field('セット貸出有')): ?> name="rental[][／]" <?php else: ?> name="rental[][<?php echo get_sub_field('助数詞'); ?>br]" <?php endif; ?> value="<?php echo ${'rental_'.get_sub_field('レンタルグッズコード')."_val"};?>" id="<?php echo get_sub_field('レンタルグッズコード'); ?>" onclick="onloadCalcPrice()">
                                                    <?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?>
                                                </label>

                                                <?php if(get_sub_field('セット貸出有')): ?>
                                                <select name="rental[][<?php echo get_sub_field('助数詞'); ?>br]" class="w5" id="<?php echo get_sub_field('レンタルグッズコード'); ?>_num" onchange="onloadCalcPrice()" disabled="">
                                                    <?php $i = 1; ?>
                                                    <?php while($i <= get_sub_field('最大選択組数')): ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?><?php echo get_sub_field('助数詞'); ?></option>
                                                    <?php $i++ ?>
                                                    <?php endwhile; ?>
                                                </select>
                                                <?php endif; ?>

                                                <?php if(${'rental_'.get_sub_field('レンタルグッズコード')."_h"}!=""): ?>
                                                <br>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ${'rental_'.get_sub_field('レンタルグッズコード')."_h"};?>
                                                <?php endif; ?>
                                            </li>
                                            <?php endif; ?>
                                            <?php
                                          endwhile;
                                        endif;
                                        ?>
                                        </ul>
                                        <p class="tax">※<?php echo $common_msg9; ?></p>
                                    </td>
                                </tr>
                                <tr>
                                    <th><?php echo $mainPersonTrip; ?></th>
                                    <td><textarea name="main_comment" cols="50" rows="15" placeholder="<?php echo $mainPersonTripDetail1; ?>&#13;&#10;<?php echo $mainPersonTripDetail2; ?>&#13;&#10;<?php echo $mainPersonTripDetail3; ?>
"></textarea></td>
                                </tr>
                                <tr>
                                    <th><?php echo $qa; ?><br>
                                        <span class="small">※<?php echo $qa_detail; ?></span></th>
                                    <td><textarea name="question" cols="50" rows="15"></textarea></td>
                                </tr>
                                <tr>
                                    <th><?php echo $shokai; ?><br><span class="small"><?php echo $shokai_detail1; ?></span></th>
                                    <td><input type="text" name="shokai" id="shokai" class="w30" size="60" value=""></td>
                                </tr>
                                <tr>
                                    <th><?php echo $contractAgreement; ?><em>※</em></th>
                                    <td>■
                                        <?php if(lang()=='zh'): ?>
                                        <a href="http://zh.nomad-r.jp/guide.html/#c09" target="_blank"><?php echo $contractAgreementDetail1; ?></a>
                                        <?php else: ?>
                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c08" target="_blank"><?php echo $contractAgreementDetail1; ?></a>
                                        <?php endif; ?>
                                        <br>
                                        <label>
                                            <input type="checkbox" name="accident" class="validate[required]" value="<?php echo $contractAgreementDetail5; ?>">
                                            <?php echo $contractAgreementDetail2; ?></label><br><br>
                                        <label>
                                            ■
                                            <?php if(lang()=='zh'): ?>
                                            <a href="http://zh.nomad-r.jp/yakkan.html" target="_blank"><?php echo $contractAgreementDetail3; ?></a>
                                            <?php else: ?>
                                            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan/" target="_blank"><?php echo $contractAgreementDetail1; ?></a>
                                            <?php endif; ?>
                                            <br>
                                            <input type="checkbox" name="yakkan" class="validate[required]" value="<?php echo $contractAgreementDetail5; ?>">
                                            <?php echo $contractAgreementDetail4; ?></label>
                                        <br></td>
                                </tr>
                                <?php if(lang()!='ja'): ?>
                                <?php if(lang()=='zh'): ?>
                                <tr>
                                    <th>
                                        給國外客戶<em>※</em>
                                    </th>
                                    <td>
                                        <p>※有關在日本自駕所需的駕照資料</p>
                                        <p class="text-indent">①如果您是簽署日內瓦公約的國家，請攜帶在有效期內的“國際駕駛執照”+“護照”<br>*請自行確認可以簽發國際駕照的國家</p>
                                        <p class="text-indent">②如果您是簽署維也納條約的國家*，請攜帶指定機關簽發的”駕照正本”+“駕照日文譯本”+“護照”<br>*台灣，瑞士，德國，法國，比利時，摩納哥，愛沙尼亞</p><br>
                                        <label><input class="validate[minCheckbox[1]]" type="checkbox" name="group1[]" id="maxcheck1" value="自行攜帶「日本國內駕駛執照」。" />自行攜帶「日本國內駕駛執照」。</label><br><br>
                                        <label><input class="validate[minCheckbox[1]]" type="checkbox" name="group1[]" id="maxcheck2" value="自行攜帶「國際駕駛執照+護照」。" />自行攜帶「國際駕駛執照+護照」。</label><br><br>
                                        <label><input class="validate[minCheckbox[1]]" type="checkbox" name="group1[]" id="maxcheck3" value="自行攜帶「駕照正本+駕照日文譯本+護照」。" />自行攜帶「駕照正本+駕照日文譯本+護照」。</label><br><br>
                                        關於台灣駕照日文譯本詳細內容請看<a href="http://zh.nomad-r.jp/faq.html" target="_blank">這裡</a>

                                    </td>
                                </tr>
                                <?php else: ?>
                                <tr>
                                    <th>
                                        <?php echo $passport1; ?><em>※</em>
                                    </th>
                                    <td>
                                        <p class="text-indent"><?php echo $passport2; ?></p>
                                        <p class="text-indent"><?php echo $passport3; ?></p><br>
                                        <label><input class="validate[minCheckbox[1]]" type="checkbox" name="group1[]" id="maxcheck1" value="<?php echo $passport4; ?>" /><?php echo $passport4; ?></label><br><br>
                                        <label><input class="validate[minCheckbox[1]]" type="checkbox" name="group1[]" id="maxcheck2" value="<?php echo $passport5; ?>" /><?php echo $passport5; ?></label>
                                    </td>
                                </tr>
                                <?php endif; ?>
                                <?php endif; ?>

                            </tbody>
                        </table>

                        <?php if(lang()=='ja'): ?>
                        <div class="attention">
                            <p>申し込み後、弊社より２営業日以内に受渡し書をメールにて返信させていただきますので内容をご確認ください。</p>
                            <p>受渡し書の合計金額等をご確認の上、1週間以内にご利用料金全額を銀行振込またはクレジットカードにてご入金ください。入金が確認された時点で正式ご予約となります。</p>
                            <p>＊1週間を過ぎてご入金が確認できない場合は、自動キャンセルとなりますことをあらかじめご了承くださいませ。<span class="hd"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04" target="_blank">キャンセル時の返金等はこちらをご参照下さい</a></span></p>
                        </div>
                        <?php endif; ?>

                        <?php if(lang()=='en'): ?>
                        <div class="attention">
                            <p>After submitting the application, we shall send you the confirmation document via email within two business days. Please check the contents and the total price. Then, please pay the full amount by bank transfer or credit card within one week. After the confirmation of the payment, the reservation shall be completed. *Please understand that if there is no payment within a week, the reservation shall be cancelled automatically.<span class="hd">
                                    <?php if(lang()=='zh'): ?>
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04" target="_blank">
                                        <?php else: ?>
                                        <a href="http://zh.nomad-r.jp/guide.html/#c04" target="_blank">
                                            <?php endif; ?>
                                            Please see the refund information here when you cancel your reservation.</a></span></p>
                        </div>
                        <?php endif; ?>

                        <?php if(lang()=='zh'): ?>
                        <div class="attention">
                            <p>申請預約後，本公司將於2個工作天內Email聯繫您，麻煩您收到後請確認內容並回信。</p>
                            <p>確認完回覆您的總金額等項目後，麻煩請於一周內匯款全額或以信用卡支付。確認付款完成後即正式完成預約。</p>
                            <p>＊若超過一周仍未收到您的款項，將自動取消您的預約。<span class="hd"><a href="http://zh.nomad-r.jp/guide.html/#c04" target="_blank">取消預約時的退款詳細請見</a></span></p>
                        </div>
                        <?php endif; ?>

                        <?php if(lang()=='th'): ?>
                        <div class="attention">
                            <p>หลังจากสมัครแล้ว ทางบริษัทจะตอบรับทางอีเมล์ภายใน2วันทำการ กรุณาตรวจทานเนื้อหาก่อนส่ง</p>
                            <p>หลังจากได้รับการยืนยันและรายละเอียดค่าใช้จ่ายแล้ว กรุณาชำระเงินด้วยการโอนหรือเครดิตการ์ดภายใน1สัปดาห์ หลังจากได้รับการยืนยันยอดเงินแล้วถือว่าการจองเสร็จสมบูรณ์</p>
                            <p>กรณีที่ไม่ชำระเงินภายใน1สัปดาห์จะยกเลิกการจองโดยอัตโนมัติ<span class="hd"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04" target="_blank">รายละเอียดการคืนเงินเมื่อยกเลิกการจอง คลิกที่นี่</a></span></p>
                        </div>
                        <?php endif; ?>

                        <?php if(is_mobile()):?>
                        <div id="sidebar" class="ml00 mr00">
                            <?php //include(TEMPLATEPATH.'/sidebar-price.php'); ?>
                            <?php include('./sidebar-price.php'); ?>
                        </div>
                        <!-- sidebar -->
                        <?php endif; ?>
                        <div id="warmsg"><?php echo $warmsg; ?></div>
                        <p id="form_submit" class="tac">
                            <input type="submit" id="submit" value="<?php echo $toConfirmationScreen; ?>" />
                        </p>
                        <input type="hidden" name="car_model_sts" id="car_model_sts"><!-- 車種ステータス -->
                        <input type="hidden" name="start_dt" id="start_dt"><!-- 貸出希望日 -->
                        <input type="hidden" name="end_dt" id="end_dt"><!-- 返却希望日 -->
                        <input type="hidden" name="go_time" id="go_time"><!-- 貸出希望時間 -->
                        <input type="hidden" name="back_time" id="back_time"><!-- 返却希望時間 -->
                        <input type="hidden" name="price_go_dispatch" id="price_go_dispatch" value=0><!-- 出発配送料 -->
                        <input type="hidden" name="price_back_dispatch" id="price_back_dispatch" value=0><!-- 返却配送料 -->
                        <input type="hidden" name="price_menseki_flg" id="price_menseki_flg" value=0><!-- 免責補償制度フラグ -->
                        <input type="hidden" name="price_pack_flg" id="price_pack_flg" value=0><!-- 安心パックフラグ -->
                        <input type="hidden" name="price_hep_flg" id="price_hep_flg" value=0><!-- 高速パスフラグ -->
                        <input type="hidden" name="jap_cdw_div" id="jap_cdw_div" value=0>
                        <input type="hidden" name="abd_cdw_div" id="abd_cdw_div" value=0>
                        <input type="hidden" name="min" id="min">
                        <input type="hidden" name="child_ridership_num" id="child_ridership_num"><!-- 子供乗車数 -->
                        <input type="hidden" name="adult_ridership_num" id="adult_ridership_num"><!-- 大人乗車数 -->
                        <input type="hidden" name="disc_plan" id="disc_plan"><!-- 割引プラン -->
                        <input type="hidden" name="disc_plan_name" id="disc_plan_name_hidden"><!-- 割引プランの名称 -->
                        <input type="hidden" name="price_basiccharge" id="price_basiccharge_hidden">
                        <!--基本料金 -->
                        <input type="hidden" name="over_price" id="over_price_hidden">
                        <!--時間外料金 -->
                        <input type="hidden" name="price_discount" id="price_discount_hidden"><!-- 割引料金 -->
                        <input type="hidden" name="price_dispatch" id="price_dispatch_hidden"><!-- 配送料 -->
                        <input type="hidden" name="price_cleaning" id="price_cleaning_hidden"><!-- 清掃料 -->
                        <input type="hidden" name="price_menseki" id="price_menseki_hidden"><!-- 免責補償制度 -->
                        <input type="hidden" name="price_safety" id="price_safety_hidden"><!-- 安心パック -->
                        <input type="hidden" name="price_bedding" id="price_bedding_hidden"><!-- 寝具クリーニング代 -->
                        <input type="hidden" name="price_goods" id="price_goods_hidden"><!-- レンタルグッズ -->
                        <input type="hidden" name="price_hep" id="price_hep_hidden"><!-- 高速乗り放題パス -->
                        <input type="hidden" name="price_subtotal" id="price_subtotal_hidden"><!-- 小計 -->
                        <input type="hidden" name="price_tax" id="price_tax_hidden"><!-- 消費税 -->
                        <input type="hidden" name="price_total" id="price_total_hidden"><!-- 合計 -->

                        <?
                    $rental_counter = 1;
                    if( have_rows('有料レンタルグッズ',$rental_post_id)):
                      while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
                        <?php if(!get_sub_field('非表示')): ?>
                        <input type="hidden" name="ren_gds_amt<?php echo $rental_counter; ?>" id="ren_gds_amt<?php echo $rental_counter; ?>">
                        <input type="hidden" name="rental_num<?php echo $rental_counter; ?>" id="rental_num<?php echo $rental_counter; ?>">
                        <input type="hidden" name="ren_gds_disp<?php echo $rental_counter; ?>" id="ren_gds_disp<?php echo $rental_counter; ?>">
                        <?php $rental_counter++; ?>
                        <?php endif; ?>
                        <?php
                    endwhile;
                    endif;
                    ?>

                        <input type="hidden" name="plan_d_free" id="plan_d_free" value=0>
                        <input type="hidden" name="plan_d_off_tsu" id="plan_d_off_tsu" value=0>
                        <input type="hidden" name="plan_d_off_syuku" id="plan_d_off_syuku" value=0>
                        <input type="hidden" name="plan_d_on_tsu" id="plan_d_on_tsu" value=0>
                        <input type="hidden" name="plan_d_on_syuku" id="plan_d_on_syuku" value=0>
                        <input type="hidden" name="plan_d_high" id="plan_d_high" value=0>
                        <input type="hidden" name="plan_d_golden" id="plan_d_golden" value=0>
                        <input type="hidden" name="plan_d_winter" id="plan_d_winter" value=0>
                        <input type="hidden" name="plan_o_free" id="plan_o_free" value=0>
                        <input type="hidden" name="plan_o_off_tsu" id="plan_o_off_tsu" value=0>
                        <input type="hidden" name="plan_o_off_syuku" id="plan_o_off_syuku" value=0>
                        <input type="hidden" name="plan_o_on_tsu" id="plan_o_on_tsu" value=0>
                        <input type="hidden" name="plan_o_on_syuku" id="plan_o_on_syuku" value=0>
                        <input type="hidden" name="plan_o_high" id="plan_o_high" value=0>
                        <input type="hidden" name="plan_o_golden" id="plan_o_golden" value=0>
                        <input type="hidden" name="plan_o_winter" id="plan_o_winter" value=0>

                        <input type="hidden" name="disc_repeater" id="disc_repeater_hidden">
                        <input type="hidden" name="disc_doumin" id="disc_doumin_hidden">
                        <input type="hidden" name="go_date_excel" id="go_date_excel">
                        <input type="hidden" name="back_date_excel" id="back_date_excel">

                        <input type="hidden" name="estimationPrice" id="estimationPrice" value="<?php echo $estimationPrice; ?>">
                        <input type="hidden" name="basicPrice" id="basicPrice" value="<?php echo $basicPrice; ?>">
                        <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>">
                        <input type="hidden" name="tax" id="tax" value="<?php echo $tax; ?>">
                        <input type="hidden" name="totalPrice" id="totalPrice" value="<?php echo $totalPrice; ?>">
                        <input type="hidden" name="vehicleDeliveryFee" id="vehicleDeliveryFee" value="<?php echo $vehicleDeliveryFee; ?>">
                        <input type="hidden" name="cleaningFee" id="cleaningFee" value="<?php echo $cleaningFee; ?>">
                        <input type="hidden" name="cleaningFee" id="cleaningFee" value="<?php echo $cleaningFee; ?>">
                        <input type="hidden" name="rentalItem" id="rentalItem" value="<?php echo $rentalItem; ?>">
                        <input type="hidden" name="bedClean" id="bedClean" value="<?php echo $bedClean; ?>">
                        <input type="hidden" name="anshinPack" id="anshinPack" value="<?php echo $anshinPack; ?>">
                        <input type="hidden" name="hep_name" id="hep_name" value="<?php echo $hep; ?>">
                        <input type="hidden" name="menseki" id="menseki" value="<?php echo $menseki; ?>">
                        <input type="hidden" name="lang" id="lang" value="<?php echo lang(); ?>">

                    </form>
                </section>
            </div>
            <!-- contents -->
            <?php if(is_pc()):?>
            <div id="sidebar">
                <?php //include(TEMPLATEPATH.'/sidebar-price.php'); ?>
                <?php include('./sidebar-price.php'); ?>
            </div>
            <!-- sidebar -->
            <?php endif; ?>
        </div>
        <!-- main_contents -->
    </div>
    <!--price -->
    <?php get_footer(); ?>
