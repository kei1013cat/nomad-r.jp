<?php

// 有料レンタルグッズ
$rental_matt = getMultiLang('Comfortable sleep matless ( Tokyo Nishikawa Air) \1,500','安眠マットレス（東京西川エアー）　１,５００円','','ฟูกเสริมนุ่มสบาย (Tokyo Nishikawa AiR®) 1500เยน');
$rental_chair = getMultiLang('Foldable chair','一人用折り畳みチェア　<br>１脚２００円','','เก้าอี้พับได้สำหรับ<br />1คน 200เยน');
$rental_cook = getMultiLang('Cooking set \500','調理・食器一式　５００円','','อุปกรณ์ทำอาหาร/ชุดจาน 500เยน');
$rental_led = getMultiLang('Chargeable LED lantern (COLEMAN) \500','コールマン充電式ＬＥＤランタン　５００円','','โคมไฟหลอดแอลอีดี 500เยน');
$rental_burner = getMultiLang('2-burner cooking stove with a windbreak and two gas cartridges \1,500','屋外用ツーバーナー（風よけ付き）ボンベ２本付き　１,５００円','','เตาแก๊สใช้ภายนอก(รวมที่กั้นลม)รวมแก๊ส2กระป๋อง  1500เยน');
$rental_tent = getMultiLang('Tent for 4 persons \ 1,500','テント４人用　１,５００円<br>※数に限りがあります。','','เต๊นท์นอนสำหรับ4คน 1500เยน');
$rental_hexa = getMultiLang('Hexa tarp \1,000','ヘキサタープ　１,０００円<br>※数に限りがあります。','','หลังคาหกเหลี่ยม 1000เยน');
$rental_cooler = getMultiLang('Cooler bag \300','クーラーバック　３００円','','กระเป๋าเก็บความเย็ย 300เยน');
$rental_bbq = getMultiLang('Barbecue set \1,400 with 2kg of charcoal for free ( Charged option for more of 2kg)','ＢＢＱセット　１,４００円　　炭は２㎏付き　　それ以上は有料在庫','','ชุดทำบาบีคิวพร้อมถ่าน2กิโล นอกนั้นมีค่าใช้จ่ายเพิ่ม  1400เยน');
$rental_bonfire = getMultiLang('Bonfire stand \500 ( Get firewood in camp site)','たき火台　５００円（マキはキャンプ場等でご用意ください）','','เตาย่าง 500เยน (กรุณาใช้งานที่บริเวณแคมปิ้ง)');
$rental_toilet = getMultiLang('Portable bathroom \ 1,800','ポータブルトイレ　１,８００円','','ชักโครกพกพา 1800เยน');
$rental_dvd = getMultiLang('DVD player \1,000','ＤＶＤプレイヤー　１,０００円','','เครื่องเล่นดีวีดี 1000เยน');
$rental_telescope = getMultiLang('kenko binoculars (10 magnifications) \300','kenko双眼鏡（１０倍ズーム）３００円','','กล้องส่องทางไกลkenko (ซูมได้10เท่า) 300เยน');
$rental_honda = getMultiLang('Honda 4 cycle silent generator \3,000 (With 5 liters gasoline)','ホンダ4サイクル静音タイプ発電機　３,０００円（5リットルガソリン付き）<br>※冬期間のみ貸出し','','เครื่องปั่นไฟฮอนด้า4สูบ  3000เยน (พร้อมน้ำมัน5ลิตร)<br />*มีใช้เช่าในช่วงฤดูหนาว');
$rental_coffee = getMultiLang('Fresh drip coffee set \300','挽き立てコーヒードリップセット ３００円','','ชุดชงกาแฟพร้อมเครื่องบด　300เยน');

// 無料車内設置備品
$rental_free_tissue = getMultiLang('Tissue, Wet tissue, Garbage bag','ティッシュ・ウェットティッシュ・ゴミ袋','','กระดาษชำระ ทิชชู่เปียก ถุงขยะ');
$rental_free_coffee = getMultiLang('Coffee maker set with coffee mill','挽き立てコーヒードリップセット','手搖式磨豆機咖啡組 300日幣','เครื่องต้มกาแฟ');
$rental_free_book = getMultiLang('Guide books ( Camping site guide, Hot spring guide, Road station guide, Sightseeing guide etc.)','各種ガイド本（キャンプ場ガイド・温泉ガイド・道の駅ガイド・ドライブ観光ガイド他）','','หนังสือนำเที่ยวต่างๆ (ที่ตั้งแคมป์ บ่อน้ำพุร้อน สถานที่ขับรถท่องเที่ยว)');
$rental_free_usb = getMultiLang('USB power outlet for smartphone ( use your own cables)','携帯スマホ充電用　ＵＳＢソケット（ケーブルはご持参ください）','','หัวแปลงยูเอสบีสำหรับชาร์ตโทรศัพท์ (กรุณานำสายชาร์ตมาเอง)');
$rental_free_vehicle = getMultiLang('Car goods( Stopping panel, Etc.)','車輛グッズ（停止板 / シルバー / マーク等）','','อุปกรณ์ติดรถ (แผ่นหยุดรถ/ไขควง/อะไหล่ เป็นต้น)');
$rental_free_stove = getMultiLang('Cartridge gas stove with wind break and a cartridge','屋外調理用カセットコンロ (風よけ付き)ボンベ付き','','เตาแก๊สพกพาสำหรับทำอาหารข้างนอก (พร้อมที่บังลมและแก๊ส)');
$rental_free_cseat = getMultiLang('Child seat ( As your request)','チャイルドシート（ご希望に応じ）','','เบาะเสริมนั่งสำหรับเด็ก (ในกรณีที่ลูกค้าต้องการ)');
$rental_free_jseat = getMultiLang('Booster seat (As your request)','ジュニアシート（ご希望に応じ）','','เบาะนั่งสำหรับเด็ก (ในกรณีที่ลูกค้าต้องการ)');
$rental_free_garbage = getMultiLang('Free garbage bag collection ( You have to separate it into Burnable, Glass bottle, Can and PET bottle )','ゴミ袋回収無料（燃えるゴミ・ビン・カン・ペットボトルの2つに分別してください）','','ถุงขยะนำกลับมาทิ้งฟรี (กรุณาแยกขยะสด กับพวกขวด กระป๋อง)');
$rental_free_scoop = getMultiLang('Shovel, towing rope, snow brush ( Only in winter)','冬期間　スコップ・牽引ロープ・大型スノーブラシ','','อุปกรณ์สำหรับฤดูหนาว จอบ เชือกลากของ พลั่วตักหิมะขนาดใหญ่');
$rental_free_wcover = getMultiLang('Front window cover (Only in winter, to avoid frozen window)','冬期間　防寒対策　フロントウィンドウカバー','','สำหรับฤดูหนาว มีผ้าคลุมกระจกหน้ารถแบบป้องกัน');
$rental_free_spray = getMultiLang('Defrost spray( Only in winter)','冬期間　凍結防止解氷材スプレー','','สเปรย์ละลายหิมะ');
$rental_free_ipad = getMultiLang('Free Wi-fi is available up to 500MB per a day with turning on your tethering of your phone','iPad（テザリングにて1日500MB までwi-fi利用可能）','iPad（テザリングにて1日500MB までwi-fi利用可能）','iPad（สามารถใช้ไวไฟได้ 500MBต่อวันผ่านตัวเชื่อมในรถ）');
$rental_free_toilet = getMultiLang('Portable bathroom','ポータブルトイレ','','ชักโครกพกพา');


?>
