<?php
// 料金・予約
$sidebar_price_01 = getMultiLang('price of each season','年間料金確認','','ราคาประจำปี');
$sidebar_price_02 = getMultiLang('Price list','料金表','','ตารางราคา');
$sidebar_price_03 = getMultiLang('Discount plans','割引プラン','','แผนส่วนลด');
$sidebar_price_04 = getMultiLang('Price sample','料金例','','ตัวอย่างการคำนวณราคา');
$sidebar_price_05 = getMultiLang('reservation<br class="sp">calender','空車状況<br class="sp">カレンダー','','ปฏิทินเช็คตารางรถว่าง');
$sidebar_reservation = getMultiLang('reservation<br class="sp">form','ご予約<br class="sp">フォーム','','แบบฟอร์มเช่ารถ');
$sidebar_yakkan = getMultiLang('Lending rules(日本語)<span class="sp"><br><br></span>','貸渡約款(日本語)<span class="sp"><br><br></span>','','เงื่อนไขการเช่ารถ');
$sidebar_yakkan_en = getMultiLang('Lending rules<br><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>(English)','Lending rules<br><span class="pc">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>(English)','','Lending rules  (English)');
$sidebar_yakkan_zh = getMultiLang('Lending rules(中国語)<span class="sp"><br><br></span>','貸渡約款(中国語)<span class="sp"><br><br></span>','','貸渡約款(中国語)');

// 車輛紹介
$sidebar_car_type05 = getMultiLang('SUNLIGHT','サンライト','','Sunlight');
$sidebar_car_type03 = getMultiLang('ZIL520','ジル５２０','','ZIL 520');
$sidebar_car_type01 = getMultiLang('CORDE BUNKS','コルドバンクス','','Corde Bunks');
$sidebar_car_type02 = getMultiLang('CORDE LEAVES','コルドリーブス','','Corde Leaves');
$sidebar_car_type04 = getMultiLang('CORDE RUNDY','コルドランディ','','Corde Rundy');


// レンタルグッズ
$sidebar_rental_01 = getMultiLang('Extra charged rental goods','有料レンタルグッズ','','อุปกรณ์เสริมที่มีค่าใช้จ่าย');
$sidebar_rental_02 = getMultiLang('Free equipment','無料車内設置備品','','อุปกรณ์ให้เช่า');

// ご利用ガイド
$sidebar_guide_01 = getMultiLang('How to make a reservation','ご予約方法','','วิธีการจอง');
$sidebar_guide_04 = getMultiLang('Cancellation policy','キャンセルポリシー','','เงื่อนไขการยกเลิก');
$sidebar_guide_02 = getMultiLang('Insurance / Guarantee','保険・保障','','ประกันภัยและการรักษาความปลอดภัย');
$sidebar_guide_03 = getMultiLang('Access','アクセス','','วิธีมาที่บริษัท');
$sidebar_guide_05 = getMultiLang('Delivery','配車について','','การจัดส่งรถ');
$sidebar_guide_09 = getMultiLang('Payments','お支払いについて','','วิธีการชำระเงิน');
$sidebar_guide_06 = getMultiLang('Contract termination','中途解約','','ยกเลิกการเชาก่อนระยะเวลา');
$sidebar_guide_08 = getMultiLang('When we cannot rent a car because of an accident or technical trouble','事故・故障<span class="pc">等により貸出不可能時の対応</span>','','การดูแลในกรณีที่ไม่สามารถให้เช่ารถต่อได้เนื่องจากอุบัติเหตุและความเสียหาย');
$sidebar_guide_07 = getMultiLang('Frequently asked questions','よくあるご質問','','คำถามที่พบบ่อย');

?>
