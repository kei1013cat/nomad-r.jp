<div id="car">


<?php if(!isset($_GET['type'])):?>
	<section class="car">
		<section>
			<h2 class="headline01">最高級プレミアムクラス</h2>
			<h3 class="big">ジル520</h3>
			<h3>豪華装備で優雅な旅を</h3>
			<div class="wifi-hosoku">
			<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
			</div>
			<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_main.jpg" alt="ジル520"/></p>
            <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg"/></a></p>
		    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03">詳細はこちら　＞</a></p>
	    </section>
	    <section>
			<h2 class="headline01">ハイクラス</h2>
			<h3 class="big">コルドバンクス</h3>
			<h3>ご家族・グループでのご利用にオススメ</h3>
			<div class="wifi-hosoku">
			<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
			</div>
			<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_main.jpg" alt="コルドバンクス"/></p>
		    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01">詳細はこちら　＞</a></p>
	    </section>
	    <section>
			<h2 class="headline01">ハイクラス</h2>
			<h3 class="big">コルドリーブス</h3>
			<h3>ご夫婦お二人でゆったりと</h3>
			<div class="wifi-hosoku">
			<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
			</div>
			<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_main.jpg" alt="コルドリーブス"/></p>
			<div class="pet-hosoku">
				<img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>Ⅱ号車のみペット乗車可能</span>
			</div>
			<p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=02">詳細はこちら　＞</a></p>
			</section>
	    <section>
			<h2 class="headline01">ハイクラス</h2>
			<h3 class="big">コルドランディ</h3>
			<h3>愛犬と共にゆったりと</h3>
			<div class="wifi-hosoku">
			<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
			</div>
			<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_lundy_main.jpg" alt="コルドランディ"/></p>
			<div class="pet-hosoku">
				<img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ペット乗車可能</span>
			</div>
			<p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04">詳細はこちら　＞</a></p>
		</section>

	</section>
<?php endif; ?>
<?php if($_GET['type'] == '03'):?>
<section class="car bunks" id="c03">

	<h2 class="headline01 typesquare_tags">ジル520Ⅰ・Ⅱ・Ⅲ・Ⅳ・Ⅴ・Ⅵ</h2>
	<h3>豪華装備で優雅な旅を</h3>
	<div class="wifi-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
	</div>
	<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_main.jpg" alt="ジル520"/></p>
    <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg"/></a></p>
    <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
    <p class="kome1">5日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>
	<table cellspacing="0" cellpadding="0" class="info">
	
		<tr>

			<th colspan="2">車輛名　</th>
			<td width="174">バンテック　ZIL520</td>
			<td width="216">使いやすい国産大人気メーカーです</td>
		</tr>
		<tr>
			<th colspan="2">ベース車両　</th>
			<td>トヨタ／カムロード</td>
			<td>キャンピングカー専用シャーシ使用</td>
		</tr>
		<tr>
			<th colspan="2">年式　</th>
			<td>平成２８年９月登録</td>
			<td>ピカピカの新車です</td>
		</tr>
		<tr>
			<th colspan="2">エンジン　</th>
			<td>ディーゼルターボ</td>
			<td>パワフルな走りで峠もラクラク</td>
		</tr>
		<tr>
			<th colspan="2">排気量　</th>
			<td>３０００ＣＣ</td>
			<td>エンジン音も静かですよ</td>
		</tr>
		<tr>
			<th colspan="2">駆動方式　</th>
			<td>フルタイム４ＷＤ</td>
			<td>冬道も安心です</td>
		</tr>
		<tr>
			<th colspan="2">燃料　</th>
			<td>軽油</td>
			<td>ガソリンではありませんので注意を！</td>
		</tr>
		<tr>
			<th colspan="2">燃費　</th>
			<td>１リッターあたり　<br />
				７～１１km</td>
			<td>低燃費で軽油は安いですのでお得</td>
		</tr>
		<tr>
			<th width="56" rowspan="2">定員</th>
			<th width="27">乗車</th>
			<td>６名</td>
			<td>広々車内ですので余裕です</td>
		</tr>
		<tr>
			<th>就寝</th>
			<td>５名</td>
			<td>ゆったり５名が寝れます</td>
		</tr>
		<tr>
			<th rowspan="3">サイズ</th>
			<th>全長</th>
			<td>５１６０㎜</td>
			<td>実はハイエースより短いのです</td>
		</tr>
		<tr>
			<th>全幅</th>
			<td>２１１０㎜</td>
			<td>普通乗用車と同じ駐車スペースＯＫ</td>
		</tr>
		<tr>
			<th>全高</th>
			<td>３１５０㎜</td>
			<td>高さだけ気を付けてくださいね</td>
		</tr>
		<tr>
			<th colspan="2">最小回転半径　</th>
			<td>４.９ｍ</td>
			<td>普通乗用車と変わらず小回りできます</td>
		</tr>
		<tr>
			<th colspan="2">たばこ喫煙　</th>
			<td>禁煙</td>
			<td>禁煙となります　ご理解ください</td>
		</tr>
		<tr>
			<th colspan="2">ペット同乗　</th>
			<td>不可</td>
			<td>現在はペット不可とさせていただきます</td>
		</tr>
	</table>

	<table cellspacing="0" cellpadding="0" class="checktable">
<caption>設備</caption>
		<tr>
			<th>車輛冷暖房</th>
			<td >〇</td>
			<th>車内ガスコンロ</th>
			<td class="check">〇</td>
		</tr>
		<tr>
			<th>FFヒーター</th>
			<td>〇</td>
			<th>社外用カセットガスコンロ</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>リアクーラー</th>
			<td>〇</td>
			<th>更衣室兼　ポータブルトイレルーム</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ルームエアコン（外部電源使用時推奨）</th>
			<td>○</td>
			<th>ドリンクホルダー付き大型テーブル</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>大容量冷蔵庫</th>
			<td>〇</td>
			<th>水洗トイレ</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>４か国語対応カーナビ</th>
			<td>〇</td>
			<th>シャワー</th>
			<td>×</td>
		</tr>
		<tr>
			<th>Bluetooth対応オーディオ　ＡＭ　ＦＭラジオ付き</th>
			<td>×</td>
			<th>サイドオーニング（使用はお断りしてます）</th>
			<td>×</td>
		</tr>
		<tr>
			<th>高音質４スピーカーシステム内臓</th>
			<td>〇</td>
			<th>３点式安全シートベルト装置</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ポータブルＤＶＤプレイヤー（オプション）</th>
			<td>〇</td>
			<th>省エネＬＥＤ照明</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>バックアイカメラ（常時撮影）</th>
			<td>〇</td>
			<th>全窓網戸＋ブラインド</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ＥＴＣ</th>
			<td>〇</td>
			<th>出入り口ドア網戸</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１００ボルト電源（外部電源使用時推奨）</th>
			<td>〇</td>
			<th>大型換気扇</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１２ボルト電源</th>
			<td>〇</td>
			<th>外部生ごみ収納庫</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>電動水道付きシンク</th>
			<td>〇</td>
			<th>携帯充電用USB電源</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>無料WI-FI</th>
			<td>〇</td>
			<th>車輛説明用タブレット</th>
			<td>〇</td>
		</tr>

		<tr>
			<th>高級安眠マットレス２枚（３枚目以降は有料）</th>
			<td>〇</td>
			<th>最新型薄型テレビ</th>
			<td>〇</td>
		</tr>

	</table>
	<p class="kome1 mb">※内装色は車輛により一部異なります事をご了承下さい</p>
	<div class="camera" id="camera-zil">
	<h3>３６０度カメラで疑似体験！</h3>
	<ul class="cf">
	<li>
<a title='ジル５２０　車輛後方右' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490295' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_01.jpg" ></p><h4>ジル５２０車輛後方右３６０度カメラ</h4></a></li>

	<li>
<a title='ジル５２０　車輛後方トイレ' target="_blank"  href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475486807' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_02.jpg" ></p><h4>ジル５２０車輛後方トイレ３６０度カメラ</h4></a></li>

	<li>
<a title='ジル５２０　車輛前方就寝仕様' target="_blank"  href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490809' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg" ></p><h4>ジル５２０車輛前方就寝仕様３６０度カメラ</h4></a></li>

<li>
<a title='ジル５２０　車輛前方' target="_blank"  href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475487141' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_04.jpg" ></p><h4>ジル５２０車輛前方３６０度カメラ</h4></a></li>
	</ul>
	</div>
	<div class="gallery">
		<ul class="cf">
		<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg"><p class="text">１２ボルト変換アダプター</p>
		</a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg" ><p class="text">ＵＳＢ充電アダプター完備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg"><p class="text">エアコンも完備　１００ボルトコンセント連結時使用可能</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg"><p class="text">キッチン　ガスコンロ・シンク完備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg"><p class="text">キッチンのガス・シンク収納時</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" alt="外国語対応ナビで海外の方も安心"><p class="text">運転席にはドリンクホルダー設置　</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg"><p class="text">運転席は高く視界良好です</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg"><p class="text">最新式ナビで安心ドライブ</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg"><p class="text">室内は随所に１００ボルトコンセントがあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg"><p class="text">車輌後方視界カメラ常時撮影で運転も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg" ><p class="text">照明類のスイッチもコンパクトにまとめてあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg"><p class="text">寝室には照明や１００ボルトコンセントもあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg"><p class="text">電圧計も設置で残充電状況もわかり安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg"><p class="text">運転席上部はベッドルームに　大人３名が余裕で就寝できる広さです</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg"><p class="text">９０リットル大型冷蔵庫完備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg"><p class="text">入口には鏡もあり女性も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg"><p class="text">４人掛けの広々メインテーブル</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg"><p class="text">キッチンまわりには豊富な収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg"><p class="text">ジル５２０は化粧室が充実</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg"><p class="text">テーブルを格納してフルフラットベッドに</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg" ><p class="text">テーブルを格納してフルフラット状態</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg"><p class="text">メイン応接テーブル後方より撮影</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg"><p class="text">リア２段ベッドルームにも照明とコンセントがあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg"><p class="text">運転席上部のバンクベッドに上がるはしご</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg"><p class="text">室内にはＵＳＢコンセント有り　スマホの充電もＯＫ</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg"><p class="text">室内後方より</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg"><p class="text">室内収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg"><p class="text">車両後方２段ベッドにも換気窓設置</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg"><p class="text">車輌前方より</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg"><p class="text">車輌前方より後方を撮影</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg"><p class="text">集中スイッチで簡単操作</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg"><p class="text">随所に関節照明</p></a></li>



<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg"><p class="text">網戸も完備</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg"><p class="text">センサー付きエントランス照明付きで夜間も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg"><p class="text">外部１００ボルトコンセント接続部</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg"><p class="text">後部には濡れた物などを格納するトランク</p></a></li>




<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg"><p class="text">後方大型トランクルーム</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg"><p class="text">車輌右後方収納</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg"><p class="text">上水道は車輛後方トランクルーム内に設置</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg"><p class="text">上水道用タンク車両左後方扉から出し入れ可能</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg"><p class="text">電動ステップで乗り降りが楽々</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg"><p class="text">燃料タンクは運転席右後ろの扉を開けます　軽油です</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg"><p class="text">車両後方より　</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg"><p class="text">車輌右側より</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg"><p class="text">車輌左後方より</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg"><p class="text">車輌右前方より </p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg"><p class="text">車輌左前方より</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg"><p class="text">車輌左側面</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg" data-lightbox="zil"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg"><p class="text">車輌前方正面</p></a></li>

	</ul>
	<p class="kome2">※内装色は車輛により一部異なります事をご了承下さい</p>

</div><!-- gallery -->
</section>
<?php endif; ?>
<?php if($_GET['type'] == '01'):?>
<section class="car zil" id="c01">
	<h2 class="headline01 typesquare_tags">コルドバンクスⅠ・Ⅱ・Ⅲ</h2>

	<h3>ご家族・グループでのご利用にオススメ<br>同型車３台をご用意</h3>
	<div class="wifi-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
	</div>
	<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_main.jpg" alt="コルドバンクス"/></p>
    <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
	<p class="kome1">6日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>
	<table cellspacing="0" cellpadding="0" class="info">
	
		<tr>
			<th colspan="2">車輛名　</th>
			<td width="174">バンテック　コルドバンクス</td>
			<td width="216">使いやすい国産大人気メーカーです</td>
		</tr>
		<tr>
			<th colspan="2">ベース車両　</th>
			<td>トヨタ／カムロード</td>
			<td>キャンピングカー専用シャーシ使用</td>
		</tr>
		<tr>
			<th colspan="2">年式　</th>
			<td>平成２８年６月登録</td>
			<td>ピカピカの新車です</td>
		</tr>
		<tr>
			<th colspan="2">エンジン　</th>
			<td>ディーゼルターボ</td>
			<td>パワフルな走りで峠もラクラク</td>
		</tr>
		<tr>
			<th colspan="2">排気量　</th>
			<td>３０００ＣＣ</td>
			<td>エンジン音も静かですよ</td>
		</tr>
		<tr>
			<th colspan="2">駆動方式　</th>
			<td>フルタイム４ＷＤ</td>
			<td>冬道も安心です</td>
		</tr>
		<tr>
			<th colspan="2">燃料　</th>
			<td>軽油</td>
			<td>ガソリンではありませんので注意を！</td>
		</tr>
		<tr>
			<th colspan="2">燃費　</th>
			<td>１リッターあたり　<br />
				8～12km</td>
			<td>低燃費で軽油は安いですのでお得</td>
		</tr>
		<tr>
			<th width="56" rowspan="2">定員</th>
			<th width="27">乗車</th>
			<td>７名</td>
			<td>広々車内ですので余裕です</td>
		</tr>
		<tr>
			<th>就寝</th>
			<td>５名</td>
			<td>ゆったり５名が寝れます</td>
		</tr>
		<tr>
			<th rowspan="3">サイズ</th>
			<th>全長</th>
			<td>４９９５㎜</td>
			<td>実はハイエースより短いのです</td>
		</tr>
		<tr>
			<th>全幅</th>
			<td>１９８０㎜</td>
			<td>普通乗用車と同じ駐車スペースＯＫ</td>
		</tr>
		<tr>
			<th>全高</th>
			<td>３１５０㎜</td>
			<td>高さだけ気を付けてくださいね</td>
		</tr>
		<tr>
			<th colspan="2">最小回転半径　</th>
			<td>４.９ｍ</td>
			<td>普通乗用車と変わらず小回りできます</td>
		</tr>
		<tr>
			<th colspan="2">たばこ喫煙　</th>
			<td>禁煙</td>
			<td>禁煙となります　ご理解ください</td>
		</tr>
		<tr>
			<th colspan="2">ペット同乗　</th>
			<td>不可</td>
			<td>現在はペット不可とさせていただきます</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" class="checktable">
<caption>設備</caption>
		<tr>
			<th>車輛冷暖房</th>
			<td >〇</td>
			<th>車内ガスコンロ</th>
			<td class="check">〇</td>
		</tr>
		<tr>
			<th>FFヒーター</th>
			<td>〇</td>
			<th>社外用カセットガスコンロ</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>リアクーラー</th>
			<td>〇</td>
			<th>更衣室兼　ポータブルトイレルーム</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ルームエアコン</th>
			<td>×</td>
			<th>ドリンクホルダー付き大型テーブル</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>大容量冷蔵庫</th>
			<td>〇</td>
			<th>ポータブルトイレ（オプション）</th>
			<td>△</td>
		</tr>
		<tr>
			<th>４か国語対応カーナビ</th>
			<td>〇</td>
			<th>シャワー</th>
			<td>×</td>
		</tr>
		<tr>
			<th>Bluetooth対応オーディオ　ＡＭ　ＦＭラジオ付き</th>
			<td>×</td>
			<th>サイドオーニング（使用はお断りしてます）</th>
			<td>×</td>
		</tr>
		<tr>
			<th>高音質４スピーカーシステム内臓</th>
			<td>〇</td>
			<th>３点式安全シートベルト装置</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ポータブルＤＶＤプレイヤー（オプション）</th>
			<td>△</td>
			<th>省エネＬＥＤ照明</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>バックアイカメラ（常時撮影）</th>
			<td>〇</td>
			<th>全窓網戸＋ブラインド</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ＥＴＣ</th>
			<td>〇</td>
			<th>出入り口ドア網戸</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１００ボルト電源（外部電源使用時のみ）</th>
			<td>〇</td>
			<th>大型換気扇</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１２ボルト電源</th>
			<td>〇</td>
			<th>外部生ごみ収納庫</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>電動水道付きシンク</th>
			<td>〇</td>
			<th>携帯充電用USB電源</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>無料WI-FI</th>
			<td>〇</td>
			<th>車輛説明用タブレット</th>
			<td>〇</td>
		</tr>
	</table>
	<p class="kome1 mb">※内装色は車輛により一部異なります事をご了承下さい</p>
	<div class="camera" id="camera-bunks">
	<h3>３６０度カメラで疑似体験！</h3>
	<ul class="cf">
	<li>
<a title='コルドバンクス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468316834' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_01.jpg" ></p><h4>コルドバンクス車輛前方３６０度カメラ</h4></a></li>

	<li>
<a title='コルドバンクス　車両後方' target="_blank"  href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313153' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_02.jpg" ></p><h4>コルドバンクス車輛後方３６０度カメラ</h4></a></li>

	<li>
<a title='コルドバンクス　就寝仕様' target="_blank"  href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313123' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_03.jpg" ></p><h4>コルドバンクス就寝仕様３６０度カメラ</h4></a></li>
	</ul>
	</div>
	<div class="gallery">
		<ul class="cf">
		<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" alt="運転席も広々"><p class="text">運転席も広々</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" alt="運転席上部はベッドルームに大人３名が就寝できる広さです"><p class="text">運転席上部はベッドルームに大人３名が就寝できる広さです</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" alt="車内には各所に照明設置"><p class="text">車内には各所に照明設置</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" alt="広いテーブルを囲み快適な空間"><p class="text">広いテーブルを囲み快適な空間</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心"><p class="text">車輛後方視界カメラ常時撮影で運転も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" alt="外国語対応ナビで海外の方も安心"><p class="text">外国語対応ナビで海外の方も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" alt="すべての窓には網戸も装備"><p class="text">すべての窓には網戸も装備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" alt="大容量６０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください"><p class="text">大容量６０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" alt="車内４スピーカーシステム搭載"><p class="text">車内４スピーカーシステム搭載</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" alt="軽油ベバストＦＦヒーター付きで冬でも暖かいです"><p class="text">軽油ベバストＦＦヒーター付きで冬でも暖かいです</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" alt="100ボルト電源（外部電源使用時のみ使用できます）"><p class="text">100ボルト電源（外部電源使用時のみ使用できます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" alt="白を基調とした車内。中央にキッチンと冷蔵庫があります"><p class="text">白を基調とした車内。中央にキッチンと冷蔵庫があります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" alt="ＬＥＤランプ設置"><p class="text">ＬＥＤランプ設置</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" alt="ユーティリティルームはポータブルトイレ設置や着替えルームに"><p class="text">ユーティリティルームはポータブルトイレ設置や着替えルームに</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" alt="後方に２段ベッドがあるのでお子様にも大人気"><p class="text">後方に２段ベッドがあるのでお子様にも大人気</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" alt="キッチン横には棚も収納"><p class="text">キッチン横には棚も収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" alt="上部にはＬＥＤ照明と棚があります"><p class="text">上部にはＬＥＤ照明と棚があります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" alt="車両後方大容量トランクには上水道用２０リットルタンク"><p class="text">車両後方大容量トランクには上水道用２０リットルタンク</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" alt="外部からの吸気換気システム完備。雨の日でも使用できます"><p class="text">外部からの吸気換気システム完備。雨の日でも使用できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" alt="広々としたリビングルーム"><p class="text">広々としたリビングルーム</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" alt="入口横にはシングルシートがあります"><p class="text">入口横にはシングルシートがあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" alt="後方２段ベッド"><p class="text">後方２段ベッド</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" alt="キッチン下にもたくさんの収納"><p class="text">キッチン下にもたくさんの収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" alt="車輛入口にある集中電源。上からメインスイッチ・クーラー・照明"><p class="text">車輛入口にある集中電源。上からメインスイッチ・クーラー・照明</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" alt="車内キッチン・ガスコンロ（市販のカセットコンロを使用できます）"><p class="text">車内キッチン・ガスコンロ（市販のカセットコンロを使用できます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" alt="シンクとキッチンです"><p class="text">シンクとキッチンです</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" alt="入口は車輛横中央部から。網戸も完備・夏も安心"><p class="text">入口は車輛横中央部から。網戸も完備・夏も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" alt="車両後方・大容量トランクルームに荷物もたっぷり収納"><p class="text">車両後方・大容量トランクルームに荷物もたっぷり収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" alt="家族グループ向きの車輛で大容量トランクも有ります"><p class="text">家族グループ向きの車輛で大容量トランクも有ります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" alt="キッチン部"><p class="text">キッチン部</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" alt="収納もたっぷり"><p class="text">収納もたっぷり</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" alt="夜間はブラインドで光を完全遮光"><p class="text">夜間はブラインドで光を完全遮光</p></a></li>



<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" alt="後方２段ベッドにはそれぞれ小窓があります"><p class="text">後方２段ベッドにはそれぞれ小窓があります</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" alt="後方２段ベット照明２段階明るさ調整有り"><p class="text">後方２段ベット照明２段階明るさ調整有り</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" alt="車輛側面"><p class="text">車輛側面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" alt="車両右前方"><p class="text">車両右前方</p></a></li>




<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" alt="車両右側面"><p class="text">車両右側面</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" alt="車両左後方"><p class="text">車両左後方</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" alt="車両左側面"><p class="text">車両左側面</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" alt="車両前方"><p class="text">車両前方</p></a></li>

<li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" alt="車輛左前方"><p class="text">車輛左前方</p></a></li>

	</ul>
	<p class="kome2">※内装色は車輛により一部異なります事をご了承下さい</p>

</div><!-- gallery -->
</section>
<?php endif; ?>
<?php if($_GET['type'] == '02'):?>
<section class="car leaves" id="c02">
	<h2 class="headline01 typesquare_tags">コルドリーブスⅠ・Ⅱ</h2>

	<h3>ご夫婦お二人でゆったりと</h3>
	<div class="wifi-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
	</div>
	<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_main.jpg" alt="コルドリーブス"/></p>
	<div class="pet-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>Ⅱ号車のみペット乗車可能</span>
	</div>
    <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
	<p class="kome1">6日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>
<table cellspacing="0" cellpadding="0" class="info">
	
		<tr>
			<th colspan="2">車輛名　</th>
			<td width="174">バンテック　コルドリーブス</td>
			<td width="216">使いやすい国産大人気メーカーです</td>
		</tr>
		<tr>
			<th colspan="2">ベース車両　</th>
			<td>トヨタ／カムロード</td>
			<td>キャンピングカー専用シャーシ使用</td>
		</tr>
		<tr>
			<th colspan="2">年式　</th>
			<td>平成２８年６月登録</td>
			<td>ピカピカの新車です</td>
		</tr>
		<tr>
			<th colspan="2">エンジン　</th>
			<td>ディーゼルターボ</td>
			<td>パワフルな走りで峠もラクラク</td>
		</tr>
		<tr>
			<th colspan="2">排気量　</th>
			<td>３０００ＣＣ</td>
			<td>エンジン音も静かですよ</td>
		</tr>
		<tr>
			<th colspan="2">駆動方式　</th>
			<td>フルタイム４ＷＤ</td>
			<td>冬道も安心です</td>
		</tr>
		<tr>
			<th colspan="2">燃料　</th>
			<td>軽油</td>
			<td>ガソリンではありませんので注意を！</td>
		</tr>
		<tr>
			<th colspan="2">燃費　</th>
			<td>１リッターあたり　<br />
				8～12km</td>
			<td>低燃費で軽油は安いですのでお得</td>
		</tr>
		<tr>
			<th width="56" rowspan="2">定員</th>
			<th width="27">乗車</th>
			<td>７名</td>
			<td>広々車内ですので余裕です</td>
		</tr>
		<tr>
			<th>就寝</th>
			<td>５名</td>
			<td>ゆったり５名が寝れます</td>
		</tr>
		<tr>
			<th rowspan="3">サイズ</th>
			<th>全長</th>
			<td>４９９５㎜</td>
			<td>実はハイエースより短いのです</td>
		</tr>
		<tr>
			<th>全幅</th>
			<td>１９８０㎜</td>
			<td>普通乗用車と同じ駐車スペースＯＫ</td>
		</tr>
		<tr>
			<th>全高</th>
			<td>３１５０㎜</td>
			<td>高さだけ気を付けてくださいね</td>
		</tr>
		<tr>
			<th colspan="2">最小回転半径　</th>
			<td>４.９ｍ</td>
			<td>普通乗用車と変わらず小回りできます</td>
		</tr>
		<tr>
			<th colspan="2">たばこ喫煙　</th>
			<td>禁煙</td>
			<td>禁煙となります　ご理解ください</td>
		</tr>
		<tr>
			<th colspan="2">ペット同乗　</th>
			<td>不可</td>
			<td>現在はペット不可とさせていただきます</td>
		</tr>
	</table>
<table cellspacing="0" cellpadding="0" class="checktable">
<caption>設備</caption>
			<tr>
				<th >車輛冷暖房</th>
				<td >〇</td>
				<th >車内ガスコンロ</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>FFヒーター</th>
				<td>〇</td>
				<th>社外用カセットガスコンロ</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>リアクーラー</th>
				<td>×</td>
				<th>更衣室兼　ポータブルトイレルーム</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>ルームエアコン（外部電源使用時のみ）</th>
				<td>〇</td>
				<th>ドリンクホルダー付き大型テーブル</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>大容量冷蔵庫</th>
				<td>〇</td>
				<th>ポータブルトイレ（オプション）</th>
				<td>△</td>
			</tr>
			<tr>
				<th>４か国語対応カーナビ</th>
				<td>〇</td>
				<th>シャワー</th>
				<td>×</td>
			</tr>
			<tr>
				<th>Bluetooth対応オーディオ　ＡＭ　ＦＭラジオ付き</th>
				<td>×</td>
				<th>サイドオーニング（使用はお断りしてます）</th>
				<td>×</td>
			</tr>
			<tr>
				<th>高音質４スピーカーシステム内臓</th>
				<td>〇</td>
				<th>３点式安全シートベルト装置</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>ポータブルＤＶＤプレイヤー（オプション）</th>
				<td>△</td>
				<th>省エネＬＥＤ照明</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>バックアイカメラ（常時撮影）</th>
				<td>〇</td>
				<th>全窓網戸＋ブラインド</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>ＥＴＣ</th>
				<td>〇</td>
				<th>出入り口ドア網戸</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>車内１００ボルト電源（外部電源使用時のみ）</th>
				<td>〇</td>
				<th>大型換気扇</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>車内１２ボルト電源</th>
				<td>〇</td>
				<th>外部生ごみ収納庫</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>電動水道付きシンク</th>
				<td>〇</td>
				<th>携帯充電用USB電源</th>
				<td>〇</td>
			</tr>
			<tr>
				<th>無料WI-FI</th>
				<td>〇</td>
				<th>車輛説明用タブレット</th>
				<td>〇</td>
			</tr>

		</table>
		<p class="kome1 mb">※内装色は車輛により一部異なります事をご了承下さい</p>
		<div class="camera" id="camera-leaves">

	<h3>３６０度カメラで疑似体験！</h3>
	<ul class="cf">
	<li>
<a title='コルドリーブス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468315363' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_01.jpg" ></p><h4>コルドリーブス車輛前方３６０度カメラ</h4></a></li>

	<li>
<a title='コルドリーブス　車両後方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468314209' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_02.jpg" ></p><h4>コルドリーブス車輛後方３６０度カメラ</h4></a></li>

	<li>
<a title='コルドリーブス　就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468320525' ><p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg" ></p><h4>コルドリーブス　就寝仕様３６０度カメラ</h4></a></li>
	</ul>
	</div>
	
	<div class="gallery">
		<ul class="cf">
		
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" alt="ゆったりと広い運転席"><p class="text">ゆったりと広い運転席</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" alt="広々とした室内。運転席上部のベットは簡単に跳ね上げて収納できます"><p class="text">広々とした室内。運転席上部のベットは簡単に跳ね上げて収納できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" alt="運転席上部のベットを下した状態。大人２名が就寝できます"><p class="text">運転席上部のベットを下した状態。大人２名が就寝できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" alt="運転席上部ベットスペースにも小窓があります"><p class="text">運転席上部ベットスペースにも小窓があります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" alt="安全な３点式シートベルト装備"><p class="text">安全な３点式シートベルト装備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" alt="光あふれるリビングルーム"><p class="text">光あふれるリビングルーム</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" alt="広い窓とテーブル"><p class="text">広い窓とテーブル</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" alt="車内はＬＥＤ照明が多数"><p class="text">車内はＬＥＤ照明が多数</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" alt="ルームエアコン装備（外部電源をつないだ時のみ使用可能）"><p class="text">ルームエアコン装備（外部電源をつないだ時のみ使用可能）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" alt="車内４スピーカーシステム搭載"><p class="text">車内４スピーカーシステム搭載</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" alt="シンク・コンロ・キッチンまわり"><p class="text">シンク・コンロ・キッチンまわり</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" alt="キッチン下部にも食器セット等を収納できます"><p class="text">キッチン下部にも食器セット等を収納できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" alt="車内ガスコンロ（主にお湯を沸かす程度にご利用くださいませ）"><p class="text">車内ガスコンロ（主にお湯を沸かす程度にご利用くださいませ）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" alt="車内キッチン用カセットガスコンロ（市販のカセットコンロをご使用できます）"><p class="text">車内キッチン用カセットガスコンロ（市販のカセットコンロをご使用できます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" alt="シンク２０リットルの上水道（冬季は利用不可）"><p class="text">シンク２０リットルの上水道（冬季は利用不可）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" alt="カセットコンロガス漏れ警報機設置で万が一も安心"><p class="text">カセットコンロガス漏れ警報機設置で万が一も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" alt="テーブルを囲みお食事や読書に最適です。"><p class="text">テーブルを囲みお食事や読書に最適です。</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" alt="ポータブルテレビ等の台に"><p class="text">ポータブルテレビ等の台に</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" alt="大容量９０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください"><p class="text">大容量９０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" alt="各部に収納棚設置"><p class="text">各部に収納棚設置</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" alt="ユーティリティルーム。トイレや着替えに防水なので濡れたものもＯＫ"><p class="text">ユーティリティルーム。トイレや着替えに防水なので濡れたものもＯＫ</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" alt="外国語対応ナビで海外の方も安心"><p class="text">外国語対応ナビで海外の方も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" alt="車輛後方視界カメラ。常時撮影で運転も安心"><p class="text">車輛後方視界カメラ。常時撮影で運転も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" alt="すべての窓には網戸も装備"><p class="text">すべての窓には網戸も装備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" alt="夜間はブラインドで光を完全遮光"><p class="text">夜間はブラインドで光を完全遮光</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" alt="外部からの新鮮な空気の換気吸気装置。雨の日でも使用できます"><p class="text">外部からの新鮮な空気の換気吸気装置。雨の日でも使用できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" alt="運転席ドリンクホルダー"><p class="text">運転席ドリンクホルダー</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" alt="ＥＴＣ設置。カードはご持参ください"><p class="text">ＥＴＣ設置。カードはご持参ください</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" alt="リビングルーム下部に集中電源装置の扉があります"><p class="text">リビングルーム下部に集中電源装置の扉があります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" alt="後方入口は防水加工なので汚れても安心"><p class="text">後方入口は防水加工なので汚れても安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" alt="後方入口横にはちょっとした買い物などを入れるスペースがあります"><p class="text">後方入口横にはちょっとした買い物などを入れるスペースがあります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" alt="後方入口・足元灯"><p class="text">後方入口・足元灯</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" alt="車両後方入口横には電源ケーブルが収納されてます"><p class="text">車両後方入口横には電源ケーブルが収納されてます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" alt="乗り降りは車輛後方から"><p class="text">乗り降りは車輛後方から</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" alt="軽油使用ＦＦベバストヒーター装備"><p class="text">軽油使用ＦＦベバストヒーター装備</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" alt="100Ｖコンセント（外部電源使用時のみ使用可能）12Ｖは常時使用可能"><p class="text">100Ｖコンセント（外部電源使用時のみ使用可能）12Ｖは常時使用可能</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" alt="左からメインスイッチ・照明集中スイッチ・水道電源"><p class="text">左からメインスイッチ・照明集中スイッチ・水道電源</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" alt="網戸も装備。暑い夏場も快適"><p class="text">網戸も装備。暑い夏場も快適</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" alt="車両左にイスやロールテーブル等の収納（鍵で開けます）"><p class="text">車両左にイスやロールテーブル等の収納（鍵で開けます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" alt="上水道給水タンクは車輛後方に設置"><p class="text">上水道給水タンクは車輛後方に設置</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" alt="燃料タンク（軽油）は車輛右前方の扉を開けてエンジンキーでロックを解除しフタを回して給油してください。"><p class="text">燃料タンク（軽油）は車輛右前方の扉を開けてエンジンキーでロックを解除しフタを回して給油してください。</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" alt="車両右の扉内には工具類、ジャッキアップが収納されてます。ＢＢＱセット等を収納できるスペースとしてもご利用できます"><p class="text">車両右の扉内には工具類、ジャッキアップが収納されてます。ＢＢＱセット等を収納できるスペースとしてもご利用できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" alt="車両右後方には生ゴミなどを一時的に収納できます"><p class="text">車両右後方には生ゴミなどを一時的に収納できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" alt="車両正面"><p class="text">車両正面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" alt="車両上部"><p class="text">車両上部</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" alt="車両斜め前方"><p class="text">車両斜め前方</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" alt="車両斜め後方"><p class="text">車両斜め後方</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" alt="車両後ろから"><p class="text">車両後ろから</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" data-lightbox="leaves"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" alt="車両左側面"><p class="text">車両左側面</p></a></li>

	</ul>
	<p class="kome2">※内装色は車輛により一部異なります事をご了承下さい</p>
</div><!-- gallery -->
</section>
<?php endif; ?>



<?php if($_GET['type'] == '04'):?>
<section class="car zil" id="c04">
<!--	<h2><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_h.jpg" alt="コルドランディ"/></h2> -->
	<h2 class="headline01 typesquare_tags">コルドランディ</h2>


	<h3>ペットと出掛けることを考えたキャンピングカー</h3>
	<div class="wifi-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg">車内でWI-FIが使えます
	</div>
	<p class="main_photo"><img src="<?php bloginfo('template_url'); ?>/images/car_lundy_main.jpg	" alt="コルドランディ"/></p>
	<div class="pet-hosoku">
		<img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ペット乗車可能</span>
	</div>
    <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
	<p class="kome1">6日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>
	<table cellspacing="0" cellpadding="0" class="info">
	
		<tr>
			<th colspan="2">車輛名　</th>
			<td width="174">バンテック　コルドランディ</td>
			<td width="216">使いやすい国産大人気メーカーです</td>
		</tr>
		<tr>
			<th colspan="2">ベース車両　</th>
			<td>トヨタ／カムロード</td>
			<td>キャンピングカー専用シャーシ使用</td>
		</tr>
		<tr>
			<th colspan="2">年式　</th>
			<td>平成２３年８月登録</td>
			<td>ペット乗車OKの車輌です</td>
		</tr>
		<tr>
			<th colspan="2">エンジン　</th>
			<td>ディーゼルターボ</td>
			<td>パワフルな走りで峠もラクラク</td>
		</tr>
		<tr>
			<th colspan="2">排気量　</th>
			<td>３０００ＣＣ</td>
			<td>エンジン音も静かですよ</td>
		</tr>
		<tr>
			<th colspan="2">駆動方式　</th>
			<td>フルタイム４ＷＤ</td>
			<td>冬道も安心です</td>
		</tr>
		<tr>
			<th colspan="2">燃料　</th>
			<td>軽油</td>
			<td>ガソリンではありませんので注意を！</td>
		</tr>
		<tr>
			<th colspan="2">燃費　</th>
			<td>１リッターあたり　<br />
				8～12km</td>
			<td>低燃費で軽油は安いですのでお得</td>
		</tr>
		<tr>
			<th width="56" rowspan="2">定員</th>
			<th width="27">乗車</th>
			<td>6名</td>
			<td>広々車内ですので余裕です</td>
		</tr>
		<tr>
			<th>就寝</th>
			<td>4名</td>
			<td>ゆったり４名が寝れます</td>
		</tr>
		<tr>
			<th rowspan="3">サイズ</th>
			<th>全長</th>
			<td>４９９５㎜</td>
			<td>実はハイエースより短いのです</td>
		</tr>
		<tr>
			<th>全幅</th>
			<td>１９８０㎜</td>
			<td>普通乗用車と同じ駐車スペースＯＫ</td>
		</tr>
		<tr>
			<th>全高</th>
			<td>２９６０㎜</td>
			<td>高さだけ気を付けてくださいね</td>
		</tr>
		<tr>
			<th colspan="2">最小回転半径　</th>
			<td>４.９ｍ</td>
			<td>普通乗用車と変わらず小回りできます</td>
		</tr>
		<tr>
			<th colspan="2">たばこ喫煙　</th>
			<td>禁煙</td>
			<td>禁煙となります　ご理解ください</td>
		</tr>
		<tr>
			<th colspan="2">ペット同乗　</th>
			<td>可</td>
			<td>要別途清掃料金</td>
		</tr>
	</table>
	<table cellspacing="0" cellpadding="0" class="checktable mb">
<caption>設備</caption>
		<tr>
			<th>車輛冷暖房</th>
			<td >〇</td>
			<th>車内ガスコンロ</th>
			<td class="check">〇</td>
		</tr>
		<tr>
			<th>FFヒーター</th>
			<td>〇</td>
			<th>社外用カセットガスコンロ</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>リアクーラー</th>
			<td>×</td>
			<th>更衣室兼　ポータブルトイレルーム</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ルームエアコン（外部電源使用時推奨）</th>
			<td>〇</td>
			<th>ドリンクホルダー付き大型テーブル</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>大容量冷蔵庫</th>
			<td>〇</td>
			<th>ポータブルトイレ（オプション）</th>
			<td>△</td>
		</tr>
		<tr>
			<th>４か国語対応カーナビ</th>
			<td>〇</td>
			<th>シャワー</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>Bluetooth対応オーディオ　ＡＭ　ＦＭラジオ付き</th>
			<td>×</td>
			<th>サイドオーニング（使用はお断りしてます）</th>
			<td>×</td>
		</tr>
		<tr>
			<th>高音質４スピーカーシステム内臓</th>
			<td>〇</td>
			<th>３点式安全シートベルト装置</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ポータブルＤＶＤプレイヤー（オプション）</th>
			<td>△</td>
			<th>省エネＬＥＤ照明</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>バックアイカメラ（常時撮影）</th>
			<td>〇</td>
			<th>全窓網戸＋ブラインド</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>ＥＴＣ</th>
			<td>〇</td>
			<th>出入り口ドア網戸</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１００ボルト電源（外部電源使用時のみ）</th>
			<td>〇</td>
			<th>大型換気扇</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>車内１２ボルト電源</th>
			<td>〇</td>
			<th>外部生ごみ収納庫</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>電動水道付きシンク</th>
			<td>〇</td>
			<th>携帯充電用USB電源</th>
			<td>〇</td>
		</tr>
		<tr>
			<th>電子レンジ　（外部電源のみ）</th>
			<td>〇</td>
			<th>車輛説明用タブレット</th>
			<td>〇</td>
		</tr>		
		<tr>
			<th>無料WI-FI</th>
			<td>〇</td>
		</tr>		

	</table>

	<div class="camera" id="camera-rundy">
	<h3>３６０度カメラで疑似体験！</h3>
	<ul class="cf">
	<li>
<p class="iframe"><a title='コルドランディ　昼間室内全体３６０度カメラ' target="_blank"  href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500901394'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_01.jpg" ></a></p><h4>コルドランディ　昼間室内全体３６０度カメラ</h4></li>

	<li>
<p class="iframe"><a title='コルドランディ　夜間室内全体３６０度カメラ' target="_blank"  href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500967782'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_02.jpg" ></a></p><h4>コルドランディ　夜間室内全体３６０度カメラ</h4></li>

	<li>
<p class="iframe"><a title='コルドランディ　夜間室内前方３６０度カメラ' target="_blank"  href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500908456'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_03.jpg" ></a></p><h4>コルドランディ　夜間室内前方３６０度カメラ</h4></li>
	</ul>
	</div>

	<div class="gallery">
<ul class="cf">
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" alt="運転席も広々"><p class="text">運転席も広々</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" alt="運転席上部のベッド（マットがつきます）"><p class="text">運転席上部のベッド（マットがつきます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" alt="運転席上部のベッドルーム左右に2名"><p class="text">運転席上部のベッドルーム　左右に2名</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" alt="ＦＦヒータースイッチエンジン掛けなくてもOKです"><p class="text">ＦＦヒータースイッチ　エンジン掛けなくてもOKです</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" alt="液晶テレビ付き"><p class="text">液晶テレビ付き</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" alt="100ボルト電源（１５００Wのインバータ付なのでプラグインがなくても使用できます）"><p class="text">100ボルト電源（１５００Wのインバータ付なのでプラグインがなくても使用できます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" alt="汚れが付きにくく清掃がラクなレザーシート"><p class="text">汚れが付きにくく清掃がラクなレザーシート</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08.jpg" alt="後部はフルフラットベットになります"><p class="text">後部はフルフラットベットになります</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" alt="エアコン付き"><p class="text">エアコン付き</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" alt="使い易いシャワー"><p class="text">使い易いシャワー</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" alt="外部からの吸気換気システム完備雨の日でも使用できます"><p class="text">外部からの吸気　換気システム完備　雨の日でも使用できます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" alt="後方にもたくさんの収納"><p class="text">後方にもたくさんの収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13_tate.jpg"  data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13.jpg" alt="室内から運転席" /></p class="text">室内から運転席</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" alt="車内キッチンガスコンロ（市販のカセットコンロを使用できます）"><p class="text">車内キッチン　ガスコンロ（市販のカセットコンロを使用できます）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" alt="広いキッチンガスコンロ付き"><p class="text">広いキッチン　ガスコンロ付き</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心"><p class="text">車輌後方視界カメラ　常時撮影で運転も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" alt="車輌後方から運転席"><p class="text">車輌後方から運転席</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" alt="大人5名くらいがゆったり座れます"><p class="text">大人5名くらいがゆったり座れます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" alt="電子レンジ付き（外部電源利用時のみ）"><p class="text">電子レンジ付き（外部電源利用時のみ）</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" alt="外国語対応ナビで海外の方も安心"><p class="text">外国語対応ナビで　海外の方も安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21.jpg" alt="収納もたくさん"><p class="text">収納もたくさん</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" alt="入り口にステップ有り"><p class="text">入り口にステップ有り</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" alt="入り口横にスイッチ類が集中"><p class="text">入り口横にスイッチ類が集中</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" alt="冷蔵庫"><p class="text">冷蔵庫</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25.jpg" alt="使い安いシャワー"><p class="text">使い安いシャワー</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26.jpg" alt="入り口からまっすぐシャワールームへ行けます"><p class="text">入り口からまっすぐシャワールームへ行けます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27.jpg" alt="入り口より後方キッチン"><p class="text">入り口より後方キッチン</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28.jpg" alt="入り口右に収納"><p class="text">入り口右に収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" alt="バックカメラ付きで安心"><p class="text">バックカメラ付きで安心</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" alt="入り口のステップも広々"><p class="text">入り口のステップも広々</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" alt="車輌右後方の収納"><p class="text">車輌右後方の収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" alt="外からシャワー等の給水ができます"><p class="text">外からシャワー等の給水ができます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33.jpg" alt="着替えシャワールームわんちゃんお汚れた足もきれいに"><p class="text">着替え　シャワールーム　わんちゃんお汚れた足もきれいに</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34.jpg" alt="入り口も防水床"><p class="text">入り口も防水床</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" alt="車輌右後方収納キャンプ道具もOK"><p class="text">車輌右後方収納　キャンプ道具もOK</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36.jpg" alt="車輌右外からシャワー室へ入れます"><p class="text">車輌右　外からシャワー室へ入れます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" alt="左後方臭いの付いたゴミなどの収納に"><p class="text">左後方　臭いの付いたゴミなどの収納に</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" alt="外部から上水道タンク給水可能"><p class="text">外部から上水道タンク給水可能</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" alt="右側の荷物収納"><p class="text">右側の荷物収納</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" alt="ペット対応に設計されたコルドランディ"><p class="text">ペット対応に設計されたコルドランディ</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41.jpg" alt="わんちゃんも屋外が見れます"><p class="text">わんちゃんも屋外が見れます</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42_tate.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42.jpg" alt="車輌後方"><p class="text">車輌後方</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" alt="ドア開放側面"><p class="text">ドア開放側面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" alt="ドア開放"><p class="text">ドア開放</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" alt="車輌後方側面"><p class="text">車輌後方側面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" alt="車輌左前方"><p class="text">車輌左前方</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" alt="車輌左側面"><p class="text">車輌左側面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" alt="車輌正面"><p class="text">車輌正面</p></a></li>
<li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" data-lightbox="bunks"><p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" alt="車輌斜め前方"><p class="text">車輌斜め前方</p></a></li>




</ul>
</div><!-- gallery -->
</section>
<?php endif; ?>

<?php if($_GET['type'] == '01' || $_GET['type'] == '02' || $_GET['type'] == '03' || $_GET['type'] == '04'):?>
<p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況　＞</a></p>
<?php endif; ?>

</div>