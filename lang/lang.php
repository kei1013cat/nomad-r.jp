<?php

// カレンダー曜日表記
$week_mon = getMultiLang('Mo','月','月','จ');
$week_tue = getMultiLang('Tu','火','火','อ');
$week_wed = getMultiLang('We','水','水','พ');
$week_thu = getMultiLang('Th','木','木','พฤ');
$week_fri = getMultiLang('Fr','金','金','ศ');
$week_sat = getMultiLang('Sa','土','土','ส');
$week_sun = getMultiLang('Su','日','日','อา');

// カレンダー月表記
$month_1 = getMultiLang('January','1 月','1月','มกราคม');
$month_2 = getMultiLang('February','2 月','2月','กุมภาพันธ์');
$month_3 = getMultiLang('March','3 月','3月','มีนาคม');
$month_4 = getMultiLang('April','4 月','4月','เมษายน');
$month_5 = getMultiLang('May','5 月','5月','พฤษภาคม');
$month_6 = getMultiLang('June','6 月','6月','มิถุนายน');
$month_7 = getMultiLang('July','7 月','7月','กรกฎาคม');
$month_8 = getMultiLang('August','8 月','8月','สิงหาคม');
$month_9 = getMultiLang('September','9 月','9月','กันยายน');
$month_10 = getMultiLang('October','10 月','10月','ตุลาคม');
$month_11 = getMultiLang('November','11 月','11月','พฤศจิกายน');
$month_12 = getMultiLang('December','12 月','12月','ธันวาคม');

// 予約フォーム
$title_main = getMultiLang('Reservation calendar','空車状況カレンダー','預約現況行程表','ปฏิทินรถว่าง');
$title = getMultiLang('Reservation & Estimation Form','ご予約・お見積りフォーム','預約・報價單','แบบฟอร์มการจองและประเมินราคา');
$title_msg = getMultiLang('The reservation of rental vehicle is available by website. Please understand that website reservation has a slight delay in showing to the reservation calendar. In some cases, the reservation date is available in the calendar, but may not be available due to this time differences.','ご予約は随時、メール・電話等により受け賜りますが、サイトへの表示には時間差があり、予約可能になっていても時間差で予約済みになってしまう場合もあります事をご了承くださいませ。','因本預約隨時皆可以用中文以Email接受您的預約，網頁上顯示的資訊可能多少有些時差，既使顯示可以預約，因承接作業有時差的關係可能會有已被他人先預約的情況發生，還請您多加見諒。','สามารถติดต่อเช่ารถได้ที่อีเมล์หรือโทรศัพท์ตามที่ลูกค้าสะดวก แต่อาจจะมีข้อมูลที่แตกต่างจากตารางบนเว็บไซต์ มีบางกรณีที่บนเว็บขึ้นว่าว่างอยู่แต่ถูกจองไปแล้ว');
$common_msg2 = getMultiLang('JPY','円','日幣','เยน');
$common_msg3 = getMultiLang('Please contact us.','要ご相談ください','須詳談','กรุณาติดต่อเรา');
$common_msg4 = getMultiLang('Select time','時間を選択','選擇時間','เลือกเวลา');
$common_msg5 = getMultiLang('Age','年齢','年齢','อายุ');
$common_msg8 = getMultiLang('yrs old','歳','歲','ปี');
$common_msg9 = getMultiLang('Tax not included.','価格は税別です','以上價格皆未稅','ราคาไม่รวมภาษี');
$common_msg10 = getMultiLang('Passenger','名','位','คน');
$common_msg15 = getMultiLang('Pet Friendly','ペット可','可攜帶寵物','สัตว์เลี้ยงขึ้นได้');
$common_msg16 = getMultiLang('(Free if your reservation is 6-day or longer. Zil is Free if your reservation is 5-day or longer)','(丸6日以上のレンタルで送迎無料。ジル520は丸5日以上で送迎無料)','(如承租6天以上可免費接送。ZiL520系5天以上可免費接送)','(เช่ารถ6วันขึ้นไปมีบริการรับส่งฟรี  เช่ารถรุ่นZIL520 5วันขึ้นไปบริการรับส่งฟรี)');
$common_msg17 = getMultiLang('Premium Saloon','高級サルーン','奢華轎車','รถหรูคันใหญ่');
$common_msg18 = getMultiLang('（Pet Friendly）','（ペットOK）','（攜帶寵物OK）','（PET OK）');
$common_msg21 = getMultiLang('Click here for dispatching charges of main airport in hokkaido','道内主要空港への配車料金はこちら','至北海道内各機場的派車費請見','ค่าใช้จ่ายในการจัดส่งไปยังสนามบินหลักต่างๆในฮอกไกโด คลิกที่นี่');
$common_msg22 = getMultiLang('Extra 3,000 JPY for shuttle pick-up service (To Kiyota office)','清田貸出ベースまでの専用ハイヤー送迎料が別途3,000円かかります','接送至清田交車専用轎車費用為3,000日幣','บริการรับส่งถึงร้านมีค่าใช้จ่าย3000เยน');
$common_msg23 = getMultiLang('Extra 5,000 JPY for shuttle pick-up service (To Kiyota office)','清田貸出ベースまでの専用ハイヤー送迎料が別途5,000円かかります','接送至清田交車専用轎車費用為5,000日幣','บริการรับส่งถึงร้านมีค่าใช้จ่าย5000เยน');
$common_msg24 = getMultiLang('Extra 3,000 JPY for shuttle pick-up service (To Kiyota office)','清田貸出ベースまでの専用ハイヤー送迎料が別途3,000円かかります','接送至清田交車専用轎車費用為3,000日幣','บริการรับส่งถึงร้านมีค่าใช้จ่าย3000เยน');
$common_msg25 = getMultiLang('Extra 5,000 JPY for shuttle pick-up service (From Kiyota office to New Chitose Airport)','清田貸出ベースから新千歳空港までの専用ハイヤー送迎料が<span class="pc"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>別途5,000円かかります','從清田交車地派車接送至新千歲機場的費用為5,000日幣','บริการรับส่งถึงร้านมีค่าใช้จ่าย5000เยน');

$common_msg26 = getMultiLang('need','必要','需要','จำเป็น');//消
$common_msg27 = getMultiLang('No need','不要','不需要','ไม่จำเป็น');//消
$common_msg28 = getMultiLang('You can lend only during the winter period','冬期間のみの貸出し','您只能在冬季租借','มีให้เช่าเฉพาะช่วงฤดูหนาว');
$common_msg29 = getMultiLang('In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.','【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。','[冬季期間]如果有使用滑雪板之客戶，因為所有的行李將無法全部放入車內，最多人數約3至4人為極限。','【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。');


$y = getMultiLang('Year','年','年','ปี');
$m = getMultiLang('Month','月','月','เดือน');
$d = getMultiLang('Date','日','日','วัน');

$detail1 = getMultiLang('details','詳細を見る','詳細請見','ดูรายละเอียด');
$detail2 = getMultiLang('details','詳細はこちら','詳細請見內','รายละเอียดเพิ่มเติมคลิกที่นี่');
$detail3 = getMultiLang('details payment methods','お支払方法の詳細はこちら','付款方法詳細請見內','วิธีชำระเงินเพิ่มเติมคลิกที่นี่');
// フォームやHP内の文言で利用
$carModel = getMultiLang('Type of Vehicle','車　種','請確認車種','ประเภทรถ');
$zil520_1 = getMultiLang('ZIL520','ジル520','ZiL520','ZIL520');
$zil520_2 = getMultiLang('ZIL520','ジル520','ZIL520','ZIL520');
$zil520_3 = getMultiLang('ZIL520Ⅲ','ジル520Ⅲ','ZIL520Ⅲ','ZIL520Ⅲ');
$zil520_4 = getMultiLang('ZIL520Ⅳ','ジル520Ⅳ','ZIL520Ⅳ','ZIL520Ⅳ');
$zil520_5 = getMultiLang('ZIL520Ⅴ','ジル520Ⅴ','ZIL520Ⅴ','ZIL520Ⅴ');
$zil520_6 = getMultiLang('ZIL520Ⅵ','ジル520Ⅵ','ZIL520Ⅵ','ZIL520Ⅵ');

$cordeBunks1 = getMultiLang('CORDE BUNKS','コルドバンクス','CORDE BUNKS','CORDE BUNKS');
$cordeBunks2 = getMultiLang('CORDE BUNKS','コルドバンクス','CORDE BUNKS','CORDE BUNKS');
$cordeBunks3 = getMultiLang('CORDE BUNKSⅢ','コルドバンクスⅢ','CORDE BUNKSⅢ','CORDE BUNKSⅢ');
$cordeLeaves1 = getMultiLang('CORDE LEAVES','コルドリーブス','CORDE LEAVES','CORDE LEAVES');
$cordeLeaves2 = getMultiLang('CORDE LEAVES','コルドリーブス','CORDE LEAVES','CORDE LEAVES');
$corderundy1 = getMultiLang('CORDE RUNDY','コルドランディ','CORDE RUNDY','CORDE RUNDY');
$sunlight1 = getMultiLang('SUNLIGHT','サンライト','SUNLIGHT','SUNLIGHT');

// value値用（確認画面とメール本文で利用）
$zil520_1_val = getMultiLang('ZIL520','ジル520','ZIL520','ZIL520');
$zil520_2_val = getMultiLang('ZIL520','ジル520','ZIL520','ZIL520');
$zil520_3_val = getMultiLang('ZIL520(3)','ジル520(3)','ZIL520(3)','ZIL520(3)');
$zil520_4_val = getMultiLang('ZIL520(4)','ジル520(4)','ZIL520(4)','ZIL520(4)');
$zil520_5_val = getMultiLang('ZIL520(5)','ジル520(5)','ZIL520(5)','ZIL520(5)');
$zil520_6_val = getMultiLang('ZIL520(6)','ジル520(6)','ZIL520(6)','ZIL520(6)');
$cordeBunks1_val = getMultiLang('CORDE BUNKS','コルドバンクス','CORDE BUNKS','CORDE BUNKS');
$cordeBunks2_val = getMultiLang('CORDE BUNKS','コルドバンクス','CORDE BUNKS','CORDE BUNKS');
$cordeBunks3_val = getMultiLang('CORDE BUNKS(3)','コルドバンクス(3)','CORDE BUNKS(3)','CORDE BUNKS(3)');
$cordeLeaves1_val = getMultiLang('CORDE LEAVES','コルドリーブス','CORDE LEAVES','CORDE BUNKS');
$cordeLeaves2_val = getMultiLang('CORDE LEAVES','コルドリーブス','CORDE LEAVES','CORDE BUNKS');
$corderundy1_val = getMultiLang('CORDE RUNDY','コルドランディ','CORDE RUNDY','CORDE RUNDY');
$sunlight1_val = getMultiLang('SUNLIGHT','サンライト','SUNLIGHT','SUNLIGHT');

$departureDesiredTime = getMultiLang('Preferred Departure Date and Time','ご出発希望日時','欲出發日程','วันเวลาออกเดินทาง');
$departureDesiredTimeDeital1 = getMultiLang('The reservation before 9 a.m. available with additional fees.','09:00以前は別途早朝貸出料金にて受付可','可承接09:00以前提早出車方案收費另計','รับรถก่อน9โมงเช้ามีค่าใช้จ่ายเพิ่มเติม');
$departureDesiredTimeDeital2 = getMultiLang('Please select the preferred departure date.','ご出発希望日を選択してください','請選擇欲出發日程','กรุณาเลือกวันเดินทาง');
$returnDesiredTime = getMultiLang('Preferred Return Date and Time','ご返却希望日時','欲還車日程','วันเวลาคืนรถ');
$returnDesiredTimeDeital1 = getMultiLang('The reservation after 6 p.m. available with additional fees.','18:00以降は別途延長料金にて受付可','可承接18:00以後延遲還車方案收費另計','คืนรถหลัง6โมงเย็นมีค่าต่อเวลา');
$returnDesiredTimeDeital2 = getMultiLang('Please select the preferred return date..','ご返却希望日を選択してください','請選擇欲還車日程','กรุณาเลือกวันที่ต้องการคืนรถ');
$departureDesiredLocation = getMultiLang('Preferred Departure Location','ご出発又は送迎希望場所','出發或希望派車地點','สถานที่ออกเดินทางหรือสถานที่ที่ต้องการรับส่ง');
$depDesiredLocationSelect1 = getMultiLang('Hokkaido Nomad Car Rental Shop/ parking space is available','ご来店／駐車場有','直接來店／有停車場有','มาที่ร้าน/มีที่จอดรถ');
$depDesiredLocationSelect2 = getMultiLang('From JR Sapporo station, north gate or main hotel in the centre of Sapporo to Kiyota office','札幌駅北口及び札幌市中心部主要ホテルから清田貸出ベースへの送迎','至札幌車站北口及札幌市中心內各大飯店派車接送至清田交車地 ','บริการรับส่งจากสถานีซัปโปโรทางออกทิศเหนือหรือโรงแรมใจกลางเมืองถึงร้าน');
$depDesiredLocationSelect3 = getMultiLang('From New Chitose Airport to Kiyota office','新千歳空港から清田貸出ベースへの送迎','至新千歲機場接送到清田交車地','รับส่งจากสนามบินชินจิโตเสะ');
$depDesiredLocationSelect5 = getMultiLang('Others','その他','其他','วิธีอื่นๆ');
$departureDesiredLocationDetail1 = getMultiLang('Please fill in name of airline company and flight number.','空港会社名 便名をご記入下さい','請填寫 航空公司 航班名稱','กรุณากรอกชื่อ สนามบินและหมายเลขเที่ยวบิน');
$departureDesiredLocationDetail3 = getMultiLang('we can keep one vehicle per customer','お車は1台までお預かりできます','可寄放一台車','รับฝากรถได้1คัน');
$returnDesiredLocation = getMultiLang('Preferred Return Location','ご返却又は送迎希望場所','還車希望接送回程的地點','สถานที่รับส่งหรือสถานที่ส่งคืนรถที่ต้องการ');
$retDesiredLocationSelect1 = getMultiLang('Hokkaido Nomad Car Rental Shop','ご来店','來店','มาที่ร้าน');
$retDesiredLocationSelect2 = getMultiLang('From Kiyota office to JR Sapporo station, north gate or main hotel in the centre of Sapporo','清田貸出ベースより札幌駅北口及び札幌市中心部主要ホテルへの送迎','從清田交車地派車接送至札幌車站北口及札幌市中心內各大飯店','บริการรับส่งจาก Kiyota Rental Spaceถึงสถานีรถไฟซัปโปโรหรือโรงแรมในตัวเมืองซัปโปโร');
$retDesiredLocationSelect3 = getMultiLang('From Kiyota office to New Chitose Airport','清田貸出ベースより新千歳空港への送迎','從清田交車地派車接送至新千歲機場','บริการรับส่งจาก Kiyota Rental Spaceถึงสนามบินชินจิโตเสะ');
$retDesiredLocationSelect5 = getMultiLang('Others','その他','其他','วิธีอื่นๆ');
$returnDesiredLocationDetail1 = getMultiLang('Please fill in name of airline company and flight number.','空港会社名 便名をご記入下さい','請填寫 航空公司 航班名稱','');
$applicant = getMultiLang('Applicant','お申込者名','申請人','ชื่อผู้จอง');
$nameKana = getMultiLang(Null,'フリガナ','英文名稱','คำอ่าน');
$nationality = getMultiLang('Country','国名','國名','ประเทศ');
$address = getMultiLang('Home address','ご住所','現居地址','ที่อยู่');
$emergency = getMultiLang('Emergency contact','緊急時連絡先','緊急聯絡人','เบอร์ติดต่อฉุกเฉิน');
$name = getMultiLang('Name','お名前','姓名','ชื่อ');
$relationship = getMultiLang('Relationship','続柄','關係','ความสันพันธ์');
$tel = getMultiLang('Telephone number','電話番号','電話號碼','เบอร์โทรศัพท์');
$email = getMultiLang('Email address','メールアドレス','Email','อีเมล์');
$birth = getMultiLang('Date of birth','生年月日','出生年月日','วันเดือนปีเกิด');
$numOfpassengers = getMultiLang('Number of passengers','ご利用人数','搭乘人數','จำนวนผู้ใช้รถ');
$NumberOfChildSeat = getMultiLang('Number of child seat','チャイルドシートのご希望数','希望加裝嬰兒座椅數','จำนวนเบาะเสริมนั่งสำหรับเด็กที่ต้องการ');
$NumberOfJuniorSeat = getMultiLang('Number of junior seat','ジュニアシートのご希望数','希望加裝兒童座椅數','จำนวนที่นั่งเด็กเล็กที่ต้องการ');
$adult = getMultiLang('Adult (more than 6 years old)','大人（6歳以上）','大人（6歲以上）','ผู้ใหญ่ (อายุเกิน6ปี)');
$child = getMultiLang('Child less than 6 years old','6歳未満のお子様','未滿6歲的小孩','เด็กที่อายุต่ำกว่า6ปี');
$driver = getMultiLang('Driver','ご利用者','駕駛人','ชื่อผู้ใช้รถ');
$driver1 = getMultiLang('Driver #1','運転される方1','駕駛人1','ผู้ขับรถคนที่1');
$driver2 = getMultiLang('Driver #2','運転される方2','駕駛人2','ผู้ขับรถคนที่2');
$driver3 = getMultiLang('Driver #3','運転される方3','駕駛人3','ผู้ขับรถคนที่3');
$driver4 = getMultiLang('Driver #4','運転される方4','駕駛人4','ผู้ขับรถคนที่4');
$driverDetail1 = getMultiLang('Only for a driver who has more than 3 years of driving experience.','運転は3年以上経験のある方に限ります。','限開車經驗3年以上的駕駛。','ผู้ขับต้องมีประสบการณ์การขับรถมากกว่า3ปี');
$driversLicenseNo = getMultiLang('Driver license number','免許証番号','駕照號碼(如持有國際駕照請填寫國際駕照號碼)','เลขที่ใบขับขี่');
$discountPlan = getMultiLang('Discount options','割引プラン','優惠方案','แผนส่วนลด');
$repeatCustomDisc = getMultiLang('Repeat customer discount','リピーター割引','熟客優惠','ส่วนลดสำหรับผู้ใช้บริการซ้ำ ');
$repeatCustomDiscDetail1 = getMultiLang('(Only for renters who rented within a year)','（1年以内のご利用があった方のみ）','（限1年以内有承租過的客戶）','(เคยใช้บริการภายใน1ปี)');
$repeatCustomDiscDetail2 = getMultiLang('It cannot combine with other discounts','他の割引との併用はできません','不得與其他優惠並用','ไม่สามารถใช้ร่วมกับส่วนลดอื่นได้');
$paymentMethods = getMultiLang('Payment method','お支払方法','付款方式','วิธรการชำระเงิน');
$paymentMethodsSelect1 = getMultiLang('Please select','選択してください','請選擇','กรุณาเลือก');
$paymentMethodsSelect2 = getMultiLang('Bank transfer in advance','事前銀行振込','提前銀行匯款','ชำระเงินล่วงหน้าผ่านธนาคาร');
$paymentMethodsSelect3 = getMultiLang('Pay by credit card','クレジットカード払い','信用卡支付','ชำระผ่านเครดิตการ์ด');
$menseki = getMultiLang('CDW','免責補償制度','免責補償制度','แผนยกเว้นการชดเชยค่าเสียหาย');
$mensekiDetail = getMultiLang('In the event of a car accident 200,000 yen will be covered (9out of 10 customers add this service)','万が一の事故で保険使用時の免責額20万円が免除になります(9割以上のお客様がご加入されております。)','','ถ้าเผื่อว่ามีอุบัติเหตุเมื่อใช้ประกันจะได้รับการยกเว้นค่าเสียหาย200000เยน (ลูกค้ากว่า90%เลือกสมัครแผนนี้)'); //　消す
$mensekiDetai3 = getMultiLang('50,000 yen at a maximum for Non Operation Charge ','ノンオペレーションチャージ最大5万円','',''); //　消す
$mensekiDetai4 = getMultiLang('200,000 yen at a maximum for the situation that the car is unable to be driven','休車補償料最大20万円','',''); //　消す
$mensekiDetai5 = getMultiLang('Excluding the fee for repairing equipment of the car','車内装備品損傷修理代実費負担は除く','',''); //　消す

$mensekiSelect1 = getMultiLang('Enroll 3000 JPY/day per vehicle','加入する (1台につき) 1日 3000円','',''); //　消す
$mensekiSelect2 = getMultiLang('Not enroll','加入しない','',''); //　消す
// デポジット
$depsit1 = getMultiLang('【 Depsit 】 For customers from overseas','海外の方限定　【デポジット】','',''); //　消す
$depsit2 = getMultiLang('※We need to have your deposit as below (as credit).','※出発時にデポジットを預かります。（クレジットでお預かり）','',''); //　消す
$depsit3 = getMultiLang('In case you apply CDW(Collision Damage Waiver) + RAP(Rental ANSHIN Package) →50,000 JPY','CDW免責補償+RAP安心パック保険加入者→50,000円','',''); //　消す
$depsit4 = getMultiLang('In case you apply only CDW(Collision Damage Waiver)→300,000 JPY','CDW免責補償のみ加入者→300,000円','',''); //　消す
$depsit5 = getMultiLang('In case you apply nothing for both (CDW & RAP)→500,000 JPY','CDW免責・RAP安心パック保険　未加入者→500,000円','',''); //　消す
$depsit6 = getMultiLang('Be noted that we will use your deposit for expenses of property damage and collision liability in case of vehicle accident, fine for traffic violation, the cost for vehicle damage and so on.','事故の際に使う免責分と交通違反時の反則金、車輌破損時の費用として実費分をお預かり金額の中からご精算させていただきます。','',''); //　消す
$depsit7 = getMultiLang('If no accidence happened or repairing , we will return all the payment after returned the car','特に該当する事故・修理等が無い場合は、車輌返却後にお預かりした金額全額をクレジット返金処理にてお戻しいたします。','',''); //　消す

// 安心パック
$anshinPack = getMultiLang('RAP','安心パック','安心方案',''); //　消す
$anshinPackDetail = getMultiLang('Now you can enjoy your trip without any worry!','これですべて安心！','',''); //　消す
$anshinPackDetai2 = getMultiLang('We adviced joinning the plan since repaired fee is costly(9out of 10 customers add this service)','※修理費は高額になるためご加入をおすすめします(9割以上のお客様がご加入されております。)','',''); //　消す
$anshinPackDetai3 = getMultiLang('50,000 yen at a maximum for Non Operation Charge ','ノンオペレーションチャージ最大5万円','',''); //　消す
$anshinPackDetai4 = getMultiLang('200,000 yen at a maximum for the situation that the car is unable to be driven','休車補償料最大20万円','',''); //　消す
$anshinPackDetai5 = getMultiLang('Exemption of fee for the equipment of the car','車内装備品損傷修理代実費負担が免除','',''); //　消す

$anshinPackSelect1 = getMultiLang('Enroll 1500 JPY/day per vehicle','加入する (1台につき) 1日 1500円','',''); //　消す

$anshinPackSelect2 = getMultiLang('Only renters who enroll Collision Damage Waiver can join this plan','上記 免責補償に加入された方のみ追加でご加入いただけます','',''); //　消す



$bedClean = getMultiLang('‘Bedding’ cleaning fee','寝具クリーニング代','寝具清潔費','ค่าทำความสะอาดเครื่องนอน');
$bedCleanDetail1 = getMultiLang('‘Bedding’ cleaning fee (500JPY)','寝具セットクリーニング代【500円】','寝具組清潔費【500日幣】','ค่าทำความสะอาดชุดเครื่องนอน (500เยน)');
$bedCleanDetail2 = getMultiLang('The price shall be charged for all passengers.','1レンタルにつきご利用人数分を頂戴いたします','會依所有乘車人數作出租計算','เช่า1ครั้งคิดตามจำนวนผู้ใช้');
$rentalItem = getMultiLang('Rental items','レンタルグッズ','租借用品','อุปกรณ์เช่าเสริม');
$rentalItemDetail1 = getMultiLang('The price is for one day rental. The maximum costs will be up to 5 day rentals.','1日あたりの料金です。最長5日目まで料金がかかります。','此為單日收費。收費上限最多五天。','ราคาต่อ1วัน ใช้ได้สูงสุด5วัน');
$rentalItemDetail2 = getMultiLang('From the sixth day, the price will be the same as for 5 days.','6日目以降は5日間分の料金で継続ご利用いただけます。','第6天後上限只收5天份的收費。','หลังจากวันที่6เป็นต้นไปจะคิดค่าบริการ5วัน');
$rentalItemDetail3 = getMultiLang('with the aluminum table','アルミロールテーブル付き','附鋁折疊桌','รวมโต๊ะอลูมิเนียม');
$hep = getMultiLang('HEP','高速乗り放題パス','行駛高速公路吃到飽','บัตรผ่านทางด่วนแบบใช้ไม่อั้น');
$hep1 = getMultiLang('You can use the free pass ETC card for the freeway with car rental.','レンタカーと一緒に高速道路乗り放題のお得なETCカードをご利用いただけます。','คุณสามารถใช้บัตรETCร่วมกับบัตรผ่านทางด่วนกับรถเช่าได้','สามารถเช่ารถร่วมกับใช้งานบัตรผ่านขึ้นทางด่วนไม่จำกัดจำนวนครั้งได้');
$hep2 = getMultiLang('（Example: Using for 14 days freeway for only 821 yen per day!）','（例：14日間利用の場合１日あたり821円で高速乗り放題！）','（例如：使用14天的話，1天相當於821日幣的北海道高速公路定額無限暢行）','（ตัวอย่างเช่น : ถ้าคุณใช้งาน14วันก็เท่ากับใช้ขึ้นทางด่วนไม่อั้นเพียงวันละ821เยน）');
$hep3 = getMultiLang('We will request the fee for the number of days you rent the car.','お車の貸出日数分の費用をお支払いいただきます。','需支付租車天數的費用。','รับชำระค่าใช้จ่ายในการเช่ารถตามจำนวนวันที่เช่า');
$hep4 = getMultiLang('海外からのお客様限定','海外からのお客様限定','外國客戶限定','');
$hep5 = getMultiLang('※Japanese citizens are not availble for this plan.','※日本国籍の方のご利用は不可です。','※日本人不可使用本方案。','ลูกค้าสัญชาติญี่ปุ่นไม่สามารถใช้งานได้');
$hep6 = getMultiLang('Hokkaido Expressway Pass(HEP）','Hokkaido Expressway Pass(HEP）','Hokkaido Expressway Pass(北海道高速公路定額無限暢行ETC卡)','Hokkaido Expressway Pass(HEP）');

$cdw1 = getMultiLang('Safe even in accident - Safety pack','事故でも安心補償パック','加保露營車保險','แม้จะเกิดอุบัติเหตุก็ไม่ต้องกังวลเรื่องค่าใช้จ่าย');
$cdw2 = getMultiLang('Because the cost of repairing the camper is very expensive, we recommend you to join.','キャンピングカーは修理代がとても高価ですので、ご加入をおすすめします。','因為露營車修理費非常昂貴，因此建議加保。','เนื่องจากการซ่อมแซมรถแคมปิ้งมีค่าใช้จ่ายสูง ขอแนะนำให้สมัคร');
$cdw3 = getMultiLang('What is CDW','CDWとは','CDW','CDWคือ');
$cdw4 = getMultiLang('By paying <span class="cdw_price2">3000</span> yen per day, you will be exempt from a total of 200,000 yen by the insurance when accident or breakage by any chance.','1日<span class="cdw_price2">3000</span>円のご加入で、万が一の事故や破損で保険使用時にお客様が負担金する免責額合計20万円が免除になります。','CDW 免責補償制度。加保1天<span class="cdw_price2">3000</span>日幣的話，萬一發生事故或毀損需使用保險時，客戶可免除合計20萬日幣的免責額。','เพียงสมัครประกันวันละ<span class="cdw_price2">3000</span>เยน เมื่อเกิดอุบัติเหตุหรือความเสียหาย ลูกค้าจะได้รับการยกเว้นการชำระค่าเสียหาย200000เยน');
$cdw5 = getMultiLang('What is RAP','RAPとは','RAP','RAPคือ');
$cdw6 = getMultiLang('In addition, with an additional 1500 yen addition per day, a maximum of 200,000 yen for cancellation compensation during the repair period and a maximum of 50,000 yen for a non-operation charge.
Repair expenses at the time of breakage of each facility inside and outside the vehicle and repair fee for damaged tires are also exempted (when used under the prescribed way)
<br>* RAP can be added only when you joined the CDW.<br><br>','さらに1日1500円の追加ご加入で、修理期間中の休車補償料最大20万円とノンオペレーションチャージ最大5万円<br>車内外各設備の破損時修理費及び損傷タイヤの修理代等も免除（規定の使用方法の範囲内でご利用時）<br><br>＊RAPはCDWにご加入いただいた場合のみ追加いただけます。<br><br>','RAP租車安心方案。再加保1天1500日幣的話，可免除修車期間中的休車賠償費上限20萬日幣、營業賠償損失Non-Operation Charge（NOC）上限5萬日幣、<br><br>＊RAP租車安心方案 限加保 CDW 免責補償制度 才可加保。<br><br>','นอกจากนี้หากเพิ่มเงินวันละ1500เยนก็จะได้รับการยกเว้นค่าเสียหายหรือค่ารถเชยในกรณีที่ต้องพักรถเพื่อซ่อมสูงสุด200000เยน ค่าซ่อมแซมล้อรถ ตัวรถภายนอกและภายใน 50000เยน (เมื่อใช้งานตามที่บริษัทกำหนด)<br><br>แผนRAPจะเพิ่มได้ต่อเมื่อสมัครแผนCDWแล้วเท่านั้น');

$cdwDetail1 = getMultiLang('For the customer who lives in Japan','日本国内在住の方はこちら','現居日本國內的客戶請見內','สำหรับผู้ที่อาศัยอยู่ในประเทศญี่ปุ่น');
$cdwDetail2 = getMultiLang('To join the CDW exemption compensation system + RAP rent-a-car Safety pack Only
<span class="cdw_price1">4500</span> yen per day for a safe trip! 90% of those have joined.','CDW免責補償制度＋RAPレンタカー安心パックに加入する　1日<span class="cdw_price1">4500</span>円　これで安心の旅へ！9割の方がご加入いただいております。','加入CDW免責補償制度+RAP租車安心套餐
一日只要<span class="cdw_price1">4500</span>日幣，即可有個安心的北海道之旅！約90%的旅客都選擇加入。','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW+แผนเช่ารถสบายใจRAP วันละ<span class="cdw_price1">4500</span>เยน แค่นี้ก็เที่ยวได้สบายใจ 90％ของลูกค้าสมัคร');
$cdwDetail3 = getMultiLang('Only to join the CDW exemption compensation system <span class="cdw_price2">3000</span> yen per day','CDW免責補償制度のみ加入する　1日<span class="cdw_price2">3000</span>円','只加保CDW免責補償制度　1天<span class="cdw_price2">3000</span>日幣','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW วันละ<span class="cdw_price2">3000</span>เยน');
$cdwDetail4 = getMultiLang('Do not join','加入しない','不加保','ไม่สมัครแผนประกันภัย');
$cdwDetail5 = getMultiLang('In case of an accident or damage, there is the case that the actual cost claim of up to 500,000 yen occurs.
','事故や破損時は最大500,000円までの実費ご請求が発生する場合がございます。','發生事故或損壞時，最高會收取500,000日圓的實際賠償費用。','ในกรณีที่เกิดอุบัติเหตุหรือความเสียหาย อาจมีการเรียกเก็บเงินตามจริงสูงสุดไม่เกิน 500,000เยน');

$cdwDetail6 = getMultiLang('For those who live abroad are here','海外在住の方はこちら','現居國外的客戶請見內','สำหรับลูกค้าชาวต่างชาติ');
$cdwDetail7 = getMultiLang('To join the CDW exemption compensation system + RAP rent-a-car Safety pack <span class="cdw_price1">4500</span> yen per day + deposit of 50,000 yen on departure  (Actual cost up to 50,000 yen at the time of accident or damage)','CDW免責補償制度＋RAPレンタカー安心パックに加入する　1日<span class="cdw_price1">4500</span>円＋出発時デポジット50,000円お預かり（事故や破損時は最大50,000円までの実費お支払い）','加保CDW免責補償制度＋RAP租車安心方案　1天<span class="cdw_price1">4500</span>日幣＋出發時預扣押金50,000日幣（如果發生事故或毀損時支付上限為50,000日幣）','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW+แผนเช่ารถสบายใจRAP วันละ<span class="cdw_price1">4500</span>เยน + ค่ามัดจำวันเดินทาง 50000เยน (ในกรณีที่เกิดอุบัติเหตุหรือความเสียหายหักตามจริงไม่เกิน50000เยน)');
$cdwDetail8 = getMultiLang('Only to join the CDW exemption compensation system <span class="cdw_price2">3000</span> yen per day + deposit of 300,000 yen on departure (Actual expenses up to 300,000 yen at the time of accident or damage)','CDW免責補償制度のみ加入する　1日<span class="cdw_price2">3000</span>円　＋出発時デポジット300,000円お預かり（事故や破損時は最大300,000円までの実費お支払い）','僅加保CDW免責補償制度　1天<span class="cdw_price2">3000</span>日幣　＋出發時預扣押金300,000日幣（發生事故或毀損時支付上限為300,000日幣）','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW  วันละ<span class="cdw_price2">3000</span>เยน + ค่ามัดจำวันเดินทาง 300000เยน (ในกรณีที่เกิดอุบัติเหตุหรือความเสียหายหักตามจริงไม่เกิน300000เยน)');
$cdwDetail9 = getMultiLang('Do not join deposit of 500,000 yen on departure (Actual expenses up to 300,000 yen at the time of accident or damage)','加入しない　出発時デポジット500,000円お預かり（事故や破損時は最大500,000円までの実費お支払い）','不加保　出發時預扣押金500,000日幣（發生事故或毀損時最多支付500,000日幣）','ไม่สมัคร ค่ามัดจำวันเดินทาง 500000เยน (ในกรณีที่เกิดอุบัติเหตุหรือความเสียหายหักตามจริงไม่เกิน500000เยน)');

$cdwDetail11 = getMultiLang('CDW免責補償制度＋RAPレンタカー安心パックに加入する','CDW免責補償制度＋RAPレンタカー安心パックに加入する','加保CDW免責補償制度＋RAP租車安心方案','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW+แผนเช่ารถสบายใจRAP');
$cdwDetail12 = getMultiLang('CDW免責補償制度のみ加入する','CDW免責補償制度のみ加入する','只加保CDW免責補償制度','สมัครแผนยกเว้นการชดเชยค่าเสียหายCDW');
$cdwDetail13 = getMultiLang('加入しない','加入しない','不加保','ไม่สมัครแผนประกันภัย');

// レンタル用品（表示名）
$rental_matt = getMultiLang('Mattress (Tokyo Nishikawa AiR®）(1500 JPY/day)','安眠マットレス（東京西川エアー）(&yen;1500/日)','舒眠床墊（東京西川AIR）（1500日幣/天）','ฟูกเสริมนุ่มสบาย (Tokyo Nishikaw a AiR®) (วันละ1500เยน)');
$rental_chair = getMultiLang('Folding chair (200 JPY/day)','一人用折り畳みチェア(&yen;200円/日)','單人用折疊椅(200日幣/天)','เก้าอี้พับได้สำหรับ1คน (วันละ200เยน)');
$rental_cook = getMultiLang('Cooking/Tableware set (500 JPY/day)','調理・食器セット(&yen;500/日)','烹飪・餐具組(500日幣/天)','อุปกรณ์ทำอาหาร/ชุดจาน (วันละ500เยน)');
$rental_led = getMultiLang('LED lantern (500 JPY/day)','LEDランタン(&yen;500/日)','LED營燈(500日幣/天)','โคมไฟหลอดแอลอีดี (วันละ500เยน)');
$rental_burner = getMultiLang('Outdoor dual fuel stove (with the wind screen) 2 set of gas containers (1500 JPY/day)','屋外用ツーバーナー（風よけ付き）ボンベ２本付き(&yen;1500/日)','戶外用瓦斯雙口爐（附遮風屏）附2罐瓦斯(1500日幣/天)','เตาแก๊สใช้ภายนอก(รวมที่กั้นลม)รวมแก๊ส2กระป๋อง (วันละ1500เยน)');
$rental_tent = getMultiLang('Tent for 4 people (1500 JPY/day)','テント4人用(&yen;1500/日)','4人用帳篷(&yen;1500/日)','เต๊นท์นอนสำหรับ4คน(วันละ1500เยน)');
$rental_hexa = getMultiLang('Hexagon tarp (1000 JPY/day)','ヘキサタープ(&yen;1000/日)','六角天幕(&yen;1000/日)','หลังคาหกเหลี่ยม(1000เยน)');
$rental_cooler = getMultiLang('Cooler bag (300 JPY/day)','クーラーバック(&yen;300/日)','保冷袋(300日幣/天)','กระเป๋าเก็บความเย็น(วันละ300เยน)');
$rental_bbq = getMultiLang('BBQ set with 2kg of charcoal. More than 2kg, the additional fee shall be applied. (1400 JPY/day)','ＢＢＱセット 炭は2kg付き それ以上は有料(&yen;1400/日) ','ＢＢＱ套組 附炭2kg 若要再加購需另加費(1400日幣/天)','ชุดทำบาบีคิวพร้อมถ่าน2กิโล นอกนั้นมีค่าใช้จ่ายเพิ่ม (วันละ1400เยน)');
$rental_bonfire = getMultiLang('Bonfire stands (500 JPY/day)','たき火台(&yen;500/日)','焚火台(烤肉爐)(500日幣/天)','เตาปิ้ง (วันละ500เยน)');
$rental_toilet = getMultiLang('Portable toilet (1800 JPY/day)','ポータブルトイレ(&yen;1800/日)','便攜式移動馬桶(1800日幣/天)','ชักโครกพกพา (วันละ1800เยน)');
$rental_dvd = getMultiLang('DVD player (1000 JPY/day)','ＤＶＤプレイヤー(&yen;1000/日) ','ＤＶＤ播放機(1000日幣/天)','เครื่องเล่นดีวีดี (วันละ1000เยน)');
$rental_telescope = getMultiLang('Kenko binoculars (10 times zoom) (300 JPY/day)','kenko双眼鏡(10倍ズーム)(&yen;300/日)','kenko望遠鏡(10倍率)(300日幣/天)','กล้องส่องทางไกลkenko (ซูมได้10เท่า) (วันละ300เยน)');
$rental_honda = getMultiLang('Honda4 cycle silent type generator (3000 JPY/day)','ホンダ4サイクル静音タイプ発電機(&yen;3000/日)','本田4循環靜音式發電機(3000日幣/天)','เครื่องปั่นไฟฮอนด้า4สูบ (วันละ3000เยน)');
$rental_coffee = getMultiLang('Fresh drip coffee set (300 JPY/day)','挽き立てコーヒードリップセット(&yen;300/日)','手搖式磨豆機咖啡組(&yen;300/日幣)','เครื่องต้มกาแฟ(วันละ300เยน)');

// レンタル用品（VALUE値）
$rental_matt_val = getMultiLang('Mattress (Tokyo Nishikawa AiR®）(1500 JPY/day)','安眠マットレス(￥1500/日)','舒眠床墊（1500日幣/天）','ฟูกเสริมนุ่มสบาย (Tokyo Nishikawa AiR®) 1500เยน');
$rental_chair_val = getMultiLang('Folding chair (200 JPY/day) *with the aluminum table','一人用折り畳みチェア(￥200/日)','單人用折疊椅(200日幣/天)','เก้าอี้พับได้สำหรับ(1คน 200เยน)');
$rental_cook_val = getMultiLang('Cooking/Tableware set (500 JPY/day)','調理・食器セット(￥500/日)','烹飪・餐具組(500日幣/天)','อุปกรณ์ทำอาหาร/ชุดจาน 500เยน');
$rental_led_val = getMultiLang('LED lantern (500 JPY/day)','LEDランタン(￥500/日)','LED營燈(500日幣/天)','โคมไฟหลอดแอลอีดี 500เยน');
$rental_burner_val = getMultiLang('Outdoor dual fuel stove (with the wind screen) 2 set of gas containers (1500 JPY/day)','屋外用ツーバーナー(￥1500/日)','外用瓦斯雙口爐(1500日幣/天)','เตาแก๊สใช้ภายนอก(รวมที่กั้นลม)รวมแก๊ส2กระป๋อง  1500เยน');
$rental_tent_val = getMultiLang('Tent for 4 people (1500 JPY/day)','テント4人用(￥1500/日)','4人用帳篷(1500日幣/天)','เต๊นท์นอนสำหรับ4คน 1500เยน');
$rental_hexa_val = getMultiLang('Hexagon tarp (1000 JPY/day)','ヘキサタープ(￥1000/日)','六角天幕(1000日幣/天)','หลังคาหกเหลี่ยม 1000เยน');
$rental_cooler_val = getMultiLang('Cooler bag (300 JPY/day)','クーラーバック(￥300/日)','保冷袋(300日幣/天)','กระเป๋าเก็บความเย็ย 300เยน');
$rental_bbq_val = getMultiLang('BBQ set with 2kg of charcoal. More than 2kg, the additional fee shall be applied. (1400 JPY/day)','ＢＢＱセット 炭は2kg付き(￥1400/日)','ＢＢＱ套組 附炭2kg(1400日幣/天)','ชุดทำบาบีคิวพร้อมถ่าน2กิโล นอกนั้นมีค่าใช้จ่ายเพิ่ม  1400เยน');
$rental_bonfire_val = getMultiLang('Bonfire stands (500 JPY/day)','たき火台(￥500/日)','焚火台(500日幣/天)','เตาย่าง 500เยน (กรุณาใช้งานที่บริเวณแคมปิ้ง)');
$rental_toilet_val = getMultiLang('Portable toilet (1800 JPY/day)','ポータブルトイレ(￥1800/日)','便攜式移動馬桶(1800日幣/天)','ชักโครกพกพา 1800เยน');
$rental_dvd_val = getMultiLang('DVD player (1000 JPY/day)','ＤＶＤプレイヤー(￥1000/日)','ＤＶＤ播放機(1000日幣/天)','เครื่องเล่นดีวีดี 1000เยน');
$rental_telescope_val = getMultiLang('Kenko binoculars (10 times zoom) (300 JPY/day)','kenko双眼鏡(10倍ズーム)(￥300/日)','kenko望遠鏡(10倍率)(300日幣/天)','กล้องส่องทางไกลkenko (ซูมได้10เท่า) 300เยน');
$rental_honda_val = getMultiLang('Honda4 cycle silent type generator (3000 JPY/day)','ホンダ4サイクル発電機(￥3000/日)','本田4循環發電機(3000日幣/天)','เครื่องปั่นไฟฮอนด้า4สูบ  3000เยน (พร้อมน้ำมัน5ลิตร)');
$rental_coffee_val = getMultiLang('Fresh drip coffee set (300 JPY/day)','挽き立てコーヒードリップセット(&yen;300/日)','挽き立てコーヒードリップセット(&yen;300/日)','เครื่องต้มกาแฟ(วันละ300เยน)');

// 補足説明
$rental_matt_h = getMultiLang('','','','');
$rental_chair_h = getMultiLang('','','','');
$rental_cook_h = getMultiLang('','','','');
$rental_led_h = getMultiLang('','','','');
$rental_burner_h = getMultiLang('','','','');
$rental_tent_h = getMultiLang('','','','');
$rental_hexa_h = getMultiLang('','','','');
$rental_cooler_h = getMultiLang('','','','');
$rental_bbq_h = getMultiLang('','','','');
$rental_bonfire_h = getMultiLang('','','','');
$rental_toilet_h = getMultiLang('','','','');
$rental_dvd_h = getMultiLang('','','','');
$rental_telescope_h = getMultiLang('','','','');
$rental_honda_h = getMultiLang('（*Available only in winter）','（※冬期間のみの貸出し）','','（*มีให้เช่าในฤดูหนาว）');
$rental_coffee_h = getMultiLang('','','','');

$mainPersonTrip = getMultiLang('Who is the main person for this trip?','今回のご旅行の主人公は？','這次旅行的主角是那位？','จุดประสงค์หลักสำหรับการท่องเที่ยวครั้งนี้');
$mainPersonTripDetail1 = getMultiLang('Ex)','(例)','(例如)','(เช่น)');
$mainPersonTripDetail2 = getMultiLang('*Traveling in Hokkaido for cerebrating husband\'s retirement.','・主人の退職記念の北海道旅行','慶祝老公退休的北海道紀念之旅','ทริปฉลองการเกษียณงานของสามี');
$mainPersonTripDetail3 = getMultiLang('*Child graduation etc','・子供の卒業祝いなど','慶祝小孩順利畢業等','ของขวัญยินดีที่ลูกเรียนจบ');
$qa = getMultiLang('Question/Request','ご質問・ご要望','詢問・要求。','คำถาม หรือความต้องการเพิ่มเติม');
$qa_detail = getMultiLang('Do you have any concerns?','不安な事などございませんか？','是否有什麼擔心的事情？','มีเรื่องที่ไม่สบายใจหรือไม่');
$contractAgreement = getMultiLang('Contract agreement','契約の同意','同意契約','ข้อตกลงในการทำสัญญา');

$contractAgreementDetail1 = getMultiLang('Here is “Coping with accidents or failures reasons that cannot be lend”.','事故・故障等により貸出不可能時の対応はこちら','因發生事故・故障等狀況而無法租借的對應措施，詳細請見','การรับมือกับกรณีที่รถเกิดอุบัติเหตุหรือเสียหายจนไม่สามารถให้เช่าได้ คลิกที่นี่');
$contractAgreementDetail2 = getMultiLang('I agree to “Coping with accidents or failures reasons that cannot be lend”','「事故・故障等により貸出不可能時の対応」に同意します','我同意「因發生事故・故障等狀況無法租借的對應措施」','ยอมรับเงื่อนไข"การรับมือกับกรณีที่รถเกิดอุบัติเหตุหรือเสียหายจนไม่สามารถให้เช่าได้ "');
$contractAgreementDetail3 = getMultiLang('Here is “Rental agreement”.','貸渡約款はこちら','租賃契約・詳細請見','');
$contractAgreementDetail4 = getMultiLang('I agree to “Rental agreement”.','「貸渡約款」に同意します ','我同意「租賃契約」','ยอมรับ "ข้อกำหนดในสัญญา"');
$contractAgreementDetail5 = getMultiLang('Confirm contents and agree','内容を確認の上、同意します','','กรุณาตรวจทานข้อมูล');

$shokai = getMultiLang('Where did you find this site','ご紹介者様','請問從哪裡得知我們的網站?','ผู้ที่แนะนำ');
$shokai_detail1 = getMultiLang('','※ご紹介様がいる場合は入力お願いいたします。','','ถ้ามีผู้แนะนำกรุณากรอกชื่อ');

$passport1 = getMultiLang('To customers from overseas','海外からの方へ','給國外客戶','เรียนลูกค้าต่างชาติ');
$passport2 = getMultiLang('①Customers from a country which is a partyof the Geneva Conventions needs an “international driver’s license” and a“passport” that is within the valid period.<br>*about countries allow to issue international driver’s license, please check on your own.','①ジュネーブ条約締結国の方は、有効期限内の「国際免許証」＋「パスポート」を持参ください<br>※国際免許証発行可能国については、ご自身でご確認ください','①如果您是簽署日內瓦公約的國家，請攜帶在有效期內的“國際駕駛執照”+“護照”<br>*請自行確認可以簽發國際駕照的國家','1. ลูกค้าท่านที่มาจากประเทศที่เป็นภาคีในสนธิสัญญาเจนีวากรุณานำใบขับขี่สากล+พาสปอตที่ยังไม่หมดอายุมาด้วย<br>*โปรดตรวจสอบประเทศที่สามารถใช้ใบขับขี่สากลด้วยตัวท่านเอง');
$passport3 = getMultiLang('②Customers from a country which is a party of the Vienna Convention needs a “driver’s license”, a “Japanese Translation of Foreign Driver’s License” that is issued from a designated agency,and a “passport”<br>*Taiwan, Switzerland, Germany, France, Belgium, Monaco, Estonia, Slovenia','②ウィーン条約締結国の方は、「自国の免許証」＋「指定機関で発給された日本語翻訳文」＋「パスポート」を持参ください<br>※台湾・スイス・ドイツ・フランス・ベルギー・モナコ・エストニア・スロベニア','②如果您是簽署維也納條約的國家，請攜帶指定機關簽發的”駕照正本”+“駕照日文譯本”+“護照”<br>*台灣，瑞士，德國，法國，比利時，摩納哥，愛沙尼亞','2.ลูกค้าท่านที่มาจากกลุ่มประเทศอนุสัญญากรุงเวียนนากรุณานำใบขับขี่ของประเทศของคุณ+ใบอนุญาตที่แปลเป็นภาษาญี่ปุ่นโดยสถาบันที่ได้รับอนุญาต+พาสปอตที่ยังไม่หมดอายุมาด้วยำ<br>*ไต้หวัน, สวิตเซอร์แลนด์, เยอรมัน, ฝรั่งเศส, เบลเยี่ยม, โมนาโก, เอสโตเนีย,สโลวีเนีย');
$passport4 = getMultiLang('I will bring an “international driver’s license” and a “passport”','「国際免許証＋パスポート」を持参します。','自行攜帶「國際駕駛執照+護照」。','นำใบขับขี่สากล+พาสปอตมาด้วย');
$passport5 = getMultiLang('I will bring a “driver’s license”, a“Japanese Translation of Foreign Driver’s License”, and a “passport”','「自国の免許証」＋「指定機関で発給された日本語翻訳文」＋「パスポート」を持参します。','自行攜帶「駕照正本+駕照日文譯本+護照」。','นำใบขับขี่ของประเทศของคุณ+ใบอนุญาตที่แปลเป็นภาษาญี่ปุ่น+พาสปอตมาด้วย');

$warmsg = getMultiLang('※Repair costs for accidents and damage are expensive for the camper. We recommend you to join the CDW and RAP!','※キャンピングカーは事故や破損の修理費が高額です。CDWとRAPのご加入をおすすめします！','※露營車如果發生事故或毀損修理費非常貴。建議加保CDW和RAP！','ค่าซ่อมแซมรถแคมปิ้งจากอุบัติเหตุหรือความเสียหายต่างๆมีราคาสูง แนะนำให้สมัครแพกเกจ CDWและRAP');
$toConfirmationScreen = getMultiLang('Confirm','確認画面へ','確認頁面','ไปที่หน้ายืนยัน');

// 料金表(サイドバー)
$estimationPrice = getMultiLang('Estimation price','お見積もり金額','報價金額','ราคาค่าใช้จ่ายโดยประมาณ');
$basicPrice = getMultiLang('Basic price','基本料金','基本費用','ราคาพื้นฐาน');
$subtotal = getMultiLang('Subtotal','小計','小計','ยอดรวม');
$extension_fee = getMultiLang('Extension Fee','延長料金','延長租金','ค่าต่อเวลา');
$tax = getMultiLang('Tax','消費税','消費稅','ภาษี');
$totalPrice = getMultiLang('Total','合計','合計','ยอดรวมทั้งหมด');
$vehicleDeliveryFee = getMultiLang('Vehicle delivery fee','配車料','派車費','ค่าส่งรถ');
$cleaningFee = getMultiLang('Cleaning fee','清掃料','清潔費','ค่าทำความสะอาด');

?>
