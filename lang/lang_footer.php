<?php
// ナビゲーション
$footer_top = getMultiLang('Top page','トップページ','首頁','Top page');

$footer_car = getMultiLang('Vehicle guide','車両紹介','車種介紹','แนะนำรถ');
$footer_car_type03 = getMultiLang('ZIL520','ジル520','Zil520','ZIL520');
$footer_car_type01 = getMultiLang('CORDE BUNKS','コルドバンクス','Corde Bunks','CordesBanks');
$footer_car_type02 = getMultiLang('CORDE LEAVES','コルドリーブス','Corde Leaves','CordesReeves');
$footer_car_type04 = getMultiLang('CORDE RUNDY','コルドランディ','Corde Rundy','Cordes Randy');
$footer_car_type05 = getMultiLang('SUNLIGHT','サンライト','Sunlight','SUNLIGHT');

$footer_price = getMultiLang('Price / Reservation','料金・ご予約','費用・預約','ราคาและการจอง');
$footer_price_02 = getMultiLang('Price List','料金表','價目表','ตารางราคา');
$footer_price_03 = getMultiLang('Special Offer','割引プラン','優惠方案','ส่วนลด');
$footer_reservation1 = getMultiLang('Booking schedule','空車状況','空車状況','ตรวจสอบรถที่ว่าง');
$footer_reservation2 = getMultiLang('Reservation form','ご予約フォーム','預約單','แบบฟอร์มการจองรถ');

$footer_rental = getMultiLang('Rental goods','レンタルグッズ','使用導覽','อุปกรณ์ให้เช่า');
$footer_guide = getMultiLang('User\'s guides','ご利用ガイド','租賃契約','วิธีการจองรถ');
$footer_yakkan = getMultiLang('Lending rules (Japanese) *don’t put spaces in parentheses','貸渡約款(日本語)','聯繫我們','ข้อตกลงการเช่ารถ(ภาษาญี่ปุ่น)');
$footer_yakkan_en = getMultiLang('Lending rules(English)','Lending rules(English)','Lending rules(English)','Lending rules(English)');
$footer_yakkan_zh = getMultiLang('Lending rules(Chinese)','貸渡約款(中国語)','貸渡約款(中国語)','貸渡約款(中国語)');
$footer_contact = getMultiLang('Contact','お問い合わせ','聯繫我們','ติดต่อสอบถาม');
$footer_tokusyou = getMultiLang('Notation based on the Specified Commercial Transaction Act','特定商取引法に基づく表記','依特定交易法之營利登記','ช้อมูลบริษัท');
$footer_tourism = getMultiLang('For foreign customers','海外からの皆様へ','給外國客戶的資訊','สำหรับชาวต่างชาติ');

$footer_reservation_ja = getMultiLang('RESERVATION(Japanese)','ご予約はこちら(Japanese)','ご予約はこちら(Japanese)','ご予約はこちら(Japanese)');
$footer_reservation_en = getMultiLang('RESERVATION(English)','RESERVATION(English)','RESERVATION(English)','RESERVATION(English)');
$footer_reservation_zh = getMultiLang('RESERVATION(Chinese)','預約(Chinese)','預約(Chinese)','預約(Chinese)');

$footer_shokai = getMultiLang('We are introduced on Rental Camping Car Net','「レンタルキャンピングカーネット」さんでもご紹介いただいております。','「レンタルキャンピングカーネット」さんでもご紹介いただいております。','ถูกแนะนำโดยเว็บ「rental-camper」');
$footer_kihu = getMultiLang('We are member of 1% FTP. 1% of the fee of Premium class ZIL520 is donated for environmental preservation activity through 1% FOR THE PLANET ( 1% FTP)','弊社は1％FTPメンバーです。プレミアムクラスのZIL520をご利用頂いた売上の一部は1％ FOR THE PLANET(1%FTP)を通して環境保護活動に寄付されます。','弊社は1％FTPメンバーです。プレミアムクラスのZIL520をご利用頂いた売上の一部は1％ FOR THE PLANET(1%FTP)を通して環境保護活動に寄付されます。','บริษัทของเราเป็นสมาชิก1％for the planet รายได้ส่วนหนึ่งจากการเช่ารถรุ่นZIL520 จะนำไปบริจาคให้กับกิจกรรมคุ้มครองสิ่งแวดล้อมของ1％FTP');

$footer_comment = getMultiLang('※If you want to take a look at the RV, please contact us in advance, we will arrange the time for your visit of Kiyota Office.<br>Stuffs might be absent for delivering the cars.<br>','※現車をご覧になりたい方は、必ず事前にご連絡頂き調整のうえ<br>清田ベースまでご来店をお願い致します。<br>スタッフは配車等で通常は不在にしております。<br>','','*สำหรับลุกค้าที่ต้องการมาดูรถ กรุณาติดต่อล่วงหน้า<br>กรุณาเดินทางมาที่ Kiyota Rental Space<br>เนื่องจากสต๊าฟต้องออกไปส่งรถจึงไม่ได้ประจำอยู่ที่บริษัท<br>');

$footer_receiv_address = getMultiLang('郵送物受け取り住所<br>〒066-0015<br>千歳市青葉2丁目17-28(北海道ノマドレンタカー千歳店)','郵送物受け取り住所<br>〒066-0015<br>千歳市青葉2丁目17-28(北海道ノマドレンタカー千歳店)','','郵送物受け取り住所<br>〒066-0015<br>千歳市青葉2丁目17-28(北海道ノマドレンタカー千歳店)');

//$footer_menkyo = getMultiLang('License number  Hokkaido District Transport Bureau certified 札運第１０９号','免許番号　北海道運輸局札幌運輸支局認定　札運第１０９号','免許番号　北海道運輸局札幌運輸支局認定　札運第１０９号','งฮอกไกโด สาขาซัปโปโร เลขที่ 109');
$footer_menkyo = getMultiLang('','','','');


$footer_pay1 = getMultiLang('Payment account','【銀行お支払い口座】','【銀行お支払い口座】','บัญชีธนาคารในการชำระเงิน');
$footer_pay2 = getMultiLang('North　Pacific Bank','北洋銀行　札幌西支店','北洋銀行　札幌西支店','North Pacific Bank - Sapporo-Nishi Branch');
$footer_pay3 = getMultiLang('Savings account number 5326111','普通　５３２６１１１','普通　５３２６１１１','เลขที่ 5326111');
$footer_pay4 = getMultiLang('Account holder  ﾎｯｶｲﾄﾞｳﾉﾏﾄﾞﾚﾝﾀｶｰ　( Hokkaido Nomad Rent a Car)','口座名　ホッカイドウノマドレンタカー','口座名　ホッカイドウノマドレンタカー','ชื่อบัญชี Hokkaido Nomad Car Rental');

$footer_delivery1 = getMultiLang('','','','');
$footer_delivery2 = getMultiLang('北海道ノマドレンタカー株式会社<br>本社　〒062-0936 札幌市豊平区平岸6条12丁目11-2','北海道ノマドレンタカー株式会社<br>本社　〒062-0936 札幌市豊平区平岸6条12丁目11-2','','北海道ノマドレンタカー株式会社<br>本社　〒062-0936 札幌市豊平区平岸6条12丁目11-2');

?>
