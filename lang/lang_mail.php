<?php
// mail.php

$pet_none = getMultiLang('None','無','沒有','ไม่มี');
$pet_unit = getMultiLang('','匹','隻','ตัว');

$price_prefix = getMultiLang('','￥','日幣','');
$price_suffix = getMultiLang('JPY','','','เยน');

$err_msg1 = getMultiLang('Error input. Please check the following and press the "return" button to revise.','入力にエラーがあります。下記をご確認の上「戻る」ボタンにて修正をお願い致します。','您的輸入資料有誤。麻煩請確認後按「返回」後修改。','มีพบข้อผิดพลาด กรุณากดปุ่ม「ย้อนกลับ」เพื่อแก้ไขข้อมูล');
$return_btn = getMultiLang('back',' 前画面に戻る','返回','กลับไปหน้าที่แล้ว');

$war_msg1 =  getMultiLang('The reservation has not been completed yet.',' まだ予約が完了しておりません','尚未完成正式預約','การจองยังไม่เสร็จสมบูรณ์');
$war_msg2 =  getMultiLang('If there is no mistake with the following contents, please press the "send" button.',' 以下の内容で間違いがなければ、「送信する」ボタンを押してください。','以下內容確認無誤的話，請按「送出」。','หากข้อมูลด้านล่างไม่มีข้อผิดพลาดกรุณากดปุ่ม「ส่ง」');

?>