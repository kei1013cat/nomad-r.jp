<?php
// ナビゲーション
$header_home = getMultiLang('HOME','ホーム','首頁','หน้าแรก');
$header_car = getMultiLang('CAR','車両紹介','車種介紹','แนะนำรถ');
$header_guide = getMultiLang('GUIDE','ご利用ガイド','使用導覽','วิธีการจองรถ');
$header_price = getMultiLang('PRICE','料金・ご予約','租金・預約','ราคา・จอง');
$header_rental = getMultiLang('RENTAL GOODS','レンタル用品','租借用品','อุปกรณ์ให้เช่า');
$header_contact = getMultiLang('CONTACT US','お問い合わせ','聯繫我們','ติดต่อเรา');

$header_sub_home = getMultiLang('','HOME','HOME','HOME');
$header_sub_car = getMultiLang('','CAR','CAR','CAR');
$header_sub_guide = getMultiLang('','GUIDE','GUIDE','GUIDE');
$header_sub_price = getMultiLang('','PRICE','PRICE','PRICE');
$header_sub_rental = getMultiLang('','RENTAL GOODS','RENTAL GOODS','RENTAL GOODS');
$header_sub_contact = getMultiLang('','CONTACT US','CONTACT US','CONTACT US');

$header_reservation = getMultiLang('Schedule / Reservation','空車状況・ご予約','空車狀況・預約請見','จองรถ  ตรวจสอบรถว่าง・จอง');
$header_japanese = getMultiLang('Japanese(日本語)','日本語','日本語版','ภาษาญี่ปุ่น(日本語)');

$header_title = getMultiLang('Hokkaido Nomad Rent a Car Enjoy Hokkaido with a Caravan! Rental Caravan company in Sapporo','北海道ノマドレンタカー　キャンピングカーで北海道を満喫！札幌のキャンピングカーレンタル会社','北海道逍遙遊露營車　自駕露營車享受北海道之旅吧！札幌的露營租車公司','Hokkaido Nomad Car rental เที่ยวเพลินทั่วฮอกไกโดด้วยรถบ้าน บริษัทเช่ารถบ้านซัปโปโร');

?>

