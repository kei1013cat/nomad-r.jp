<?php
/*
Template Name: page-blogslist
*/
?>
<?php get_header(); ?>

<div class="wrapper">
<section>
<h2><img src="<?php bloginfo('template_url'); ?>/images/news_h2.png" alt="NEWS/新着情報" /></h2>

<div class="news">
<?php
		$wp_query = new WP_Query();
		$param = array(
			'posts_per_page' => '100', //表示件数。-1なら全件表示
			'post_status' => 'publish',
			'orderby' => 'date', //ID順に並び替え
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
					<?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<dl class="cf">
						<dt><?php the_time('Y.m.d'); ?></dt>
						<dd> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
							<?php the_title(); ?>
							</a> </dd>
					</dl>
					<?php endwhile; ?>
			<?php else : ?>
			記事が見つかりません。
					<?php endif; ?>
					<?php wp_reset_query(); ?>


</div><!-- news -->
</section>
</div><!-- wrapper -->

<?php get_footer(); ?>
