<?php
/*
Template Name: page-en
*/
?>
<?php get_header(); ?>

<div id="index">


    <div id="main_image" class="<?php echo lang(); ?>">
        <div id="slider" class="flexslider">
            <ul class="slides <?php echo lang(); ?>">

                <li id="slider_bg10">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>国内最高級ラグジュアリーサルーン<br>サンライトで最高の旅を！</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg11">
                    <div class="wrapper">
                        <div class="catch right">
                            <p>無料送迎！！</p>
                            <h3>「JR千歳駅から3分」<br>「新千歳空港から7分」</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg1">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg2">
                    <div class="wrapper">
                        <div class="catch right">
                            <p>ノマドのキャンピングカーは、全てディーゼル４ＷＤ、ＦＦヒーター付き<br class="pc">
                                北海道の真冬でも室内は温かく快適に過ごせるバンテック社製の車輛です</p>
                            <h3>北海道のパウダースノーを遊び尽くそう！<br class="pc">
                                ニセコ～富良野～大雪　キャンピングカーならゲレンデ一番乗り！
                            </h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg3">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg4">
                    <div class="wrapper">
                        <div class="catch right">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg5">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg6">
                    <div class="wrapper">
                        <div class="catch right">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg7">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg8">
                    <div class="wrapper">
                        <div class="catch right">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>

                <li id="slider_bg9">
                    <div class="wrapper">
                        <div class="catch left">
                            <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                            <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                            <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                        </div>
                        <!-- catch -->
                    </div>
                    <!-- wrapper -->
                </li>
            </ul>
        </div>
        <div id="carousel" class="flexslider cf">
            <ul class="slides">
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide10_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide11_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide01_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide09_thumb.jpg?v=20190826" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide03_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide04_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide05_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide06_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide07_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide08_thumb.jpg" class="alpha60" alt="" /></a> </li>
                <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide02_thumb.jpg?v=20190826" class="alpha60" alt="" /></a> </li>
            </ul>
        </div>
    </div>
    <!-- main_image -->

    <section class="topcar <?php echo lang(); ?>">
        <div class="wrapper1000">
            <p class="topmsg">Hokkaido No1. Luxury class rental caravan office</p>
        </div>
        <!--         <h2><span class="big">キャンピングカー</span>で<span class="big">北海道</span>の隠れた魅力発見の<span class="blue">旅</span>へ!</h2>
        <p class="catch">全車バンテック社製最新型車輌導入！<br />
      高級感のある清潔・安心な車輌をご提供</p>
      <p class="wifi">
      ＼&nbsp;&nbsp;<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg" width="45" height="45" alt="WI-FI">全車WI-FI完備&nbsp;&nbsp;／</p> -->

        <div class="movie_link">
            <ul class="cf carusel">
                <li>
                    <a href="https://www.youtube.com/watch?v=XRp0ZH2O4Cs" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img4.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.1</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=TvyD6Gu8INI" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img6.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.2</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=4oIJviKiNxw" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img1.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.3</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=vGsgDr1oaVY" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img3.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.4</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=ustY5Um00Uo&list=UUOPpdk9YO5CE6g9XBDCYdvg&index=4" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img5.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.5</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=P4YUBXS_nQU" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img2.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                    <p>Nomad movie.6</p>
                </li>
                <li>
                    <a href="https://www.youtube.com/watch?v=CTDOHjWnJbo&list=UUOPpdk9YO5CE6g9XBDCYdvg&index=2" target="_blank">
                        <img src="<?php bloginfo('template_url'); ?>/images/index_movie_img7.jpg?v=<?php echo date('YmdHis'); ?>" alt=""></a>
                    <p>Nomad movie.7</p>
                </li>
            </ul>
            <div id="arrows">
                <div class="slick-next">
                    <img src="<?php bloginfo('template_url'); ?>/images/nextbtn.svg" alt="→">
                </div>
                <div class="slick-prev">
                    <img src="<?php bloginfo('template_url'); ?>/images/prevbtn.svg" alt="←">
                </div>
            </div>
        </div>

        <section class="topcontent">
            <div class="bg_black">
                <div class="wrapper1000">

                    <div class="premium_class enter-bottom">
                        <h2 class="plmclass1">Explore Hokkaido in refined elegance</h2>

                        <?php if(is_pc()): ?>
                        <div class="toptitle cf pc">
                            <div class="left">
                                <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo1.jpg?v=20190516" />
                            </div>
                            <div class="center">
                                <h3><span>Brand new</span><br>Luxury class</h3>
                                <img src="<?php bloginfo('template_url'); ?>/images/plmclass_underline.png" />
                                <h4>SUNLIGHT</h4>
                            </div>
                            <div class="right">
                                <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo2.jpg?v=20190516" />
                            </div>
                        </div>
                        <?php else: ?>
                        <div class="toptitle cf">
                            <div class="top">
                                <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo_sp.jpg" />

                            </div>

                            <div class="bottom">
                                <h3><span>Brand new</span><br>Luxury class</h3>
                                <img src="<?php bloginfo('template_url'); ?>/images/plmclass_underline.png" />
                                <h4>SUNLIGHT</h4>
                            </div>

                        </div>


                        <?php endif; ?>


                        <div class="top_inner">
                            <div class="car_outer cf">
                                <div class="car_left cf">
                                    <h2 class="car_title pc">SUNLIGHT<span>- Luxury class -</span></h2>
                                    <div class="car_photo">
                                        <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05"><img src="<?php bloginfo('template_url'); ?>/images/sunlight_car_pic.jpg" /></a>
                                    </div>
                                    <div class="car_inside pc">
                                        <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=05#camera-sunlignt"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.png">
                                                <div class="mask">
                                                    <div class="caption">Check inside with 360° view camera</div>
                                                </div>
                                                <!-- mask -->
                                            </a>
                                            <!--
                                            <ul class="mark cf">
                                                <li>
                                                    <img class="wifi" src="<?php bloginfo('template_url'); ?>/images/wifi.jpg" />
                                                </li>
                                            </ul>
-->

                                        </div>
                                        <!-- inside -->
                                    </div>
                                    <!-- car_inside -->
                                </div>
                                <!-- car_left -->
                                <div class="car_right">
                                    <!--
                                    <h3>Free pick up at New Chitose Airport or hotel in Sapporo with 5-day or more rentals<br>
                                    </h3>
-->
                                    <br><br>
                                    <h4>¥56,000~per day</h4>

                                    <div class="car_center sp">
                                        <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=05#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.jpg">
                                                <div class="mask">
                                                    <div class="caption">Check inside with 360° view camera</div>
                                                </div>
                                            </a>
                                        </div>
                                        <!-- inside -->
                                    </div>
                                    <!-- car_center -->

                                    <dl class="cf">
                                        <dt>Seating capacity</dt>
                                        <dd>Seats 6 (sleeps 5)</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt>Fuel</dt>
                                        <dd>Diesel(Turbo)</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt>Drive train</dt>
                                        <dd>FF 2WD</dd>
                                    </dl>
                                    <div class="link_btn">
                                        <ol class="plemclass_btn cf">
                                            <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_3">Price</a></li>
                                            <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability</a></li>
                                        </ol>
                                    </div>
                                    <!-- link_btn -->
                                </div>
                                <!-- car_right -->
                            </div>
                            <!-- car_outer -->
                        </div>
                        <!-- top_inner -->
                    </div>
                    <!-- premium_class -->
                </div>
                <!-- wrapper1000 -->
            </div>
            <!-- bg_black -->
        </section>
        <!-- topcontent -->

        <section class="btmcontent">
            <div class="wrapper1000">
                <ul>
                    <li class="car_cont enter-bottom">
                        <div class="car_outer cf">
                            <div class="car_left cf">
                                <h2 class="car_title">ZIL520<span>- Premium class -</span></h2>
                                <div class="car_photo">
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" /></a>
                                    <p>Luxury trip with luxury equipment</p>
                                </div>
                                <div class="car_inside pc">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil">
                                            <img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                            <!-- mask -->
                                        </a>
                                        <ul class="mark cf">

                                            <li>
                                                <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_inside -->
                            </div>
                            <!-- car_left -->
                            <div class="car_right">
                                <!--
                                <h3>Free pick up at New Chitose Airport or hotel in Sapporo with 5-day or more rentals<br>
                                </h3>
-->
                                <br><br>
                                <h4>¥30,000~per day</h4>

                                <div class="car_center sp">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_center -->

                                <dl class="cf">
                                    <dt>Seating capacity</dt>
                                    <dd>Seats 6 (sleeps 5)</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Fuel</dt>
                                    <dd>Diesel(Turbo)
                                    </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Drive train</dt>
                                    <dd>4WD / 4
                                        speed automatic</dd>
                                </dl>
                                <div class="link_btn">
                                    <ol class="plemclass_btn cf">
                                        <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_2">Price</a></li>
                                        <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability</a></li>
                                    </ol>
                                </div>
                                <!-- link_btn -->
                            </div>
                            <!-- car_right -->
                        </div>
                        <!-- car_outer -->
                    </li>

                    <li class="car_cont enter-bottom">
                        <div class="car_outer cf">
                            <div class="car_left cf">
                                <h2 class="car_title">CORDE BUNKS<span>- High class -
                                    </span></h2>
                                <div class="car_photo">
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" /></a>
                                    <p>Recommend for Family or group</p>
                                </div>
                                <div class="car_inside pc">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=01#camera-bunks"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                            <!-- mask -->
                                        </a>
                                        <ul class="mark cf">

                                            <li>
                                                <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_inside -->
                            </div>
                            <!-- car_left -->
                            <div class="car_right">
                                <!--
                                <h3>Free pick up at New Chitose Airport or hotel in Sapporo with 6-day or more rentals<br>
                                </h3>
-->
                                <br><br>
                                <h4>¥22,000~per day</h4>

                                <div class="car_center sp">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.jpg">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_center -->

                                <dl class="cf">
                                    <dt>Seating capacity</dt>
                                    <dd>7 persons for driving<br>(sleeping for 5 persons)</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Fuel</dt>
                                    <dd>
                                        Diesel (Turbo)
                                    </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Drive train</dt>
                                    <dd>speed automatic</dd>
                                </dl>
                                <div class="link_btn">
                                    <ol class="plemclass_btn cf">
                                        <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">Price</a></li>
                                        <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability</a></li>
                                    </ol>
                                </div>
                                <!-- link_btn -->
                            </div>
                            <!-- car_right -->
                        </div>
                        <!-- car_outer -->
                    </li>

                    <li class="car_cont enter-bottom">
                        <div class="car_outer cf">
                            <div class="car_left cf">
                                <h2 class="car_title">CORDE LEAVES<span>- High class -</span></h2>
                                <div class="car_photo">
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" /></a>
                                    <p>Relaxing trip for a couple</p>
                                </div>
                                <div class="car_inside pc">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360°<br>view camera</div>
                                            </div>
                                            <!-- mask -->
                                        </a>
                                        <ul class="mark cf">

                                            <li>
                                                <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_inside -->
                            </div>
                            <!-- car_left -->
                            <div class="car_right">
                                <!--
                                <h3>Free pick up at New Chitose Airport or hotel in Sapporo with 6-day or more rentals<br>
                                </h3>
-->
                                <br><br>
                                <h4>¥22,000 / a day</h4>

                                <div class="car_center sp">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.jpg">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_center -->

                                <dl class="cf">
                                    <dt>Seating capacity</dt>
                                    <dd>7 persons for driving<br>(sleeping for 5 persons)</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Fuel</dt>
                                    <dd>
                                        Diesel (Turbo)
                                    </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Drive train</dt>
                                    <dd>speed automatic</dd>
                                </dl>
                                <div class="link_btn">
                                    <ol class="plemclass_btn cf">
                                        <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">Price</a></li>
                                        <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability</a></li>
                                    </ol>
                                </div>
                                <!-- link_btn -->
                            </div>
                            <!-- car_right -->
                        </div>
                        <!-- car_outer -->
                    </li>


                    <li class="car_cont enter-bottom">
                        <div class="coming-soon">
                            <div class="inner">Coming Soon</div>
                        </div>
                        <div class="car_outer cf">
                            <div class="car_left cf">
                                <h2 class="car_title">CORDE RUNDY<span>- High class -</span></h2>
                                <div class="car_photo">
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_rundy.jpg" /></a>
                                    <p>A caravan for trip with pets</p>
                                </div>
                                <div class="car_inside pc">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=04#camera-rundy"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360°<br>view camera</div>
                                            </div>
                                            <!-- mask -->
                                        </a>
                                        <ul class="mark cf">

                                            <li>
                                                <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_inside -->
                            </div>
                            <!-- car_left -->
                            <div class="car_right">
                                <!--
                                <h3>Free pick up at New Chitose Airport or hotel in Sapporo with 6-day or more rentals<br>
                                </h3>
-->
                                <br><br>
                                <h4>¥22,000 / a day</h4>

                                <div class="car_center sp">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.jpg">
                                            <div class="mask">
                                                <div class="caption">Check inside with 360° view camera</div>
                                            </div>
                                        </a>
                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_center -->

                                <dl class="cf">
                                    <dt>Seating capacity</dt>
                                    <dd>7 persons for driving<br>(sleeping for 5 persons)</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Fuel</dt>
                                    <dd>
                                        Diesel (Turbo)
                                    </dd>
                                </dl>
                                <dl class="cf">
                                    <dt>Drive train</dt>
                                    <dd>speed automatic</dd>
                                </dl>
                                <div class="link_btn">
                                    <ol class="plemclass_btn cf">
                                        <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">Price</a></li>
                                        <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability</a></li>
                                    </ol>
                                </div>
                                <!-- link_btn -->
                            </div>
                            <!-- car_right -->
                        </div>
                        <!-- car_outer -->
                    </li>


                </ul>
                <!-- btncontent -->
                <p class="comment enter-bottom">
                    <span>*Recommended for no more than four people if you have luggage like suitcases ( for all vehicle types)<br>
                        *High class Camping car,"Corde Bunks" and "Corde Leaves" have vehicles exclusively for pet rides. When you rent a camping car which pet allowed, in addition to the basic fee, "Pet Cleaning Fee" is charged.</span>
                </p>

                <p class="form_btn enter-bottom"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Concise estimate simulation</a></p>
            </div>
            <!-- wrapper -->
        </section>
        <!-- btmcontent -->

        <section class="point">
            <div class="wrapper">
                <h2 class="headline01"><span class="small2">For providing luxury trip in Hokkadio</span><br><span class="blue">9</span> reasons to choose Hokkaido Nomad Car Rental</h2>

                <ul class="cf">
                    <li class="fead1">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic01.jpg" alt="全て平成28年登録の新車"></p>
                        <h3>We provide brand-new vehicles<br class="pc"><br class="pc"></h3>
                        <p class="text">We provide safe, high-class, luxury travel. All vehicles are new.</p>
                    </li>
                    <li class="fead2">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic02.jpg" alt="清潔で快適な車内"></p>
                        <h3>Interiors are always kept clean and comfortable.</h3>
                        <p class="text">Safety, reliability, and cleanliness are our priority. Our specialized staff cleans, sterilizes, and deodorizes cars every time.</p>
                    </li>
                    <li class="fead3">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic03.jpg" alt="冬も暖かいＦＦヒーター付き"></p>
                        <h3>FF heater equipped for warming in the winter.<br class="pc"></h3>
                        <p class="text">All vehicles come equipped with clean diesel and FF heating. Perfect for extended stay skiers and snowboarders.</p>
                    </li>
                </ul>
                <ul class="cf">
                    <li class="fead1">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic04.jpg" alt="半日5千円～得々ノマド割引"></p>
                        <h3>Foreign language support staff. Foreigner limited express payment option.</h3>
                        <p class="text">Our staff speak English, Korean, and Chinese.We can provide <a href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">Hokkaido Expressway Passes (foreign tourists only)</a></p>
                    </li>
                    <li class="fead2">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic05.jpg" alt="24時間安心のロードサービス"></p>
                        <h3>GPS and 24 hour road service<br class="pc"><br class="pc"><br class="pc"></h3>
                        <p class="text">24 hours a day and 365 days a year we are here to help.</p>
                    </li>
                    <li class="fead3">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic06.jpg?v=201808" alt=""></p>
                        <h3>Introducing selected caravan parking spaces<br class="pc"><br class="pc"></h3>
                        <p class="text">Introducing exclusive caravan parking spaces away from the busy caravan parking and camping spots. (pay-based service)<br />We introduce you luxury parking space which accept only one customer per a day.<br />Make a reservation here (you need to make a reservation from external link below)<br />→　<a href="https://shachuoo.com" target="_blank">Caravan parking shachuoo</a></p>
                    </li>
                </ul>
                <ul class="cf">
                    <li class="fead1">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic07.jpg" alt="充実のアクティビティレンタル"></p>
                        <h3>Upgrade activity rentals</h3>
                        <p class="text">We have plenty of extra, free goods and amenities. We can prepare even more if you request in advance.</p>
                    </li>
                    <li class="fead2">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic08.jpg" alt="配車サービス"></p>
                        <h3>New Chitose airport ⇔ Chitose office ,FREE transport !</h3>
                        <p class="text">It’s just 10 minutes by taxi from New Chitose airport to our Chitose office.
                            With your arrival at our Chitose office, you will receive refund of the taxi fare by presenting receipt of taxi.</p>
                    </li>
                    <li class="fead3">

                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic09.jpg" alt="急な日程変更にも対応"></p>
                        <h3>We can handle sudden reschedules</h3>
                        <p class="text">We can amend the schedule if the reason is due to health problems, work issues, etc. for free one time only and only if you reschedule within six months.<br>
                            ※Your deposit can not be refunded.</p>
                    </li>
                </ul>
                <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Check availability / Reservation　＞</a></p>
            </div>
            <!-- wrapper -->
        </section>

        <!--
        <section class="area">
            <div class="wrapper">
                <h2 class="headline01">キャンピングカーで旅をしよう！<br>ノマドおすすめ車中泊スポット紹介</h2>
            </div>

            <ul class="cf">

                <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area1.jpg') no-repeat;">
                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=douou&lang=<?php echo lang();?>">
                        <h3>道央エリア</h3>
                        <div class="hover"></div>
                    </a>
                </li>
                <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area3.jpg') no-repeat;">
                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=douhoku&lang=<?php echo lang();?>">
                        <h3>道北エリア</h3>
                        <div class="hover"></div>
                    </a>
                </li>
                <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area4.jpg') no-repeat;">
                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=doutou&lang=<?php echo lang();?>">
                        <h3>道東エリア</h3>
                        <div class="hover"></div>
                    </a>
                </li>
                <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area2.jpg') no-repeat;">
                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=dounan&lang=<?php echo lang();?>">
                        <h3>道南エリア</h3>
                        <div class="hover"></div>
                    </a>
                </li>
            </ul>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?lang=<?php echo lang();?>">すべて見る　＞</a></p>
        </section>
-->
        <!-- area -->

        <p class="catch2"><span>Get away from the hustle and bustle of the city... Nomad Life</span>
            <a class="btn" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>tourism/">Welcome to HOKKAIDO TOURISM<br>For foreign customers</a>
        </p>

        <div class="wrapper info cf">
            <?php
                $wp_query = new WP_Query();
                $param = array(
                    'posts_per_page' => '5', //表示件数。-1なら全件表示
                    'post_status' => 'publish',
                    'orderby' => 'date', //ID順に並び替え
                    'order' => 'DESC'
                );
                $wp_query->query($param);?>
            <?php if($wp_query->have_posts()):?>
            <section class="news post">
                <h2 class="headline01 typesquare_tags">What's new</h2>

                <?php while($wp_query->have_posts()) :?>
                <?php $wp_query->the_post(); ?>
                <dl class="cf">
                    <dt>
                        <?php the_time('Y.m.d'); ?>
                    </dt>
                    <dd> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                            <?php the_title(); ?>
                        </a> </dd>
                </dl>
                <?php endwhile; ?>
            </section>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <section class="blog post">
                <?php
                $wp_query = new WP_Query();
                $param = array(
                    'posts_per_page' => '10', //表示件数。-1なら全件表示
                    'post_type' => 'tabi',
                    'post_status' => 'publish',
                    'orderby' => 'date', //ID順に並び替え
                    'order' => 'DESC'
                );
                $wp_query->query($param);?>
                <?php if($wp_query->have_posts()):?>
                <div class="post_list white">
                    <h2 class="headline01 typesquare_tags">Nomad travel information</h2>

                    <?php while($wp_query->have_posts()) : $wp_query->the_post();
                        $link ="";
                        $link = get_post_meta( $post->ID, 'link', true);
                        ?>
                    <dl class="cf">
                        <dt>
                            <p class="thumb"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php if(has_post_thumbnail()):?><?php echo get_the_post_thumbnail($post->ID, 'medium'); ?><?php else:?><img src="<?php bloginfo('template_url'); ?>/images/nophoto_thumb.jpg"><?php endif; ?></a></p>
                        </dt>
                        <dd>
                            <?php if($link != "") : ?>
                            <a href="<?php echo $link; ?>">
                                <?php the_title(); ?>
                            </a>
                            <?php else: ?>
                            <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                <?php the_title(); ?>
                            </a>
                            <?php endif; ?>
                        </dd>
                    </dl>
                    <?php endwhile; ?>

                </div>
                <!-- post_list -->
                <?php endif; ?>
                <?php wp_reset_query(); ?>
            </section>
        </div>
        <!-- wrapper -->
</div>
<?php get_footer(); ?>
