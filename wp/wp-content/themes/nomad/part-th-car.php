<div id="car">


    <?php if(!isset($_GET['type'])):?>
    <section class="car">
        <section>
            <h2 class="headline01">รถรุ่นใหม่ล่าสุด Luxury class</h2>
            <h3 class="big">SUNLIGHT</h3>
            <h3>เที่ยวชมความสวยงามของฮอกไกโดด้วยห้องคุณภาพ
            </h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_sunlight.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05">Specification　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">รถรุ่นใหม่ล่าสุด Premium Class</h2>
            <h3 class="big">ZIL 520</h3>
            <h3>ท่องเที่ยวอย่างหรูหราสะดวกสบาย</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
            </div>
            <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg" /></a></p>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/car?type=03">รายละเอียดคลิกที่นี่　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">HI CLASS</h2>
            <h3 class="big">Corde Bunks</h3>
            <h3>แนะนำสำหรับครอบครัวหรือการท่องเที่ยวเป็นกลุ่ม</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/car?type=01">รายละเอียดคลิกที่นี่　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">HI CLASS</h2>
            <h3 class="big">Corde Leaves</h3>
            <h3>พักผ่อนอย่างสงบแบบคู่รัก</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/car?type=02">รายละเอียดคลิกที่นี่　＞</a></p>
        </section>

        <section>
            <h2 class="headline01">HI CLASS</h2>
            <h3 class="big">Corde Rundy</h3>
            <h3>รถแคมปิ้งสำหรับผู้ที่มีความคิดอยากออกไปเที่ยวกับสัตว์เลี้ยง
            </h3>
            <div class="main_photo cf">
                <div class="coming-soon">
                    <div class="inner">Coming Soon
                    </div>
                </div>
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_rundy.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04">รายละเอียดคลิกที่นี่　＞</a></p>
        </section>

    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '05'):?>
    <section class="car sunlight" id="c05">

        <h2 class="headline01 typesquare_tags">SUNLIGHT</h2>
        <h3>เที่ยวชมความสวยงามของฮอกไกโดด้วยห้องคุณภาพ
        </h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_sunlight.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <!--        <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>-->
        <!--        <p class="kome1">5日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>-->
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>

                <th colspan="2">ชื่อรถ</th>
                <td width="174">Sunlight T67</td>
                <td width="216">ใช้ง่าย ผลิตโดยบริษัทยอดนิยมภายในประเทศ</td>
            </tr>
            <tr>
                <th colspan="2">บริษัทผู้ผลิต</th>
                <td>FIAT DUCATO</td>
                <td>รุ่นใหม่ล่าสุดของ FIAT</td>
            </tr>
            <tr>
                <th colspan="2">ปีที่ผลิต</th>
                <td>APR 2019</td>
                <td>รถใหม่เคลือบเงา</td>
            </tr>
            <tr>
                <th colspan="2">เครื่องยนต์</th>
                <td>Turbo-diesel</td>
                <td>ขับเคลื่อนทรงพลังแม้ทางขรุขระ</td>
            </tr>
            <tr>
                <th colspan="2">ขนาดเครื่องยนต์　</th>
                <td>3000cc</td>
                <td>เครื่องยนต์เงียบ</td>
            </tr>
            <tr>
                <th colspan="2">ระบบขับเคลื่อน</th>
                <td>Front-engine Front-wheel 2WD</td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">เบรก</th>
                <td>Anti-Lock Brake System </td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">ตำแหน่งคนขับ</th>
                <td>พวงมาลัยด้านขวา</td>
                <td>ทัศนวิสัยดี ขับขี่ได้ง่าย</td>
            </tr>
            <tr>
                <th colspan="2">เกียร์กระปุก</th>
                <td>ควบคุมสะดวก 6ระดับ</td>
                <td>ขับขี่ได้แทบไม่ต่างจากระบบออโต้</td>
            </tr>
            <tr>
                <th colspan="2">เชื้อเพลิง</th>
                <td>Diesel 90L</td>
                <td>ไม่ใช้แก๊สโซลีน กรุณาระวัง</td>
            </tr>
            <tr>
                <th colspan="2">ระยะทาง</th>
                <td>11km/ลิตร</td>
                <td>ประหยัดเพราะใช้น้ำมันดีเซล</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">จำนวนคน</th>
                <th width="27">ที่นั่ง</th>
                <td>6ที่นั่ง</td>
                <td>ภายในกว้างขวางนั่งสบาย</td>
            </tr>
            <tr>
                <th>ที่นอน</th>
                <td>6ที่นั่ง</td>
                <td>ไซส์ผู้ใหญ่5คน+เด็ก1คน</td>
            </tr>
            <tr>
                <th rowspan="3">ขนาด</th>
                <th>ความยาว</th>
                <td>7.35เมตร</td>
                <td>ติดตั้งเซ็นเซอร์และกล้องหลัง ขับขี่สบายใจ</td>
            </tr>
            <tr>
                <th>ความกว้าง</th>
                <td>2.32เมตร</td>
                <td></td>
            </tr>
            <tr>
                <th>ความสูง</th>
                <td>2.91เมตร</td>
                <td>กรุณาระวังเรื่องความสูงของรถ</td>
            </tr>
            <tr>
                <th rowspan="4">ขนาดเตียง</th>
                <th>เตียงแบบดึงลง</th>
                <td>1.95x1.4x1.1เมตร</td>
                <td>ผู้ใหญ่สามารถนอนได้2คน</td>
            </tr>
            <tr>
                <th>เตียงแบบปู</th>
                <td>2.1x1เมตร</td>
                <td>นอนคนเดียวสบายๆ</td>
            </tr>
            <tr>
                <th>เตียงด้านหลัง</th>
                <td>2.1x0.8x2.05x0.8เมตร</td>
                <td>ผู้ใหญ่2คนนอนสบายๆ</td>
            </tr>
            <tr>
                <th>เมื่อปรับใช้เตียงหลัง</th>
                <td>2.1x2.1x2.05เมตร</td>
                <td>ผู้ใหญ่2คนกับเด็ก1คนนอนได้</td>
            </tr>
            <tr>
                <th colspan="2">สูบบุหรี่</th>
                <td>ห้าม</td>
                <td>ไม่สามารถสูบบุหรี่ในรถได้ </td>
            </tr>
            <tr>
                <th colspan="2">สัตว์เลี้ยง</th>
                <td>ไม่ได้</td>
                <td>ปัจจุบันไม่สามารถนำสัตว์เลี้ยงขึ้นรถได้</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>สิ่งอำนวยความสะดวก</caption>
            <tr>
                <th>แผงฮีทเตอร์ (แบบแก๊ส)</th>
                <td>〇</td>
                <th>เตาแก๊สภายในรถยนต์</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>เครื่องปรับอากาศบริเวณคนขับ</th>
                <td>〇</td>
                <th>เตาแก๊สปิกนิก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แผงปรับอุณหภูมิ</th>
                <td>×</td>
                <th>ห้องแต่งตัวและห้องน้ำเคลื่อนที่ </th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ตู้เย็นขนาดใหญ่2ประตู </th>
                <td>〇</td>
                <th>โต๊ะใหญ่และมีที่วางเครื่องดื่ม</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เนวิเกเตอร์ รองรับ4ภาษา</th>
                <td>〇</td>
                <th>ชักโครกพกพา (ใช้ในกรณีฉุกเฉิน)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ระบบวิทยุ สเตอดิโอ ที่รองรับบลูทูธ</th>
                <td>×</td>
                <th>ฝักบัว (ไม่มีให้บริการ)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>ลำโพงเครื่องเสียงที่รองรับบลูทูธ</th>
                <td>〇</td>
                <th>กันสาดด้านข้าง(ไม่อนุญาติให้ใช้)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>กล้องมองหลัง(ตอนถอยรถ)</th>
                <td>〇</td>
                <th>เข็มขัดนิรภัยสามจุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>กล้องรอบคัน3ตัวและหน้าจอมอนิเตอร์ (ด้านหน้า+ด้านหลัง+ด้านข้าง)</th>
                <td>〇</td>
                <th>ม่านบังแดดที่หน้าต่างทุกบาน</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เซ็นเซอร์ด้านหลัง</th>
                <td>〇</td>
                <th>ประตูตาข่าย</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องอ่านการ์ด (กรุณาใส่การ์ดเพิ่มเติม)</th>
                <td>〇</td>
                <th>ช่องระบายอากาศขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟ100โวลต์ (รวมถึงการใช้งานนอกรถ)</th>
                <td>〇</td>
                <th>ห้องเก็บสัมภาระขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟภายในรถ12โวลต์</th>
                <td>〇</td>
                <th>ถังเก็บขยะด้านนอกรถ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>อ่างล้างจานที่มีน้ำปะปา</th>
                <td>〇</td>
                <th>ช่องชาร์ตแบต USB</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ถังเก็บน้ำขนาด122ลิตร</th>
                <td>〇</td>
                <th>ทีวีจอแบนรุ่นล่าสุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ถังระบาย้ำขนาด92ลิตร</th>
                <td>〇</td>
                <th>เครื่องชาร์จแบบพกพา</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องยึดสมาร์ทโฟนบริเวณที่นั่งคนขับ</th>
                <td>〇</td>
                <th>ระบบทำน้ำอุ่น(ใช้แก๊ส)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ติดตั้งถังแก๊ส8กิโลกรัม2ตัว</th>
                <td>〇</td>
                <th>ซันรูฟ3ตำแหน่ง</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แบตเตอรี่เสริม</th>
                <td>〇</td>
                <th>ที่นั่งคนขับสามารถปรับหมุนได้</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>หลอดประหยัดไฟ LED</th>
                <td>〇</td>
                <th>ที่ยึดไอแพดบริเวณที่นั่งคนขับ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ที่ยึดสมาร์ทโฟนบริเวณที่นั่งคนขับ</th>
                <td>〇</td>
            </tr>
        </table>
        <p class="kome1 mb">※แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>
        <div class="camera" id="camera-sunlignt">
            <h3>มองด้วยกล้อง360องศา</h3>
            <ul class="cf">
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/qNaqtRzu9lHAFk6dV6Cs1whFo" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">เมื่อมองจากเตียงที่2
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/nJTf8mBifuveF86mZNBJDFLlI" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">ห้องน้ำ
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/rlcoEYsj0X58Bf4NyZQQdDrl2" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">เมื่อมองจากห้องนอนสาม
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/abx5uwU4BT2xpChXGmQHx5MY4" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">ด้านหน้ารถ
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/fqqzXJFaL3G9yWwyRGVa8KHRI" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">กลางรถ
                    </p>
                </li>
            </ul>
        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo01.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo01.jpg">
                            <p class="text">ที่นั่งคนขับสูง ทัศนวิศัยดี</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo02.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo02.jpg">
                            <p class="text">บริเวณแผงหน้าปัด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo03.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo03.jpg">
                            <p class="text">เกียร์กระปุกควบคุม6ระดับ ขับขี่ได้แทบไม่ต่างจากระบบออโต้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo04.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo04.jpg">
                            <p class="text">สามารถชาร์จแบตได้จากช่องจุดไฟและช่องUSB ขนาด12โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo05.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo05.jpg">
                            <p class="text">มีอุปกรณ์ยึดสมาร์ทโฟนไปจนถึงไอแพด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo06.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo06.jpg">
                            <p class="text">ติดตั้งที่ยึดสมาร์ทโฟน สามารถดูจีพีเอสได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo07.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo07.jpg">
                            <p class="text">ด้านบนในส่วนคนขับ มีบานซันรูฟขนาดใหญ่</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo08.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo08.jpg">
                            <p class="text">ที่นั่งด้านข้าง นั่งได้2คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo09.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo09.jpg">
                            <p class="text">การจัดเรียงที่นั่งขณะขับขี่ปกติ 1</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo10.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo10.jpg">
                            <p class="text">การจัดเรียงที่นั่งขณะขับขี่ปกติ 2</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo11.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo11.jpg">
                            <p class="text">ที่นั่งคนขับสามารถหมุนเข้าหาด้านผู้โดยสารได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo12.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo12.jpg">
                            <p class="text">การจัดเรียงที่นั่งสำหรับจัดเลี้ยง 6ที่นั่ง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo13.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo13.jpg">
                            <p class="text">เบาะ2ที่นั่งสามารถปรับเป็นเตียงนอนสำหรับผู้ใหญ่ได้1คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo14.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo14.jpg">
                            <p class="text">ด้านหลังที่นั่งคนขับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo15.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo15.jpg">
                            <p class="text">พื้นที่ห้องครัวที่ใช้งานง่าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo16.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo16.jpg">
                            <p class="text">เตาแก๊ส LP สามหัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo17.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo17.jpg">
                            <p class="text">น้ำปะปาและอ่างล้างจานขนาดใหญ่</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo18.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo18.jpg">
                            <p class="text">ระบบระบายอากาศในครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo19.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo19.jpg">
                            <p class="text">ที่เก็บของมากมายบริเวณห้องครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo20.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo20.jpg">
                            <p class="text">ตู้แช่เย็นขนาดใหญ่ (ใช้แก๊สหรือสวิตซ์ไฟอัตโนมัติ)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo21.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo21.jpg">
                            <p class="text">ด้านบนก็มีที่เก็บของมากมาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo22.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo22.jpg">
                            <p class="text">ห้องน้ำ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo23.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo23.jpg">
                            <p class="text">ชักโครกแบบสูญญากาศ (ไม่มีน้ำชำระ)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo24.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo24.jpg">
                            <p class="text">โต๊ะเครื่องแป้ง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo25.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo25.jpg">
                            <p class="text">ปุ่มกดสำหรับดึงเตียงลง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo26.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo26.jpg">
                            <p class="text">เตียงแบบดึงลง ผู้ใหญ่สามารถนอนได้2คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo27.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo27.jpg">
                            <p class="text">ด้านบนเป็นเตียงแบบดึงลง ด้านล่างเป็นเตียงแบบปรับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo28.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo28.jpg">
                            <p class="text">บันไดสำหรับปีนขึ้นเตียงแบบดึงลง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo29.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo29.jpg">
                            <p class="text">เตียงแบบดึงลงคุณสามารถมองเห็นท้องฟ้าที่เต็มไปด้วยดวงดาวจากเพดานสกายไลท์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo30.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo30.jpg">
                            <p class="text">สวิตซ์เครื่องทำความร้อน ข้างบนสำหรับทำน้ำร้อน เครื่องหมายเปลวไฟด้านล่างเพื่อให้ความร้อน + น้ำร้อน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo31.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo31.jpg">
                            <p class="text">เบรกเกอร์ไฟฟ้า 100โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo32.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo32.jpg">
                            <p class="text">ช่องปล่อยลมเครื่องทำความร้อนและเต้าเสียบ 100โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo33.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo33.jpg">
                            <p class="text">แผงคุมพลังงานส่วนกลาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo34.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo34.jpg">
                            <p class="text">มีที่เก็บของมากมายในห้อง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo35.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo35.jpg">
                            <p class="text">ซันรูฟด้านบนครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo36.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo36.jpg">
                            <p class="text">ซันรูฟด้านบนที่นั่งคนขับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo37.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo37.jpg">
                            <p class="text">ซันรูฟด้านบนห้องนอนด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo38.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo38.jpg">
                            <p class="text">ซันรูฟด้านบนห้องน้ำ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo39.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo39.jpg">
                            <p class="text">ห้องนอนด้านหลัง สามารถนอนได้2คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo40.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo40.jpg">
                            <p class="text">มุมมองในห้องจากเตียงด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo41.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo41.jpg">
                            <p class="text">เตียงด้านหลัง สามารถปรับให้นอน3คนได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo42.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo42.jpg">
                            <p class="text">บันไดสำหรับใช้กับเตียงด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo43.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo43.jpg">
                            <p class="text">ด้านบนของเตียงก็มีที่เก็บของมากมาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo44.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo44.jpg">
                            <p class="text">ช่องเติมเชื้อเพลิงด้านหลังรถ (เชื้อเพลิงเป็นดีเซล)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo45.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo45.jpg">
                            <p class="text">ทางใส่ถังเก็บน้ำ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo46.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo46.jpg">
                            <p class="text">เคเบิ้ลเชื่อมต่อไฟนอก100โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo47.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo47.jpg">
                            <p class="text">ช่องทางจัดการห้องน้ำแบบสูญญากาศ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo48.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo48.jpg">
                            <p class="text">แก๊สLPขนาด8ลิตร 2ถังเต็ม</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo49.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo49.jpg">
                            <p class="text">ขั้นบันได</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo50.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo50.jpg">
                            <p class="text">ห้องเก็บสัมภาระขนาดใหญ่ด้านหลัง ใหญ่พอจะเก็บรถจักรยานได้2คัน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo51.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo51.jpg">
                            <p class="text">ห้องเก็บสัมภาระก็มีไฟฟ้าและเครื่องทำความร้อน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo52.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo52.jpg">
                            <p class="text">ด้านหน้ารถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo53.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo53.jpg">
                            <p class="text">ตัวรถด้านขวา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo54.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo54.jpg">
                            <p class="text">ตัวรถด้านซ้าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo55.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo55.jpg">
                            <p class="text">ด้านหลังรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo56.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo56.jpg">
                            <p class="text">รถด้านหน้าขวา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo57.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo57.jpg">
                            <p class="text">รถด้านหลังขวา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo58.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo58.jpg">
                            <p class="text">รถสูงประมาณ3เมตร</p>
                    </a></li>
            </ul>
            <p class="kome2">※แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '03'):?>
    <section class="car bunks" id="c03">

        <h2 class="headline01 typesquare_tags">ZIL 520</h2>
        <h3>ท่องเที่ยวอย่างหรูหราสะดวกสบาย</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
        </div>
        <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg" /></a></p>
        <!--        <p class="kome1">เช่าตั้งแต่5วันขึ้นไป บริการรับส่งฟรีจากสนามบินชินจิโตะเสะ สถานีรถไฟซัปโปโรและโรงแรมบริเวณใจกลางเมือง</p>-->
        <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>

                <th colspan="2">ชื่อรถ</th>
                <td width="174">Vantech ZIL 520</td>
                <td width="216">ใช้ง่าย ผลิตโดยบริษัทยอดนิยมภายในประเทศ</td>
            </tr>
            <tr>
                <th colspan="2">บริษัทผู้ผลิต</th>
                <td>TOYOTA/CAMROAD</td>
                <td>ใช้เครื่องยนต์เฉพาะของรถแคมปิ้ง</td>
            </tr>
            <tr>
                <th colspan="2">ปีที่ผลิต</th>
                <td>2016 SEP</td>
                <td>รถใหม่เคลือบเงา</td>
            </tr>
            <tr>
                <th colspan="2">เครื่องยนต์</th>
                <td>Turbo-diesel</td>
                <td>ขับเคลื่อนทรงพลังแม้ทางขรุขระ</td>
            </tr>
            <tr>
                <th colspan="2">ขนาดเครื่องยนต์</th>
                <td>3000cc</td>
                <td>เครื่องยนต์เงียบ</td>
            </tr>
            <tr>
                <th colspan="2">ระบบขับเคลื่อน</th>
                <td>Fulltime 4WD</td>
                <td>ปลอดภัยแม้บนถนนในฤดูหนาว</td>
            </tr>
            <tr>
                <th colspan="2">เชื้อเพลิง</th>
                <td>Diesel</td>
                <td>ไม่ใช้แก๊สโซลีน กรุณาระวัง</td>
            </tr>
            <tr>
                <th colspan="2">ระยะทาง</th>
                <td>7-11km/ลิตร</td>
                <td>ประหยัดเพราะใช้น้ำมันดีเซล</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">จำนวนคน</th>
                <th width="27">ที่นั่ง</th>
                <td>6ที่นั่ง</td>
                <td>ภายในกว้างขวางนั่งสบาย</td>
            </tr>
            <tr>
                <th>ที่นอน</th>
                <td>5ที่นอน</td>
                <td>นอนได้ถึง5คน</td>
            </tr>
            <tr>
                <th rowspan="3">ขนาด</th>
                <th>ความยาว</th>
                <td>5.16เมตร</td>
                <td>สั้นกว่า toyota hiace</td>
            </tr>
            <tr>
                <th>ความกว้าง</th>
                <td>2.11เมตร</td>
                <td>ใช้พื้นที่จอดรถเท่ารถปกติ</td>
            </tr>
            <tr>
                <th>ความสูง</th>
                <td>3.15เมตร</td>
                <td>กรุณาระวังเรื่องความสูงของรถ</td>
            </tr>
            <tr>
                <th colspan="2">รัศมีวงเลี้ยว</th>
                <td>4.9เมตร</td>
                <td>เหมือนรถยนต์ใช้งานทั่วไป</td>
            </tr>
            <tr>
                <th colspan="2">สูบบุหรี่</th>
                <td>ห้าม</td>
                <td>ไม่สามารถสูบบุหรี่ในรถได้ </td>
            </tr>
            <tr>
                <th colspan="2">สัตว์เลี้ยง</th>
                <td>ไม่ได้</td>
                <td>ไม่สามารถนำสัตว์เข้าไปในรถได้</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>สิ่งอำนวยความสะดวก</caption>
            <tr>
                <th>เครื่องปรับอากาศภายในรถ</th>
                <td>〇</td>
                <th>เตาแก๊สภายในรถยนต์</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>เครื่องทำความร้อน</th>
                <td>〇</td>
                <th>เตาแก๊สปิกนิก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แผงปรับอุณหภูมิ</th>
                <td>〇</td>
                <th>ห้องแต่งตัวและสุขาเคลื่อนที่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องปรับอากาศ (ขอแนะนำให้ใช้เมื่อต่อกับไฟฟ้าด้านนอก)</th>
                <td>○</td>
                <th>โต๊ะใหญ่และมีที่วางเครื่องดื่ม</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ตู้เย็นขนาดใหญ่</th>
                <td>〇</td>
                <th>ชักโครก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เนวิเกเตอร์ รองรับ4ภาษา</th>
                <td>〇</td>
                <th>ฝักบัว</th>
                <td>×</td>
            </tr>
            <tr>
                <th>ระบบวิทยุ สเตอดิโอ ที่รองรับบลูทูธ</th>
                <td>〇</td>
                <th>กันสาดด้านข้าง(ไม่อนุญาติให้ใช้)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>เข็มขัดนิรภัยสามจุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเสียงคุณภาพสูง ลำโพง4ตัว</th>
                <td>〇</td>
                <th>หลอดประหยัดไฟ LED</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเล่นแผ่น DVD (อุปกรณ์เสริม)</th>
                <td>△</td>
                <th>ม่านบังแดดที่หน้าต่างทุกบาน</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>กล้องมองหลัง</th>
                <td>〇</td>
                <th>ประตูตาข่าย</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ</th>
                <td>〇</td>
                <th>พัดลมระบายอากาศขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟ100โวลต์ (รวมถึงการใช้งานนอกรถ)</th>
                <td>〇</td>
                <th>ถังเก็บขยะด้านนอกรถ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟภายในรถ12โวลต์</th>
                <td>〇</td>
                <th>ช่องชาร์ตแบต USB</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>อ่างล้างจานที่มีน้ำปะปา</th>
                <td>〇</td>
                <th>มีแท็บเลตคู่มือการใช้รถ</th>
                <td>〇</td>
            </tr>

            <tr>

                <th>โทรทัศน์แขวนผนังรุ่นล่าสุด</th>
                <td>〇</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>

        <p class="kome1 mb">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>
        <div class="camera" id="camera-zil">
            <h3>ภาพถ่ายด้วยกล้อง360องศา</h3>
            <ul class="cf">
                <li>
                    <a title='ジル５２０　車輛後方右' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490295'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_01.jpg"></p>
                        <h4>ZIL520 ถ่ายด้วยกล้อง360องศาด้านหลังทางขวา</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛後方トイレ' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475486807'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_02.jpg"></p>
                        <h4>ZIL520 ถ่ายด้วยกล้อง360องศาห้องน้ำด้านหลังรถ</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛前方就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490809'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg"></p>
                        <h4>ZIL520 ถ่ายด้วยกล้อง360องศาเตียงนอนด้านหน้า</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475487141'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_04.jpg"></p>
                        <h4>ZIL520 ถ่ายด้วยกล้อง360องศาด้านหน้า</h4>
                    </a></li>
            </ul>

        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg">
                            <p class="text">อแด๊ปเตอร์12โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg">
                            <p class="text">USBอแด๊ปเตอร์สำหรับชาจ์ตไฟ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg">
                            <p class="text">เครื่องปรับอากาศที่ใช้งานกับเคลื่อนจ่ายไฟ100โวล์ต</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg">
                            <p class="text">ครัว เตาแก๊ส และอ่างล้างจาน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg">
                            <p class="text">ครัว เตาแก๊ส หลังการจัดเก็บ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">ที่วางเครื่องดื่มบริเวณคนขับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg">
                            <p class="text">ที่นั่งคนขับสูง ทำให้มีวิสัยทัศน์ที่ดี</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg">
                            <p class="text">เนวิเกเตอร์รุ่นใหม ขับขี่ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg">
                            <p class="text">ภายในห้องโดยสารมีเต้าเสียบสำหรับเครื่องใช้ไฟฟ้าได้ถึง100โวล์ต</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg">
                            <p class="text">มีกล้องหลังแบบเก็บภาพมุมกว้าง จึงขับขี่ได้อยางสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg">
                            <p class="text">มีจุดรวมแผงควบคุมไฟต่างๆ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg">
                            <p class="text">มีสวิทซ์ไฟและเต้าเสีบยบริเวณทีนอน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg">
                            <p class="text">ติดตั้งเครื่องวัดความดันไฟฟ้า สามารถตรวจเช็คสภาพและปริมาณการใช้ไฟฟ้าได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg">
                            <p class="text">เตียงนอนด้านบนที่นั่งคนขับนั้นมีความกว้างที่สามารถนอนได้ถึงสามคน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg">
                            <p class="text">ตู้เย็นขนาด90ลิตร</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg">
                            <p class="text">ประตูทางเข้ามีกระจกมองเห็นได้อย่างชัดเจน ปลอดภัยสำหรับผู้หญิง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg">
                            <p class="text">โต๊ะหลักขนาดใหญ่ใช้งานได้4คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg">
                            <p class="text">มีที่เก็บของมากมายบริเวณครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg">
                            <p class="text">ห้องสุขาของ ZIL520 ใช้งานได้ครบครัน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg">
                            <p class="text">พับโต๊ะขึ้นก็กลายเป็นเตียงนอน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg">
                            <p class="text">ภาพหลังเก็บโต๊ะและปูเตียง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg">
                            <p class="text">ภาพโต๊ะหลักแบบเต็ม</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg">
                            <p class="text">เตียง2ชั้นที่ห้องนอนหลังรถก็มีสวิทซ์และปลั๊กไฟ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg">
                            <p class="text">บันไดสำหรับปีนขึ้นเตียงด้านบนที่นังคนขับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg">
                            <p class="text">มีหัวแปลงUSBสำหรับช้าจ์ตโทรศัพท์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg">
                            <p class="text">ภาพภายในห้องจากด้านหลังรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg">
                            <p class="text">ที่เก็บของภายในรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg">
                            <p class="text">มีพัดลมระบายอากาศตรงเตียง2ชั้นด้วย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg">
                            <p class="text">ภาพภายในห้องจากด้านหน้ารถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg">
                            <p class="text">ภาพภายในห้องด้านหลังรถที่ถ่ายจากด้านหน้า</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg">
                            <p class="text">สวิทซ์ใช้งานง่าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg">
                            <p class="text">มีแสงสวางทั่วถึง</p>
                    </a></li>



                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg">
                            <p class="text">มีหน้าต่างมุ้งลวด</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg">
                            <p class="text">มีไฟอัติโนมัตที่ประตูทางเข้า ทำให้ใช้งานกลางคืนได้อยางปลอดภัย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg">
                            <p class="text">ส่วนของการเชื่อมต่อไฟฟ้า100โวล์ต</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg">
                            <p class="text">ช่องเก็บของเปียกท้ายรถ</p>
                    </a></li>




                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg">
                            <p class="text">ท้ายรถมีห้องเก็บของขนาดใหญ่</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg">
                            <p class="text">ช่องเก็บของท้ายรถด้านขวา</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg">
                            <p class="text">น้ำปะปาถูกติดตั้งไว้ในห้องเก็บของท้ายรถ</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg">
                            <p class="text">ถังน้ำปะปาสามารถเติมได้ที่ท้ายรถด้านซ้าย</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg">
                            <p class="text">บันไดไฟฟ้า</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg">
                            <p class="text">ถังน้ำมันเชื้อเพลิงอยู่ด้านหลังประตูฝั่งคนขับ</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg">
                            <p class="text">ภาพด้านหลังของรถ</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg">
                            <p class="text">ภาพรถด้านขวา</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg">
                            <p class="text">ภาพด้รถด้านซ้ายหลัง</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg">
                            <p class="text">ภาพด้านหน้าทางขวาของรถ</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg">
                            <p class="text">ภาพด้านหน้าทางซ้าย</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg">
                            <p class="text">ภาพรถด้านซ้าย</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg">
                            <p class="text">ภาพรถด้านหน้า</p>
                    </a></li>

            </ul>
            <p class="kome2">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>
    <?php if($_GET['type'] == '01'):?>
    <section class="car zil" id="c01">
        <h2 class="headline01 typesquare_tags">Corde Bunks</h2>

        <h3>แนะนำสำหรับครอบครัวหรือการท่องเที่ยวเป็นกลุ่ม</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
        </div>
        <!--        <p class="kome1">เช่าตั้งแต่6วันขึ้นไป บริการรับส่งฟรีจากสนามบินชินจิโตะเสะ สถานีรถไฟซัปโปโรและโรงแรมบริเวณใจกลางเมือง</p>-->
        <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">ชื่อรถ</th>
                <td width="174">Vantech Corde Bunks</td>
                <td width="216">ใช้ง่าย ผลิตโดยบริษัทยอดนิยมภายในประเทศ</td>
            </tr>
            <tr>
                <th colspan="2">บริษัทผู้ผลิต</th>
                <td>TOYOTA/CAMROAD</td>
                <td>ใช้เครื่องยนต์เฉพาะของรถแคมปิ้ง</td>
            </tr>
            <tr>
                <th colspan="2">ปีที่ผลิต</th>
                <td>2016 JUN</td>
                <td>รถใหม่เคลือบเงา</td>
            </tr>
            <tr>
                <th colspan="2">เครื่องยนต์</th>
                <td>Turbo-diesel</td>
                <td>ขับเคลื่อนทรงพลังแม้ทางขรุขระ</td>
            </tr>
            <tr>
                <th colspan="2">ขนาดเครื่องยนต์</th>
                <td>3000cc</td>
                <td>เครื่องยนต์เงียบ</td>
            </tr>
            <tr>
                <th colspan="2">ระบบขับเคลื่อน</th>
                <td>Fulltime 4WD</td>
                <td>ปลอดภัยแม้บนถนนในฤดูหนาว</td>
            </tr>
            <tr>
                <th colspan="2">เชื้อเพลิง</th>
                <td>Diesel</td>
                <td>ไม่ใช้แก๊สโซลีน กรุณาระวัง</td>
            </tr>
            <tr>
                <th colspan="2">ระยะทาง</th>
                <td>8-12km/ลิตร</td>
                <td>ประหยัดเพราะใช้น้ำมันดีเซล</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">จำนวนคน</th>
                <th width="27">ที่นั่ง</th>
                <td>7ที่นั่ง</td>
                <td>ภายในกว้างขวางนั่งสบาย</td>
            </tr>
            <tr>
                <th>ที่นอน</th>
                <td>5ที่นอน</td>
                <td>นอนได้ถึง5คน</td>
            </tr>
            <tr>
                <th rowspan="3">ขนาด</th>
                <th>ความยาว</th>
                <td>4.99เมตร</td>
                <td>สั้นกว่า toyota hiace</td>
            </tr>
            <tr>
                <th>ความกว้าง</th>
                <td>1.98เมตร</td>
                <td>ใช้พื้นที่จอดรถเท่ารถปกติ</td>
            </tr>
            <tr>
                <th>ความสูง</th>
                <td>3.15เมตร</td>
                <td>กรุณาระวังเรื่องความสูงของรถ</td>
            </tr>
            <tr>
                <th colspan="2">รัศมีวงเลี้ยว</th>
                <td>4.9เมตร</td>
                <td>เหมือนรถยนต์ใช้งานทั่วไป</td>
            </tr>
            <tr>
                <th colspan="2">สูบบุหรี่</th>
                <td>ห้าม</td>
                <td>ไม่สามารถสูบบุหรี่ในรถได้ </td>
            </tr>
            <tr>
                <th colspan="2">สัตว์เลี้ยง</th>
                <td>ไม่ได้</td>
                <td>ไม่สามารถนำสัตว์เข้าไปในรถได้</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>สิ่งอำนวยความสะดวก</caption>
            <tr>
                <th>เครื่องปรับอากาศภายในรถ</th>
                <td>〇</td>
                <th>เตาแก๊สภายในรถยนต์</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>เครื่องทำความร้อน</th>
                <td>〇</td>
                <th>เตาแก๊สปิกนิก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แผงปรับอุณหภูมิ</th>
                <td>〇</td>
                <th>ห้องแต่งตัวและสุขาเคลื่อนที่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องปรับอากาศ</th>
                <td>×</td>
                <th>โต๊ะใหญ่และมีที่วางเครื่องดื่ม</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ตู้เย็นขนาดใหญ่</th>
                <td>〇</td>
                <th>ชักโครกพกพา (อุปกรณ์เสริม)</th>
                <td>△</td>
            </tr>
            <tr>
                <th>เนวิเกเตอร์ รองรับ4ภาษา</th>
                <td>〇</td>
                <th>ฝักบัว</th>
                <td>×</td>
            </tr>
            <tr>
                <th>ระบบวิทยุ สเตอดิโอ ที่รองรับบลูทูธ</th>
                <td>〇</td>
                <th>กันสาดด้านข้าง(ไม่อนุญาติให้ใช้)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>เข็มขัดนิรภัยสามจุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเสียงคุณภาพสูง ลำโพง4ตัว</th>
                <td>〇</td>
                <th>หลอดประหยัดไฟ LED</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเล่นแผ่น DVD (อุปกรณ์เสริม)</th>
                <td>△</td>
                <th>ม่านบังแดดที่หน้าต่างทุกบาน</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>กล้องมองหลัง</th>
                <td>〇</td>
                <th>ประตูตาข่าย</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องอานการ์ด</th>
                <td>〇</td>
                <th>พัดลมระบายอากาศขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟ100โวลต์ (รวมถึงการใช้งานนอกรถ)</th>
                <td>〇</td>
                <th>ถังเก็บขยะด้านนอกรถ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟภายในรถ12โวลต์</th>
                <td>〇</td>
                <th>ช่องชาร์ตแบต USB</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>อ่างล้างจานที่มีน้ำปะปา</th>
                <td>〇</td>
                <th>มีแท็บเลตคู่มือการใช้รถ</th>
                <td>〇</td>
            </tr>

            <tr>
                <!--
                <th>無料WI-FI（1日500MBまで）</th>
                <td>〇</td>
-->
                <th>最新型薄型テレビ</th>
                <td>×</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>


        <p class="kome1 mb">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>
        <div class="camera" id="camera-bunks">
            <h3>ภาพถ่ายด้วยกล้อง360องศา</h3>
            <ul class="cf">
                <li>
                    <a title='コルドバンクス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468316834'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_01.jpg"></p>
                        <h4>Corde Bunksถ่ายด้วยกล้อง360องศาด้านหลัง</h4>
                    </a></li>

                <li>
                    <a title='コルドバンクス　車両後方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313153'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_02.jpg"></p>
                        <h4>Corde Bunksถ่ายด้วยกล้อง360องศาบริเวณที่นอน</h4>
                    </a></li>

                <li>
                    <a title='コルドバンクス　就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313123'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_03.jpg"></p>
                        <h4>Corde Bunksถ่ายด้วยกล้อง360องศาเตียงนอนด้านหน้า</h4>
                    </a></li>
            </ul>
        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" alt="運転席も広々">
                            <p class="text">พื้นที่ส่วนขับขี่กว้างขวาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" alt="運転席上部はベッドルームに大人３名が就寝できる広さです">
                            <p class="text">เตียงนอนด้านบนที่นั่งคนขับนั้นมีความกว้างที่สามารถนอนได้ถึงสามคน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" alt="車内には各所に照明設置">
                            <p class="text">ติดต้งไฟหลายจุดในรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" alt="広いテーブルを囲み快適な空間">
                            <p class="text">มีพื้นที่ว่างรอบโต๊ะขนาดใหญ่</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心">
                            <p class="text">มีกล้องหลังแบบเก็บภาพมุมกว้าง จึงขับขี่ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">เนวิเกเตอร์รองรับภาษาตางประเทศ คนต่างชาติก็ใช้ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" alt="すべての窓には網戸も装備">
                            <p class="text">หน้าต่างทุกบานติดตั้งมุ้งลวด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" alt="大容量６０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください">
                            <p class="text">ตู้เย็นขนาด60ลิตร สำหรับแช่เย็นแช่แข็ง ใช้ได้ตอนรถวิ่งและใช้ไฟฟ้าด้านนอกรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" alt="車内４スピーカーシステム搭載">
                            <p class="text">ภายในรถมีลำโพง4ตัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" alt="軽油ベバストＦＦヒーター付きで冬でも暖かいです">
                            <p class="text">เครื่องทำความร้อนแบบใช้เชื้อเพลิง แม้แต่ฤดูหนาวก็อุ่นสบาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" alt="100ボルト電源（外部電源使用時のみ使用できます）">
                            <p class="text">ปลั๊กไฟ100โวลต์ (ใช้ด้านนอกรถก็ได้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" alt="白を基調とした車内。中央にキッチンと冷蔵庫があります">
                            <p class="text">ตกแต่งภายในด้วยโทนสีขาว มีครัวและตู้เย็นอยู่ตรงกลาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" alt="ＬＥＤランプ設置">
                            <p class="text">ใช้หลอดไฟLED</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" alt="ユーティリティルームはポータブルトイレ設置や着替えルームに">
                            <p class="text">มีห้องว่างไว้สำหรับใช้เป็นสุขาหรือห้องแตงตัวได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" alt="後方に２段ベッドがあるのでお子様にも大人気">
                            <p class="text">ส่วนท้ายรถมีเตียง2ชั้น เด็กๆชอบมาก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" alt="キッチン横には棚も収納">
                            <p class="text">ตู้เก็บของด้านข้างครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" alt="上部にはＬＥＤ照明と棚があります">
                            <p class="text">ตู้เก็บของด้านบนก็มีไฟLED</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" alt="車両後方大容量トランクには上水道用２０リットルタンク">
                            <p class="text">ที่เก็บของขนาดใหญ่ท้ายรถที่แท๊งก์น้ำขนาด20ลิตร</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" alt="外部からの吸気換気システム完備。雨の日でも使用できます">
                            <p class="text">พัดลมระบายอากาศติดตั้งจากด้านนอก สามารถใช้ได้แม้วันที่ฝนตก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" alt="広々としたリビングルーム">
                            <p class="text">ห้องนั่งเล่นกว้างขวาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" alt="入口横にはシングルシートがあります">
                            <p class="text">ข้างทางเข้ามีโซฟาเดี่ยว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" alt="後方２段ベッド">
                            <p class="text">เตียง2ชั้นด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" alt="キッチン下にもたくさんの収納">
                            <p class="text">ใต้ที่ทำอาหารมีที่เก็บของมากมาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" alt="車輛入口にある集中電源。上からメインスイッチ・クーラー・照明">
                            <p class="text">บริเวณทางเข้ารถมีแผงควบคุม จากด้านบนคือ สวิทซ์หลัก ปุ่มควบคุมความเย็น และสวิทซ์ไฟ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" alt="車内キッチン・ガスコンロ（市販のカセットコンロを使用できます）">
                            <p class="text">ห้องครัวภายในรถมีเตาแก๊ส สามารถเป็นกับเตาแก๊สพกพาได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" alt="シンクとキッチンです">
                            <p class="text">อ่างล้างจานและที่ทำอาหาร</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" alt="入口は車輛横中央部から。網戸も完備・夏も安心">
                            <p class="text">ประตูทางเข้าอยู่ตรงกลางตัวรถ ติดตั้งมุ้งลวดแล้ว ฤดูร้อนก็ใช้สบาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" alt="車両後方・大容量トランクルームに荷物もたっぷり収納">
                            <p class="text">มีที่เก็บของขนาดใหญ่ท้ายรถ เก็บสัมภาระได้เป็นจำนวนมาก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" alt="家族グループ向きの車輛で大容量トランクも有ります">
                            <p class="text">เนื่องจากเป็นรถสำหรับครอบครัวหรือกลุ่มคน จึงมีตัวถังขนาดใหญ่</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" alt="キッチン部">
                            <p class="text">ห้องครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" alt="収納もたっぷり">
                            <p class="text">ที่เก็บของมากมาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" alt="夜間はブラインドで光を完全遮光">
                            <p class="text">มีม่านกั้นแสง</p>
                    </a></li>



                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" alt="後方２段ベッドにはそれぞれ小窓があります">
                            <p class="text">ที่เตียง2ชั้นมีหน้าต่างเล็กๆ</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" alt="後方２段ベット照明２段階明るさ調整有り">
                            <p class="text">เตียง2ชั้นมีทั้งหน้าต่างและหลอดไฟ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" alt="車輛側面">
                            <p class="text">ด้านข้างรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" alt="車両右前方">
                            <p class="text">ภาพด้านหน้าทางขวาของรถ</p>
                    </a></li>




                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" alt="車両右側面">
                            <p class="text">ภาพรถด้านขวา</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" alt="車両左後方">
                            <p class="text">ภาพด้รถด้านซ้ายหลัง</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" alt="車両左側面">
                            <p class="text">ภาพรถด้านซ้าย</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" alt="車両前方">
                            <p class="text">ภาพรถด้านหน้า</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" alt="車輛左前方">
                            <p class="text">ภาพด้านหน้าทางซ้าย</p>
                    </a></li>

            </ul>
            <p class="kome2">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>
    <?php if($_GET['type'] == '02'):?>
    <section class="car leaves" id="c02">
        <h2 class="headline01 typesquare_tags">Corde Leaves</h2>

        <h3>พักผ่อนอย่างสงบแบบคู่รัก</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>ทางเรามีบริการรถยนต์ที่อนุญาตให้นำสัตว์เลี้ยงขึ้นได้</span>
        </div>
        <!--        <p class="kome1">เช่าตั้งแต่6วันขึ้นไป บริการรับส่งฟรีจากสนามบินชินจิโตะเสะ สถานีรถไฟซัปโปโรและโรงแรมบริเวณใจกลางเมือง</p>-->
        <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">ชื่อรถ</th>
                <td width="174">Vantech Corde Leaves</td>
                <td width="216">ใช้ง่าย ผลิตโดยบริษัทยอดนิยมภายในประเทศ</td>
            </tr>
            <tr>
                <th colspan="2">บริษัทผู้ผลิต</th>
                <td>TOYOTA/CAMROAD</td>
                <td>ใช้โครงรถแคมปิ้งโดยเฉพาะ</td>
            </tr>
            <tr>
                <th colspan="2">ปีที่ผลิต</th>
                <td>2016 JUN</td>
                <td>รถใหม่เคลือบเงา</td>
            </tr>
            <tr>
                <th colspan="2">เครื่องยนต์</th>
                <td>Turbo-diesel</td>
                <td>ขับเคลื่อนทรงพลังแม้ทางขรุขระ</td>
            </tr>
            <tr>
                <th colspan="2">ขนาดเครื่องยนต์</th>
                <td>3000cc</td>
                <td>เครื่องยนต์เงียบ</td>
            </tr>
            <tr>
                <th colspan="2">ระบบขับเคลื่อน</th>
                <td>Fulltime 4WD</td>
                <td>ปลอดภัยแม้บนถนนในฤดูหนาว</td>
            </tr>
            <tr>
                <th colspan="2">เชื้อเพลิง</th>
                <td>Diesel</td>
                <td>ไม่ใช้แก๊สโซลีน กรุณาระวัง</td>
            </tr>
            <tr>
                <th colspan="2">ระยะทาง</th>
                <td>8-12km/ลิตร</td>
                <td>ประหยัดเพราะใช้น้ำมันดีเซล</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">จำนวนคน</th>
                <th width="27">ที่นั่ง</th>
                <td>7ที่นั่ง</td>
                <td>ภายในกว้างขวางนั่งสบาย</td>
            </tr>
            <tr>
                <th>ที่นอน</th>
                <td>5ที่นอน</td>
                <td>นอนได้ถึง5คน</td>
            </tr>
            <tr>
                <th rowspan="3">ขนาด</th>
                <th>ความยาว</th>
                <td>4.99เมตร</td>
                <td>สั้นกว่า toyota hiace</td>
            </tr>
            <tr>
                <th>ความกว้าง</th>
                <td>1.98เมตร</td>
                <td>ใช้พื้นที่จอดรถเท่ารถปกติ</td>
            </tr>
            <tr>
                <th>ความสูง</th>
                <td>3.15เมตร</td>
                <td>กรุณาระวังเรื่องความสูงของรถ</td>
            </tr>
            <tr>
                <th colspan="2">รัศมีวงเลี้ยว</th>
                <td>4.9เมตร</td>
                <td>เหมือนรถยนต์ใช้งานทั่วไป</td>
            </tr>
            <tr>
                <th colspan="2">สูบบุหรี่</th>
                <td>ห้าม</td>
                <td>ไม่สามารถสูบบุหรี่ในรถได้ </td>
            </tr>
            <tr>
                <th colspan="2">สัตว์เลี้ยง</th>
                <td>ไม่ได้</td>
                <td>ไม่สามารถนำสัตว์เข้าไปในรถได้</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>สิ่งอำนวยความสะดวก</caption>
            <tr>
                <th>เครื่องปรับอากาศภายในรถ</th>
                <td>〇</td>
                <th>เตาแก๊สภายในรถยนต์</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>เครื่องทำความร้อน</th>
                <td>〇</td>
                <th>เตาแก๊สปิกนิก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แผงปรับอุณหภูมิ</th>
                <td>×</td>
                <th>ห้องแต่งตัวและสุขาเคลื่อนที่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องปรับอากาศ (ใช้งานด้วยแบตเตอรี่นอกห้อง)</th>
                <td>○</td>
                <th>โต๊ะใหญ่และมีที่วางเครื่องดื่ม</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ตู้เย็นขนาดใหญ่</th>
                <td>〇</td>
                <th>ชักโครกพกพา (อุปกรณ์เสริม)</th>
                <td>△</td>
            </tr>
            <tr>
                <th>เนวิเกเตอร์ รองรับ4ภาษา</th>
                <td>〇</td>
                <th>ฝักบัว</th>
                <td>×</td>
            </tr>
            <tr>
                <th>ระบบวิทยุ สเตอดิโอ ที่รองรับบลูทูธ</th>
                <td>〇</td>
                <th>กันสาดด้านข้าง(ไม่อนุญาติให้ใช้)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>เข็มขัดนิรภัยสามจุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเสียงคุณภาพสูง ลำโพง4ตัว</th>
                <td>〇</td>
                <th>หลอดประหยัดไฟ LED</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเล่นแผ่น DVD (อุปกรณ์เสริม)</th>
                <td>△</td>
                <th>ม่านบังแดดที่หน้าต่างทุกบาน</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>กล้องมองหลัง</th>
                <td>〇</td>
                <th>ประตูตาข่าย</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องอานการ์ด</th>
                <td>〇</td>
                <th>พัดลมระบายอากาศขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟ100โวลต์ (รวมถึงการใช้งานนอกรถ)</th>
                <td>〇</td>
                <th>ถังเก็บขยะด้านนอกรถ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟภายในรถ12โวลต์</th>
                <td>〇</td>
                <th>ช่องชาร์ตแบต USB</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>อ่างล้างจานที่มีน้ำปะปา</th>
                <td>〇</td>
                <th>มีแท็บเลตคู่มือการใช้รถ</th>
                <td>〇</td>
            </tr>

            <tr>
                <th>最新型薄型テレビ</th>
                <td>×</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>



        <p class="kome1 mb">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>
        <div class="camera" id="camera-leaves">

            <h3>ภาพถ่ายด้วยกล้อง360องศา</h3>
            <ul class="cf">
                <li>
                    <a title='コルドリーブス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468315363'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_01.jpg"></p>
                        <h4>CordeLeavesถ่ายด้วยกล้อง360องศาด้านหลัง</h4>
                    </a></li>

                <li>
                    <a title='コルドリーブス　車両後方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468314209'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_02.jpg"></p>
                        <h4>CordeLeavesถ่ายด้วยกล้อง360องศาบริเวณที่นอน</h4>
                    </a></li>

                <li>
                    <a title='コルドリーブス　就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468320525'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg"></p>
                        <h4>CordeLeaves ถ่ายด้วยกล้อง360องศาเตียงนอนด้านหน้า</h4>
                    </a></li>
            </ul>

        </div>

        <div class="gallery">
            <ul class="cf">

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" alt="ゆったりと広い運転席">
                            <p class="text">พื้นที่ส่วนขับขี่กว้างขวาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" alt="広々とした室内。運転席上部のベットは簡単に跳ね上げて収納できます">
                            <p class="text">ภายในกว้างขวาง เตียงนอนด้านบนของส่วนขับขี่สามารถพับเก็บได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" alt="運転席上部のベットを下した状態。大人２名が就寝できます">
                            <p class="text">ที่นอนด้านบนของส่วนขับขี่สามารถนอนได้2คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" alt="運転席上部ベットスペースにも小窓があります">
                            <p class="text">ที่นอนด้านบนส่วนคนขับก็มีหน้าต่างขนาดเล็ก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" alt="安全な３点式シートベルト装備">
                            <p class="text">ปลอดภัยด้วยเข็มขัดนิรภัย3จุด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" alt="光あふれるリビングルーム">
                            <p class="text">ห้องนั่งเล่นที่แสงแดดส่องทั่วถึง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" alt="広い窓とテーブル">
                            <p class="text">หน้าต่างบานใหญ่และโต๊ะ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" alt="車内はＬＥＤ照明が多数">
                            <p class="text">ภายในรถติดตั้งหลอดไฟＬＥＤหลายดวง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" alt="ルームエアコン装備（外部電源をつないだ時のみ使用可能）">
                            <p class="text">เครื่องปรับอากาศ สามารถต่อกับไฟด้านนอกเพื่อใช้งานได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" alt="車内４スピーカーシステム搭載">
                            <p class="text">ภายในรถมีลำโพง4ตัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" alt="シンク・コンロ・キッチンまわり">
                            <p class="text">บริเวณอ่างล้างจาน ครัว และเตา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" alt="キッチン下部にも食器セット等を収納できます">
                            <p class="text">สามารถเก็บชุดเครื่องครัวไว้ใต้ครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" alt="車内ガスコンロ（主にお湯を沸かす程度にご利用くださいませ）">
                            <p class="text">(ใช้สำหรับต้มน้ำร้อนหรืออาหารงายๆ)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" alt="車内キッチン用カセットガスコンロ（市販のカセットコンロをご使用できます）">
                            <p class="text">เตาแก๊สในครัวบนรถ(สามารถนำออกไปใช้ด้านนอกแบบเตาแก๊สพกพาได้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" alt="シンク２０リットルの上水道（冬季は利用不可）">
                            <p class="text">อ่างล้างจานขนาด20ลิตร (ไม่สามารถใช้ได้ในฤดูหนาว)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" alt="カセットコンロガス漏れ警報機設置で万が一も安心">
                            <p class="text">มีการติดตั้ฃสัญญาณจับแก๊สรั่ว จึงมั่นใจในความปลอดภัย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" alt="テーブルを囲みお食事や読書に最適です。">
                            <p class="text">โต๊ะสำหรับอ่านหนังสือหรือล้อมวงกันรับประทานอาหาร</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" alt="ポータブルテレビ等の台に">
                            <p class="text">แท่นวางโทรทัศน์แบบพกพา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" alt="大容量９０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください">
                            <p class="text">ตู้เย็นขนาด90ลิตรสำหรับแช่เย็นและแช่แข็ง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" alt="各部に収納棚設置">
                            <p class="text">ตู้เก็บของบริเวณต่างๆ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" alt="ユーティリティルーム。トイレや着替えに防水なので濡れたものもＯＫ">
                            <p class="text">ห้องว่าง ใช้เป็นห้องน้ำห้องแต่งตัวหรือห้องเก็บของเปียกก็ได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">เนวิเกเตอร์รองรับภาษาตางประเทศ คนต่างชาติก็ใช้ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" alt="車輛後方視界カメラ。常時撮影で運転も安心">
                            <p class="text">มีกล้องหลังแบบเก็บภาพมุมกว้าง จึงขับขี่ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" alt="すべての窓には網戸も装備">
                            <p class="text">หน้าต่างทุกบานติดตั้งมุ้งลวด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" alt="夜間はブラインドで光を完全遮光">
                            <p class="text">มีม่านกั้นแสง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" alt="外部からの新鮮な空気の換気吸気装置。雨の日でも使用できます">
                            <p class="text">พัดลมระบายอากาศติดตั้งจากด้านนอก สามารถใช้ได้แม้วันที่ฝนตก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" alt="運転席ドリンクホルダー">
                            <p class="text">ที่วางเครื่องดื่มบริเวณคนขับ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" alt="ＥＴＣ設置。カードはご持参ください">
                            <p class="text">มีเครื่องอ่านการ์ด กรุณาใส่การ์ดลงไป</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" alt="リビングルーム下部に集中電源装置の扉があります">
                            <p class="text">ด้านล่างของห้องนั่งเล่นมีปลั๊กไฟ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" alt="後方入口は防水加工なので汚れても安心">
                            <p class="text">ทางเข้าด้านหลังเป็นพื้นแบบกันน้ำ สะอาดมั่นใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" alt="後方入口横にはちょっとした買い物などを入れるスペースがあります">
                            <p class="text">ด้านข้างทางเข้าทางด้านหลังมีช่องเก็บของขนาดเล็ก สามารถใช้เก็บของที่ซื้อมาเล็กๆน้อยๆได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" alt="後方入口・足元灯">
                            <p class="text">ทางเข้าด้านหลังและพรมเช็ดเท้า</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" alt="車両後方入口横には電源ケーブルが収納されてます">
                            <p class="text">สายไฟถูกจัดเก็บไว้ที่บริเวณทางเข้าด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" alt="乗り降りは車輛後方から">
                            <p class="text">ทางขึ้นลงรถอยู่ด้านหลังรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" alt="軽油使用ＦＦベバストヒーター装備">
                            <p class="text">เครื่องทำความร้อนแบบใช้เชื้อเพลิง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" alt="100Ｖコンセント（外部電源使用時のみ使用可能）12Ｖは常時使用可能">
                            <p class="text">ปลั๊กไฟ100โวลต์และ12โวลต์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" alt="左からメインスイッチ・照明集中スイッチ・水道電源">
                            <p class="text">จากด้านซ้าย สวิทซ์หลัก สวิทซ์ไฟ สวิทซ์ควบคุมน้ำปะปา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" alt="網戸も装備。暑い夏場も快適">
                            <p class="text">ติดตั้งมุ้งลวดบริเวณประตู ฤดูร้อนก็สบาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" alt="車両左にイスやロールテーブル等の収納（鍵で開けます）">
                            <p class="text">ด้านซ้ายของตัวรถเป็นที่เก็บเก้าอี้และโต๊ะพับได้ (เปิดด้วยกุญแจ)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" alt="上水道給水タンクは車輛後方に設置">
                            <p class="text">ถังบรรจุน้ำปะปาติดตั้งไว้ที่ท้ายรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" alt="燃料タンク（軽油）は車輛右前方の扉を開けてエンジンキーでロックを解除しフタを回して給油してください。">
                            <p class="text">เพื่อเปิดถังเชื้อเพลิงที่อยู่บริเวณหน้ารถด้านขวา กรุณาปลดล็อคเพื่อเปิดฝา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" alt="車両右の扉内には工具類、ジャッキアップが収納されてます。ＢＢＱセット等を収納できるスペースとしてもご利用できます">
                            <p class="text">อุปกรณ์ดูแลรถรวมถึงแม่แรงยกรถจะถูกเก็บไว้ที่เก็บของด้านขวาของตัวรถ และยังมีพื้นที่เหลือสำหรับจัดเก็บอุปกรณ์ทำบาร์บีคิว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" alt="車両右後方には生ゴミなどを一時的に収納できます">
                            <p class="text">ท้ายรถด้านขวามีที่จัดเก็บขยะสดหรือขยะเปียก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" alt="車両正面">
                            <p class="text">ด้านหน้าตรงของรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" alt="車両上部">
                            <p class="text">ภาพรถจากมุมบน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" alt="車両斜め前方">
                            <p class="text">ภาพรถมุมเอียงจากด้านหน้า</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" alt="車両斜め後方">
                            <p class="text">ภาพรถมุมเอียงจากด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" alt="車両後ろから">
                            <p class="text">ภาพรถจากด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" alt="車両左側面">
                            <p class="text">ภาพรถด้านซ้าย</p>
                    </a></li>

            </ul>
            <p class="kome2">แสงและสีภายในรถแตกต่างกันไปในแต่ละรุ่น</p>
        </div><!-- gallery -->
    </section>
    <?php endif; ?>



    <?php if($_GET['type'] == '04'):?>
    <section class="car zil" id="c04">
        <!--    <h2><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_h.jpg" alt="コルドランディ"/></h2> -->
        <h2 class="headline01 typesquare_tags">Corde Rundy</h2>


        <h3>รถแคมปิ้งสำหรับผู้ที่มีความคิดอยากออกไปเที่ยวกับสัตว์เลี้ยง</h3>
        <div class="main_photo cf">
            <div class="coming-soon">
                <div class="inner">Coming Soon</div>
            </div>
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_lundy_main.jpg" alt="コルドランディ" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>สามารถนำสัตว์เลี้ยงขึ้นรถได้</span>
        </div>
        <!--        <p class="kome1">เช่าตั้งแต่6วันขึ้นไป บริการรับส่งฟรีจากสนามบินชินจิโตะเสะ สถานีรถไฟซัปโปโรและโรงแรมบริเวณใจกลางเมือง</p>-->
        <p class="kome1">【冬期間】スキー・スノーボードでご利用のお客様は、すべての荷物を車内に入れますので3～4名が限界です。</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">ชื่อรถ</th>
                <td width="174">Vantech Corde Rundy</td>
                <td width="216">ใช้ง่าย ผลิตโดยบริษัทยอดนิยมภายในประเทศ</td>
            </tr>
            <tr>
                <th colspan="2">บริษัทผู้ผลิต</th>
                <td>TOYOTA/CAMROAD</td>
                <td>ใช้เครื่องยนต์เฉพาะของรถแคมปิ้ง</td>
            </tr>
            <tr>
                <th colspan="2">ปีที่ผลิต</th>
                <td>2011 AUG</td>
                <td>เป็นรถที่นำสัตว์เลี้ยงขึ้นรถได้</td>
            </tr>
            <tr>
                <th colspan="2">เครื่องยนต์</th>
                <td>Turbo-diesel</td>
                <td>ขับเคลื่อนทรงพลังแม้ทางขรุขระ</td>
            </tr>
            <tr>
                <th colspan="2">ขนาดเครื่องยนต์</th>
                <td>3000cc</td>
                <td>เครื่องยนต์เงียบ</td>
            </tr>
            <tr>
                <th colspan="2">ระบบขับเคลื่อน</th>
                <td>Fulltime 4WD</td>
                <td>ปลอดภัยแม้บนถนนในฤดูหนาว</td>
            </tr>
            <tr>
                <th colspan="2">เชื้อเพลิง</th>
                <td>Diesel</td>
                <td>ไม่ใช้แก๊สโซลีน กรุณาระวัง</td>
            </tr>
            <tr>
                <th colspan="2">ระยะทาง</th>
                <td>8-12km/ลิตร</td>
                <td>ประหยัดเพราะใช้น้ำมันดีเซล</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">จำนวนคน</th>
                <th width="27">ที่นั่ง</th>
                <td>6ที่นั่ง</td>
                <td>ภายในกว้างขวางนั่งสบาย</td>
            </tr>
            <tr>
                <th>ที่นอน</th>
                <td>4ที่นอน</td>
                <td>นอนได้ถึง4คน</td>
            </tr>
            <tr>
                <th rowspan="3">ขนาด</th>
                <th>ความยาว</th>
                <td>4.99เมตร</td>
                <td>สั้นกว่า toyota hiace</td>
            </tr>
            <tr>
                <th>ความกว้าง</th>
                <td>1.98เมตร</td>
                <td>ใช้พื้นที่จอดรถเท่ารถปกติ</td>
            </tr>
            <tr>
                <th>ความสูง</th>
                <td>2.96เมตร</td>
                <td>กรุณาระวังเรื่องความสูงของรถ</td>
            </tr>
            <tr>
                <th colspan="2">รัศมีวงเลี้ยว</th>
                <td>4.9เมตร</td>
                <td>เหมือนรถยนต์ใช้งานทั่วไป</td>
            </tr>
            <tr>
                <th colspan="2">สูบบุหรี่</th>
                <td>ห้าม</td>
                <td>ไม่สามารถสูบบุหรี่ในรถได้ </td>
            </tr>
            <tr>
                <th colspan="2">สัตว์เลี้ยง</th>
                <td>ได้</td>
                <td>มีการเก็บค่าทำความสะอาดเพิ่มเติม</td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="checktable mb">
            <caption>สิ่งอำนวยความสะดวก</caption>
            <tr>
                <th>เครื่องปรับอากาศภายในรถ</th>
                <td>〇</td>
                <th>เตาแก๊สภายในรถยนต์</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>เครื่องทำความร้อน</th>
                <td>〇</td>
                <th>เตาแก๊สปิกนิก</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แผงปรับอุณหภูมิ</th>
                <td>×</td>
                <th>ห้องแต่งตัวและสุขาเคลื่อนที่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องปรับอากาศ (ขอแนะนำให้ใช้เมื่อต่อกับไฟฟ้าด้านนอก)</th>
                <td>〇</td>
                <th>โต๊ะใหญ่และมีที่วางเครื่องดื่ม</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ตู้เย็นขนาดใหญ่</th>
                <td>〇</td>
                <th>ชักโครกพกพา (อุปกรณ์เสริม)</th>
                <td>△</td>
            </tr>
            <tr>
                <th>เนวิเกเตอร์ รองรับ4ภาษา</th>
                <td>〇</td>
                <th>ฝักบัว</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ระบบวิทยุ สเตอดิโอ ที่รองรับบลูทูธ</th>
                <td>×</td>
                <th>กันสาดด้านข้าง(ไม่อนุญาติให้ใช้)</th>
                <td>×</td>
            </tr>
            <tr>
                <th>เครื่องเสียงคุณภาพสูง ลำโพง4ตัว</th>
                <td>〇</td>
                <th>เข็มขัดนิรภัยสามจุด</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องเล่นแผ่น DVD (อุปกรณ์เสริม)</th>
                <td>△</td>
                <th>หลอดประหยัดไฟ LED</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>กล้องมองหลัง</th>
                <td>〇</td>
                <th>ม่านบังแดดที่หน้าต่างทุกบาน</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>เครื่องอานการ์ด</th>
                <td>〇</td>
                <th>ประตูตาข่าย</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟ100โวลต์ (รวมถึงการใช้งานนอกรถ)</th>
                <td>〇</td>
                <th>พัดลมระบายอากาศขนาดใหญ่</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>แหล่งจ่ายไฟภายในรถ12โวลต์</th>
                <td>〇</td>
                <th>ถังเก็บขยะด้านนอกรถ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>อ่างล้างจานที่มีน้ำปะปา</th>
                <td>〇</td>
                <th>ช่องชาร์ตแบต USB</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ไมโครเวฟ</th>
                <td>〇</td>
                <th>มีแท็บเลตคู่มือการใช้รถ</th>
                <td>〇</td>
            </tr>
            <!--
            <tr>
                <th>ไวไฟฟรี</th>
                <td>〇</td>
            </tr>-->

        </table>

        <div class="camera" id="camera-rundy">
            <h3>ภาพถ่ายด้วยกล้อง360องศา</h3>
            <ul class="cf">
                <li>
                    <p class="iframe"><a title='コルドランディ　昼間室内全体３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500901394'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_01.jpg"></a></p>
                    <h4>CordeRundyถ่ายด้วยกล้อง360องศาเตอนกลางวัน</h4>
                </li>

                <li>
                    <p class="iframe"><a title='コルドランディ　夜間室内全体３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500967782'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_02.jpg"></a></p>
                    <h4>CordeRundyถ่ายด้วยกล้อง360องศาเตอนกลางคืน</h4>
                </li>

                <li>
                    <p class="iframe"><a title='コルドランディ　夜間室内前方３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500908456'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_03.jpg"></a></p>
                    <h4>Front CordeRundyถ่ายด้วยกล้อง360องศาเตอนกลางคืนจากด้านหน้ารถ</h4>
                </li>
            </ul>

        </div>

        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" alt="運転席も広々">
                            <p class="text">พื้นที่ส่วนขับขี่กว้างขวาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" alt="運転席上部のベッド（マットがつきます）">
                            <p class="text">ที่นอนด้านบนส่วนขับขี่(มีฟูกให้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" alt="運転席上部のベッドルーム左右に2名">
                            <p class="text">ที่นอนด้านบนส่วนขับขี่ ทั้งซ้ายและขวานอนได้2คน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" alt="ＦＦヒータースイッチエンジン掛けなくてもOKです">
                            <p class="text">สวิทซ์เครื่องทำความร้อน ไม่ต้องใส่เชื้อเพลิงก็ได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" alt="液晶テレビ付き">
                            <p class="text">โทรทัศน์แอลซีดี</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" alt="100ボルト電源（１５００Wのインバータ付なのでプラグインがなくても使用できます）">
                            <p class="text">ปลั๊กไฟ100โวลต์ (ด้วยระบบอินเวอร์เตอร์1500วัตต์ ไม่ต้องมีปลั๊กอินก็ใช้งานได้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" alt="汚れが付きにくく清掃がラクなレザーシート">
                            <p class="text">ที่นั่งเบาะหนังทำความสะอาดง่าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08.jpg" alt="後部はフルフラットベットになります">
                            <p class="text">ด้านหลังรถปูเป็นที่นอนเต็มพื้นที่</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" alt="エアコン付き">
                            <p class="text">เครื่องปรับอากาศ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" alt="使い易いシャワー">
                            <p class="text">ฝักบัวพร้อมใช้งาน</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" alt="外部からの吸気換気システム完備雨の日でも使用できます">
                            <p class="text">พัดลมระบายอากาศติดตั้งจากด้านนอก สามารถใช้ได้แม้วันที่ฝนตก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" alt="後方にもたくさんの収納">
                            <p class="text">มีที่เก็บของมากมาย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13.jpg" alt="室内から運転席" /> class="text">ส่วนขับขี่ถ่ายจากด้านในรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" alt="車内キッチンガスコンロ（市販のカセットコンロを使用できます）">
                            <p class="text">เตาแก๊สในครัวบนรถ(สามารถนำออกไปใช้ด้านนอกแบบเตาแก๊สพกพาได้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" alt="広いキッチンガスコンロ付き">
                            <p class="text">ห้องครัวกว้างขวางและเตตาแก๊ส</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心">
                            <p class="text">มีกล้องหลังแบบเก็บภาพมุมกว้าง จึงขับขี่ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" alt="車輌後方から運転席">
                            <p class="text">ส่วนขับขี่ถ่ายจากด้านหลังรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" alt="大人5名くらいがゆったり座れます">
                            <p class="text">ผู้ใหญ่นั่งได้5คนสบายๆ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" alt="電子レンジ付き（外部電源利用時のみ）">
                            <p class="text">มีไมโครเวฟ (ใช้ไฟฟ้าด้านนอกได้)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">เนวิเกเตอร์รองรับภาษาตางประเทศ คนต่างชาติก็ใช้ได้อย่างสบายใจ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21.jpg" alt="収納もたくさん">
                            <p class="text">มีที่เก็บของเยอะแยะ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" alt="入り口にステップ有り">
                            <p class="text">มีบันไดขึ้นรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" alt="入り口横にスイッチ類が集中">
                            <p class="text">บริเวณทางเข้ามีแผงรวมสวิทซ์</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" alt="冷蔵庫">
                            <p class="text">ตู้เย็น</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25.jpg" alt="使い安いシャワー">
                            <p class="text">ฝักบัวใช้ง่าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26.jpg" alt="入り口からまっすぐシャワールームへ行けます">
                            <p class="text">จากทางเข้าสามารถตรงเข้าห้องอาบน้ำได้เลย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27.jpg" alt="入り口より後方キッチン">
                            <p class="text">จากทางเข้า ด้านหลังคือห้องครัว</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28.jpg" alt="入り口右に収納">
                            <p class="text">ที่เก็บของด้านขวามือของทางเข้า</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" alt="バックカメラ付きで安心">
                            <p class="text">ติดกล้องหลัง ปลอดภัย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" alt="入り口のステップも広々">
                            <p class="text">พื้นที่พักเท้าบริเวณทางเข้าก็กว้างขวาง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" alt="車輌右後方の収納">
                            <p class="text">ที่เก็บของท้ายรถด้านขวา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" alt="外からシャワー等の給水ができます">
                            <p class="text">สามารถใช้ฝักบัวหรือต่อสายยางจากด้านนอก</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33.jpg" alt="着替えシャワールームわんちゃんお汚れた足もきれいに">
                            <p class="text">เปลี่ยนเสื้อผ้าในห้องอาบน้ำหรือล้างเท้าสุนัขได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34.jpg" alt="入り口も防水床">
                            <p class="text">ทางเข้าเป็นพื้นแบบกันน้ำ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" alt="車輌右後方収納キャンプ道具もOK">
                            <p class="text">ที่เก็บของท้ายรถด้านขวาใช้เก็บอุปกรณ์แคมปิ้งได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36.jpg" alt="車輌右外からシャワー室へ入れます">
                            <p class="text">เข้าห้องอาบน้ำจากด้านนอกได้ทางขวาของตัวรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" alt="左後方臭いの付いたゴミなどの収納に">
                            <p class="text">ด้านซ้ายท้ายรถมีที่เก็บขยะสด</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" alt="外部から上水道タンク給水可能">
                            <p class="text">ใช้ถังเก็บน้ำจากด้านนอกได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" alt="右側の荷物収納">
                            <p class="text">ที่เก็บสัมภาระทางด้านขวา</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" alt="ペット対応に設計されたコルドランディ">
                            <p class="text">Corde Rundy รถเพื่อรองรับสัตว์เลี้ยง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41.jpg" alt="わんちゃんも屋外が見れます">
                            <p class="text">สุนัขสามารถมองเห็นวิวด้านนอกได้</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42.jpg" alt="車輌後方">
                            <p class="text">ท้ายรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" alt="ドア開放側面">
                            <p class="text">ภาพรถตอนเปิดประตู</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" alt="ドア開放">
                            <p class="text">เปิดประตูรถ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" alt="車輌後方側面">
                            <p class="text">ภาพรถจากด้านหลัง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" alt="車輌左前方">
                            <p class="text">ภาพรถด้านหน้าซ้าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" alt="車輌左側面">
                            <p class="text">ภาพรถด้านซ้าย</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" alt="車輌正面">
                            <p class="text">ภาพรถหน้าตรง</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" alt="車輌斜め前方">
                            <p class="text">ภาพรถมุมเอียงจากด้านหน้า</p>
                    </a></li>




            </ul>
        </div><!-- gallery -->
    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '01' || $_GET['type'] == '02' || $_GET['type'] == '03' || $_GET['type'] == '04'):?>
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">ตรวจสอบรถว่าง　＞</a></p>
    <?php endif; ?>

</div>
