<script>
    $(function() {
        $(document).on('click', '.open-options1', function(event) {
            event.preventDefault();
            $('.modal-options1').iziModal('open');
        });
        $('.modal-options1').iziModal({
            headerColor: '#0a8bcf', //ヘッダー部分の色
            width: 600, //横幅
            overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
            fullscreen: true, //全画面表示
            transitionIn: 'fadeInUp', //表示される時のアニメーション
            transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
        });
    });

</script>

<section class="reserve" id="c01">
    <h2 class="headline01">How to make a reservation</h2>
    <h3 class="headline01 typesquare_tags"><span class="blue">You can rent a caravan with four simple steps</h3>

    <p class="step"><img src="<?php bloginfo('template_url'); ?>/images/guide_reserve_step_<?php echo lang(); ?>.jpg?v=201706" alt="STEP" /></p>
    <dl class="cf">
        <dt><span class="left">STEP01</span><span class="right">Choose a car and make a reservation</span></dt>
        <dd>
            <p>Please check <a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">booking schedule and fill </a> in the required reservation form and send. At this moment the reservation is provisional.<br />
                We check the information and send payment details by E-mail within 2 business days.<br />
                You can wait for someone to cancel their reservation because a provisional reservation may be cancelled in 1 week.<br>
                <br>
                ＊If our E-mail doesn't reach you, it might be in the spam box of your e-mail software. In that case please contact info@nomad-r.jp</p>
        </dd>
    </dl>
    <dl class="cf">
        <dt><span class="left">STEP02</span><span class="right">Pay within one week to complete your official reservation.</span></dt>
        <dd>
            <p>Please check the total amount and make your payment by bank transfer or credit card within one week. The moment we verify the payment, your reservation is officially completed. <span class="red">If the payment is not made within one week, the reservation is automatically cancelled.</span><br>
                If you pay by credit card, we will notify you by E-mail<br>
                <p class="border">Payment account<br />
                    North　Pacific Bank<br>
                    Savings account 5326111<br>
                    Account holder ﾎｯｶｲﾄﾞｳﾉﾏﾄﾞﾚﾝﾀｶｰ　( Hokkaido Nomad Car Rental) </p>
                <p>*Please check the <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04">cancellation policy</a></p>

    </dl>
    <dl class="cf">
        <dt><span class="left">STEP03</span><span class="right">Departure</span></dt>
        <dd>
            <p>Meet up at the agreed upon time and location. We will explain how to use the car and let you check and sign the contract. It takes about 40-50 minutes.</p>
            <p>After checking everything, you can leave for a great trip and safely enjoy wonderful Hokkaido. If you have any concerns or would like to hear any of our recommended locations, feel free to contact us. We always make our best effort to make your trip </p>
            <ul>
                <li>*If you might be late, please call our cellphone.</li>
                <li>*If you are over 30 minutes late for the appointment without calling, we will cancel your reservation and charge the cancellation fee as specified. We make an exception in case of delayed flights.</li>
            </ul>
        </dd>
    </dl>
    <dl class="cf">
        <dt><span class="left">STEP04</span><span class="right">Gas up</span></dt>
        <dd>
            <p>Gas up the vehicle within 3km from where the car is suppose to be returned.<span class="red"> We check the receipt of the gas. Make sure you get it.</span></p>
            <ul>
                <li><span class="red">*If you fill with wrong fuel, no matter if you have insurance or anything, we charge you repairing fee and suspended compensation</span></li>
                <li>*If you would like to change the return time, please call our cellphone.</li>
                <li>*If you would like extend the time, we will charge you a fee. If it could affect our next customer, we might refuse the request.</li>
            </ul>
        </dd>
    </dl>
</section>
<section class="cancel" id="c04">
    <h2 class="headline01">Cancellation policy</h2>
    <p>If you have to cancel your reservation, we will charge you a cancellation fee.</p>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>You are charged \10,000 as a cancelation fee if you cancel the reservation one month before the reserved rental day.</td>
        </tr>
        <tr>
            <td>1 month ( 30 days) before ~ 1 week(7days) before from the day you rent a car Half of the rent</td>
        </tr>
        <tr>
            <td>You are charged 80% of the rent if you cancel the reservation one to six days before the day of the reserved rental day.</td>
        </tr>
        <tr>
            <td>The day you rent a car 100% of the rent</td>
        </tr>
    </table>
    <p>In the case of a work related or medical emergency <br />the cancellation fee will be waived if you reschedule within six months.<br />
        *If you change the day of the rental, the rent you already paid is not refunded.</p>

    <h3>Refunding</h3>
    <div class="indent">
        <p>* When the reservation is canceled, we refund the rest of the money to your bank account with the procedure below.</p>
        <p>＜Transfer to the account＞<br>
            We transfer the money minus the cancellation fee. (bank transfer fees apply)</p>
        <p>
            ＜If you pay by credit card＞<br>We transfer the money minus the cancellation fee and credit card fee (3.25%)<br class="pc">(bank transfer fees apply)</p>
    </div>
    <h3>If you shorten the days of the rental</h3>

    <div class="ex">

        <div class="indent">
            <p>We charge you the cancellation fee by counting days.</p>
            <p class="bold">For example, if you shorten the contract period from Aug 12-Aug 15 to Aug 12-Aug 14, it is counted as a one-day reduction.</p>
            <dl>
                <dt>7/With notification by July 12</dt>
                <dd>The rent for one day is refunded (In the case of a CORDE BUNKS or LEAVES, \40,000 refunded)</dd>
                <dt>With notification between July 13 and Aug 5</dt>
                <dd>50% of the rent for 1 day is charged as a cancellation fee (In the case of a CORDE BUNKS or LEAVES, \20,000 refunded)</dd>
                <dt>With notification between Aug 6 and Aug 11</dt>
                <dd>80% of the rent for 1 day is charged as a cancellation fee (In case CORDE BUNKS or LEAVES, \8,000 refunded)</dd>
                <dt>With notification on the departure day</dt>
                <dd>100% of the one-day rental fee is charged as a cancellation fee</dd>
            </dl>
        </div>
    </div>

    <section id="c10">
        <h3>For foreign customers</h3>
        <div class="indent">
            <p>We recommend you have CDW and RAP<br>* If you don't apply for both, you need to pay a deposit (\500,000) by credit card.</p>
        </div>
    </section>

    <h4>Deposit (Only for foreigners)</h4>
    <div class="indent">
        <p>When you depart , we ask you to pay your deposit by credit card.<br>
            If you apply for both, your deposit will be \50,000<br>
            If you only apply for CDW →\300,000<br>
            If you apply for neither →\500,000<br>
            <br>
            Any fines or repair fees will be deducted from the deposit.
            <br><br>
            If there are no accidents, fines, or damages, your deposit will be returned in full by credit card.</p>
    </div>
</section>
<section class="hoken" id="c02">
    <h2 class="headline01">Insurance / Guarantee</h2>

    <h3>About Insurance / Guarantee </h3>
    <section class="about">

        <h4>□Basic Insurance & Amount of Compensation<span class="small">（included in rental charge）</span></h4>
        <div class="indent line_b">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>&nbsp;</th>
                    <td>&nbsp;</td>
                    <td>amount of<br>compensation</td>
                    <td>
                        deductible（deductible customers have to bear in the case of accident）
                    </td>
                </tr>
                <tr>
                    <th>bodily injury</th>
                    <td>only one person</td>
                    <td><span class="blue">unlimited</span></td>
                    <td>unnecessary</td>
                </tr>
                <tr>
                    <th>property damage</th>
                    <td rowspan="2">only one accident</td>
                    <td><span class="blue">unlimited</span></td>
                    <td><span class="red bold2">customers have to bear \100,000</span></td>
                </tr>
                <tr>
                    <th>vehicle</th>
                    <td><span class="blue">current price</span></td>
                    <td><span class="red bold2">customers have to bear \100,000</span></td>
                </tr>
                <tr>
                    <th rowspan="2">personal injury</th>
                    <td colspan="3" class="tal">※maximum \50,000,000 per person</td>
                </tr>
                <tr>
                    <td colspan="3" class="tal">※In spite of the failure ration of a driver, in the case of injury （including physical impediment） and death of passengers by accident, the amount of damage will be compensated.（maximum amount is \50,000,000. Insurance company determine the amount of damage based on the insurance clause.）</td>
                </tr>
            </table>

            <p>
                <span class="red bold">＊In the case of accident and damage, customers have to bear these expenses <br>below in addition to deductible.</span></p>

            <div class="under_arrow"></div>

            <ul class="line_box">
                <li>●NOC（Non-Operation-Charge）if the van still works・・・<span class="red">\20,000</span><br>
                </li>
                <li>
                    ●NOC (Non-Operato-Charge) if the van doesn’t work・・・<span class="red">\50,000</span>
                </li>
                <li>
                    ●Compensation of broken car's operation・・・・・・・・・ <span class="red">\200,000<span class="small">（the maximum amount）
                            （\10,000 per day　the maximum is 20days long）</span></span>
                </li>
                <li>
                    ●Changing cost of burst tire・・・・・・・・・・・・・・・・ <span class="red">\30,000</span>
                </li>
                <li>
                    ●Damage of equipment, dirt and repairing cost・・・ <span class="red">Actual expenses</span>
                </li>
            </ul>
            <div class="pt">
                <h4 class="underline">Repairing cost of camper van is really expensive!</h4>
                <p>＊Those who don’t take out an insurance covers the accident of camper van.<br>
                </p>

                <h4 class="pt">＊Those who wouldn’t like to care about expensive repairing cost.</h4>
                <div class="under_arrow"></div>
                <h4 class="blue">We recommend to use additional compensation option.</h4>
                <p>＊Approximately 60% of foreign customers use this option.<br>
                </p>
            </div>

        </div>

        <section id="option">
            <h4>□Additional Compensation Option</h4>

            <p>Repairing cost of camper van is very expensive, therefore we recommend to use additional compensation option<br>
                <span class="red bold">＊In the case of accident, customers have to bear these expenses below.</span></p>

            <div class="indent line_b">
                <table cellspacing="0" cellpadding="0">
                    <tr>
                        <th rowspan="2"><span class="blue">＊Even though the<br> term of rental is <br>longer than 15 days, you<br> only need to pay for the<br> 15 day cost.<br>This system will be applied<br> to the whole trip.</span></th>
                        <th rowspan="2">①Basic Insurance</th>
                        <th rowspan="2">②only CDW<br>\3,000 per day</th>
                        <th rowspan="2">③CDW+RAP<br>\4,500 per day</th>
                    </tr>
                    <tr> </tr>
                    <tr>
                        <th>Deductible of accident<br>（property damage）</th>
                        <td><span class="red">\100,000</span></td>
                        <td><span class="blue">\0</span></td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>Deductible of accident<br>（vehicle）</th>
                        <td><span class="red">\100,000</span></td>
                        <td><span class="blue">\0</span></td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>NOC（if the van still works）</th>
                        <td><span class="red">\20,000</span></td>
                        <td><span class="red">\20,000</span> </td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>NOC（if the van<br>doesn’t work）</th>
                        <td><span class="red">\50,000</span></td>
                        <td><span class="red">\50,000</span> </td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>Compensation of broken <br>car's operation<br><span class="small">（\10,000 per day　the <br>maximum is 20days long）<br>\200,000<br>（the maximum amount）</span></th>
                        <td><span class="red">\200,000</span></td>
                        <td><span class="red">\200,000</span></td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>When tires got burst</th>
                        <td><span class="red">\30,000</span></td>
                        <td><span class="red">\30,000</span></td>
                        <td><span class="blue">\0</span></td>
                    </tr>
                    <tr>
                        <th>Damage of equipment<br>（dirt・scratches・repairing<br> cost of broken equipment）</th>
                        <td><span class="red">Actual expenses</span></td>
                        <td><span class="red">Actual expenses</span></td>
                        <td><span class="blue">\0<br>（when you use the van normally）</span></td>
                    </tr>
                    <tr>
                        <th><span class="red"><span class="fsize_l">Total amount of cost<br>customors have to bear<br>in the case of accident.</span><br>（we will take a deposit<br>when the car is returned）</span></th>
                        <td><span class="red fsize_l">\500,000</span></td>
                        <td><span class="red fsize_l">\300,000</span></td>
                        <td><span class="blue fsize_l">\0</span></td>
                    </tr>
                </table>
                <p>＊Finding any scratches on a vehicle, we will take the expenses above. We will pay you back the balance after repairing.</p>
            </div>

            <h4>□Our company will issue the billing statement and the receipt if you use your own special contract of travel compensation .</h4>
            <div class="indent line_b">
                <h5>①Examples of dents, cracks and deformations customers have to bear <span class="red">\500,000</span>（it takes about 20-90 days to repair）</h5>
                <img src="<?php bloginfo('template_url'); ?>/images/guide_tokuyaku_hoken1.jpg" />
                <h5 class="pt">②Examples of scrathes (not with deformations) customers have to bear <span class="red">\300,000</span>（it takes about 15-30 days to repair）</h5>
                <img src="<?php bloginfo('template_url'); ?>/images/guide_tokuyaku_hoken2.jpg" />
            </div>

        </section>

        <h4>Insurance coverage</h4>
        <div class="indent line_b">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>&nbsp;</th>
                    <td>&nbsp;</td>
                    <td>Amount of coverage</td>
                    <td>Amount you have to pay</td>
                </tr>
                <tr>
                    <th>Personal injury</th>
                    <td>For one person</td>
                    <td>Unlimited</td>
                    <td>None</td>
                </tr>
                <tr>
                    <th>Property damage</th>
                    <td rowspan="2">For one accident</td>
                    <td>Unlimited</td>
                    <td><span class="red bold">\100,000</span></td>
                </tr>
                <tr>
                    <th>CDW(Collision Damage Waiver)</th>
                    <td>Current price </td>
                    <td><span class="red bold">\100,000</span></td>
                </tr>
                <tr>
                    <th>Passenger injury</th>
                    <td colspan="3" class="tal">\50,000,000 maximum for each person<br />
                        *The insurance compensates the loss for the injury or death of passengers regardless of percentage of fault. (Up to \50,000,000 : The amount of damage is determined by an insurance company with its insurance rules.)</td>
                </tr>
            </table>
        </div>
    </section>
    <section>
        <h4 class="big">CDW(Collision Damage Waiver)<span class="red">*We recommend you buy this.</span></h4>
        <div class="indent">
            <p>You don't have to pay the deductible for property or car damage in case of an accident.<br />
                <span class="comm red">*You have to fasten your seat belt as a condition for this insurance.</span></p>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th scope="col">Price(for one vehicle per a day) </th>
                    <th scope="col">Amount you have to pay (Property and a car)</th>
                </tr>
                <tr>
                    <td>
                        <p class="red bold"><span style="font-size:36px;">\3,000</span><br>(without tax)</p>
                    </td>
                    <td>
                        <table border="0" class="price_info">
                            <tr>
                                <td align="center">
                                    <p>
                                        \100,000</p>
                                    <p>
                                </td>
                                <td><span class="arrow">→</span></td>
                                <td align="center">
                                    <span class="price">\0</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p class="comm">*You cannot terminate the contract<br />
                *It costs the whole price of a day if you rent just half of the day.<br>
                If you rent a car more than fifteen days and less than one month, you are only charged the price for fifteen days.</p>
        </div>
    </section>
</section>
<section class="hoken service">
    <h3>About road service</h3>
    <section>
        <h4>Contents and coverage of road service</h4>
        <div class="indent">
            <h5>①Car transporting service</h5>
            <ul class="mark">
                <li>We compensate up to \300,000 for one accident</li>
                <li>Please cover extra cost ( over \ 300,000)</li>
                <li>We choose the factory to transport.</li>
            </ul>
            <h5>②Immediate action service</h5>
            <ul class="mark">
                <li>If the car has any trouble such as battery dead or anything make the car disable to be driven, emergency measure ( only things can be fixed within 30 min) If the trouble is not be able to applied by the service or exceed of the coverage, you have cover the fee. (You have to pay the repair fee if the damage is extreme (in excess of the compensation coverage).</li>
            </ul>
            <div class="scroll">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th rowspan="9" scope="col">Immediate action detail</th>
                        <td rowspan="4" scope="col">A battery, etc.</td>
                        <td class="tal" scope="col">Jump-start</td>
                        <td scope="col">○</td>
                    </tr>
                    <tr>
                        <td class="tal">Changing bulbs, fuses</td>
                        <td>○</td>
                    </tr>
                    <tr>
                        <td class="tal">Other repairing which can be done within 30 min.</td>
                        <td>○</td>
                    </tr>
                    <tr>
                        <td class="tal">Refilling coolant.</td>
                        <td>○</td>
                    </tr>
                    <tr>
                        <td>A car key</td>
                        <td class="tal">Unlocking the door when you lock the keys in the vehicle※３</td>
                        <td>○</td>
                    </tr>
                    <tr>
                        <td rowspan="2">A tire</td>
                        <td class="tal">
                            <p>Changing flat tire into spare tire<br />
                                *If the car doesn't have a spare tire, we bring the car to the nearest gas station instead of using a flat tire repair kit.<br class="pc">
                                The transport fee is free in this case.</p>
                        </td>
                        <td>○</td>
                    </tr>
                    <tr>
                        <td class="tal">If you get stuck, we can pull the vehicle back onto the road (within one meter).</td>
                        <td>○</td>
                    </tr>
                </table>
            </div>
            <!-- scroll -->
            <ul class="mark">
                <li>The cost for tire repair or replacement will be covered by customer</li>
            </ul>
            <p>
                *3 Available if you have rental documents and are able to prove identification</p>
            <ul class="mark">
                <li>-In the case same name is on rental documents and drivers license</li>
                <li>-In the case same company name is on rental documents and company ID and drivers license</li>
            </ul>
            <ul class="comm">
                <li>*If you do not meet certain conditions, it will be borne by the customer</li>
                <li>*When you use services not listed above, you have to pay on the spot</li>
                <li>*Please contact in advance with Customer center of Mitsui Sumitomo Insurance Co.,Ltd(0120-258-365) or Okuruma QQ Tai(Road service) (0120-096-991)<br />
                    If you are from abroad, please call 0476-31-3643</li>
                <li>*English support is available 24 hours and other languages are available from 9AM to 10PM<br>
                    If you make outside arrangements without letting us know, we cannot provide guidance or make arrangements.</li>
            </ul>
            <p>◎Some part of road services above are included in the company’s insurance.</p>
        </div>
    </section>
</section>
<section class="hoken about">
    <h3>About your coverage</h3>
    <section>
        <h4>□Non Operation Charge</h4>
        <div class="indent line_b">
            <p>If a car needs to be repaired or cleaned because of accident, theft, technical trouble or damage for which the company is not to blame, you will be charged the price below as a part of "Sales compensation" (Damages of equipment inside or burn holes in car seats are also compensated as NOC)</p>
            <p>●If the car is still able to be driven and returned to the designated place<span class="pc">・・</span> <span class="red bold">\20,000</span><br />
                ●If the car is not able to be driven<span class="pc">・・・・・・・・・・・・・・・・・・・・・・</span> <span class="red bold">\50,000</span><br />
                Notice: Even if you buy CDW, you have to pay for NOC.</p>
        </div>
        <h4>□Transportation fee</h4>
        <div class="indent line_b">
            <p>We cover fees up to \150,000, fees over that price have to be covered by you.</p>
        </div>
        <h4>□Damage of equipment, stains and bad odors are not covered by insurance</h4>
        <div class="indent line_b">
            <p>All of the costs have to be covered by you <br />
                If damages are not covered by insurance, all of the cost of damaged equipment or stains and odors have to be covered by you.</p>
        </div>

        <h4>□About pet allowed vehicles</h4>
        <div class="indent line_b">
            <p>Only a dog is allowed on vehicles<br>\8,000 per a dog, \10,000 per two dogs and \12,000 per over three dogs have to be paid.<br>
                In a case the dog damages a car,accidentally takes a piss or crap or makes a car smelly, no matter if you buy CDW or RAP, we charge you the cost to make the car cleaned </p>
        </div>

        <p class="big">*Pet is allowed on the vehicle only pet allowed vehicles. Smoking is not allowed in all vehicles. </p>
        <p class="big">*About using kitchen</p>
        <p class="pl2"><span class="red bold">Our car has gas stove but use for only boiling water or something like it.</span><br />
            Grilling, stewing, hotpot or something smelly or smoky is not allowed. Instead of it, we have free potable gas stove so please cook outside of the vehicles. ( Please understand for all customers comfort)</p>

        <p class="big">*If there are worries of bed-wetting of children, we have sheets for that. Please ask us.</p>
        <p class="big">*We cannot rent a car to drivers with less than three years of driving experience</p>

        <h4>□Suspended compensation</h4>
        <div class="indent line_b">
            <p><span class="bold f15">\10,000(without tax) per day. 20 days maximum</span><br />
                In a case of damaged equipment, stains, bad smells not covered by insurance, we charge you \10,000(without tax) per day up to 20 days maximum. Repair or cleaning is done by factories we choose and terms depend on them.</p>

        </div>
        <h4 id="c2rap"><span class="small">Recommend !</span><span class="big">□RAP</span> </h4>
        <div class="indent ">
            <p>More than 90% of our customers buy RAP. Customers who buy CDW is able to buy RAP as an option.<br />
                ①Free from NOC<br />
                ②Free from suspended compensation( \10,000(without tax) *20 days maximum)<br />
                ③Free repair flat tire or alternative tires if you get flat tires. *1 *2<br />
                ④Free repair fee of damaged equipment ( Only if you use them following the rules)</p>
            <div class="scroll">
                <!--             <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th scope="col">Price \1,500(without tax) per day</th>
                    <th scope="col">If you buy RAP…</th>
                </tr>
                <tr>
                    <td><p class="bold red" ><span style="font-size:28px;">1,500 円</span> <span style="font-size:14px;">(税別)</span></p></td>
                    <td><table border="0" class="price_info">
                      <tr>
                        <td class="noc">・ノンオペレーションチャージ(NOC)<span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>20,000円～50,000円<br />
・休車補償料　最大200,000円<br />
・車内備品損傷修理代　実費<br>
・パンク・バースト時タイヤ代　実費
</td>
                        <td><span class="arrow">→</span></td>
                        <td><span class="price">0</span><span class="en">円</span></td>
                      </tr>
                    </table></td>
                </tr>
            </table>
 -->
            </div>
            <!-- scroll -->
            <ul class="comm">
                <li>*You cannot buy or terminate RAP during the rental period.</li>
                <li>*Even if you rent a car for half of a day, price is same as for a day</li>
                <li>*If the rental term is 15 days to 1 month, RAP costs you the price of 15 days and compensation is for the term you rent.</li>
                <li>*Alternative tire when you get a flat tire is the same grade as the flat one.( \20.000 maximum)</li>
                <li>*You might have to pay temporally for the repair fee or money for buying the alternative tire.<br />
                    In that case, we will pay you back but we need a receipt.</li>
            </ul>
        </div><!-- indent -->
    </section>
</section>
<section class="hoken about">
    <h3>Notices</h3>
    <section>
        <h4>□In cases RAP is not able to cover</h4>
        <div class="indent line_b">
            <h5>①If you don't complete proper procedures such as contacting the police or us.</h5>
            <h5>②You are in violation of lending rules</h5>
            <p class="pl2">◆Damages due to illegal parking ◆Drinking and driving<br />
                ◆drug use ◆ Expanding the rental term without notification ◆Driving by drivers<br />
                ◆You rent the car to others ◆Drive without a driver's license ( Suspended license or license for other class of vehicle are also same thing)<br />
                ◆If you settle out of court without contacting us ◆Using a car for any test or competition or towing or pushing other cars.<br />
                ◆If the case is regarded as the case of exception agreement of our rending rules</p>
            <h5>③If the case is regarded as the case of exception agreement of an insurance we buy</h5>
            <p class="pl2">◆Accidents on purpose ◆Losing or breaking a key ◆Breaking or damaging property you own, use or manage<br />
                ◆Natural disaster ( Earthquake tsunami or etc.)</p>
            <h5>④Free repair fee of damaged equipment ( Only if you use them following the rules)</h5>
            <p class="pl2">◆If you park the vehicle and leave the key inside and the car is stolen<br />
                ◆Repair costs due to rough use<br />
                ◆Stain or damage of equipment inside 　◆Breaking or losing equipment<br />
                ◆Any damage due to improper fitting of snow chains, carriers or child seats<br />
                ◆Car damage due to driving off road (accident off of publicly maintained roads)<br />
                ◆Repairing costs due to wrong fuel usage</p>
            <h5>⑤Other</h5>
            <ul class="comm">
                <li>*Any damage or loss beyond insurance coverage must be covered by customer.</li>
                <li>*A report to the police is needed to get compensated if a car is hit-and-run or broken into</li>
            </ul>
        </div>
        <h4>□Checking additional equipment</h4>
        <div class="indent line_b">
            <p>We are not responsible for any accident due to improper fitting of additional equipment. Please install it by yourself. Even if our staff installs it, please check if it is installed property by yourself</p>
        </div>
        <h4>□Cautions when using navigation system</h4>
        <div class="indent line_b">
            <p>The car navigation system is a tool which helps you reach your destination but faulty information or guidance might be given depending on location and driving conditions. Please follow the actual traffic rules and signs. We are not responsible for the loss of time or money due to faulty information or guidance.</p>
        </div>
        <h4>□About returning time</h4>
        <div class="indent line_b">
            <p>Please return the car during business hours. If you want to change the appointment time or are going to be late, please contact us. If you expand your rental term without a contact, any insurance or compensation cannot be applied.</p>
        </div>
        <h4>□About fuel</h4>
        <div class="indent line_b">
            <p><span class="red">All of our cars run on diesel. If you fill it up with gasoline, DO NOT START THE ENGINE! Contact us and clean the fuel tank. Any damage due to wrong fuel must be covered by customers even if you have CDW or RAP.</span><br />
                <br />
                "FULL" is when the nozzle is inserted properly and it automatically stops.<br />
                *If you cannot fill the car up because of your schedule, we charge you for the gas by calculating trip distance. It is more expensive than a gas station. The price depends on the gas station.</p>
        </div>
        <h4>□About illegal parking tickets and fines</h4>
        <div class="indent line_b">
            <p><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig1_<?php echo lang(); ?>.jpg" /></p>
            <p>*If you don't submit the parking ticket and receipt of the fine (in case we cannot confirm the payment), we charge you the penalty below.</p>
            <p>●The penalty<span class="red bold">\30,000( including tax)</span></p>
            <p>*If you submit an illegal parking ticket and receipt a day after, we refund the penalty to you. The money will be transferred to your account but the fee must be paid by you.<br />
                *If we cannot confirm the payment and you don't pay the penalty, we will not rent a car in the future.</p>
        </div>
        <h4>□About an accidents and breakdowns</h4>
        <p class="big tac ">If you have an accident or the car breaks down</p>
        <p class="tac"><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig2_<?php echo lang(); ?>.jpg" /><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig3_<?php echo lang(); ?>.jpg?v=201706" /></p>
        <div class="line_box">
            <p><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_tel1_<?php echo lang(); ?>.jpg" /><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_tel2_<?php echo lang(); ?>.jpg" /></p>
            <p>Foreign customers… call <span class="tel"><span class="sp"><br></span>0476-31-3643</span></p>
            <ul class="comm">
                <li><span class="red">*English support is available 24 hours and other languages are available from 9AM to 10PM</span></li>
                <li><span class="red">*Even if the accident is not an big issue, like you hit a car next to you when you open the door or you hit a pillar and the rear bumper gets dented, it is still necessary to follow procedures properly.</span></li>
                <li>*If an accident occurs, no matter how serious it is, a report is necessary to the police and us.</li>
                <li>*If the report or procedure is not completed, insurance or compensation cannot be applied.</li>
                <li>*Conversations might be recorded to confirm the details correctly. We handle personal information following our privacy policy.</li>
            </ul>
        </div>
    </section>
</section>
<section class="access" id="c03">
    <h2 class="headline01">Access</h2>
    <p><span class="bold">Vehicle rental base ( Kiyota base)<br />
            Business hours 9AM-6PM</span></p>
    <div id="map" class="map" style="margin-bottom:4%;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2918.822910965028!2d141.45739461562474!3d42.982003903589224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDLCsDU4JzU1LjIiTiAxNDHCsDI3JzM0LjUiRQ!5e0!3m2!1sja!2sjp!4v1512112465952" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <h5>「By car」</h5>
    <p>Search "2 Utsukushigaoka 2jo 6chome Kiyota-ku Sapporo" in the car navigation system<br />
        Hokkaido Expressway three minutes from Kitahiroshima IC (※ It is one road south of National Route 36, it will be behind Mizuka Hospital.)<br />
        Only one car can park.<br>
        *We cannot compensate if your car is burglarized, damaged, etc. while your car is parked.<br />
    </p>
    <h5>「By bus」</h5>
    <dl id="acMenu">
        <dt class="cf">Coming from Sapporo<p class="accordion_icon"><span></span><span></span></p>
        </dt>
        <dd>
            <ul>
                <li>
                    <h5>1. For customers come from Sapporo Bus Terminal of Hokkaido Chuo Bus</h5>
                    <p>Take a "千歳線急行千歳駅前行き(Chitose-sen Kyuko Chitose-eki iki) and get off at "里塚南"(Satozuka-Minami) 3min walk from the bus stop 「里塚南」で下車　バス停より徒歩３分<br>
                        Notice) We are located behind of 里塚病院(Satozuka-byoin) across the route 36. 株式会社ナッツRV(Kabushiki-gaisha nattu RV) is different company.<br>
                        <a class="link" href="#map">Our location</a><br>
                        〒004-0812　Utsukushigaoka2-6-2 Kiyota-ku Sapporo-shi</p>
                    <p>
                        <a class="link" href="http://www.chuo-bus.co.jp/city_route/" target="_blank">The map of the bus stop</a><br>
                    </p>
                </li>
                <li>
                    <h5>For customers come from Fukuzumi Station of the subway toho-line</h5>
                    <p>a.Take "千歳線急行千歳駅前行き(Chitose-sen Kyuko Chitose-eki iki) and get off at "里塚南"(Satozuka-Minami) 3min walk from the bus stop<span class="pc"><br>
                            &nbsp;&nbsp;&nbsp;</span>Notice) We are located behind of 里塚病院(Satozuka-byoin) across the route 36. 株式会社ナッツRV(Kabushiki-gaisha nattu RV) is different company.<br>
                        b.Take "福95 三井アウトレットパーク(Mitsui Outlet Park)or 大曲工業団地(Ohmagari Kogyo danchi)行き"and get off at "美しが丘3条6丁目(Utsukushigaoka3jo 6chome). 8min walk from the bus stop.<br>
                        <a class="link" href="#map">Our location</a><br>
                        〒004-0812　Utsukushigaoka2-6-2 Kiyota-ku Sapporo-shi</p>
                    <p>
                        <a class="link" href="http://www.chuo-bus.co.jp/city_route/" target="_blank">The map of the bus stop</a><br>
                    </p>
                </li>
            </ul>
        </dd>
        <dt class="cf">Coming from New Chitose Airport<p class="accordion_icon"><span></span><span></span></p>
        </dt>
        <dd>
            <ul>
                <li>
                    <p>Take a "札幌都心行き(Sapporo toshin iki)of Hokuto-kotsu or Chuo bus) and get off at "里塚南(Satozuka minami)" It takes 3min away from the bus stop.<br>
                        Notice) We are located behind of 里塚病院(Satozuka-byoin) across the route 36. 株式会社ナッツRV(Kabushiki-gaisha nattu RV) is different company.<br>
                        <a class="link" href="#map">Our location</a><br>
                        〒004-0812　Utsukushigaoka2-6-2 Kiyota-ku Sapporo-shi</p>
                    <p>
                        <a class="link" href="http://www.hokto.co.jp/a_chitose_sap_t.htm" target="_blank">Time table of airport bus .</a><br>
                        <a class="link" href="http://www.hokto.co.jp/a_airport_index.htm#map-sapporo" target="_blank">The map of the bus stop</a><br>
                        <a class="link" href="http://www.hokto.co.jp/english-timetable.html" target="_blank">English page is here.</a>
                    </p>
                </li>
            </ul>
        </dd>
    </dl>

</section>
<section class="access" id="c05">

    <h2 class="headline01">Pick up and car delivery</h2>


    <h5>《From New Chitose Airport》</h5>
    <p>
        Please take a taxi from New Chitose Airport to our Chitose office by yourself.<br>
        It’s about 10min drive from airport to our Chitose office.<br>
        And we will refund the taxi fare when you arrive at Chitose office.<br>
        (Please show us receipt of taxi when you arrive at Chitose office.)<br>
        The address of Chitose office is<br>
        “17-28, Aoba 2Chome Chitose-shi” , in English.<br>
        「千歳市青葉2丁目17－28」 is the address of our Chitose office in Japanese.<br>
        So please show this address to taxi driver.
    </p>
    <h5>《Transport fee》</h5>
    <p>
        ◆New Chitose Airport ⇔ Chitose office ： Free<br>
        ◆Sapporo Station or hotels in downtown of Sapporo ⇔ Chitose office ： 8000JPY<br>
        ◆Kiyota base ： Transport service is unavailable at Kiyota base.
    </p>

    <h5>《Delivery area》~beside area above~</h5>
    <p>
        Asahikawa airport　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\20,000(One way)<br />
        Memanbetsu airport &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\50,000(One way)<br />
        Kushiro airport &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\50,000(One way)<br />
        Hakodate airport　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\40,000(One way)<br />
        *Delivery fee is not able to be discounted with long-term rent.<br />
        *We estimate quote with season or place you want to deliver.<br />
        Please understand that we might not be able to deliver depending on conditions.</p>
</section>
<section class="pay" id="c09">
    <h2 class="headline01">About payment</h2>
    <p>Please make a payment by transfer or credit card within 1 week after your reservation.</p>
</section>
<section class="cancel" id="c06">
    <h2 class="headline01">Terminate contract</h2>
    <p>If you cancel during your contract period, we charge you half the price of the remaining term of the contract as a termination fee. </p>
</section>
<section class="cancel" id="c08">
    <h2 class="headline01">When we cannot rent a car due to an accident or technical trouble</h2>
    <p>If we cannot rent a car because the previous customer has caused an accident or technical trouble, an alternative car will be supplied.<br />
        If we don't have an alternative car because we are fully booked, we refund all of the money transferred and cancel the contract.<br />
        <br />
        Please understand that it is very difficult to find alternative vehicles because RVs are of a rare sort.<br />
        Even if we cannot rent any car, we cannot compensate you for any hotel or transportation costs, etc.<br />
        We do our best to serve our customers. However, please understand that car availability depends on the season and there are situations where we will be unable to help. </p>
</section>
<section class="faq" id="c07">
    <h2 class="headline01">Frequently asked questions</h2>
    <h4>How to rent a car</h4>
    <dl>
        <dt>Q、Do we need to make a reservation ?</dt>
        <dd>A、If we have a car available, it is possible so please make a call. But preparation might not be finished by the time you desire.</dd>
    </dl>
    <dl>
        <dt>Q、What do we need to make a reservation?</dt>
        <dd>A、Driver's license of all drivers<br>
            *We cannot rent a car to a driver with less than three years experience. (We might make an exception depending on the condition.)</dd>
    </dl>
    <dl>
        <dt>Q、Do you accept credit cards?</dt>
        <dd>A、We accept VISA, MASTER CARD, AMERICAN EXPRESS, JCB, DINERS CLUB</dd>
    </dl>
    <dl>
        <dt>Q、Can I park my car while I rent a car?</dt>
        <dd>A、We only have one parking space per car rental. We cannot guarantee against theft or damage of your vehicle. Do not leave any valuables in your car.</dd>
    </dl>
    <dl>
        <dt>Q、Do we need to fill it up when we return the car?</dt>
        <dd>A、Yes. If you cannot fill it up, we charge you \3,000 per quarter divisions of a scale.<br>
            It is more expensive than a gas station. Please fill it up before your return it.</dd>
    </dl>
    <dl>
        <dt>Q、Could you pick us up from our home, airport or hotel?</dt>
        <dd>A、We pick you up by taxi or our car at Sapporo Station, hotels in the downtown area or New Chitose Airport and drive to Kiyota rental base.<br>
            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05">more</a></dd>
    </dl>
    <section id="c11">
        <dl>
            <dt>Q、Can we rent a car with an international license?</dt>
            <dd>A、If you are a foreign customer, please submit your international license and passport.<br>
                For foreign customers, we recommend you buy CDW and RAP.<br>
                * Deposit is needed before your departure.<br>
                <h4>For foreign customer[Deposit]</h4>
                *You have to pay a deposit when you depart (by credit card)<br>
                CDW+RAP→\50,000<br>
                Only CDW　→　\300,000<br>
                No CDW or RAP →　\500,000<br><br>
                We pay money from the deposit for an accident , traffic violation or repairing cost.<br><br>
                If any accident or damage to repair, we refund the money by credit card.

            </dd>
        </dl>
    </section>
    <dl>
        <dt>Q、Can we ship our luggage before our arrival?</dt>
        <dd>A、Yes you can. We can store your luggage while you rent a car because it takes a space inside.</dd>
    </dl>
    <h4>About fee</h4>
    <dl>
        <dt>Q、How much is it?</dt>
        <dd>A、The price is different depends on season.<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c01">Please check price calendar and price list</a>.<br>
            We also offer a discount plan.<br>
            If you plan long term, please talk to us about it.</dd>
    </dl>
    <dl>
        <dt>Q、Any extra fee?</dt>
        <dd>A、If you want to start the rental period outside of regular business hours, there will be an extra cost per hour. Extension might be refused depending on booking schedule.</dd>
    </dl>
    <dl>
        <dt>Q、Any other extra fees?</dt>
        <dd>A、We have many options such as an insurance or goods. We would like to offer as many goods and services as possible. Please inform us of any requests.</dd>
    </dl>
    <dl>
        <dt>Q、If we cancel, since when it count?</dt>
        <dd>A、Since the day a reservation is completed.<br>
            *If you cancel because of inevitable situation such as your child's medical condition, we don't charge you any cancelation fee up to once only if you make a reservation again within 6 months.<br>
            *If you shorten a term of a rent, it cost <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04">cancelation fee</a> by the days.</dd>
    </dl>
    <dl>
        <dt>Q、If we terminate the contract after the rental period starts, is there a cancelation fee?</dt>
        <dd>A、If you cancel the car rental after we rent you the car because of your situation and terminate the contract, we charge you half of the price of the rest of the term of the contract as a termination fee.</dd>
    </dl>
    <h4>About insurance or compensation</h4>
    <dl>
        <dt>Q、Q. What if we cause an accident?</dt>
        <dd>A、In case of traffic accident, an insurance for person or property can be applied.<br>
            but you have to cover \100,000 for each compensation personal injury and property damage.<br>
            In addition, compensation amount exceeding compensation of insurance, non operation charge (20,000 ~ 50,000 yen), rest break compensation fee 1 day 10,000 yen (up to 200,000 yen) will be charged.<br>
            <br>
            In case of accident, cost above maybe expensive, we suggest you CDW(\3,000 per a day) to make collusion fee free.<br>
            <br>
            We also have RAP(\1,500 per a day) as an option which lets you free from NOC + suspended compensation fee + repairing cost of damaging interior as long as proper use. RV equipment is expensive so we recommend you have this one.<br>
            <br>
            *RAP cannot cover repairing cost or suspended compensation fee due to filling the tank with the wrong fuel or improper.</dd>
    </dl>
    <dl>
        <dt>Q、What if we break the equipment inside?</dt>
        <dd>A、We charge you all the cost of repairing or replacing . Most RV equipment is imported and expensive. Please handle carefully.<br>
            *If you have RAP(\1,500 per day), you are free from any charges due to equipment damage as long as it is not used improperly. *Only customers who apply for CDW can apply for RAP as an option.
        </dd>
    </dl>
    <h4>About the vehicle and driving</h4>
    <dl>
        <dt>Q、Can we drive with a normal driver's license?</dt>
        <dd>A、Yes, you can.
            <br><br>They are all AT vehicles so you can drive with an AT limited license.<br>
            ＊We cannot let those drive who have less than three years driving experience.<br>
            ＊But if your driving experience is less than three years, we may let you drive in cases such as: you drive everyday or you are a professional driver and are very confident, and apply for CDW and RAP. Please talk to us.</dd>
    </dl>
    <dl>
        <dt>Q、What are the seating and sleeping capacities?</dt>
        <dd>A、CORDE BUNKS and CORDE LEAVES seats 7 and sleeps 5. ZIL520 seats 6 and sleeps 7.
            <!-- CORDE RUNDY seats 6 and sleeps 4. -->Vantech cars are very roomy so we believe you’ll be comfortable.</dd>
    </dl>
    <dl>
        <dt>Q、Do you have child seats or booster seats?</dt>
        <dd>A、We have child seats and junior seats for <span class="bold">free of charge</span>. They have three-point seat belts.<br>
            Child seats are mandatory for children under 5 years old. Please inform us how many and how old your kids are when you make a reservation.</dd>
    </dl>
    <dl>
        <dt>Q、Can our pets ride with us?</dt>
        <dd>A、Only certain vehicles allow pets. ( Some of CORDE LEAVES , Some of CORDE BUNKS )<br />
            Please use pet cages, but do not put them in the seats or on the beds. A cleaning fee is charged separately for each pet rental. *Japanese says ゲージ　not ケージ*<br>
            If a dog damages the car due to odor, urination, or defecation, no matter if you buy CDW or RAP, we charge you the cleaning cost <br>
            As for the smell, we judge if it’s ok or not by sniffing. Please understand in advance.<br>
            Allowed pets are only dogs. Cats or any other animals are not allowed.
        </dd>
    </dl>
    <dl>
        <dt>Q、What kind of fuel do they use?</dt>
        <dd>A、All of our cars use diesel.<br>
            Never use the wrong fuel. If you fill it up with the wrong fuel, DO NOT START THE ENGINE and please call us. We will arrange for the repairs. All costs will be covered by customers.</dd>
    </dl>
    <dl>
        <dt>Q、How good is the fuel efficiency?</dt>
        <dd>A、Their engines are 3,000cc diesel turbo so fuel efficiency is high. It is like 8-12km/L. And also the price is cheaper than gasoline. It is an economical way to drive across Hokkaido.</dd>
    </dl>
    <dl>
        <dt>Q、Do they have navigation systems?</dt>
        <dd>A、All cars have concise operation navigation systems with four language support.</dd>
    </dl>
    <dl>
        <dt>Q、Do they have ETCs?</dt>
        <dd>A、Yes they do. Please use your ETC cards.</dd>
    </dl>
    <dl>
        <dt>Q、Do they have side awning?</dt>
        <dd>A、We cannot allow the use of side awning because it causes damages.</dd>
    </dl>
    <dl>
        <dt>Q、Do they have car stereo?</dt>
        <dd>A、All of the cars’ navigation systems have built-in audio. There are no CD players but CORDE LEAVES No.1 and CORDE BUNKS No.1 have Bluetooth connections. Other cars have USB or AUX connections.</dd>
    </dl>
    <dl>
        <dt>Q、Is it okay to drive on snow?</dt>
        <dd>A、All cars have AT and full time 4WD. They have studless tires from November to the end of April.<br>
            Caravans are great if you go skiing or snowboarding! Tours enjoying powder snow in Niseko, Furano or Mt.Taisetsu are amazing. FF heaters are equipped which are able to be used without the engine on. You can stay warm even in winter.<br>
            <span class="bold">*We don't recommend you drive if you are not skilled on snow. Caravans are heavy so driving on snow or icy roads requires significant skill and experience.</span></dd>
    </dl>
    <dl>
        <dt>Q、Are there any special customs with RVs?</dt>
        <dd>A、You have probably noticed that RV drivers wave when they pass each other. It is a Hokkaido custom and motorcyclists also do the same kind of thing. You should try it! It feels so good.</dd>
    </dl>
    <dl>
        <dt>Q、Can we check out the inside of the vehicles?</dt>
        <dd>A、Yes, you can as long as the car is not being rented. Please inform us before your visit because we are often out of our office.</dd>
    </dl>
    <h4>About interior</h4>
    <dl>
        <dt>Q、Do they have bathrooms?</dt>
        <dd>A、ZIL520 come equipped with bathrooms. We can install portable bathrooms into other cars with a charge. We think it might not be very comfortable for those who are not used to that kind of bathroom. Please use it for emergencies when you are far from town or stuck in a traffic jam. We recommend gas stations or convenience stores since they are more comfortable.</dd>
    </dl>

    <dl>
        <dt>Q、Does the car have a tap?</dt>
        <dd>A、Yes it does. All of our car have a tap with 20 liters of water and sink. It is okay to drink but we recommend you to drink a bottled water.<br>*Not available in winter</dd>
    </dl>


    <dl>
        <dt>Q、Is smoking okay ?</dt>
        <dd>A、Smoking is not allowed inside. Please smoke outside.<br>
            *When you return a car and the car smells of smoke or we find ashes in the car, even if it is just a little, we charge you \20,000 for cleaning. (Non-smoking staff check and decide if a car needs to be cleaned.) If the cleaning can't be finished in time for the next customer and he or she ends up cancelling, we charge you the cancellation fee.<br>
            Many people use our cars so please understand.</dd>
    </dl>
    <dl>
        <dt>Q、Can we cook inside?</dt>
        <dd>A、We are sorry but you can't. Many people use our cars so grilling, stewing, frying and any other cooking is not allowed. Gas stoves are equipped in a kitchen but they are for boiling water. We have free gas cartridge stoves and gas cartridges for outside so please cook outside. (Many other customers use our cars so please understand.)<br>*When you return your car and we can sense an unpleasant smell, we charge you \20,000 for cleaning. (Our staff will check and decide if it needs to be cleaned). If the cleaning doesn't finish before next rental and he or she ends up cancelling, we charge you The cancellation fee.</dd>
    </dl>
    <dl>
        <dt>Q、Do cars have 100V power outlets?</dt>
        <dd>A、If the external AC100V power is plugged in, you can use 100V. An air conditioner also can be powered on if the external AC100V is plugged in.<br>
            You can charge cell or smartphone with the 12V cigarette power outlet or USB power outlet inside. ( Please use your USB cables)<br>
            A laptop which consumes a little power can be used with an inverter convert 12V into 100V. If you need to use your laptop for work, we offer you a free inverter so please inform us when you make a reservation. (Devices which consume lots of power can not be used because it might damage the car.)
        </dd>
        <dt>Q、How do we deal with bedding?</dt>
        <dd>A、From a sanitary point of view, we cannot allow you to sleep on a bed or sheets directly. We have rental bedding sets (\500 per a day) so please use them. They are cleaned by a cleaning company so they are always clean. </dd>
    </dl>
    <dl>
        <dt>Q、How long does the fridge work?</dt>
        <dd>A、We charge the sub-battery full when we rent the car but it is dead within a half day to one day if the engine is off.(depends on the situation) <br>
            Driving conditions can affect a lot so we recommend you use it on full power when you drive and turn it off when you park.<br>
            *If the sub-battery is dead, the engine will still start because the car battery separate.<br>
            *If the internal battery is dead, none of the equipment will work including the FF heater and lights. You have to plug in an external 100V power or drive to charge it.<br>
            *It charges with one day driving.</dd>
    </dl>
    <dl>
        <dt>Q、How long can a FF heater work?</dt>
        <dd>A、You don't have to worry about fuel for the heater. It is automatically filled from the car’s fuel tank. If you use it on full power, it consumes less than 1 liter but the fan consumes battery power. When we rent a car , the battery is full but it is dead within 1 or 2 days without the engine on.<br>
            It depends on how long you drive so we recommend you turn it off when you sleep.<br>
            *When you use it, please set the dial nearly full. If you set the temperature low, the thermostat works frequently and battery is easily run down.<br>
            *If the sub-battery is dead, the car battery is different so the engine can be started.</dd>
    </dl>
    <dl>
        <dt>Q、When can we use AC inside?</dt>
        <dd>A、<span class="bold">CORDE LEAVES, ZIL520
                <!--and CORDE RUNDY -->have AC inside and it works only when 100V external power is connected. Please do not use it for heating. If you want to make it warm, please use FF heater.<br>
                <span class="bold">Using the heater function of the AC might cause technical trouble.( Because the AC is modified for an RV)<br>
                    <br>
                    <span class="bold">〈Other notices about battery〉</span><br>
                    ・An electric kettle or a hair drier cannot be used because they consume a lot of power except for when the external power is connected at a camping site, etc.<br>
                    ・Devices which drive motors like fans consume a lot of power momentarily and might damage fuses.<br>
                    *There is a large ventilator in the RV that can be used as a fan on the ceiling.</dd>
    </dl>
    <dl>
        <dt>Q、What does an RV come equipped with originally?</dt>
        <dd>A、
            Cleaning set, medical set, guide books, smartphone charger, USB power outlet, car goods, gas cartridge stove, folding table, chairs, child seat, booster seat and so on. Please check here. → <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c02">Free rental goods</a><br>
            Please choose from charged goods options. Please check here. → <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c01">charged goods options</a></dd>
    </dl>
    <dl>
        <h4>About place to stay</h4>
        <dt>Q、Where should we park and stay?</dt>
        <dd>A、You can park wherever you want and stay but if you are a beginner, campsites or roadside stations or spa parking lots are also a good choice. (You need permission)<br>
            There are charges if you stay at campsites but preparation and putting away dishes or garbage, bath and bathroom are easy and comfortable so it is also a good choice. What's more, they have external 100V power so you can use electric devices.<br>
            *Using chairs, tables and stuff at roadside stations or parking lots is a breach of manners. Please do not do it.<br>
            *There are many wild animals such as bears. Please be aware if you stay in a forest.<br>
            <br>
            We will soon prepare private campsites with great scenery for individual groups. Please look forward to it!</dd>
    </dl>
</section>
