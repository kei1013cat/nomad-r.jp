<?php get_header(); ?>

<?php include(TEMPLATEPATH.'/part-title.php'); ?>
<?php include(TEMPLATEPATH.'/part-pan.php'); ?>

<div id="main_contents" class="wrapper cf">

	<div id="contents">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<article <?php post_class(); ?>>
			<div class="entry-header">
				<h3 class="entry-title"><time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>"><?php the_time( 'Y.m.d'  ); ?></time>
<?php the_title(); ?></h3>
			</div>

			<section class="entry-content">
				<?php if (has_post_thumbnail() ):?>
					<p class="thumb"><?php echo get_the_post_thumbnail($post->ID, 'large'); ?></p>
				<?php endif; ?>
				<?php the_content(); ?>
			</section>
		</article>
<!--
		<?php wp_link_pages( 'before=<nav class="pages-link">&after=</nav>&link_before=<span>&link_after=</span>' ); ?>

		<nav class="navigation" id="nav-below">
			<ul class="cf">
				<?php previous_post_link( '<li class="nav-previous">&lt;&nbsp;前の記事：%link</li>', '%title', true ); ?>
				<?php next_post_link( '<li class="nav-next">&gt;&nbsp;次の記事：%link</li>', '%title', true ); ?>
			</ul>
		</nav>
-->
		<?php endwhile; endif; ?>
	</div>
	<!-- contents -->
		<?php get_sidebar(); ?>
</div>
<!-- main_contents -->

<?php get_footer(); ?>

