<?php require_once('./lang/lang_rental.php'); ?>
<?php require('./price_table.php'); ?>

<section class="rental free" id="c01">
	<h2 class="headline01 typesquare_tags">Cleaning bedding fee</h2>
	<div class="gallery_top">
		<ul class="cf">
	<li>
				<p class="photo"><a href="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" data-lightbox="charge"><img src="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" alt="タオルケット・ベッドシーツ・枕セット　５００円"></a>
				<p class="text">Bedding set 【Blanket (Thicker in winter) Bed sheet and Pillow( Come with Pillow cover)】<br /><br />

\500 will be charged as cleaning fee for each person.<br>
( We have bedding cleaned in cleaning factory for your comfort)</p>
				</a></li></ul>
				</div><!-- gallery -->
	<h2 class="headline01 typesquare_tags">Rental goods with extra charge</h2>	
	<h3>The price below is for one day without tax.<br>
		The price is for one day rental. The maximum costs will be up to 5 day rentals.<br>
		※From the sixth day, the price will be the same as for 5 days.
	</h3>
	<div class="gallery">
		<ul class="cf">
			<?php
			$i = 1;
			if( have_rows('有料レンタルグッズ',$rental_post_id)):
			  while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
			  	<?php if(!get_sub_field('非表示')): ?>
				<?php $rental_img = get_sub_field('画像'); ?>
				<li class="fead<?php echo $i; ?>">
					<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
						<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?>"></p>
						<p class="text"><?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?></p>
					</a>

				</li>
				<?php endif; ?>
			<?php
			  $i++;
			  if($i==3) { $i=1; }
			  endwhile;
			endif;
			?>
		</ul>
		<p class="tax">All prices don't include tax</p>
	</div>
	<!-- gallery -->
</section>
<section class="rental charge" id="c02">
	<h2 class="headline01 typesquare_tags">Free equipment</h2>	

	<div class="gallery">
		<ul class="cf">

		<?php
		$i = 1;
		if( have_rows('無料車内設置備品',$rental_post_id)):
		  while( have_rows('無料車内設置備品',$rental_post_id) ): the_row(); ?>
		  	<?php if(!get_sub_field('非表示')): ?>
			<?php $rental_img = get_sub_field('画像'); ?>
			<li class="fead<?php echo $i; ?>">
				<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
					<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?>"></p>
					<p class="text"><?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?></p>
				</a>

			</li>
			<?php endif; ?>
		<?php
		  $i++;
		  if($i==3) { $i=1; }
		  endwhile;
		endif;
		?>

		</ul>
	</div>
	<!-- gallery -->
</section>
