<?php require_once('./price_table.php'); ?>

<section class="cal" id="c01">
    <h2 class="headline01 typesquare_tags">ปฏิทินราคาช่วงเวลาต่างๆ<span class="blue">!</span></h2>

    <ul class="cal_area cf"><?php dynamic_sidebar('Calendar'); ?></ul>
    <div class="season cf">
        <div class="cf">
            <dl class="cf">
                <dt class="color1"></dt>
                <dd>ออฟซีซั่น วันธรรมดา</dd>
            </dl>
            <dl class="cf">
                <dt class="color2"></dt>
                <dd>ออฟซีซั่น วันศ-ส-อาและวันหยุด</dd>
            </dl>
            <dl class="cf">
                <dt class="color3"></dt>
                <dd>ออนซีซั่น วันธรรมดา</dd>
            </dl>
        </div>
        <div class="cf">
            <dl class="cf">
                <dt class="color4"></dt>
                <dd>ออนซีซั่น วันศ-ส-อาและวันหยุด</dd>
            </dl>
            <dl class="cf">
                <dt class="color5"></dt>
                <dd>ไฮซีชั่น</dd>
            </dl>
            <!--         <dl class="cf">
            <dt class="color6"></dt>
            <dd>ハイシーズン特別日</dd>
        </dl> -->
            <dl class="cf">
                <dt class="color8"></dt>
                <dd>ช่วงฤดูหนาว</dd>
            </dl>
        </div>
    </div>
    <!--season -->

    <table cellspacing="0" cellpadding="0" class="style01">
        <tr>
            <th>ออฟซีซั่น</th>
            <td>1ตุลาคม-20ธันวาคม<br />
                1มีนาคม-25เมษายน</td>
        </tr>
        <tr>
            <th>ออนซีซั่น</th>
            <td>
                7พฤษภาคม-30มิถุนายน<br />
                1กันยายน-30กันยายน</td>
        </tr>
        <tr>
            <th>ไฮซีชั่น</th>
            <td>1กรกฎาคม-31สิงหาคม</td>
        </tr>
        <!--         <tr>
            <th><span class="text">ハイシーズン特別日</span></th>
            <td><span class="text">お盆、年末年始、その他</span></td>
        </tr>
 -->
        <tr>
            <th><span class="text">ช่วงฤดูหนาว</span></th>
            <td><span class="text">22ธันวาคม-28กุมภาพันธ์</span></td>
        </tr>
    </table>

</section>
<section class="pricetable" id="c02">
    <h2 class="headline01 typesquare_tags">ตารางราคา</h2>

    <h2 class="luxury">最高級ラグジュアリークラス</h2>
    <div class="box">
        <h3 id="c02_3" class="zil">SUNLIGHT
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic10.jpg" width="104" alt="サンライト">
        </h3>
    </div>
    <!-- box -->
    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['luxury']['free']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['luxury']['free']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา </td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['day']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['half']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['day']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['half']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['day']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['half']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['day']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['half']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['day']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['half']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php endif; ?>

        <p>ราคาทั้งหมดยังไม่รวมภาษี
        </p>
        <p>เพื่อป้องกันความเสียหายและอุบัติเหตุ สมัครแผนประกันภัยเช่ารถสบายใจ (มีค่าใช้จ่าย)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">ไปที่หน้าประกันภัย</a></p>
        <p>สำหรับลูกค้าชาวต่างชาติขอแนะนำให้สมัครทั้ง2แผนประกันภัย</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">รายละเอียดเพิ่มเติมคลิกที่นี่</a> </p>
        <p>冬期間トイレ水道利用不可です。</p>
    </div>

    <h2 class="plmclass-2">รถหรูระดับพรีเมียม</h2>
    <div class="box">
        <h3 id="c02_2" class="zil"><span>รถหรูคันใหญ่</span>ZIL 520<img src="<?php bloginfo('template_url'); ?>/images/price_pic03.jpg" alt="ジル520"></h3>
    </div>
    <!-- box -->
    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา </td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['day']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['half']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['day']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['half']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['day']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['half']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['day']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['half']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php endif; ?>

        <p>ราคาทั้งหมดยังไม่รวมภาษี
        </p>
        <p>เพื่อป้องกันความเสียหายและอุบัติเหตุ สมัครแผนประกันภัยเช่ารถสบายใจ (มีค่าใช้จ่าย)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">ไปที่หน้าประกันภัย</a></p>
        <p>สำหรับลูกค้าชาวต่างชาติขอแนะนำให้สมัครทั้ง2แผนประกันภัย</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">รายละเอียดเพิ่มเติมคลิกที่นี่</a> </p>
        <p>ติดตั้งเครื่องปรับอากาศ(สามารถเปิดใช้ได้ปกติ) มีโทรทัศน์แขวนผนังรุ่นล่าสุด</p>
    </div>

    <h2 class="highclass-2">ระดับไฮคลาส</h2>
    <div class="box">
        <h3 id="c02_1">Corde Bunks<img src="<?php bloginfo('template_url'); ?>/images/price_pic01.jpg" width="97" alt="コルドバンクス"></h3>
        <h3>Corde Leaves<img src="<?php bloginfo('template_url'); ?>/images/price_pic02.jpg" width="104" alt="コルドリーブス"></h3><br class="pc">
    </div>
    <div class="scroll">
        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา</td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['day']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['half']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['time']); ?>เยน</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php else:?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">ราคา1วัน</td>
                <td class="h">ราคาครึ่งวัน</td>
                <td class="h">เช่ารถก่อนหรือหลังเวลาทำการ1ชั่วโมง</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">ออฟซีซั่น</th>
                <td class="color1">วันธรรมดา</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['day']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['half']); ?>เยน</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color2">วันศ-ส-อา</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['day']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['half']); ?>เยน</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">ออนซีซั่น</th>
                <td class="color3">วันธรรมดา</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['half']); ?>เยน</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['time']); ?>เยน</td>
            </tr>
            <tr>
                <td class="color4">วันศ-ส-อา</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['day']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['half']); ?>เยน</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ไฮซีชั่น</th>
                <td class="color5"><?php echo number_format($price_table['high']['high']['day']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['high']['high']['half']); ?>เยน</td>
                <td class="color5"><?php echo number_format($price_table['high']['high']['time']); ?>เยน</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ช่วงฤดูหนาว</th>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['day']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['half']); ?>เยน</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['time']); ?>เยน</td>
            </tr>
        </table>
        <?php endif; ?>
        <p>ราคาทั้งหมดยังไม่รวมภาษี</p>
        <p>เพื่อป้องกันความเสียหายและอุบัติเหตุ สมัครแผนประกันภัยเช่ารถสบายใจ (มีค่าใช้จ่าย)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">ไปที่หน้าประกันภัย</a></p>
        <p>สำหรับลูกค้าชาวต่างชาติขอแนะนำให้สมัครทั้ง2แผนประกันภัย</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">รายละเอียดเพิ่มเติมคลิกที่นี่</a></p>
        <p>สัตว์เลี้ยงที่นำขึ้นรถได้มีเพียงสุนัขเท่านั้น และมีค่าใช้จ่ายในการทำความสะอาดเพิ่มเติ่ม 1ตัว8000เยน 2ตัว10000เยน 3ตัวขึ้นไป12000เยน</p>
        <br>

    </div>

    <h4>เวลาทำการ 9-18นาฬิกา</h4>
    <p class="text">ระยะเวลาในการเช่าและคืนรถครึ่งวันก็คือ 9-12โมงเช้า หรือ 12-18นาฬิกา<br />
        ถ้ามาคืนรถช้ากว่าเวลาทำการ 18-22นาฬิกา <br>
        ถ้ามาคืนหลัง22นาฬิกาจะคิดราคาเต็มวัน</p>
    <h4>ข้อควรระวังในการคืนรถไม่ตรงเวลา</h4>
    <p class="text">ในกรณีที่ลูกค้านำรถมาคืนช้าเกินกว่า5ชั่วโมง ไม่ว่าจะเพราะรถติดหรืออื่นๆ ซึ่งส่งผลให้ไม่สามารถทำความสะอาดและเตรียมรถให้ลูกค้าท่านต่อไป หรือกรณีที่ส่งผลให้ลูกค้าท่านต่อไปยกเลิกการจอง ลูกค้าต้องจ่ายค่าต่อเวลารถและรับผิดชอบค่าเยโอกาสจากลูกค้าใหม่ โดยคิดตามราคาจริง กรุณาระวัง</p>

    <h4>（Foreign Visitors to Japan Only）<br>เรียนลูกค้าชาวต่างชาติ</h4>
    <p class="text">บริษัทของเราเป็นตัวแทนจำหน่าย<br>Hokkaido Expressway Pass(HEP) (สำหรับลูกค้าชาวต่างชาติ)<br>สามาถเช่ารถพร้อมซื้อบัตรผ่านสำหรับขึ้นทางด่วนแบบเหมาที่ใช้กับเครื่องอ่านบัตรได้<br>（เช่น ใช้งาน14วัน ก็จะสามารถขึ้นทางด่วนกี่ครั้งก็ได้ในราคาวันละแค่807เยน）<br>คิดราคาตามจำนวนวันที่เช่ารถ<br>ลูกค้าสัญชาติญี่ปุ่นไม่สามารถใช้ได้<br><a class="link" href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">รายละเอียดเพิ่มเติมคลิกที่นี่</a></p>

</section>
<section class="discount" id="c03">
    <h2 class="headline01 typesquare_tags">แผนสิทธิประโยชน์และส่วนลด<span class="blue">!</span></h2>
    <table>
        <tr>
            <th>ส่วนลดสำหรับเช่าระยะยาว</th>
            <td class="pd2">เช่า7วันขึ้นไปลด10%<br>
                เช่า14วันขึ้นไปลด15%<br>
                เช่า21วันขึ้นไปลด20%</td>
        </tr>
        <tr>
            <th>ส่วนลดสำหรับผู้สูงอายุ</th>
            <td>รับส่วนลด10% เมื่อลูกค้าอายุเกิน55ปี</td>
        </tr>
        <tr>
            <th>ส่วนลดสำหรับผู้กลับมาใช้บริการอีกครั้ง</th>
            <td>รับส่วนลด10% สำหรับผู้ที่เคยใช้บริการแล้ว ไม่เกิน1ปี</td>
        </tr>

    </table>
    <div>
        <p>※อย่างเลือกใช่ส่วนลดได้เพียง1อย่าง（ในกรณีที่ใช้ได้หลายสิทธิ กรุณาเลือกสิทธิที่ดีที่สุดเพียง1อย่าง）</p>
    </div>
</section>
<section class="example" id="c04">
    <h2 class="headline01 typesquare_tags">ตัวอย่างการคิดราคา<span class="blue">!</span></h2>

    <ul class="cf">
        <li>1วันคือ<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic04.jpg" width="83" alt="１日とは"></li>
        <li>ครึ่งวัน(ช่วงเช้า)คือ<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic05.jpg" width="83" alt="半日(午前)とは"></li>
        <li>ครึ่งวัน(ช่วงบ่าย)คือ<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic06.jpg" width="83" alt="半日（午後）とは"></li>
    </ul>
    </h3>
    <div class="item cf">
        <div class="left">
            <h4>ตัวอย่างที่1</h4>
            <p> ประเภทรถ ： Corde Bunks<br>
                ในกรณ๊ที่ค่าเช่ารถ1วันราคา <?php echo number_format($price_table['high']['free']['day']); ?>เยน</p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic07.jpg" alt="コルドバンクス" /></p>
        </div>
        <!-- right -->
    </div><!-- item -->
    <table>
        <tr>
            <th>วันที่เช่า</th>
            <td>วันที่1</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['high']['free']['day']); ?>เยน</td>
            <td class="nobg"></td>
        </tr>
        <tr>
            <th>วันที่คืน</th>
            <td>วันที่2</td>
            <td>～11：30</td>
            <td><?php echo number_format($price_table['high']['free']['half']); ?>เยน</td>
            <td class="nobg"></td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2" class="red"><span>คิดเป็นเงิน</span><?php echo number_format($price_table['high']['free']['day'] + $price_table['high']['free']['half']); ?>เยน＋ภาษี</td>
            <td class="nobg"></td>
        </tr>
    </table>

    <div class="item cf">
        <div class="left">
            <h4>ตัวอย่างที่2</h4>
            <p> ประเภทรถ ： Corde Leaves<br>
                ในกรณ๊ที่ค่าเช่ารถ1วันราคา <?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน </p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic08.jpg" alt="コルドリーブス" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>วันที่เช่า</th>
            <td>วันที่1</td>
            <td>13：00～</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['half']); ?>เยน</td>
        </tr>
        <tr>
            <th></th>
            <td>วันที่2</td>
            <td>เช่ารถเต็มวัน</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน</td>
        </tr>
        <tr>
            <th></th>
            <td>วันที่3</td>
            <td>เช่ารถเต็มวัน</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน</td>
        </tr>
        <tr>
            <th>วันที่คืน</th>
            <td>วันที่4</td>
            <td>～17：30</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>เยน</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>คิดเป็นเงิน</span><?php echo number_format(($price_table['high']['on_tsu']['day'] * 3) + $price_table['high']['on_tsu']['half']); ?>เยน＋ภาษี</td>
        </tr>
    </table>
    <div class="item cf">
        <div class="left">
            <h4>ตัวอย่างที่3</h4>
            <p> ประเภทรถ ： ZIL520<br>
                ในกรณ๊ที่ค่าเช่ารถ1วันราคา <?php echo number_format($price_table['premium']['on_tsu']['day']); ?>เยน</p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic09.jpg" alt="ジル520" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>วันที่เช่า</th>
            <td>วันที่1</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>เยน</td>
        </tr>
        <tr>
            <th></th>
            <td>วันที่2</td>
            <td>เช่ารถเต็มวัน</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>เยน</td>
        </tr>
        <tr>
            <th>วันที่คืน</th>
            <td>วันที่3</td>
            <td>～12：00</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>เยน</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>คิดเป็นเงิน</span><?php echo number_format(($price_table['premium']['on_tsu']['day'] * 2) + $price_table['premium']['on_tsu']['half'] ); ?>เยน＋ภาษี</td>
        </tr>
    </table>
</section>
<section class="empty" id="c05">
    <!--<h2><img src="<?php bloginfo('template_url'); ?>/images/price_tit05.jpg" alt="空車状況カレンダー"/></h2>
    <ul>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス1 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=coh8o21ju2itn8uah33819p678%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス2 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=kbsrmr4pml0afjgjsrg10svqn4%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_3">
            <h4 class="green"> コルドリーブス <img src="<?php bloginfo('template_url'); ?>/images/car_pic002.png" alt="コルドリーブス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=nfota0r3s60uftr61e5nv94k1o%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_1">
            <h4 class="yellow"> ジル520 <img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="ジル520"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=d7hnk3hgae5lbsr6fjsl1dc528%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
    </ul>
            <p class="text">ご予約は随時　メール　電話等により受け賜りますが　サイトへの表示には時間差があり、予約可能になっていても時間差で予約済みになってしまう場合もあります事をご了承くださいませ。</p><br class="sp">-->
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">เช่ารถคลิกที่นี่　＞</a></p>

</section>
