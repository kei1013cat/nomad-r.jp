
<?php get_header(); ?>

<div id="title">
	<div class="wrapper">
		<?php $slug_name = basename(get_permalink()); ?>
		<h2><?=$post->post_title;?></h2>
	</div>
</div>
<!-- title -->

<?php include(TEMPLATEPATH.'/part-pan.php'); ?>
<div class="wrapper">
	<div id="main_contents" class="cf">
		<div id="contents" class="mr15">
			<section>
			お探しのページは見つかりませんでした<br>
			<a href="<?php bloginfo('url'); ?>">トップページより再度アクセスしてください</a>
			</section>
		</div>
		<!-- contents -->
		<?php get_sidebar(); ?>
	</div>
	<!-- main_contents --> 
</div>
<!-- wrapper -->

<?php get_footer(); ?>
