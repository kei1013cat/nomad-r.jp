<?php
/*
Template Name: page-newslist
*/
?>
<?php get_header(); ?>

<?php include(TEMPLATEPATH.'/part-title.php'); ?>
<?php include(TEMPLATEPATH.'/part-pan.php'); ?>

<div id="main_contents" class="wrapper cf">
	<?php get_sidebar(); ?>
	<div id="contents">
		<div id="archive">
	<?php
				$wp_query = new WP_Query();
				$param = array(
					'posts_per_page' => '20', //表示件数。-1なら全件表示
					'post_status' => 'publish',
					'orderby' => 'date', //ID順に並び替え
					'order' => 'DESC'
				);
				$wp_query->query($param);?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article>
				<div class="entry-header">
					<h2 class="entry-title">
						<a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
								<?php the_time( get_option( 'date_format' ) ); ?>
							</time><?php the_title(); ?></a>&nbsp;&nbsp;
					</h2>
				</div>
				<?php if (has_post_thumbnail() ):?>
				<p class="thumb"><?php echo get_the_post_thumbnail($post->ID, 'medium'); ?></p>
				<?php endif; ?>
				<div class="entry-content">
					<?php the_excerpt(); ?>
				</div>
				<!--
				<p class="tar"><a href="<?php the_permalink(); ?>" class="more-link">続きを読む<span class="meta-nav">&gt;</span></a></p>
				-->

			</article>
			<?php endwhile; endif; ?>
		</div>	<!-- single -->
	</div>
	<!-- contents -->
	
</div>
<!-- main_contents -->

<?php get_footer(); ?>

