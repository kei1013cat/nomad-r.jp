<?php get_header(); ?>

<?php include(TEMPLATEPATH.'/part-title.php'); ?>
<?php include(TEMPLATEPATH.'/part-pan.php'); ?>

<div id="main_contents" class="wrapper cf">
	
	<div id="contents">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article class="cf">

				<div class="entry-header">
					<h2 class="entry-title">
						<a title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>"><time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
								<?php the_time( get_option( 'date_format' ) ); ?>
							</time><?php the_title(); ?></a>&nbsp;&nbsp;
					</h2>
				</div>
				<?php if (has_post_thumbnail() ):?>
				<p class="thumb"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail($post->ID, 'medium'); ?></a></p>
				<?php endif; ?>
				<div class="entry-content">
					<a href="<?php the_permalink(); ?>"><?php the_excerpt(); ?></a>
				</div>
				<!--
				<p class="tar"><a href="<?php the_permalink(); ?>" class="more-link">続きを読む<span class="meta-nav">&gt;</span></a></p>
				-->

			</article>
			<?php endwhile; endif; ?>
	</div>
	<!-- contents -->
	<?php get_sidebar(); ?>
</div>
<!-- main_contents -->

<?php get_footer(); ?>

