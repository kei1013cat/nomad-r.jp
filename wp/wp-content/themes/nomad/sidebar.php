<?php require_once('./lang/lang_sidebar.php'); ?>
<div id="sidebar">
    <?php if($post->post_name =='price' || $post->post_name =='yakkan' || $post->post_name =='yakkan-en' || $post->post_name =='yakkan-zh'): ?>
    <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c01"><?php echo $sidebar_price_01; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02"><?php echo $sidebar_price_02; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c03"><?php echo $sidebar_price_03; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c04"><?php echo $sidebar_price_04; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c05"><?php echo $sidebar_price_05; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>reservation.php"><?php echo $sidebar_reservation; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan/"><?php echo $sidebar_yakkan; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-en/"><?php echo $sidebar_yakkan_en; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-zh/"><?php echo $sidebar_yakkan_zh; ?></a></li>
    </ul>
    <?php endif; ?>

    <?php if($post->post_name =='car'): ?>
    <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05"><?php echo $sidebar_car_type05; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><?php echo $sidebar_car_type03; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01"><?php echo $sidebar_car_type01; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=02"><?php echo $sidebar_car_type02; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04"><?php echo $sidebar_car_type04; ?></a></li>
    </ul>
    <?php endif; ?>

    <?php if($post->post_name =='rental'): ?>
    <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c01"><?php echo $sidebar_rental_01; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c02"><?php echo $sidebar_rental_02; ?></a></li>
    </ul>
    <?php endif; ?>

    <?php if($post->post_name =='guide'): ?>
    <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c01"><?php echo $sidebar_guide_01; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04"><?php echo $sidebar_guide_04; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02"><?php echo $sidebar_guide_02; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c03"><?php echo $sidebar_guide_03; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05"><?php echo $sidebar_guide_05; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c09"><?php echo $sidebar_guide_09; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c06"><?php echo $sidebar_guide_06; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c08"><?php echo $sidebar_guide_08; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan/"><?php echo $sidebar_yakkan; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-en/"><?php echo $sidebar_yakkan_en; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-zh/"><?php echo $sidebar_yakkan_zh; ?></a></li>
        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c07"><?php echo $sidebar_guide_07; ?></a></li>
    </ul>
    <?php endif; ?>


    <?php if(is_single() || is_archive() || $post->post_name =='news'): ?>
    <div id="side_news">
        <h2>最新の記事</h2>
        <ol>
            <?php
            $type = $post->post_type;

            $wp_query = new WP_Query();
            $param = array(
                'posts_per_page' => '5', //表示件数。-1なら全件表示
                'post_status' => 'publish',
                'orderby' => 'date', //ID順に並び替え
                'order' => 'DESC'
            );
            if($post->post_type == "tabi"){
                $param = $param+array('post_type'=>'tabi');

            }

            $wp_query->query($param);?>
            <?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <li>
                <?php $link = get_post_meta($post->ID,'link_url',true); ?>
                <?php if($link) : ?>
                <a href="<?php echo $link; ?>"><?php the_title(); ?></a>
                <?php else : ?>
                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php the_title(); ?></a>
                <?php endif; ?>
            </li>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </ol>

        <h2>月別一覧</h2>
        <ol>
            <?php if($post->post_type == "tabi"){?>
            <?php wp_get_archives('limit=12&post_type=tabi'); ?>
            <?php }else{ ?>
            <?php wp_get_archives('limit=12');  ?>
            <?php } ?>
        </ol>
        <!--
        <h2>年別一覧</h2>
        <ol>
            <?php wp_get_archives('type=yearly'); /* limit=12 ←月数(12ヶ月=1年) */ ?>
        </ol>

        <h2>カテゴリ</h2>
        <ol>
        <?php
               $cat_all = get_terms( "category", "fields=all&get=all" );
            foreach($cat_all as $value):
         ?>
        <li><a href="<?php echo get_category_link($value->term_id); ?>"><?php echo $value->name;?></a></pli>
        <?php endforeach; ?>
        </ol>
-->
    </div><!-- side_news -->
    <?php endif; ?>
</div>
<!-- sidebar -->
