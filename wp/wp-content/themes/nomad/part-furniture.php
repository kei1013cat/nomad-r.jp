<section class="flexform">
<h3><img src="<?php bloginfo('template_url'); ?>/images/flexform_h.png" /></h3>
<p class="brand">取り扱いブランド</p>
<p>イタリアデザイン界の巨匠Antonio・Citterioがデザイン＆総合監修を務めるプレステージファニチャーブランド FLEXFORM。<br>
確かな品質とデザイン性で、世界中55か国の顧客を魅了し続けています。</p>

</section>
<div id="works">
  <?php
				$wp_query = new WP_Query();
				$param = array(
					'posts_per_page' => '-1', //表示件数。-1なら全件表示
					'post_status' => 'publish',
					'post_type' => 'case-furniture',
				);
				$wp_query->query($param);?>
  <?php if ( have_posts() ) : ?>
  
  <section class="work_list cf">
    <div id="wrapper">
      <div id="container" class="cf">
  
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="item"><a href="<?php $image = get_field('画像');echo $image['sizes']['case_size']; ?>" rel="lightbox">
    <?php if(get_field('一覧用画像')):?>
    <img src="<?php $image = get_field('一覧用画像');echo $image['sizes']['case_size_thumb']; ?>" alt="<?php the_title();?>" />
    <?php else:?>
    <img src="<?php $image = get_field('画像');echo $image['sizes']['case_size_thumb']; ?>" alt="<?php the_title();?>" />
    <?php endif; ?>   
    </a></div>
    <?php endwhile; ?>

</div>
    </div>
  </section>

  <?php endif; ?>
  <?php wp_reset_query(); ?>
  
  <section class="bottom_tel">
    <p><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>renovation/"><img src="<?php bloginfo('template_url'); ?>/images/bottom_tel.jpg" ></a></p>
  </section>
</div>
<!-- works -->