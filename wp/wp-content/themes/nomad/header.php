<?php require_once('./lang/lang_header.php'); ?>
<?php
/*
if(lang() == "ja"){
    echo "ja";
}
if(lang() == "th"){
    echo "th";
}
if(lang() == "zh"){
    echo "zh";
}
if(lang() == "en"){
    echo "en";
}
*/
?>
<!DOCTYPE html>


<html <?php language_attributes(); ?>>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-118964282-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-118964282-1');

    </script>

    <?php if(is_pc()): ?>
    <meta content="width=1100" name="viewport">
    <?php else: ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php endif; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(isset($_GET['lang']) && $_GET['lang'] == 'zh'): ?>
        北海道逍遙露營車
        <?php else:?>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo ' | '; } ?>
        <?php bloginfo('name'); ?>
        <?php endif; ?>
    </title>
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.1.min.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>


    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/lightbox.css" type="text/css">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico">

    <?php if(is_mobile()){ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css?v=<?php echo date("YmdHi") ?>" type="text/css">
    <?php }else{ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css?v=<?php echo date("YmdHi") ?>" type="text/css">
    <?php } ?>

    <?php if(basename($_SERVER["PHP_SELF"]) == 'mail.php'):?>
    <?php $post = new stdClass;$post->post_name = 'price';$post->post_title = 'ご予約フォーム';?>
    <?php endif; ?>
    <?php if(basename($_SERVER["PHP_SELF"]) == 'reservation.php'):?>
    <?php $post = new stdClass;$post->post_name = 'price';$post->post_title = 'ご予約フォーム';?>
    <script src="<?php bloginfo('template_url'); ?>/js/ajaxzip3.js" charset="UTF-8"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.autotab.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.validate.min.js"></script>

    <script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine.js"></script>
    <!-- <script src="<?php bloginfo('template_url'); ?>/js/validation/jquery.validationEngine-ja.js"></script>
 -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/validation/validationEngine.jquery.css">


    <script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.ui.datepicker-<?php echo lang(); ?>.min.js"></script>

    <?php endif; ?>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery-ui.css">

    <?php if(is_pc()): ?>
    <script src="<?php bloginfo('template_url'); ?>/js/menu_fixing.js"></script>
    <?php endif; ?>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/izimodal/css/iziModal.min.css" type="text/css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/izimodal/js/iziModal.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>

    <script type="text/javascript">
        jQuery(function($) {
            $("#acMenu dt").on("click", function() {
                $(this).next().slideToggle();
                // activeが存在する場合
                if ($(this).children(".accordion_icon").hasClass('active')) {
                    // activeを削除
                    $(this).children(".accordion_icon").removeClass('active');
                } else {
                    // activeを追加
                    $(this).children(".accordion_icon").addClass('active');
                }
            });
        });

    </script>

    <script src="<?php bloginfo('template_url'); ?>/js/jquery.matchHeight-min.js"></script>
    <script>
        jQuery(function($) {
            $('.matchHeight').matchHeight();
        });

    </script>

    <!-- slick -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick-theme.css" type="text/css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.min.js"></script>
    <script>
        jQuery(function($) {
            $('.carusel').slick({
                autoplay: true,
                dots: true,
                autoplaySpeed: 2500,
                slidesToShow: 4,
                slidesToScroll: 4,

                infinite: true,
                appendArrows: $('#arrows'),
                responsive: [{
                        breakpoint: 1424,
                        settings: {
                            centerMode: true,
                            centerPadding: '40px',
                            dots: true,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            centerMode: true,
                            centerPadding: '40px',
                            dots: true,
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            infinite: true,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            dots: true,
                            centerMode: true,
                            vertical: false,
                            dots: false,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                ]
            });
        });

    </script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script>
        jQuery(function($) {
            window.sr1 = ScrollReveal({
                reset: false,
                mobile: false
            });
            sr1.reveal('.enter-left', {
                origin: 'left',
                distance: '20%',
                duration: 500,
                scale: 1.0,
                delay: 200,
                opacity: 0,
            });
            sr1.reveal('.enter-right', {
                origin: 'right',
                distance: '20%',
                duration: 100,
                scale: 1.0,
                delay: 200,
                opacity: 0,
            });
            sr1.reveal('.enter-top', {
                origin: 'top',
                distance: '20%',
                duration: 500,
                scale: 1.0,
                delay: 600,
                opacity: 0,
            });
            sr1.reveal('.enter-bottom', {
                origin: 'bottom',
                distance: '20%',
                duration: 800,
                scale: 1.0,
                delay: 500,
                opacity: 0,
            });
            sr1.reveal('.rotate', {
                origin: 'top',
                distance: '20%',
                duration: 1200,
                scale: 1.0,
                delay: 300,
                opacity: 0,
                rotate: {
                    x: 300,
                    y: 300,
                    z: 300
                }
            });
            sr1.reveal('.fead', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 300,
                opacity: 0,
            });
            sr1.reveal('.fead1', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 200,
                opacity: 0,
            });
            sr1.reveal('.fead2', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 250,
                opacity: 0,
            });
            sr1.reveal('.fead3', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 300,
                opacity: 0,
            });
            sr1.reveal('.fead4', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 350,
                opacity: 0,
            });
            sr1.reveal('.fead5', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 400,
                opacity: 0,
            });
            sr1.reveal('.fead6', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 450,
                opacity: 0,
            });
            sr1.reveal('.fead7', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 500,
                opacity: 0,
            });
            sr1.reveal('.fead8', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 550,
                opacity: 0,
            });
            sr1.reveal('.fead9', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 600,
                opacity: 0,
            });
            sr1.reveal('.fead10', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 650,
                opacity: 0,
            });
            sr1.reveal('.fead-last', {
                distance: '0%',
                duration: 500,
                scale: 1.0,
                delay: 800,
                opacity: 0,
            });

        });

    </script>

    <?php //if ( is_home()  ) {?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.easing.1.3.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/flexslider/flexslider.css" type="text/css" />
    <script src="<?php bloginfo('template_url'); ?>/js/flexslider/jquery.flexslider.js"></script>
    <script type="text/javascript" charset="utf-8">
        $(window).load(function() {
            $('#carousel').flexslider({
                animation: "slide",
                animationLoop: true,
                animationSpeed: 800,
                slideshow: false,
                pauseOnAction: false,
                slideshowSpeed: 2000,
                initDelay: 2500,
                multipleKeyboard: true,
                asNavFor: "#slider",
                <?php if(!is_mobile()):?>
                controlNav: true,
                itemWidth: 70,
                itemMargin: 20
                <?php endif;?>
                <?php if(is_mobile()):?>
                controlNav: false,
                itemWidth: 26,
                itemMargin: 10
                <?php endif;?>
            });
            $('#slider').flexslider({
                animation: "fade",
                controlNav: false,
                animationLoop: true,
                animationSpeed: 1000,
                slideshow: true,
                pauseOnAction: false,
                slideshowSpeed: 3500,
                <?php if(is_mobile()):?>
                directionNav: false,
                <?php endif;?>
                multipleKeyboard: true,
                sync: "#carousel"
            });
        });

    </script>
    <?php// } ?>

    <!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->

    <!-- fb -->
    <?php if(isset($_GET['lang']) && $_GET['lang'] == 'zh'): ?>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="北海道逍遙遊露營車 | 自駕露營車盡情享受北海道！" />
    <meta property="og:url" content="http://nomad-r.jp/reservation.php?lang=zh" />
    <meta property="og:image" content="http://nomad-r.jp/wp/wp-content/themes/nomad/images/fb_ico.jpg" />
    <meta property="og:description" content="北海道的露營車出租專門店「北海道逍遙遊露營車」，為您準備舒適、安心的旅程。全北海道皆為您準備了景點介紹、各種充沛的加租商品！" />
    <meta property="og:locale" content="ja_JP" />
    <meta property="og:site_name" content="北海道逍遙遊露營車" />
    <?php else: ?>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="北海道ノマドレンタカー | キャンピングカーレンタルならノマドレンタカーへ　キャンピングカーで北海道を満喫！" />
    <meta property="og:url" content="<?php bloginfo('url'); ?>/" />
    <meta property="og:image" content="<?php bloginfo('template_url'); ?>/images/fb_ico.jpg" />
    <meta property="og:description" content="キャンピングカーレンタル「北海道ノマドレンタカー」は、快適で安心な旅のお手伝いをいたします。全道オススメスポットのご紹介や、充実のアクティビティレンタルもご用意！" />
    <meta property="og:locale" content="ja_JP" />
    <meta property="og:site_name" content="北海道ノマドレンタカー" />
    <?php endif; ?>

    <script type="text/javascript" src="//webfonts.sakura.ne.jp/js/sakura.js"></script>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" />
</head>
<?php
    $body_id = "";
    if ( is_home() ) {
        $body_id = ' id="index"';
    }else if ( is_page() ) {
        $body_id = ' id="'.$post->post_name.'" class="subpage"';
    }else if ( is_single() ) {
        $body_id = ' id="single"';
    }else if ( is_archive() ) {
        $body_id = ' id="archive" class="subpage"';
    }
?>
<body<?php echo $body_id; ?>>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>

    <div id="outer">

        <?php if((isset($_GET['lang']) && $_GET['lang'] == 'zh') && is_pc()): ?>
        <?php include('http://zh.nomad-r.jp/header.php'); ?>
        <?php else: ?>

        <header id="header">
            <div class="wrapper cf">
                <h1>
                    <?php echo $header_title; ?>
                </h1>
                <div class="top cf">
                    <?php if(is_pc()): ?>
                    <h2><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>"><img src="<?php bloginfo('template_url'); ?>/images/header_logo_mark.png" alt="北海道ノマドレンタカー"></a></h2>

                    <?php endif; ?>
                    <?php if(is_mobile()): ?>
                    <h2><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>"><img src="<?php bloginfo('template_url'); ?>/images/header_logo_mark_sp.png" alt="北海道ノマドレンタカー"></a></h2>
                    <?php endif; ?>
                    <?php if(is_pc()){ ?>
                    <p class="btn-ja"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>"><?php echo $header_reservation; ?></a></p>

                    <?php if(lang()=="ja"): ?>
                    <p class="btn-en"><a href="<?php bloginfo('url'); ?>/en/">English</a></p>
                    <p class="btn-zh"><a href="http://zh.nomad-r.jp/" target="_blank">繁體中文</a></p>
                    <p class="btn-th"><a href="<?php bloginfo('url'); ?>/th/">ภาษาไทย</a></p>
                    <?php else: ?>
                    <p class="btn-en"><a href="<?php bloginfo('url'); ?>/"><?php echo $header_japanese ?></a></p>
                    <?php endif;?>

                    <?php
  $url = $_SERVER['REQUEST_URI'];
  if(strstr($url,'reservation.php') != true and strstr($url,'mail.php') != true):
?>

                    <?php if(lang()=="ja"): ?>
                    <div class="lang">
                        <div id="google_translate_element"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'ja',
                                    includedLanguages: 'fr,ja,ko,zh-CN',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                }, 'google_translate_element');
                            }

                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </div>
                    <?php endif; ?>
                    <?php endif; ?>
                    <p class="card"><img src="<?php bloginfo('template_url'); ?>/images/header_card.jpg" alt="対応クレジットカード"></p>
                </div><!-- top -->

                <nav>
                    <ul class="<?php echo lang();?> cf">
                        <li class="nav_index"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>"><?php echo $header_home; ?>
                                <?php if($header_sub_home!=""): ?><span><?php echo $header_sub_home; ?></span><?php endif; ?></a>
                        </li>
                        <li class="nav_car"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/"><?php echo $header_car; ?>
                                <?php if($header_sub_car!=""): ?><span><?php echo $header_sub_car; ?></span><?php endif; ?></a>
                        </li>
                        <li class="nav_guide"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/"><?php echo $header_guide; ?>
                                <?php if($header_sub_guide!=""): ?><span><?php echo $header_sub_guide; ?></span><?php endif; ?></a>
                        </li>
                        <li class="nav_price"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/"><?php echo $header_price; ?>
                                <?php if($header_sub_price!=""): ?><span><?php echo $header_sub_price; ?></span><?php endif; ?></a>
                        </li>
                        <li class="nav_rental"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/"><?php echo $header_rental; ?>
                                <?php if($header_sub_rental!=""): ?><span><?php echo $header_sub_rental; ?></span><?php endif; ?></a>
                        </li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/"><?php echo $header_contact; ?>
                                <?php if($header_sub_contact!=""): ?><span><?php echo $header_sub_contact; ?></span><?php endif; ?></a>
                        </li>
                    </ul>
                </nav>

                <?php } ?>
                <?php if(is_mobile()){ ?>
                <div class="menu cf">
                    <p id="closeMenu"> <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/header_close.png"></a> </p>
                    <p id="openMenu"> <a href="#"> <img src="<?php bloginfo('template_url'); ?>/images/header_menu.png"> </a> </p>
                </div>
                <!-- menu -->
                <div id="layerMenu">
                    <ul>
                        <li class="nav_index"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>"><?php echo $header_home; ?></a></li>
                        <li class="nav_car"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/"><?php echo $header_car; ?></a></li>
                        <li class="nav_guide"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/"><?php echo $header_guide; ?></a></li>
                        <li class="nav_price"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/"><?php echo $header_price; ?></a></li>
                        <li class="nav_rental"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/"><?php echo $header_rental; ?></a></li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/contact/"><?php echo $header_contact; ?></a></li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/tourism/">海外の方へ（Welcome to HOKKAIDO TOURISM）</a></li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=ja">ご予約はこちら</a></li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=en">RESERVATION</a></li>
                        <li class="nav_contact"><a href="http://zh.nomad-r.jp/">預約</a></li>
                        <li class="nav_contact"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=th">ภาษาไทย</a></li>
                    </ul>
                </div>
                <!-- layerMenu -->

                <div id="overray">
                </div>

                <?php
  $url = $_SERVER['REQUEST_URI'];
  if(strstr($url,'reservation.php') != true and strstr($url,'mail.php') != true):
?>
                <div class="cf">
                    <p class="card"><img src="<?php bloginfo('template_url'); ?>/images/header_card<?php mobile_img();?>.jpg" alt="対応クレジットカード"></p>

                    <?php if(lang()=="ja"): ?>

                    <div class="lang">
                        <div id="google_translate_element"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'ja',
                                    includedLanguages: 'fr,ja,ko,zh-CN',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                }, 'google_translate_element');
                            }

                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    </div>
                    <?php endif;?>

                </div>

                <?php endif; ?>

                <?php //if ((is_home() || is_front_page() ) || (strstr($url,'reservation.php') == true)) : ?>
                <ul class="reservation cf">
                    <?php if(lang()=="ja"): ?>
                    <li class="btn-ja"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=ja">空車状況・ご予約(Japanese)</a></li>
                    <li class="btn-en"><a href="<?php bloginfo('url'); ?>/en/">English</a></li>
                    <li class="btn-zh"><a href="http://zh.nomad-r.jp/">繁體中文</a></li>
                    <li class="btn-th"><a href="<?php bloginfo('url'); ?>/th/">ภาษาไทย</a></li>
                    <?php else: ?>
                    <li class="btn-en"><a href="<?php bloginfo('url'); ?>/"><?php echo $header_japanese ?></a></li>
                    <li class="btn-ja"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>"><?php echo $header_reservation; ?></a></li>
                    <?php endif;?>
                </ul>


                <?php //endif; ?>

                <?php } ?>
            </div>
            <!-- wrapper -->
        </header>
        <div id="header-dammy"></div>

        <?php endif; ?>
