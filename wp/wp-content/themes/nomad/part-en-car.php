<div id="car">


    <?php if(!isset($_GET['type'])):?>
    <section class="car">
        <section>
            <h2 class="headline01">Luxury class</h2>
            <h3 class="big">SUNLIGHT</h3>
            <h3>Explore Hokkaido in refined elegance
            </h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_sunlight.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05">Specification　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">Premium class</h2>
            <h3 class="big">ZIL520</h3>
            <h3>Luxury trip with luxury equipment</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
            </div>
            <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg" /></a></p>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03">Specification　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">High class</h2>
            <h3 class="big">CORDE BUNKS</h3>
            <h3>Recommend for Family or group</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01">Specification　＞</a></p>
        </section>
        <section>
            <h2 class="headline01">High class</h2>
            <h3 class="big">CORDE LEAVES</h3>
            <h3>Relaxing trip for a couple</h3>
            <div class="main_photo cf">
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=02">Specification　＞</a></p>
        </section>

        <section>
            <h2 class="headline01">High class</h2>
            <h3 class="big">CORDE RUNDY</h3>
            <h3>A caravan for trip with pets</h3>
            <div class="main_photo cf">
                <div class="coming-soon">
                    <div class="inner">Coming Soon
                    </div>
                </div>
                <div class="car_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/car_pic_rundy.jpg" />
                </div>
                <!-- car_photo -->
                <div class="side_photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926" />
                </div>
                <!-- side_photo -->
            </div>
            <!-- main_photo -->
            <div class="pet-hosoku">
                <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
            </div>
            <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04">Specification　＞</a></p>
        </section>

    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '05'):?>
    <section class="car sunlight" id="c05">

        <h2 class="headline01 typesquare_tags">SUNLIGHT</h2>
        <h3>Explore Hokkaido in refined elegance
        </h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_sunlight.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <!--        <p class="kome1">In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.</p>-->
        <!--        <p class="kome1">5日以上のご利用で、千歳空港または札幌市内中心部のホテル等より無料送迎サービス付き</p>-->
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>

                <th colspan="2">車輛名　</th>
                <td width="174">サンライトＴ67</td>
                <td width="216">使いやすい国産大人気メーカーです</td>
            </tr>
            <tr>
                <th colspan="2">ベース車両　</th>
                <td>フィアット　デュカト</td>
                <td>最新型フィアットベースです</td>
            </tr>
            <tr>
                <th colspan="2">年式　</th>
                <td>平成３１年４月登録</td>
                <td>ピカピカの新車です</td>
            </tr>
            <tr>
                <th colspan="2">エンジン　</th>
                <td>ディーゼルターボ</td>
                <td>燃費が良いです</td>
            </tr>
            <tr>
                <th colspan="2">排気量　</th>
                <td>２３００ＣＣ</td>
                <td>エンジン音も静かですよ</td>
            </tr>
            <tr>
                <th colspan="2">駆動方式　</th>
                <td>ＦＦ ２ＷＤ</td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">ブレーキ　</th>
                <td>ＡＢＳ付き</td>
                <td></td>
            </tr>
            <tr>
                <th colspan="2">運転席　</th>
                <td>右ハンドル</td>
                <td>視界が良いので運転しやすいです</td>
            </tr>
            <tr>
                <th colspan="2">ミッション　</th>
                <td>6速コンフォートマティック</td>
                <td>ほぼオートマ間隔で乗れます</td>
            </tr>
            <tr>
                <th colspan="2">燃料　</th>
                <td>ディーゼル９０&#8467;</td>
                <td>ガソリンではありませんので注意を！</td>
            </tr>
            <tr>
                <th colspan="2">燃費　</th>
                <td>１リッター　約１１km前後</td>
                <td>低燃費で軽油は安いですのでお得</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">定員</th>
                <th width="27">乗車</th>
                <td>６名</td>
                <td>広々車内ですので余裕です</td>
            </tr>
            <tr>
                <th>就寝</th>
                <td>６名</td>
                <td>大人５＋子供１名の６名が寝れます</td>
            </tr>
            <tr>
                <th rowspan="3">サイズ</th>
                <th>全長</th>
                <td>７３５０㎜</td>
                <td>バックカメラ・ソナー完備で安心</td>
            </tr>
            <tr>
                <th>全幅</th>
                <td>２３２０㎜</td>
                <td></td>
            </tr>
            <tr>
                <th>全高</th>
                <td>２９１０㎜</td>
                <td>高さだけ気を付けてくださいね</td>
            </tr>
            <tr>
                <th rowspan="4">ベッド寸法</th>
                <th>プルダウンベッド</th>
                <td>１９５０㎜×１４００㎜<br>×１１００㎜</td>
                <td>大人2名が寝れます</td>
            </tr>
            <tr>
                <th>ダイネットベッド</th>
                <td>２１００㎜×１０００㎜</td>
                <td>一人でゆっくり寝れます</td>
            </tr>
            <tr>
                <th>リアベッド</th>
                <td>２１００㎜×８００㎜×２０５０㎜×８００㎜</td>
                <td>大人2名がゆったり</td>
            </tr>
            <tr>
                <th>リアベッド展開時</th>
                <td>２１００㎜×２１００㎜<br>×２０５０㎜</td>
                <td>大人2名とお子さんが並んでもゆったり</td>
            </tr>
            <tr>
                <th colspan="2">たばこ喫煙　</th>
                <td>禁煙</td>
                <td>禁煙となります　ご理解ください</td>
            </tr>
            <tr>
                <th colspan="2">ペット同乗　</th>
                <td>不可</td>
                <td>現在はペット不可とさせていただきます</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>設備</caption>
            <tr>
                <th>リアFFヒーター（LPガス）</th>
                <td>〇</td>
                <th>車内三口ガスコンロ (LPガス）</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>運転席エアコン</th>
                <td>〇</td>
                <th>社外用カセットガスコンロ（カセットガス）</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>リアクーラー</th>
                <td>×</td>
                <th>更衣室兼　トイレルーム </th>
                <td>〇</td>
            </tr>
            <tr>
                <th>２ドア大容量冷凍・冷蔵庫 </th>
                <td>〇</td>
                <th>カップホルダー付き大型テーブル</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>４か国語対応カーナビ</th>
                <td>〇</td>
                <th>カセットトイレ（緊急時利用タイプ）</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Bluetooth対応オーディオ　ＡＭ　ＦＭラジオ付き</th>
                <td>×</td>
                <th>シャワー（レンタルでは利用不可）</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応オーディオスピーカー</th>
                <td>〇</td>
                <th>サイドオーニング（使用はお断りしてます）</th>
                <td>×</td>
            </tr>
            <tr>
                <th>バックアイカメラ（後退時）</th>
                <td>〇</td>
                <th>３点式安全シートベルト装置</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>３方向ｶﾒﾗ＆ﾓﾆﾀｰ（前方+後方+直前直側）</th>
                <td>〇</td>
                <th>全窓網戸＋ブラインド</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>バックソナー</th>
                <td>〇</td>
                <th>出入り口ドア網戸</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ （カードは持参ください）</th>
                <td>〇</td>
                <th>大型換気口</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>車内１００ボルト電源（外部電源使用時）</th>
                <td>〇</td>
                <th>大型トランクルーム</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>車内１２ボルト電源</th>
                <td>〇</td>
                <th>携帯充電用USB電源</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>電動水道付きシンク</th>
                <td>〇</td>
                <th>最新型薄型テレビ</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>給水タンク122ℓ</th>
                <td>〇</td>
                <th>走行充電</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>排水タンク92ℓ</th>
                <td>〇</td>
                <th>温水システム（ガス式）</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>運転席マップ利用時スマホホルダー</th>
                <td>〇</td>
                <th>サンルーフ３ヵ所</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>排水タンク92ℓ</th>
                <td>〇</td>
                <th>温水システム（ガス式）</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>８㎏ガスボンベ２本搭載</th>
                <td>〇</td>
                <th>運転席シート回転式</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>サブバッテリー</th>
                <td>〇</td>
                <th>運転席ipad 設置ホルダー</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>省エネＬＥＤ照明</th>
                <td>〇</td>
                <th>運転席スマホ設置ホルダー</th>
                <td>〇</td>
            </tr>
        </table>
        <p class="kome1 mb">※内装色は車輛により一部異なります事をご了承下さい</p>
        <div class="camera" id="camera-sunlignt">
            <h3>３６０度カメラで疑似体験！</h3>
            <ul class="cf">
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/qNaqtRzu9lHAFk6dV6Cs1whFo" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">セカンドベッド使用時
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/nJTf8mBifuveF86mZNBJDFLlI" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">トイレルーム
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/rlcoEYsj0X58Bf4NyZQQdDrl2" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">トリプルベッド使用時
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/abx5uwU4BT2xpChXGmQHx5MY4" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">車輌前方
                    </p>
                </li>
                <li>
                    <div class="photo">
                        <blockquote data-width="720" class="ricoh-theta-spherical-image"><a href="https://theta360.com/s/fqqzXJFaL3G9yWwyRGVa8KHRI" target="_blank"></a></blockquote>
                        <script async src="https://theta360.com/widgets.js" charset="utf-8"></script>
                    </div>
                    <p class="text">車輌中央
                    </p>
                </li>
            </ul>
        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo01.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo01.jpg">
                            <p class="text">高くて視界が良い運転席</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo02.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo02.jpg">
                            <p class="text">インパネまわり</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo03.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo03.jpg">
                            <p class="text">6速コンフォートマティック　ほぼATに近い動きです</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo04.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo04.jpg">
                            <p class="text">12VシガーソケットとUSBからスマホ電源がとれます</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo05.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo05.jpg">
                            <p class="text">スマホからipadまで設置できる専用ホルダー</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo06.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo06.jpg">
                            <p class="text">運転席スマホ設置ホルダー付き　GPSナビもこれで安心</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo07.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo07.jpg">
                            <p class="text">運転席上部には大型サンルーフ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo08.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo08.jpg">
                            <p class="text">サイドシートに2名上乗車</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo09.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo09.jpg">
                            <p class="text">通常走行時のシートアレンジ１</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo10.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo10.jpg">
                            <p class="text">通常走行時のシートアレンジ２</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo11.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo11.jpg">
                            <p class="text">回転式運転席及び助手席</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo12.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo12.jpg">
                            <p class="text">宴会モードのシートアレンジ6名が囲めます</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo13.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo13.jpg">
                            <p class="text">セカンドシートをダイネットベッドにした状態大人1名が就寝</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo14.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo14.jpg">
                            <p class="text">運転席から後方</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo15.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo15.jpg">
                            <p class="text">使い安いキッチンスペース</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo16.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo16.jpg">
                            <p class="text">本格的三ツ口LPガスコンロ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo17.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo17.jpg">
                            <p class="text">水道と大型シンク</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo18.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo18.jpg">
                            <p class="text">キッチン換気システム</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo19.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo19.jpg">
                            <p class="text">キッチンまわりは収納がたっぷり</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo20.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo20.jpg">
                            <p class="text">大型冷凍冷蔵庫（LPガスまたは電気自動切り替え式）</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo21.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo21.jpg">
                            <p class="text">上部にも収納がたっぷり</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo22.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo22.jpg">
                            <p class="text">トイレルーム</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo23.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo23.jpg">
                            <p class="text">トイレはカセット式（レンタル時は水洗での利用はできません）</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo24.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo24.jpg">
                            <p class="text">洗面化粧台</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo25.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo25.jpg">
                            <p class="text">プルダウンベッドを下ろすスイッチ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo26.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo26.jpg">
                            <p class="text">プルダウンベッド大人2名が就寝できます　</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo27.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo27.jpg">
                            <p class="text">上がプルダウンベッド　下がダイネットベッド　</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo28.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo28.jpg">
                            <p class="text">プルダウンベッドの専用はしご</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo29.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo29.jpg">
                            <p class="text">プルダウンベッド　天井の天窓から星空が見えます</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo30.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo30.jpg">
                            <p class="text">FFヒータースイッチ　上が温水用　下の炎マークが暖房＋温水用　</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo31.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo31.jpg">
                            <p class="text">100V電気ブレーカー</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo32.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo32.jpg">
                            <p class="text">ヒーター送風口と100Vコンセント</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo33.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo33.jpg">
                            <p class="text">集中電源パネル</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo34.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo34.jpg">
                            <p class="text">室内にはたくさんの収納があります</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo35.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo35.jpg">
                            <p class="text">キッチン上部天窓</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo36.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo36.jpg">
                            <p class="text">運転席上部天窓</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo37.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo37.jpg">
                            <p class="text">後方ベッドルーム上部天窓</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo38.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo38.jpg">
                            <p class="text">トイレルーム上部天窓</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo39.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo39.jpg">
                            <p class="text">リアベッドルーム　2名就寝仕様</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo40.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo40.jpg">
                            <p class="text">リアベッドより室内方面</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo41.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo41.jpg">
                            <p class="text">リアベッド　トリプル（3人）就寝仕様</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo42.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo42.jpg">
                            <p class="text">リアベッドトリプル利用時はしご</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo43.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo43.jpg">
                            <p class="text">リアベッド上部にもたくさんの収納が有り</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo44.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo44.jpg">
                            <p class="text">助手席の後ろに給油口（燃料は軽油です）</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo45.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo45.jpg">
                            <p class="text">給水タンク入り口</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo46.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo46.jpg">
                            <p class="text">外部100電源ケーブル接続口</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo47.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo47.jpg">
                            <p class="text">カセットトイレ取り出し口</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo48.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo48.jpg">
                            <p class="text">LPガス8リットル×2本搭載</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo49.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo49.jpg">
                            <p class="text">電動ステップ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo50.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo50.jpg">
                            <p class="text">リア大容量トランクルーム　自転車2台がそのまま入る大きさ</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo51.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo51.jpg">
                            <p class="text">リアトランクルームには照明　暖房も入ります</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo52.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo52.jpg">
                            <p class="text">車輌正面</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo53.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo53.jpg">
                            <p class="text">車輌右側面</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo54.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo54.jpg">
                            <p class="text">車輌左側面</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo55.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo55.jpg">
                            <p class="text">車輌後方</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo56.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo56.jpg">
                            <p class="text">車輌右前方</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo57.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo57.jpg">
                            <p class="text">車輌右後方</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo58.jpg" data-lightbox="sunlight">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_sunlight_photo58.jpg">
                            <p class="text">車輌の高さは約3メートルです</p>
                    </a></li>
            </ul>
            <p class="kome2">※内装色は車輛により一部異なります事をご了承下さい</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '03'):?>
    <section class="car bunks" id="c03">

        <h2 class="headline01 typesquare_tags">ZIL520</h2>
        <h3>Luxury trip with luxury equipment</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
        </div>
        <p class="oneplanet"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg" /></a></p>
        <!--        <p class="kome1">Free pick up service at New Chitose Airport or hotels in Sapporo when you rent for five days or more</p>-->
        <p class="kome1">In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>

                <th colspan="2">Name</th>
                <td width="174">Vantech ZIL520</td>
                <td width="216">Popular, user-friendly, Japanese manufacturer</td>
            </tr>
            <tr>
                <th colspan="2">Base vehicle</th>
                <td>TOYOTA CAMROAD</td>
                <td>Special RV chassis</td>
            </tr>
            <tr>
                <th colspan="2">Year</th>
                <td>2016 September registed</td>
                <td>Shiny new car</td>
            </tr>
            <tr>
                <th colspan="2">Engine</th>
                <td>Turbo charged diesel</td>
                <td>With powerful engine it’s easy to drive even in the mountains!</td>
            </tr>
            <tr>
                <th colspan="2">Displacement</th>
                <td>3000cc</td>
                <td>Quiet engine </td>
            </tr>
            <tr>
                <th colspan="2">Drive system</th>
                <td>Full time 4WD</td>
                <td>Safe on winter roads</td>
            </tr>
            <tr>
                <th colspan="2">Fuel</th>
                <td>Diesel</td>
                <td>It is not a gasoline engine!</td>
            </tr>
            <tr>
                <th colspan="2">Fuel consumption </th>
                <td>Per 1 liter<br />
                    ７～１１km</td>
                <td>High fuel efficiency and diesel is cheap</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">Seating capacity</th>
                <th width="27">Driving</th>
                <td>６</td>
                <td>Roomy inside</td>
            </tr>
            <tr>
                <th>Sleeping</th>
                <td>５</td>
                <td>Comfortable for 5 </td>
            </tr>
            <tr>
                <th rowspan="3">Dimensions</th>
                <th>Length</th>
                <td>５１６０㎜</td>
                <td>Shorter than an ordinary Japanese van</td>
            </tr>
            <tr>
                <th>Width</th>
                <td>２１１０㎜</td>
                <td>Able to be parked in normal car parking spaces</td>
            </tr>
            <tr>
                <th>Height</th>
                <td>３１５０㎜</td>
                <td>Please be aware of the height</td>
            </tr>
            <tr>
                <th colspan="2">Minimum turning radius</th>
                <td>４.９ｍ</td>
                <td>Same as a normal car</td>
            </tr>
            <tr>
                <th colspan="2">Smoking</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
            <tr>
                <th colspan="2">Pets</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>equipment</caption>
            <tr>
                <th>Air conditioner</th>
                <td>〇</td>
                <th>Inside gas stove</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>FF heater</th>
                <td>〇</td>
                <th>Gas cartridge stove for outside use</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear cooler</th>
                <td>〇</td>
                <th>Changing room / Portable bathroom)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Air conditioner for back cabin（only external power connected）</th>
                <td>○</td>
                <th>Large table with drink holders</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Large refrigerator</th>
                <td>〇</td>
                <th>Toilet</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Car navigation system ( 4 language support )</th>
                <td>〇</td>
                <th>Shower</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Audio ( Bluetooth, AM, FM radio)</th>
                <td>〇</td>
                <th>Side awning</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>Three-point seat belts</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>High quality 4-speaker system</th>
                <td>〇</td>
                <th>LED room light</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Portable DVD player ( extra charge )</th>
                <td>△</td>
                <th>Screens and blinds for all windows</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear camera ( Activated all times)</th>
                <td>〇</td>
                <th>Screen for entrance door</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ</th>
                <td>〇</td>
                <th>Large fan</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>100V Power outlet inside ( Only external power connected)</th>
                <td>〇</td>
                <th>Garbage storage outside</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>12V Power outlet inside</th>
                <td>〇</td>
                <th>USB power outlet for phones</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Sink with electric powered tap</th>
                <td>〇</td>
                <th>Tablet PC to check car manual</th>
                <td>〇</td>
            </tr>

            <tr>
                <th>The latest flat model TV</th>
                <td>〇</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>


        <p class="kome1 mb">*Please understand that the color inside is different depends on a car</p>
        <div class="camera" id="camera-zil">
            <h3>Simulate with 360°camera !</h3>
            <ul class="cf">
                <li>
                    <a title='ジル５２０　車輛後方右' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490295'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_01.jpg"></p>
                        <h4>Back right 360°camera</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛後方トイレ' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475486807'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_02.jpg"></p>
                        <h4>Rear bathroom 360°camera</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛前方就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475490809'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg"></p>
                        <h4>Front bed 360°camera</h4>
                    </a></li>

                <li>
                    <a title='ジル５２０　車輛前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468408427&s=s1475487141'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_04.jpg"></p>
                        <h4>Front 360°camera</h4>
                    </a></li>
            </ul>
        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo01.jpg">
                            <p class="text">12V adapter</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo02.jpg">
                            <p class="text">USB charger adapter</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo03.jpg">
                            <p class="text">Air conditioning! Available with 100V power outlet connected</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo04.jpg">
                            <p class="text">Kitchen　Gas stove 　Sink</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo05.jpg">
                            <p class="text">Gas stove and Sink storage</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo06.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">Driver's seat cup holder</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo07.jpg">
                            <p class="text">Driver's seat height provides enough visual range</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo08.jpg">
                            <p class="text">Brand new navigation gives you comfortable driving</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo09.jpg">
                            <p class="text">100V power outlets</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo10.jpg">
                            <p class="text">Comfortable drive with rear camera recording all the time</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo11.jpg">
                            <p class="text">Switches for the lights are organized compactly</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo12.jpg">
                            <p class="text">Lighting and 100V power outlets in bedroom</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo13.jpg">
                            <p class="text">Voltmeter to check remaining power</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo14.jpg">
                            <p class="text">The upper area of the driver's seat transforms into a bedroom. Wide enough to sleep three adults.</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo15.jpg">
                            <p class="text">Large, 90-liter fridge</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo16.jpg">
                            <p class="text">Entrance mirror convenient for women</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo17.jpg">
                            <p class="text">Main large table seats four</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo18.jpg">
                            <p class="text">Plenty of storage space in kitchen</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo19.jpg">
                            <p class="text">Well furnished bathroom in ZIL520</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo20.jpg">
                            <p class="text">Table folds away into full flat bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo21.jpg">
                            <p class="text">Table folds away into full flat bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo22.jpg">
                            <p class="text">From the back side of the main table</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo23.jpg">
                            <p class="text">Lights and power outlets in the rear bunk bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo24.jpg">
                            <p class="text">A ladder to reach bed room in upper area of driver's seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo25.jpg">
                            <p class="text">USB power outlets for charging smart phones</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo26.jpg">
                            <p class="text">From the back side of the room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo27.jpg">
                            <p class="text">Storage inside</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo28.jpg">
                            <p class="text">Ventilation windows in rear bunk bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo29.jpg">
                            <p class="text">From front side</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo30.jpg">
                            <p class="text">From front side </p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo31.jpg">
                            <p class="text">Easy operation with centralized switch panel</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo32.jpg">
                            <p class="text">There is plenty of indirect lighting</p>
                    </a></li>



                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo34.jpg">
                            <p class="text">Screen is provided</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo35.jpg">
                            <p class="text">Sensor light in the entrance for safety in the night</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo33.jpg">
                            <p class="text">External 100V power connection</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo36.jpg">
                            <p class="text">Rear trunk for wet things, etc.</p>
                    </a></li>




                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo37.jpg">
                            <p class="text">Large rear storage area</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo38.jpg">
                            <p class="text">Large rear storage area</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo39.jpg">
                            <p class="text">Water tap in rear trunk</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo40.jpg">
                            <p class="text">Water tank is reachable through rear left door</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo41.jpg">
                            <p class="text">Easy boarding with electric powered step</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo42.jpg">
                            <p class="text">The fuel tank is located behind the driver's door. Diesel only.</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo43.jpg">
                            <p class="text">Rear</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo44.jpg">
                            <p class="text">Right</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo45.jpg">
                            <p class="text">Rear left</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo46.jpg">
                            <p class="text">Front right </p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo47.jpg">
                            <p class="text">Front left</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo48.jpg">
                            <p class="text">Left</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg" data-lightbox="zil">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_zil_photo49.jpg">
                            <p class="text">Front</p>
                    </a></li>

            </ul>
            <p class="kome2">*Interior color varies.</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>
    <?php if($_GET['type'] == '01'):?>
    <section class="car zil" id="c01">
        <h2 class="headline01 typesquare_tags">CORDE BUNKS</h2>

        <h3>Recommend for Family or group</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
        </div>
        <!--        <p class="kome1">Free pick up service at New Chitose Airport or hotels in Sapporo when you rent for six days or more</p>-->
        <p class="kome1">In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">Name</th>
                <td width="174">Vantech CORDE BUNKS</td>
                <td width="216">Popular, user-friendly, Japanese manufacturer</td>
            </tr>
            <tr>
                <th colspan="2">Base vehicle</th>
                <td>TOYOTA CAMROAD</td>
                <td>Special RV chassis</td>
            </tr>
            <tr>
                <th colspan="2">Year</th>
                <td>2016 June registed</td>
                <td>Shiny new car</td>
            </tr>
            <tr>
                <th colspan="2">Engine</th>
                <td>Turbo charged diesel</td>
                <td>With powerful engine it’s easy to drive even in the mountains!</td>
            </tr>
            <tr>
                <th colspan="2">Displacement</th>
                <td>3000cc</td>
                <td>Quiet engine </td>
            </tr>
            <tr>
                <th colspan="2">Drive system</th>
                <td>Full time 4WD</td>
                <td>Safe on winter roads</td>
            </tr>
            <tr>
                <th colspan="2">Fuel</th>
                <td>Diesel</td>
                <td>It is not a gasoline engine!</td>
            </tr>
            <tr>
                <th colspan="2">Fuel consumption </th>
                <td>Per 1 liter<br />
                    8～12km</td>
                <td>High fuel efficiency and diesel is cheap</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">Seating capacity</th>
                <th width="27">Driving</th>
                <td>７</td>
                <td>Roomy inside</td>
            </tr>
            <tr>
                <th>Sleeping</th>
                <td>５</td>
                <td>Comfortable for 5 </td>
            </tr>
            <tr>
                <th rowspan="3">Dimensions</th>
                <th>Length</th>
                <td>４９９５㎜</td>
                <td>Shorter than an ordinary Japanese van</td>
            </tr>
            <tr>
                <th>Width</th>
                <td>１９８０㎜</td>
                <td>Able to be parked in normal car parking spaces</td>
            </tr>
            <tr>
                <th>Height</th>
                <td>３１５０㎜</td>
                <td>Please be aware of the height</td>
            </tr>
            <tr>
                <th colspan="2">Minimum turning radius</th>
                <td>４.９ｍ</td>
                <td>Same as a normal car</td>
            </tr>
            <tr>
                <th colspan="2">Smoking</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
            <tr>
                <th colspan="2">Pets</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>equipment</caption>
            <tr>
                <th>Air conditioner</th>
                <td>〇</td>
                <th>Inside gas stove</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>FF heater</th>
                <td>〇</td>
                <th>Gas cartridge stove for outside use</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear cooler</th>
                <td>〇</td>
                <th>Changing room / Portable bathroom)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Air conditioner for back cabin（only external power connected）</th>
                <td>×</td>
                <th>Large table with drink holders</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Large refrigerator</th>
                <td>〇</td>
                <th>Toilet(option)</th>
                <td>△</td>
            </tr>
            <tr>
                <th>Car navigation system ( 4 language support )</th>
                <td>〇</td>
                <th>Shower</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Audio ( Bluetooth, AM, FM radio)</th>
                <td>〇</td>
                <th>Side awning</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>Three-point seat belts</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>High quality 4-speaker system</th>
                <td>〇</td>
                <th>LED room light</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Portable DVD player ( extra charge )</th>
                <td>△</td>
                <th>Screens and blinds for all windows</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear camera ( Activated all times)</th>
                <td>〇</td>
                <th>Screen for entrance door</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ</th>
                <td>〇</td>
                <th>Large fan</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>100V Power outlet inside ( Only external power connected)</th>
                <td>〇</td>
                <th>Garbage storage outside</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>12V Power outlet inside</th>
                <td>〇</td>
                <th>USB power outlet for phones</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Sink with electric powered tap</th>
                <td>〇</td>
                <th>Tablet PC to check car manual</th>
                <td>〇</td>
            </tr>

            <tr>
                <th>The latest flat model TV </th>
                <td>×</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>

        <p class="kome1 mb">*Interior color is different by each car</p>
        <div class="camera" id="camera-bunks">
            <h3>Simulate with 360°camera !</h3>
            <ul class="cf">
                <li>
                    <a title='コルドバンクス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468316834'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_01.jpg"></p>
                        <h4>Back right 360°camera</h4>
                    </a></li>

                <li>
                    <a title='コルドバンクス　車両後方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313153'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_02.jpg"></p>
                        <h4>Rear bathroom 360°camera</h4>
                    </a></li>

                <li>
                    <a title='コルドバンクス　就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468313123'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car01_03.jpg"></p>
                        <h4>Bed 360°camera</h4>
                    </a></li>
            </ul>

        </div>
        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo01.jpg" alt="運転席も広々">
                            <p class="text">Roomy drivers seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo02.jpg" alt="運転席上部はベッドルームに大人３名が就寝できる広さです">
                            <p class="text">Wide enough for 3 adults sleeping in the upper area of the driver's seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo03.jpg" alt="車内には各所に照明設置">
                            <p class="text">Lights are installed in the points</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo04.jpg" alt="広いテーブルを囲み快適な空間">
                            <p class="text">Comfortable room with wide table</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo05.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心">
                            <p class="text">Comfortable drive with rear camera recording all the time</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo06.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">Foreign language navigation support</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo07.jpg" alt="すべての窓には網戸も装備">
                            <p class="text">Screen is provided on all windows</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo08.jpg" alt="大容量６０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください">
                            <p class="text">60L capacity fridge/freezer Mainly use while driving </p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo09.jpg" alt="車内４スピーカーシステム搭載">
                            <p class="text">High quality 4 speaker system is installed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo10.jpg" alt="軽油ベバストＦＦヒーター付きで冬でも暖かいです">
                            <p class="text">Diesel FF heater is provided for warm driving in winter</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo11.jpg" alt="100ボルト電源（外部電源使用時のみ使用できます）">
                            <p class="text">100V Power outlet inside ( Only external power connected)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo12.jpg" alt="白を基調とした車内。中央にキッチンと冷蔵庫があります">
                            <p class="text">White interior Kitchen and fridge is in center</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo13.jpg" alt="ＬＥＤランプ設置">
                            <p class="text">LED lights are equipped</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo14.jpg" alt="ユーティリティルームはポータブルトイレ設置や着替えルームに">
                            <p class="text">Utility room is for potable bathroom or changing room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo15.jpg" alt="後方に２段ベッドがあるのでお子様にも大人気">
                            <p class="text">Bank bed is in rear Kids will like it</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo16.jpg" alt="キッチン横には棚も収納">
                            <p class="text">Shelf is by the kitchen</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo17.jpg" alt="上部にはＬＥＤ照明と棚があります">
                            <p class="text">LED lights and shelf is above</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo18.jpg" alt="車両後方大容量トランクには上水道用２０リットルタンク">
                            <p class="text">Water tank is in rear trunk</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo19.jpg" alt="外部からの吸気換気システム完備。雨の日でも使用できます">
                            <p class="text">Ventilation system is installed and okay to use when it rains</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo20.jpg" alt="広々としたリビングルーム">
                            <p class="text">Roomy living room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo21.jpg" alt="入口横にはシングルシートがあります">
                            <p class="text">Single seat is by the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo22.jpg" alt="後方２段ベッド">
                            <p class="text">Rear bank bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo23.jpg" alt="キッチン下にもたくさんの収納">
                            <p class="text">Large storage under the kitchen</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo24.jpg" alt="車輛入口にある集中電源。上からメインスイッチ・クーラー・照明">
                            <p class="text">Centralized switch panel. Main is on the top, A/C is middle and lights are on the bottom</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo25.jpg" alt="車内キッチン・ガスコンロ（市販のカセットコンロを使用できます）">
                            <p class="text">Kitchen gas stove ( you can use cartridge gas stove also)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo26.jpg" alt="シンクとキッチンです">
                            <p class="text">Sink and kitchen</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo27.jpg" alt="入口は車輛横中央部から。網戸も完備・夏も安心">
                            <p class="text">The entrance is in the middle. Screen is provided so comfortable in the summer</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo28.jpg" alt="車両後方・大容量トランクルームに荷物もたっぷり収納">
                            <p class="text">Rear large trunk. Big luggage can fit</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo29.jpg" alt="家族グループ向きの車輛で大容量トランクも有ります">
                            <p class="text">Family or group use car and large trunk is provided</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo30.jpg" alt="キッチン部">
                            <p class="text">Kitchen room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo31.jpg" alt="収納もたっぷり">
                            <p class="text">Large storage</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo32.jpg" alt="夜間はブラインドで光を完全遮光">
                            <p class="text">You can shut the light with blinds in the night</p>
                    </a></li>



                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo34.jpg" alt="後方２段ベッドにはそれぞれ小窓があります">
                            <p class="text">Window is provided for each bank bed</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo35.jpg" alt="後方２段ベット照明２段階明るさ調整有り">
                            <p class="text">2 steps light adjuster in rear bank bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo33.jpg" alt="車輛側面">
                            <p class="text">Side</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo36.jpg" alt="車両右前方">
                            <p class="text">Right front</p>
                    </a></li>




                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo37.jpg" alt="車両右側面">
                            <p class="text">Right side</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo38.jpg" alt="車両左後方">
                            <p class="text">Rear</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo39.jpg" alt="車両左側面">
                            <p class="text">Left side</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo40.jpg" alt="車両前方">
                            <p class="text">Front</p>
                    </a></li>

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_photo41.jpg" alt="車輛左前方">
                            <p class="text">Left front</p>
                    </a></li>

            </ul>
            <p class="kome2">*Interior color is different by each car</p>

        </div><!-- gallery -->
    </section>
    <?php endif; ?>
    <?php if($_GET['type'] == '02'):?>
    <section class="car leaves" id="c02">
        <h2 class="headline01 typesquare_tags">CORDE LEAVES</h2>

        <h3>Relaxing trip for a couple</h3>
        <div class="main_photo cf">
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" />
            </div>
            <!-- car_photo -->
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png" />
            </div>
            <!-- side_photo -->
        </div>
        <!-- main_photo -->
        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
        </div>
        <!--        <p class="kome1">Free pick up service at New Chitose Airport or hotels in Sapporo when you rent for six days or more</p>-->
        <p class="kome1">In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">Name</th>
                <td width="174">Vantech CORDE LEAVES</td>
                <td width="216">Popular, user-friendly, Japanese manufacturer</td>
            </tr>
            <tr>
                <th colspan="2">Base vehicle</th>
                <td>TOYOTA CAMROAD</td>
                <td>Special RV chassis</td>
            </tr>
            <tr>
                <th colspan="2">Year</th>
                <td> 2016 June registed</td>
                <td>Shiny new car</td>
            </tr>
            <tr>
                <th colspan="2">Engine</th>
                <td>Turbo charged diesel</td>
                <td>With powerful engine it’s easy to drive even in the mountains!</td>
            </tr>
            <tr>
                <th colspan="2">Displacement</th>
                <td>3000cc</td>
                <td>Quiet engine </td>
            </tr>
            <tr>
                <th colspan="2">Drive system</th>
                <td>Full time 4WD</td>
                <td>Safe on winter roads</td>
            </tr>
            <tr>
                <th colspan="2">Fuel</th>
                <td>Diesel</td>
                <td>It is not a gasoline engine!</td>
            </tr>
            <tr>
                <th colspan="2">Fuel consumption </th>
                <td>Per 1 liter<br />
                    8～12km</td>
                <td>High fuel efficiency and diesel is cheap</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">Seating capacity</th>
                <th width="27">Driving</th>
                <td>７</td>
                <td>Roomy inside</td>
            </tr>
            <tr>
                <th>Sleeping</th>
                <td>５</td>
                <td>Comfortable for 5 </td>
            </tr>
            <tr>
                <th rowspan="3">Dimensions</th>
                <th>Length</th>
                <td>４９９５㎜</td>
                <td>Shorter than an ordinary Japanese van</td>
            </tr>
            <tr>
                <th>Width</th>
                <td>１９８０㎜</td>
                <td>Able to be parked in normal car parking spaces</td>
            </tr>
            <tr>
                <th>Height</th>
                <td>３１５０㎜</td>
                <td>Please be aware of the height</td>
            </tr>
            <tr>
                <th colspan="2">Minimum turning radius</th>
                <td>４.９ｍ</td>
                <td>Same as a normal car</td>
            </tr>
            <tr>
                <th colspan="2">Smoking</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
            <tr>
                <th colspan="2">Pets</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
        </table>

        <table cellspacing="0" cellpadding="0" class="checktable">
            <caption>equipment</caption>
            <tr>
                <th>Air conditioner</th>
                <td>〇</td>
                <th>Inside gas stove</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>FF heater</th>
                <td>〇</td>
                <th>Gas cartridge stove for outside use</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear cooler</th>
                <td>×</td>
                <th>Changing room / Portable bathroom)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Air conditioner for back cabin（only external power connected）</th>
                <td>○</td>
                <th>Large table with drink holders</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Large refrigerator</th>
                <td>〇</td>
                <th>Toilet(option)</th>
                <td>△</td>
            </tr>
            <tr>
                <th>Car navigation system ( 4 language support )</th>
                <td>〇</td>
                <th>Shower</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Audio ( Bluetooth, AM, FM radio)</th>
                <td>〇</td>
                <th>Side awning</th>
                <td>×</td>
            </tr>
            <tr>
                <th>Bluetooth対応ドックスピーカー</th>
                <td>〇</td>
                <th>Three-point seat belts</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>High quality 4-speaker system</th>
                <td>〇</td>
                <th>LED room light</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Portable DVD player ( extra charge )</th>
                <td>△</td>
                <th>Screens and blinds for all windows</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear camera ( Activated all times)</th>
                <td>〇</td>
                <th>Screen for entrance door</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ</th>
                <td>〇</td>
                <th>Large fan</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>100V Power outlet inside ( Only external power connected)</th>
                <td>〇</td>
                <th>Garbage storage outside</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>12V Power outlet inside</th>
                <td>〇</td>
                <th>USB power outlet for phones</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Sink with electric powered tap</th>
                <td>〇</td>
                <th>Tablet PC to check car manual</th>
                <td>〇</td>
            </tr>

            <tr>
                <!--
                <th>無料WI-FI（1日500MBまで）</th>
                <td>〇</td>
-->
                <th>The latest flat model TV</th>
                <td>×</td>
                <th>100V変換インバーター(ノートパソコン対応)</th>
                <td>〇</td>
            </tr>

        </table>

        <p class="kome1 mb">*Interior color is different by each car</p>
        <div class="camera" id="camera-leaves">

            <h3>Simulate with 360°camera !</h3>
            <ul class="cf">
                <li>
                    <a title='コルドリーブス　車両前方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468315363'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_01.jpg"></p>
                        <h4>Front 360°camera</h4>
                    </a></li>

                <li>
                    <a title='コルドリーブス　車両後方' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468314209'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car02_02.jpg"></p>
                        <h4>Rear 360°camera</h4>
                    </a></li>

                <li>
                    <a title='コルドリーブス　就寝仕様' target="_blank" href='http://360player.net/viewerController?u=u1468316831&p=p1468315105&s=s1468320525'>
                        <p class="iframe"><img src="<?php bloginfo('template_url'); ?>/images/camera360_car03_03.jpg"></p>
                        <h4>Bed room 360°camera</h4>
                    </a></li>
            </ul>

        </div>

        <div class="gallery">
            <ul class="cf">

                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo01.jpg" alt="ゆったりと広い運転席">
                            <p class="text">Roomy driver's seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo02.jpg" alt="広々とした室内。運転席上部のベットは簡単に跳ね上げて収納できます">
                            <p class="text">Roomy inside. Bed in upper area of driver's seat can be folded easily</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo03.jpg" alt="運転席上部のベットを下した状態。大人２名が就寝できます">
                            <p class="text">Bed in upper area unfolded. Wide enough for 2 adults sleeping</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo04.jpg" alt="運転席上部ベットスペースにも小窓があります">
                            <p class="text">Small window is on the wall of the bed room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo05.jpg" alt="安全な３点式シートベルト装備">
                            <p class="text">Safety three-point seat belts</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo06.jpg" alt="光あふれるリビングルーム">
                            <p class="text">Bright living room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo07.jpg" alt="広い窓とテーブル">
                            <p class="text">Big windows and wide table</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo08.jpg" alt="車内はＬＥＤ照明が多数">
                            <p class="text">Many LED lights inside</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo09.jpg" alt="ルームエアコン装備（外部電源をつないだ時のみ使用可能）">
                            <p class="text">Air conditioning! Available with 100V power outlet connected</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo10.jpg" alt="車内４スピーカーシステム搭載">
                            <p class="text">4-speaker system is installed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo11.jpg" alt="シンク・コンロ・キッチンまわり">
                            <p class="text">Kitchen sink gas stove</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo12.jpg" alt="キッチン下部にも食器セット等を収納できます">
                            <p class="text">Sink storage</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo13.jpg" alt="車内ガスコンロ（主にお湯を沸かす程度にご利用くださいませ）">
                            <p class="text">Inside gas stove ( for boiling water only)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo14.jpg" alt="車内キッチン用カセットガスコンロ（市販のカセットコンロをご使用できます）">
                            <p class="text">Inside gas stove (Normal gas cartridge is okay)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo15.jpg" alt="シンク２０リットルの上水道（冬季は利用不可）">
                            <p class="text">20 liters of tap water(not available in winter)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo16.jpg" alt="カセットコンロガス漏れ警報機設置で万が一も安心">
                            <p class="text">Gas leaking alert is installed for your safety</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo17.jpg" alt="テーブルを囲みお食事や読書に最適です。">
                            <p class="text">The table is the best for meal or reading</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo18.jpg" alt="ポータブルテレビ等の台に">
                            <p class="text">Good place for Portable TV to be on</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo19.jpg" alt="大容量９０リットル冷凍・冷蔵庫。主に走行中と外部電源使用時に冷やしてください">
                            <p class="text">Large refrigerator(90-liters) Recommend for cooing when you are driving or external power is connected</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo20.jpg" alt="各部に収納棚設置">
                            <p class="text">Many shelves and storages</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo21.jpg" alt="ユーティリティルーム。トイレや着替えに防水なので濡れたものもＯＫ">
                            <p class="text">Utility room. Good for portable bathroom or changing</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo22.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">Foreign language navigation support</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo23.jpg" alt="車輛後方視界カメラ。常時撮影で運転も安心">
                            <p class="text">Comfortable drive with rear camera recording all the time</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo24.jpg" alt="すべての窓には網戸も装備">
                            <p class="text">Screens and blinds for all windows</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo25.jpg" alt="夜間はブラインドで光を完全遮光">
                            <p class="text">Blinds shut lights in the night</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo26.jpg" alt="外部からの新鮮な空気の換気吸気装置。雨の日でも使用できます">
                            <p class="text">Ventilation system to get fresh air is installed. You can use it if it is rain</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo27.jpg" alt="運転席ドリンクホルダー">
                            <p class="text">Drink holder</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo28.jpg" alt="ＥＴＣ設置。カードはご持参ください">
                            <p class="text">ETC is installed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo29.jpg" alt="リビングルーム下部に集中電源装置の扉があります">
                            <p class="text">Centralized switch panel is under the living room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo30.jpg" alt="後方入口は防水加工なので汚れても安心">
                            <p class="text">Entrance in the back is water proof so easy to clean up</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo31.jpg" alt="後方入口横にはちょっとした買い物などを入れるスペースがあります">
                            <p class="text">Good space for small shopping near the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo32.jpg" alt="後方入口・足元灯">
                            <p class="text">Entrance in the back Foot light</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo33.jpg" alt="車両後方入口横には電源ケーブルが収納されてます">
                            <p class="text">Power cable is keep in the space by the entrance in the back</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo34.jpg" alt="乗り降りは車輛後方から">
                            <p class="text">You can get off and on through the entrance in the back</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo35.jpg" alt="軽油使用ＦＦベバストヒーター装備">
                            <p class="text">You can get off and on through the entrance in the back</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo36.jpg" alt="100Ｖコンセント（外部電源使用時のみ使用可能）12Ｖは常時使用可能">
                            <p class="text">FF heater is installed for diesel vehicle</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo37.jpg" alt="左からメインスイッチ・照明集中スイッチ・水道電源">
                            <p class="text">Main switch on the left, Light switch in the center, Tap switch on the right</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo38.jpg" alt="網戸も装備。暑い夏場も快適">
                            <p class="text">Screen is equipped for comfortable summer</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo39.jpg" alt="車両左にイスやロールテーブル等の収納（鍵で開けます）">
                            <p class="text">Storage in the left side for chairs and table(the key is needed)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo40.jpg" alt="上水道給水タンクは車輛後方に設置">
                            <p class="text">The tank for tap water is set in rear area</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo41.jpg" alt="燃料タンク（軽油）は車輛右前方の扉を開けてエンジンキーでロックを解除しフタを回して給油してください。">
                            <p class="text">The fuel tank is front right. Open the cover with the key of the car and refuel it</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo42.jpg" alt="車両右の扉内には工具類、ジャッキアップが収納されてます。ＢＢＱセット等を収納できるスペースとしてもご利用できます">
                            <p class="text">A jack and tools is in the storage on the right side of the car. You can also use the room for keep BBQ grill and stuff</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo43.jpg" alt="車両右後方には生ゴミなどを一時的に収納できます">
                            <p class="text">You can keep garbage in the storage in rear right of the car</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo44.jpg" alt="車両正面">
                            <p class="text">Front</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo45.jpg" alt="車両上部">
                            <p class="text">Upper side</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo46.jpg" alt="車両斜め前方">
                            <p class="text">Front right</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo47.jpg" alt="車両斜め後方">
                            <p class="text">Rear right</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo48.jpg" alt="車両後ろから">
                            <p class="text">Rear</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" data-lightbox="leaves">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_leaves_photo50.jpg" alt="車両左側面">
                            <p class="text">Left side</p>
                    </a></li>

            </ul>
            <p class="kome2">*Interior color is different by each car</p>
        </div><!-- gallery -->
    </section>
    <?php endif; ?>



    <?php if($_GET['type'] == '04'):?>
    <section class="car zil" id="c04">
        <!--    <h2><img src="<?php bloginfo('template_url'); ?>/images/car_bunks_h.jpg" alt="コルドランディ"/></h2> -->
        <h2 class="headline01 typesquare_tags">CORDE RUNDY</h2>


        <h3>A caravan for trip with pets</h3>
        <div class="main_photo cf">
            <div class="coming-soon">
                <div class="inner">Coming Soon</div>
            </div>
            <div class="car_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/car_lundy_main.jpg" alt="コルドランディ" />
            </div>
            <div class="side_photo">
                <img src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926" />
            </div>
        </div>

        <div class="pet-hosoku">
            <img src="<?php bloginfo('template_url'); ?>/images/pets_logo.png"><span>We also have a pet-enabled vehicle.</span>
        </div>
        <!--        <p class="kome1">Free pick up service at New Chitose Airport or hotels in Sapporo when you rent for six days or more</p>-->
        <p class="kome1">In winter season, If you use a car for skiing or snowbording, 3-4 person is maximum with those equipments.</p>
        <table cellspacing="0" cellpadding="0" class="info">

            <tr>
                <th colspan="2">Name</th>
                <td width="174">Vantech CORDE RUNDY</td>
                <td width="216">Popular, user-friendly, Japanese manufacturer</td>
            </tr>
            <tr>
                <th colspan="2">Base vehicle</th>
                <td>TOYOTA CAMROAD</td>
                <td>Special RV chassis</td>
            </tr>
            <tr>
                <th colspan="2">Year</th>
                <td>2016 August registed</td>
                <td>Pets allowed vehicle</td>
            </tr>
            <tr>
                <th colspan="2">Engine</th>
                <td>Turbo charged diesel</td>
                <td>With powerful engine it’s easy to drive even in the mountains!</td>
            </tr>
            <tr>
                <th colspan="2">Displacement</th>
                <td>3000cc</td>
                <td>Quiet engine </td>
            </tr>
            <tr>
                <th colspan="2">Drive system</th>
                <td>Full time 4WD</td>
                <td>Safe on winter roads</td>
            </tr>
            <tr>
                <th colspan="2">Fuel</th>
                <td>Diesel</td>
                <td>It is not a gasoline engine!</td>
            </tr>
            <tr>
                <th colspan="2">Fuel consumption </th>
                <td>Per 1 liter<br />
                    8～12km</td>
                <td>High fuel efficiency and diesel is cheap</td>
            </tr>
            <tr>
                <th width="56" rowspan="2">Seating capacity</th>
                <th width="27">Driving</th>
                <td>6</td>
                <td>Roomy inside</td>
            </tr>
            <tr>
                <th>Sleeping</th>
                <td>4</td>
                <td>Comfortable for 4 persons</td>
            </tr>
            <tr>
                <th rowspan="3">Dimensions</th>
                <th>Length</th>
                <td>４９９５㎜</td>
                <td>Shorter than an ordinary Japanese van</td>
            </tr>
            <tr>
                <th>Width</th>
                <td>１９８０㎜</td>
                <td>Able to be parked in normal car parking spaces</td>
            </tr>
            <tr>
                <th>Height</th>
                <td>２９６０㎜</td>
                <td>Please be aware of the height</td>
            </tr>
            <tr>
                <th colspan="2">Minimum turning radius</th>
                <td>４.９ｍ</td>
                <td>Same as a normal car</td>
            </tr>
            <tr>
                <th colspan="2">Smoking</th>
                <td>Not allowed</td>
                <td>Please understand</td>
            </tr>
            <tr>
                <th colspan="2">Pets</th>
                <td>OK</td>
                <td>Cleaning fee will be charged</td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="checktable mb">
            <caption>equipment</caption>
            <tr>
                <th>Air conditioner</th>
                <td>〇</td>
                <th>Inside gas stove</th>
                <td class="check">〇</td>
            </tr>
            <tr>
                <th>FF heater</th>
                <td>〇</td>
                <th>Gas cartridge stove for outside use</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear cooler</th>
                <td>×</td>
                <th>Changing room / Portable bathroom)</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>A/C ( Recommend to use only when external power is connected)</th>
                <td>〇</td>
                <th>Large table with drink holders</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Large refrigerator</th>
                <td>〇</td>
                <th>Flush toilets</th>
                <td>△</td>
            </tr>
            <tr>
                <th>Car navigation system ( 4 language support )</th>
                <td>〇</td>
                <th>Shower</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Audio ( Bluetooth, AM, FM radio)</th>
                <td>×</td>
                <th>Side awning（Please do not use）</th>
                <td>×</td>
            </tr>
            <tr>
                <th>High quality 4-speaker system</th>
                <td>〇</td>
                <th>Three-point seat belts</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Portable DVD player ( extra charge )</th>
                <td>△</td>
                <th>LED room light</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Rear camera ( Activated all times)</th>
                <td>〇</td>
                <th>Screens and blinds for all windows</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>ＥＴＣ</th>
                <td>〇</td>
                <th>Screen for entrance door</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>100V Power outlet inside a car ( Only external power connected)</th>
                <td>〇</td>
                <th>Large fan</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>12V Power outlet inside a car</th>
                <td>〇</td>
                <th>Garbage storage outside</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Sink with electric powered tap</th>
                <td>〇</td>
                <th>USB power outlet for phones</th>
                <td>〇</td>
            </tr>
            <tr>
                <th>Microwave (external power connection is recommended)</th>
                <td>〇</td>
                <th>Tablet PC to check car manual</th>
                <td>〇</td>
            </tr>
            <!--
            <tr>
                <th>Free Wi-Fi</th>
                <td>〇</td>
            </tr>-->

        </table>

        <div class="camera" id="camera-rundy">
            <h3>Simulate with 360°camera !</h3>
            <ul class="cf">
                <li>
                    <p class="iframe"><a title='コルドランディ　昼間室内全体３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500901394'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_01.jpg"></a></p>
                    <h4>Room view with 360°camera in the day</h4>
                </li>

                <li>
                    <p class="iframe"><a title='コルドランディ　夜間室内全体３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500967782'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_02.jpg"></a></p>
                    <h4>Room view with 360°camera in the night</h4>
                </li>

                <li>
                    <p class="iframe"><a title='コルドランディ　夜間室内前方３６０度カメラ' target="_blank" href='http://360player.net/viewerController.php?u=u1468316831&p=p1500906516&s=s1500908456'><img src="<?php bloginfo('template_url'); ?>/images/camera360_car04_03.jpg"></a></p>
                    <h4>Front view with 360°camera in the night</h4>
                </li>
            </ul>

        </div>

        <div class="gallery">
            <ul class="cf">
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo01.jpg" alt="運転席も広々">
                            <p class="text">Roomy driver's seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo02.jpg" alt="運転席上部のベッド（マットがつきます）">
                            <p class="text">Upper bed room of driver's seat</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo03.jpg" alt="運転席上部のベッドルーム左右に2名">
                            <p class="text">Upper bed room of driver's seat for two</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo04.jpg" alt="ＦＦヒータースイッチエンジン掛けなくてもOKです">
                            <p class="text">FF heater switch. No need to start engine</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo05.jpg" alt="液晶テレビ付き">
                            <p class="text">LCD TV</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo06.jpg" alt="100ボルト電源（１５００Wのインバータ付なのでプラグインがなくても使用できます）">
                            <p class="text">100V power outlet. (works without plugins because it has 1500W inverter)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo07.jpg" alt="汚れが付きにくく清掃がラクなレザーシート">
                            <p class="text">Leather seats Easy to clean up and dirt proof</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo08.jpg" alt="後部はフルフラットベットになります">
                            <p class="text">Rear seat is transformable into flat bed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo09.jpg" alt="エアコン付き">
                            <p class="text">AC is installed</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo10.jpg" alt="使い易いシャワー">
                            <p class="text">Easy operation shower</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo11.jpg" alt="外部からの吸気換気システム完備雨の日でも使用できます">
                            <p class="text">Ventilation system is equipped and okay to use when it rains</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo12.jpg" alt="後方にもたくさんの収納">
                            <p class="text">Roomy storages in the rear</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo13.jpg" alt="室内から運転席" /></p class="text">Driver's seat from the room</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo14.jpg" alt="車内キッチンガスコンロ（市販のカセットコンロを使用できます）">
                            <p class="text">Kitchen and gas stove ( Normal gas cartridge is okay)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo15.jpg" alt="広いキッチンガスコンロ付き">
                            <p class="text">Wide kitchen with gas stove</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo16.jpg" alt="車輛後方視界カメラ常時撮影で運転も安心">
                            <p class="text">Comfortable drive with rear camera recording all the time</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo17.jpg" alt="車輌後方から運転席">
                            <p class="text">Driver's seat from back side of the car</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo18.jpg" alt="大人5名くらいがゆったり座れます">
                            <p class="text">Roomy seat for 5 adults</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo19.jpg" alt="電子レンジ付き（外部電源利用時のみ）">
                            <p class="text">Microwave( Only when the external power outlet is connected)</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo20.jpg" alt="外国語対応ナビで海外の方も安心">
                            <p class="text">Foreign language is supported on the navigation. Comfortable for foreign drivers</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo21.jpg" alt="収納もたくさん">
                            <p class="text">Many storage places</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo22.jpg" alt="入り口にステップ有り">
                            <p class="text">A step is in the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo23.jpg" alt="入り口横にスイッチ類が集中">
                            <p class="text">Centralized switch panel is by the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo24.jpg" alt="冷蔵庫">
                            <p class="text">Fridge</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo25.jpg" alt="使い安いシャワー">
                            <p class="text">Easy operation shower</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo26.jpg" alt="入り口からまっすぐシャワールームへ行けます">
                            <p class="text">Shower room is forward from the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo27.jpg" alt="入り口より後方キッチン">
                            <p class="text">The rear kitchen from the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo28.jpg" alt="入り口右に収納">
                            <p class="text">A storage right by the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo29.jpg" alt="バックカメラ付きで安心">
                            <p class="text">Rear camera helps you safety driving</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo30.jpg" alt="入り口のステップも広々">
                            <p class="text">Wide step in the entrance</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo31.jpg" alt="車輌右後方の収納">
                            <p class="text">A storage in the right rear of the car</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo32.jpg" alt="外からシャワー等の給水ができます">
                            <p class="text">You can refill water for shower from outside</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo33.jpg" alt="着替えシャワールームわんちゃんお汚れた足もきれいに">
                            <p class="text">Changing room of shower room. Also good for wash a dogs feet</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo34.jpg" alt="入り口も防水床">
                            <p class="text">Water proof floor</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo35.jpg" alt="車輌右後方収納キャンプ道具もOK">
                            <p class="text">A storage rear right is good for camping gear</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo36.jpg" alt="車輌右外からシャワー室へ入れます">
                            <p class="text">You can access shower room from right side of the car</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo37.jpg" alt="左後方臭いの付いたゴミなどの収納に">
                            <p class="text">A storage left rear is good for garbage</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo38.jpg" alt="外部から上水道タンク給水可能">
                            <p class="text">You can refill tap water from outside</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo39.jpg" alt="右側の荷物収納">
                            <p class="text">A storage in the right </p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo40.jpg" alt="ペット対応に設計されたコルドランディ">
                            <p class="text">CORDE RUNDY is designed for pet accept vehicle</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo41.jpg" alt="わんちゃんも屋外が見れます">
                            <p class="text">A dog can see outside from the window</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42_tate.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo42.jpg" alt="車輌後方">
                            <p class="text">Rear</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo43.jpg" alt="ドア開放側面">
                            <p class="text">Door open</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo44.jpg" alt="ドア開放">
                            <p class="text">Door open</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo45.jpg" alt="車輌後方側面">
                            <p class="text">Rear</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo46.jpg" alt="車輌左前方">
                            <p class="text">Front left</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo47.jpg" alt="車輌左側面">
                            <p class="text">Left side</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo48.jpg" alt="車輌正面">
                            <p class="text">Front</p>
                    </a></li>
                <li><a href="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" data-lightbox="bunks">
                        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/car_rundy_photo49.jpg" alt="車輌斜め前方">
                            <p class="text">Front right</p>
                    </a></li>




            </ul>
        </div><!-- gallery -->
    </section>
    <?php endif; ?>

    <?php if($_GET['type'] == '01' || $_GET['type'] == '02' || $_GET['type'] == '03' || $_GET['type'] == '04'):?>
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Availability　＞</a></p>
    <?php endif; ?>

</div>
