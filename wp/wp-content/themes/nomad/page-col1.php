<?php
/*
Template Name: page-1col
*/
?>
<?php get_header(); ?>

<?php include(TEMPLATEPATH.'/part-title.php'); ?>
<?php include(TEMPLATEPATH.'/part-pan.php'); ?>

<div id="main_contents" class="wrapper cf">
	<div id="contents_center">

		<?php
		$page = TEMPLATEPATH.'/part-'.$post->post_name.'.php';
		if (file_exists($page)) {
			include ($page);
		}?>
	 	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<?php else : ?>
		<?php include (TEMPLATEPATH . '/404.php'); ?>
		<?php endif; ?>
	</div>
	<!-- contents -->

</div>
<!-- main_contents -->

<?php get_footer(); ?>
