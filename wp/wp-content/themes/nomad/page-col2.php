<?php
/*
Template Name: page-2col
*/
?>
<?php get_header(); ?>

<?php include(TEMPLATEPATH.'/part-title.php'); ?>
<?php include(TEMPLATEPATH.'/part-pan.php'); ?>

<div id="main_contents" class="wrapper cf">
	<?php get_sidebar(); ?>
	<div id="contents">

		<?php
		$parent_slug = '';
		if($post->post_parent){
			$parent_slug = get_page_uri($post->post_parent).'-';
		}

		$page = TEMPLATEPATH.'/part-'.$parent_slug.$post->post_name.'.php';

		if (file_exists($page)) {
			include ($page);
		}?>
	 	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; ?>
		<?php else : ?>
		<?php include (TEMPLATEPATH . '/404.php'); ?>
		<?php endif; ?>
	</div>
	<!-- contents -->
	
</div>
<!-- main_contents -->

<?php get_footer(); ?>
