<?php require_once('./price_table.php'); ?>

<section class="cal" id="c01">
    <h2 class="headline01 typesquare_tags">カレンダー<span class="small">でお得な</span>年間料金確認<span class="blue">!</span></h2>

    <ul class="cal_area cf"><?php dynamic_sidebar('Calendar'); ?></ul>
    <div class="season cf">
        <div class="cf">
            <dl class="cf">
                <dt class="color1"></dt>
                <dd>オフシーズン平日</dd>
            </dl>
            <dl class="cf">
                <dt class="color2"></dt>
                <dd>オフシーズン 金土日・祝日</dd>
            </dl>
            <dl class="cf">
                <dt class="color3"></dt>
                <dd>オンシーズン 平日</dd>
            </dl>
        </div>
        <div class="cf">
            <dl class="cf">
                <dt class="color4"></dt>
                <dd>オンシーズン 金土日・祝日</dd>
            </dl>
            <dl class="cf">
                <dt class="color5"></dt>
                <dd>ハイシーズン 全日</dd>
            </dl>
            <!--         <dl class="cf">
            <dt class="color6"></dt>
            <dd>ハイシーズン特別日</dd>
        </dl> -->
            <dl class="cf">
                <dt class="color8"></dt>
                <dd>ウィンターシーズン</dd>
            </dl>
        </div>
    </div>
    <!--season -->

    <table cellspacing="0" cellpadding="0" class="style01">
        <tr>
            <th>オフシーズン</th>
            <td>10月1日～12月20日<br />
                3月1日～4月25日</td>
        </tr>
        <tr>
            <th>オンシーズン</th>
            <td>
                5月7日～6月30日<br />
                9月1日～9月30日</td>
        </tr>
        <tr>
            <th>ハイシーズン</th>
            <td>7月1日～8月31日</td>
        </tr>
        <!--         <tr>
            <th><span class="text">ハイシーズン特別日</span></th>
            <td><span class="text">お盆、年末年始、その他</span></td>
        </tr>
 -->
        <tr>
            <th><span class="text">ウィンターシーズン</span></th>
            <td><span class="text">12月21日～2月28日</span></td>
        </tr>
    </table>

</section>
<section class="pricetable" id="c02">
    <h2 class="headline01 typesquare_tags">料金表</h2>

    <h2 class="luxury">最高級ラグジュアリークラス</h2>
    <div class="box">
        <h3 id="c02_3" class="zil">サンライト
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic10.jpg" width="104" alt="サンライト">
        </h3>
    </div>
    <!-- box -->

    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['luxury']['free']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['luxury']['free']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['luxury']['free']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['luxury']['high']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['luxury']['high']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['luxury']['high']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['luxury']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1">&yen;<?php echo number_format($price_table['luxury']['free']['day']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['luxury']['free']['half']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['luxury']['free']['time']); ?></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['day']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['half']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['luxury']['off_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['day']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['half']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['luxury']['on_tsu']['time']); ?></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['day']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['half']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['luxury']['on_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5">&yen;<?php echo number_format($price_table['luxury']['high']['day']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['luxury']['high']['half']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['luxury']['high']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['luxury']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php endif; ?>

        <p>※価格はすべて税別価格です。
        </p>
        <p>※万が一の事故や破損の際も安心のノマドレンタカー安心パック保険もご用意（有料オプション）</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">保険のページへ</a></p>
        <p>※海外の方はCDWとRAP両方のご加入をおすすめします。</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">詳しくはこちら</a> </p>
        <p>※冬期間トイレ水道利用不可です。</p>
    </div>


    <h2 class="plmclass-2">最高級プレミアムクラス</h2>
    <div class="box">
        <h3 id="c02_2" class="zil"><span>高級サルーン</span>ジル520<img src="<?php bloginfo('template_url'); ?>/images/price_pic03.jpg" alt="ジル520"></h3>
    </div>
    <!-- box -->
    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['premium']['free']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['premium']['free']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['premium']['free']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['premium']['off_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['premium']['off_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['premium']['off_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_tsu']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_tsu']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_tsu']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['premium']['on_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['premium']['high']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['premium']['high']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['premium']['high']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['premium']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1">&yen;<?php echo number_format($price_table['premium']['free']['day']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['premium']['free']['half']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['premium']['free']['time']); ?></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2">&yen;<?php echo number_format($price_table['premium']['off_syuku']['day']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['premium']['off_syuku']['half']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['premium']['off_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3">&yen;<?php echo number_format($price_table['premium']['on_tsu']['day']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['premium']['on_tsu']['half']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['premium']['on_tsu']['time']); ?></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4">&yen;<?php echo number_format($price_table['premium']['on_syuku']['day']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['premium']['on_syuku']['half']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['premium']['on_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5">&yen;<?php echo number_format($price_table['premium']['high']['day']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['premium']['high']['half']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['premium']['high']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['premium']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php endif; ?>

        <p>※価格はすべて税別価格です。
        </p>
        <p>※万が一の事故や破損の際も安心のノマドレンタカー安心パック保険もご用意（有料オプション）</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">保険のページへ</a></p>
        <p>※海外の方はCDWとRAP両方のご加入をおすすめします。</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">詳しくはこちら</a> </p>
        <p>※ルームエアコン付き（常時利用可能）・最新薄型テレビ付き </p>
    </div>

    <h2 class="highclass-2">ハイクラス</h2>
    <div class="box">
        <h3 id="c02_1">コルドバンクス<img src="<?php bloginfo('template_url'); ?>/images/price_pic01.jpg" width="97" alt="コルドバンクス"></h3>
        <h3>コルドリーブス<img src="<?php bloginfo('template_url'); ?>/images/price_pic02.jpg" width="104" alt="コルドリーブス"></h3><br class="pc">
    </div>


    <!-- <p class="camp">6月末まで早期予約割り引きキャンペーン延長！！</p> -->
    <div class="scroll">
        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['high']['free']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['free']['day'] - $disc_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['high']['free']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['free']['half'] - $disc_half_amt); ?></span></td>
                <td class="color1"><span class="price">&yen;<?php echo number_format($price_table['high']['free']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['free']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['high']['off_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['off_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['high']['off_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['off_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color2"><span class="price">&yen;<?php echo number_format($price_table['high']['off_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['off_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['high']['on_tsu']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_tsu']['day'] - $disc_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['high']['on_tsu']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_tsu']['half'] - $disc_half_amt); ?></span></td>
                <td class="color3"><span class="price">&yen;<?php echo number_format($price_table['high']['on_tsu']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_tsu']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['high']['on_syuku']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_syuku']['day'] - $disc_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['high']['on_syuku']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_syuku']['half'] - $disc_half_amt); ?></span></td>
                <td class="color4"><span class="price">&yen;<?php echo number_format($price_table['high']['on_syuku']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['on_syuku']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['high']['high']['day']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['high']['day'] - $disc_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['high']['high']['half']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['high']['half'] - $disc_half_amt); ?></span></td>
                <td class="color5"><span class="price">&yen;<?php echo number_format($price_table['high']['high']['time']); ?></span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;&yen;<?php echo number_format($price_table['high']['high']['time'] - $disc_time_amt); ?></span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php else:?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">1日料金</td>
                <td class="h">半日料金</td>
                <td class="h">営業時間外<br>1時間延長</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">オフシーズン</th>
                <td class="color1">平　日</td>
                <td class="color1">&yen;<?php echo number_format($price_table['high']['free']['day']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['high']['free']['half']); ?></td>
                <td class="color1">&yen;<?php echo number_format($price_table['high']['free']['time']); ?></td>
            </tr>
            <tr>
                <td class="color2">金土日</td>
                <td class="color2">&yen;<?php echo number_format($price_table['high']['off_syuku']['day']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['high']['off_syuku']['half']); ?></td>
                <td class="color2">&yen;<?php echo number_format($price_table['high']['off_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">オンシーズン</th>
                <td class="color3">平　日</td>
                <td class="color3">&yen;<?php echo number_format($price_table['high']['on_tsu']['day']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['high']['on_tsu']['half']); ?></td>
                <td class="color3">&yen;<?php echo number_format($price_table['high']['on_tsu']['time']); ?></td>
            </tr>
            <tr>
                <td class="color4">金土日</td>
                <td class="color4">&yen;<?php echo number_format($price_table['high']['on_syuku']['day']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['high']['on_syuku']['half']); ?></td>
                <td class="color4">&yen;<?php echo number_format($price_table['high']['on_syuku']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">ハイシーズン全日</th>
                <td class="color5">&yen;<?php echo number_format($price_table['high']['high']['day']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['high']['high']['half']); ?></td>
                <td class="color5">&yen;<?php echo number_format($price_table['high']['high']['time']); ?></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">ウィンターシーズン</th>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['day']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['half']); ?></td>
                <td class="color8">&yen;<?php echo number_format($price_table['high']['winter']['time']); ?></td>
            </tr>
        </table>
        <?php endif; ?>
        <p>※価格はすべて税別価格です。</p>
        <p>※万が一の事故や破損の際も安心のノマドレンタカー安心パック保険もご用意（有料オプション）</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">保険のページへ</a></p>
        <p>※海外の方はCDWとRAP両方のご加入をおすすめします。</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">詳しくはこちら</a></p>
        <p>※ペット乗車可能車は『犬』のみとさせていただきます。別途清掃料として1匹8000円、2匹10000円、3匹以上12000円を頂戴します。</p>
        <br>

    </div>

    <h4>営業受付時間：９時～１８時</h4>
    <p class="text">半日とは　午前は９時～１２時、午後は１２時～１８時までの間に貸出・返却の場合<br />
        １８時～２２時までのご返却延長は１時間ごとの延長料金が加算されます。<br>
        ２２時以降のご返却は１日分の追加料金が加算されます。</p>
    <h4>返却予定時間遅延時の注意事項</h4>
    <p class="text">渋滞含め、お客様の都合により返却予定時間が５時間を超え、次のお客様に貸し出す際の車両清掃準備等に影響が出る場合や遅延により次のお客様がキャンセル時には、ご自身の車両延長料金の他、次のご予約のお客様のご予約期間分全ての実費料金をご負担いただく事がございますので、遅延にはくれぐれもご注意ください。</p>

    <h4>（Foreign Visitors to Japan Only）<br>海外からのお客様へ</h4>
    <p class="text">当社は[訪日外国人限定]<br>Hokkaido Expressway Pass(HEP)取り扱い店です。<br>レンタカーと一緒に高速道路乗り放題のお得なETCカードをご利用いただけます。<br>（例：14日間利用の場合1日あたり807円で高速乗り放題！）<br>※お車の貸出日数分の費用をお支払いいただきます。<br>※日本国籍の方のご利用は不可です。<br><a class="link" href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">詳細はこちら</a></p>

</section>
<section class="discount" id="c03">
    <h2 class="headline01 typesquare_tags">お得<span class="small">な</span>割引きプラン<span class="blue">!</span></h2>
    <table>
        <tr>
            <th>長期割引</th>
            <td class="pd2">７日以上のご利用で１０％引き<br>
                １４日以上のご利用で１５％引き<br>
                ２１日以上のご利用で２０％引き</td>
        </tr>
        <tr>
            <th>シニア割引き</th>
            <td>５５歳以上の方のご利用時　１０％引き</td>
        </tr>
        <tr>
            <th>リピーター割引</th>
            <td>過去1年にご利用の方　１０％引き</td>
        </tr>
        <tr>
            <th>道民割引</th>
            <td>運転される方が北海道在住の場合　１０％引き</td>
        </tr>
    </table>
    <div>
        <p>※割引の併用はできません（重複の場合、いずれか割引率の高い方１点をご利用いただけます）</p>
    </div>
</section>
<section class="example" id="c04">
    <h2 class="headline01 typesquare_tags">レンタル料金例<span class="blue">!</span></h2>

    <ul class="cf">
        <li>1日とは<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic04.jpg?v=201901" width="83" alt="１日とは"></li>
        <li>半日（午前）とは<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic05.jpg" width="83" alt="半日(午前)とは"></li>
        <li>半日（午後）とは<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic06.jpg" width="83" alt="半日（午後）とは"></li>
    </ul>

    <div class="item cf">
        <div class="left">
            <h4>例1</h4>
            <p> 車種：コルドバンクス<br>
                1日の貸出料金が<?php echo number_format($price_table['high']['free']['day']); ?>円の場合</p>
        </div><!-- left -->
        <div class="right">
            <p><img src="<?php bloginfo('template_url'); ?>/images/price_pic07.jpg" alt="コルドバンクス" /></p>
        </div>
        <!-- right -->
    </div>
    <!-- item -->
    <table>
        <tr>
            <th>貸出日</th>
            <td>1日目</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['high']['free']['day']); ?>円</td>
            <td class="nobg"></td>
        </tr>
        <tr>
            <th>返却日</th>
            <td>2日目</td>
            <td>～11：30</td>
            <td><?php echo number_format($price_table['high']['free']['half']); ?>円</td>
            <td class="nobg"></td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2" class="red"><span>合計</span><?php echo number_format($price_table['high']['free']['day'] + $price_table['high']['free']['half']); ?>円＋税</td>
            <td class="nobg"></td>
        </tr>
    </table>

    <div class="item cf">
        <div class="left">
            <h4>例2</h4>
            <p> 車種：コルドリーブス<br>
                1日の貸出料金が<?php echo number_format($price_table['high']['on_tsu']['day']); ?>円の場合 </p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic08.jpg" alt="コルドリーブス" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>貸出日</th>
            <td>1日目</td>
            <td>13：00～</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['half']); ?>円</td>
        </tr>
        <tr>
            <th></th>
            <td>2日目</td>
            <td>終日貸出</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>円</td>
        </tr>
        <tr>
            <th></th>
            <td>3日目</td>
            <td>終日貸出</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>円</td>
        </tr>
        <tr>
            <th>返却日</th>
            <td>4日目</td>
            <td>～17：30</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>円</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>合計</span><?php echo number_format(($price_table['high']['on_tsu']['day'] * 3) + $price_table['high']['on_tsu']['half']); ?>円＋税</td>
        </tr>
    </table>
    <div class="item cf">
        <div class="left">
            <h4>例3</h4>
            <p> 車種：ジル520<br>
                1日の貸出料金が<?php echo number_format($price_table['premium']['on_tsu']['day']); ?>円の場合 </p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic09.jpg" alt="ジル520" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>貸出日</th>
            <td>1日目</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>円</td>
        </tr>
        <tr>
            <th></th>
            <td>2日目</td>
            <td>終日貸出</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>円</td>
        </tr>
        <tr>
            <th>返却日</th>
            <td>3日目</td>
            <td>～12：00</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>円</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>合計</span><?php echo number_format(($price_table['premium']['on_tsu']['day'] * 2) + $price_table['premium']['on_tsu']['half'] ); ?>円＋税</td>
        </tr>
    </table>
</section>
<section class="empty" id="c05">
    <!--<h2><img src="<?php bloginfo('template_url'); ?>/images/price_tit05.jpg" alt="空車状況カレンダー"/></h2>
    <ul>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス1 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=coh8o21ju2itn8uah33819p678%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス2 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=kbsrmr4pml0afjgjsrg10svqn4%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_3">
            <h4 class="green"> コルドリーブス <img src="<?php bloginfo('template_url'); ?>/images/car_pic002.png" alt="コルドリーブス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=nfota0r3s60uftr61e5nv94k1o%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_1">
            <h4 class="yellow"> ジル520 <img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="ジル520"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=d7hnk3hgae5lbsr6fjsl1dc528%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
    </ul>
            <p class="text">ご予約は随時　メール　電話等により受け賜りますが　サイトへの表示には時間差があり、予約可能になっていても時間差で予約済みになってしまう場合もあります事をご了承くださいませ。</p><br class="sp">-->
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">ご予約はこちら　＞</a></p>

</section>
