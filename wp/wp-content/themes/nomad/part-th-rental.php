<?php require_once('./lang/lang_rental.php'); ?>
<?php require('./price_table.php'); ?>

<section class="rental free" id="c01">
	<h2 class="headline01 typesquare_tags">ค่าทำความสะอาดเครื่องนอน</h2>
	<div class="gallery_top">
		<ul class="cf">
	<li>
				<p class="photo"><a href="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" data-lightbox="charge"><img src="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" alt="タオルケット・ベッドシーツ・枕セット　５００円"></a>
				<p class="text">ชุดเครื่องนอน (รวมผ้าเช็ด (ฤดูหนาวจะเป็นผ้าห่มแบบหนา) และผ้าปูที่นอน)<br /><br />

เช่ารถ1ครั้งคิดค่าทำความสะอาดตามจำนวนคนใช้ คนละ500เยน<br>
(ทางเราใช้บริการทำความสะอาดเฉพาะทางกับเครื่องนอน เพื่อความสบายใจและสุขอนามัยของลูกค้า)</p>
				</a></li></ul>
				</div><!-- gallery -->
	<h2 class="headline01 typesquare_tags">อุปกรณ์เสริมที่มีค่าใช้จ่าย</h2>	
	<h3>ราคานี้คือราคาสำหรับ1วันไม่รวมภาษี (ครึ่งวันก็ราคาเดียวกัน)<br>
	ราคาต่อ1วัน ใช้ได้สูงสุด5วัน<br>
	※หลังจากวันที่6เป็นต้นไปจะคิดค่าบริการ5วัน
	</h3>
	<div class="gallery">
		<ul class="cf">
			<?php
			$i = 1;
			if( have_rows('有料レンタルグッズ',$rental_post_id)):
			  while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
			  	<?php if(!get_sub_field('非表示')): ?>
				<?php $rental_img = get_sub_field('画像'); ?>
				<li class="fead<?php echo $i; ?>">
					<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
						<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?>"></p>
						<p class="text"><?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?></p>
					</a>

				</li>
				<?php endif; ?>
			<?php
			  $i++;
			  if($i==3) { $i=1; }
			  endwhile;
			endif;
			?>
		</ul>
		<p class="tax">ราคาทั้งหมดยังไม่รวมภาษี</p>
	</div>
	<!-- gallery -->
</section>
<section class="rental charge" id="c02">
	<h2 class="headline01 typesquare_tags">อุปกรณ์ใช้ฟรีที่ติดตั้งไว้ในรถ</h2>	

	<div class="gallery">
		<ul class="cf">
		<?php
		$i = 1;
		if( have_rows('無料車内設置備品',$rental_post_id)):
		  while( have_rows('無料車内設置備品',$rental_post_id) ): the_row(); ?>
		  	<?php if(!get_sub_field('非表示')): ?>
			<?php $rental_img = get_sub_field('画像'); ?>
			<li class="fead<?php echo $i; ?>">
				<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
					<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?>"></p>
					<p class="text"><?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?></p>
				</a>

			</li>
			<?php endif; ?>
		<?php
		  $i++;
		  if($i==3) { $i=1; }
		  endwhile;
		endif;
		?>
		</ul>
	</div>
	<!-- gallery -->
</section>
