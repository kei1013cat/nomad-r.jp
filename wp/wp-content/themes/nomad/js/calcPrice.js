/* 設定　*/
const c_max_date = "2020-4-30"; // 指定日(yyyy-mm-dd)以降のカレンダーは選択できない
const c_rental_goods_max_days = 5; // 最長のレンタル日数(指定した日数以降は料金加算されない)
const c_pack_max_days = 15; // 最長の安心パック日数(指定した日数以降は料金加算されない)
const c_safe_max_days = 15; // 最長の免責補償制度日数(指定した日数以降は料金加算されない)
const c_open_start_time = 9; // 営業時間開始
const c_open_end_time = 18; // 営業時間終了
const c_dispatch_go_sapporo_price = 3000; // 出発配車料（札幌市内）
const c_dispatch_go_airport_price = 5000; // 出発配車料（千歳空港）
const c_dispatch_back_sapporo_price = 3000; // 返却配車料（札幌市内）
const c_dispatch_back_airport_price = 5000; // 返却配車料（千歳空港）
const c_price_cleaning1 = 8000; // 清掃料金(1匹)
const c_price_cleaning2 = 10000;　// 清掃料金(2匹)
const c_price_cleaning3 = 12000;　// 清掃料金(3匹以上)
const c_safe_price = 3000; // 免責補償料金(1日あたり)
const c_pack_price = 1500; // 安心パック料金(1日あたり)
const c_safe_luxury_price = 6000; // 免責補償料金(1日あたり) サンライト用
const c_pack_luxury_price = 3000; // 安心パック料金(1日あたり) サンライト用

const c_bedcleaning_price = 500; // ベッドクリーニング料金
const c_senior_age = 55;	// シニア年齢
const c_tax_rate = 10;	// 消費税％指定
const c_rental_dispatch_max_days = 5;	// 配送料がかかる最大レンタル日数（例：3なら４日以降無料）
const c_premium_rental_dispatch_max_days = 4;	// 高級サルーン用の配送料がかかる最大レンタル日数（例：3なら４日以降無料）

// 1日から14日までの増分料金
var objHepUnitPrice = {
	1:3200,
	2:500,
	3:1500,
	4:1100,
	5:500,
	6:500,
	7:500,
	8:600,
	9:500,
	10:600,
	11:500,
	12:500,
	13:500,
	14:500
}

// 現在の言語取得
var lang = lang();

// 多言語対応　******************************************
var objTextLang = {
	discount_plan_name: {ja: "割引プラン",en: "Discount plan",zh: "優惠方案","th": "แผนส่วนลด"},
	discount_name: {ja: "引き",en: "Discount",zh: "引き",th: "引き"},
	discount_plan_nomad_name: {ja: "ノマドワーク割引",en: "Nomad work discount",zh: "遊牧工作優惠","th": "ส่วนลด Nomad work"},
	discount_plan_longterm_name: {ja: "長期割引",en: "Long term discount",zh: "長期優惠",th: "ส่วนลดสำหรับเช่าระยะยาว"},
	discount_plan_senior_name: {ja: "シニア割引",en: "Senior discount",zh: "敬老優惠",th: "ส่วนลดสำหรับผู้สูงอายุ"},
	discount_plan_repeater_name: {ja: "リピーター割引",en: "Repeater discount",zh: "熟客優惠",th: "ส่วนลดสำหรับผู้กลับมาใช้บริการอีกครั้ง"},
	time_select_default_value: {ja: "時間を選択",en: "Select time",zh: "選擇時間",th: "เลือกเวลา"}
};

// *****************************************************

/* グローバル変数 */
var g_min;
var objPlanDetail = {};
var g_daysCnt = 0;　// 日数カウント（半日含む）

$(window).load(function(){
	// 返却日の選択範囲の設定
	$('#datepicker_2').datepicker("option", "minDate", $("#min").val());
	onloadCalcPrice();
});

jQuery(document).ready(function() { 

	//バリデーション
	jQuery("#form1").validationEngine();

	// カレンダー表示
	$("#datepicker1").datepicker();
	$("#datepicker2").datepicker();


	jQuery("input[name='zip']").keyup(function(event){
//		AjaxZip3.zip2addr( 'zip[][-]', 'zip[][]','address', 'address' );
		AjaxZip3.zip2addr( 'zip', '','address', 'address' );
	 });

});

/* カレンダーイベント */
jQuery(document).ready(function() {
var nx = 0
$.datepicker.setDefaults($.datepicker.regional['ja']);

$('#datepicker_1').datepicker({
	dateFormat: 'yy/mm/dd',
	minDate: new Date(),
	maxDate: $.datepicker.parseDate('yy-mm-dd', c_max_date), 
	onSelect: function(selectedDate) {
		//出発希望日から最短の返却希望日を計算
		var today   = new Date();
		var selDate = new Date(selectedDate);
		var msDiff = selDate.getTime() - today.getTime();
		nx = Math.floor(msDiff / (1000 * 60 * 60 *　24));
		nx = nx+1;
		var min = '+'+nx+'d';
		$("#min").val(min);
		$('#datepicker_2').datepicker("option", "minDate", min);
		$("#start_dt").val(selectedDate);

		if ($("#end_dt").val() != "") {	
			if ($("#start_dt").val() >= $("#end_dt").val()) {
				$("#end_dt").val(selectedDate);
			}
		}

		// disp_chenge($("#start_dt").val(),$("#end_dt").val());

       	// 料金の再計算
		onloadCalcPrice();
	}
});





$('#datepicker_2').datepicker({ 
	dateFormat: 'yy/mm/dd',
	maxDate: $.datepicker.parseDate('yy-mm-dd', c_max_date),	
	onSelect: function(selectedDate) {
		$("#end_dt").val(selectedDate)

       	// 料金の再計算
		onloadCalcPrice();		
	}
});
/* 郵便番号から住所の自動入力 */
jQuery("input[name='portalcode[data][1]']").keyup(function(event){
	AjaxZip3.zip2addr( 'portalcode[data][0]', 'portalcode[data][1]','address', 'address' );
 });
});

/* 出発希望時間のセット */
$(function($) {
    $('#goTime').change(function() {
        $("#go_time").val($(this).val());
       	// 料金の再計算
		onloadCalcPrice();		
    });
});

/* 返却希望時間のセット */
$(function($) {
    $('#backTime').change(function() {
		$("#back_time").val($(this).val());
       	// 料金の再計算
		onloadCalcPrice();		
    });
});

/* Start バグ対応 2017.10.24 */
/* ご出発希望日時をテキスト入力した場合の処理*/
$(function($) {
	$('#datepicker_1').change(function() {
       	// 料金の再計算
		onloadCalcPrice();		

	});
});

$(function($) {
	$('#datepicker_2').change(function() {
       	// 料金の再計算
		onloadCalcPrice();		
	});
});
/* End バグ対応 2017.10.24 */

function setBirthYear() {
	$("#year").val($("#year option:selected").text());

   	// 料金の再計算
	onloadCalcPrice();		
}
function setBirthMonth() {
	$("#month").val($("#month option:selected").text());
   	// 料金の再計算
	onloadCalcPrice();		
}
function setBirthDay() {
	$("#day").val($("#day option:selected").text());
   	// 料金の再計算
	onloadCalcPrice();		
}

/***********************************
  関数名： chgControlPlace
  概要：　出発-返却場所のテキストコントロール制御
  引数：　
  戻り値： なし
************************************/
function chgControlPlace() {
    if($('input[name="place_go"]:eq(0)').prop('checked')) {
        $("#go05").prop('disabled',false);
    } else {
        $("#go05").prop('disabled',true);
    }

    if($('input[name="place_back"]:eq(0)').prop('checked')) {
        $("#back05").prop('disabled',false);
    } else {
        $("#back05").prop('disabled',true);

    }
}

/***********************************
  関数名： chgControlSafe
  概要：　免責補償制度 安心パックのコントロール制御
  引数：　
  戻り値： なし
************************************/
function chgControlSafe() {
    if($('input[name="safe"]:eq(0)').prop('checked')) {
		$("#pack").prop('disabled',false);
    }
    if($('input[name="safe"]:eq(1)').prop('checked')) {
		$("#pack").prop('disabled',true);
		$('input[name="pack"]:eq(1)').prop('checked','checked');
    }
}

/***********************************
  関数名： chgControlCleaning
  概要：　ペット選択コントロール制御
  引数：　
  戻り値： なし
************************************/
function chgControlCleaning() {
//	for (var i = 1; i <= Object.keys(objPetRideList).length; i++) {
//		if(($("#car_model_sts").val()==i) && (objPetRideList['model' + i]==true)) {
//			$('#pet_disp').css('display','block');
//			break;
//		} else {

//			$('#pet_disp').css('display','none');
//		}
//	}
	if($("#car_model_sts").val()!="") {
		if(objCarModel[$("#car_model_sts").val()]['pet']) {
			$('#pet_disp').show();
		} else {
			$('#pet_disp').hide();
		}
	}

}

/***********************************
  関数名： chgControlHep
  概要：　高速料金選択コントロール制御
  引数：　
  戻り値： なし
************************************/
function chgControlHep(rental_days) {
	if(rental_days >= 2) {
		$('#hep_disp').css('display','');
	} else {
		$('#hep_disp').css('display','none');
		$("input[name='hep']:eq(1)").prop('checked', true);
	}
}

/***********************************
  関数名： onloadCalcPrice
  概要：　初期読込時の計算
  引数：　
  戻り値： なし
************************************/
function onloadCalcPrice() {
	$('#submit').attr('disabled', 'disabled');

	// 出発-返却場所のテキストコントロール制御
	chgControlPlace();

    // 車種のセット
    setModelPlan();
	// 出発日のセット
	setStartDate();
	// 返却日のセット
	setEndDate();
	// 出発時間のセット
	setGoTime();
	// 返却時間のセット
	setBackTime();
    // 車種からペット選択コントロールを制御
    chgControlCleaning();
    // 出発配車料金のセット
    setDispatch(1);
    // 返却配車料金のセット
    setDispatch(2);
    // クリーニング料金のセット
    setCleaningPrice();
    // 免責補償料金のセット
   // setSafe();
    // 安心パック料金のセット
   // setPack();
   // CDWのセット
    setCdw();
    // レンタル料金のセット
    setRentalGoods();
    // 小学生以上の乗車数の料金のセット
    setRidershipNum(1);
    // 6歳未満の乗車数の料金のセット
	setRidershipNum(2);
	// リピーター割引セット
	setDiscRepeater();
	// 道民割引セット
	setDiscDoumin();
	// ドライバー１の年齢セレクトボックスの選択を変更
	setDriverAge();
	// HEP料金のセット
	setHep();

	// 計算処理
	if (existsCheckVal()) {calcPrice();}

	$('#submit').removeAttr('disabled');	
}

/***********************************
  関数名： setModelPlan
  概要：　車種選択のセット
  引数：　 なし 
  戻り値： なし
************************************/
function setModelPlan() {
//	var elements = document.getElementsByName( "car" );
	// 選択状態の値を取得
//	for ( var model="", i=elements.length; i--; ) {
//		if ( elements[i].checked ) {
//			// 2018.7.27 バグ修正
//			$("#car_model_sts").val(elements[i].getAttribute("id").substr(-1,1));
//			$("#car_model_sts").val(elements[i].getAttribute("id").substr(5));
//			break ;
//		}
//	}
//alert($("input[name='car']:checked").data('car'));
	$("#car_model_sts").val($("input[name='car']:checked").data('car'));

	if($("#car_model_sts").val()!="") {

		if (objCarModel[$("#car_model_sts").val()]['class'] == "luxury") {
			$(".cdw_price1").text(c_safe_luxury_price + c_pack_luxury_price);
			$(".cdw_price2").text(c_safe_luxury_price);

		} else {
			$(".cdw_price1").text(c_safe_price + c_pack_price);
			$(".cdw_price2").text(c_safe_price);
		}

		// if (objCarModel[$("#car_model_sts").val()]['class'] == "luxury" || objCarModel[$("#car_model_sts").val()]['class'] == "premium") {
		// 	$("#sel_adult1").hide();
		// 	$("#sel_adult2").show();
		// } else {
		// 	$("#sel_adult1").show();
		// 	$("#sel_adult2").hide();
		// }

	}

}

/***********************************
  関数名： setStartDate
  概要： 
  引数：　   
  戻り値： なし
************************************/
function setStartDate() {
	var currentDate = new Date($( "#datepicker_1" ).datepicker( "getDate" ));
	var formattedDate = currentDate.getFullYear() + '/' + (currentDate.getMonth() + 1) + '/' + currentDate.getDate();
	if(formattedDate!="1970/1/1") {
		$("#start_dt").val(formattedDate);
	}
}

/***********************************
  関数名： setEndDate
  概要： 
  引数：　   
  戻り値： なし
************************************/
function setEndDate() {
	var currentDate = new Date($( "#datepicker_2" ).datepicker( "getDate" ));
	var formattedDate = currentDate.getFullYear() + '/' + (currentDate.getMonth() + 1) + '/' + currentDate.getDate();
	if(formattedDate!="1970/1/1") {
		$("#end_dt").val(formattedDate);
	}
}

/***********************************
  関数名： setGoTime
  概要： 
  引数：　   
  戻り値： なし
************************************/
function setGoTime() {
	var selectVal = $("#goTime option:selected").text();
	if(selectVal!=objTextLang["time_select_default_value"][lang]) {
		$("#go_time").val(selectVal);	
	}
}

/***********************************
  関数名： setBackTime
  概要： 
  引数：　   
  戻り値： なし
************************************/
function setBackTime() {
	var selectVal = $("#backTime option:selected").text();
	if(selectVal!=objTextLang["time_select_default_value"][lang]) {
		$("#back_time").val(selectVal);	
	}
}

/***********************************
  関数名： setDispatch
  概要： 配送料金のセット
  引数：　dispatch_div(1:出発 2:返却)   
  戻り値： なし
************************************/
function setDispatch(dispatch_div) {
	switch (dispatch_div) {

		//出発
		case 1:
			if($('input[name="place_go"]:eq(0)').prop('checked')) {
				//ご来店／駐車場有 
				$("#price_go_dispatch").val(0);
			} else if($('input[name="place_go"]:eq(1)').prop('checked')) {
				//札幌駅北口
				$("#price_go_dispatch").val(8000);
			} else if($('input[name="place_go"]:eq(2)').prop('checked')) {
				//新千歳空港
				$("#price_go_dispatch").val(0);
			} else if($('input[name="place_go"]:eq(3)').prop('checked')) {
				$("#price_go_dispatch").val(0);
			} else {
				$("#price_go_dispatch").val(0);
			}
			break;
		//返却
		case 2:
			if($('input[name="place_back"]:eq(0)').prop('checked')) {
				//ご来店
				$("#price_back_dispatch").val(0);
			} else if($('input[name="place_back"]:eq(1)').prop('checked')) {
				//札幌駅北口
				$("#price_back_dispatch").val(8000);
			} else if($('input[name="place_back"]:eq(2)').prop('checked')) {
				//新千歳空港
				$("#price_back_dispatch").val(0);
			} else if($('input[name="place_back"]:eq(3)').prop('checked')) {
				//新千歳空港
				$("#price_back_dispatch").val(0);
			} else {
				$("#price_back_dispatch").val(0);
			}
			break;
	}
}

/***********************************
  関数名： setDiscRepeater
  概要： リピーター割引のセット 
  引数：　なし
  戻り値： なし
************************************/
function setDiscRepeater() {
	if ($('#disc_repeater').prop('checked')) {
		$("#disc_repeater_hidden").val(1);
	} else {
		$("#disc_repeater_hidden").val(0);
	}
}

/***********************************
  関数名： setDiscDoumin
  概要： 道民割引のセット 
  引数：　なし
  戻り値： なし
************************************/
function setDiscDoumin() {
	if ($('#disc_doumin').prop('checked')) {
		$("#disc_doumin_hidden").val(1);
	} else {
		$("#disc_doumin_hidden").val(0);
	}
}


/***********************************
  関数名： setCleaningPrice
  概要： クリーニング料金のセット 
  引数：　なし
  戻り値： なし
************************************/
function setCleaningPrice() {

	if ($('#pet').prop('checked')) {
		$('[name=pet_num]').prop('disabled', false);
		switch (parseInt($('[name=pet_num]').val())) {
			case 1:
				$("#price_cleaning").val(c_price_cleaning1);
				break;
			case 2:
				$("#price_cleaning").val(c_price_cleaning2);
				break;
			case 3:
				$("#price_cleaning").val(c_price_cleaning3);
				break;
		}

	} else {
		$('[name=pet_num]').prop('disabled', true);		
		$("#price_cleaning").val(0);
	}

}

/***********************************
  関数名： setSafe 
  概要： 免責補償料金のセット 
  引数：　なし
  戻り値： なし
  備考：現在使ってない関数
************************************/
function setSafe() {
	if($('input[name="safe"]:eq(0)').prop('checked')) {
		$("#price_menseki_flg").val(1);
	} else {
		$("#price_menseki_flg").val(0);
	}
}

/***********************************
  関数名： setPack 
  概要： 安心パック料金のセット 
  引数：　なし
  戻り値： なし
  備考：現在使ってない関数
************************************/
function setPack() {
	if($('input[name="pack"]:eq(0)').prop('checked')) {
		$("#price_pack_flg").val(1);
	} else {
		$("#price_pack_flg").val(0);
	}
}

/***********************************
  関数名： setCdw 
  概要：  
  引数：　なし
  戻り値： なし
************************************/
function setCdw() {

	if($('input[name="cdw"]:eq(0)').prop('checked') || $('input[name="cdw"]:eq(3)').prop('checked')) {
		// CDW免責補償制度＋RAPレンタカー安心パックに加入する
		$("#price_pack_flg").val(1);
		$("#price_menseki_flg").val(1);
		document.getElementById("warmsg").style.display="none";

	} else if($('input[name="cdw"]:eq(1)').prop('checked') || $('input[name="cdw"]:eq(4)').prop('checked')) {
		// CDW免責補償制度のみ加入する
		$("#price_pack_flg").val(0);
		$("#price_menseki_flg").val(1);
		document.getElementById("warmsg").style.display="none";

	} else if ($('input[name="cdw"]:eq(2)').prop('checked') || $('input[name="cdw"]:eq(5)').prop('checked')){
		//加入しない
		$("#price_pack_flg").val(0);
		$("#price_menseki_flg").val(0);
		document.getElementById("warmsg").style.display="block";

	}
}

/***********************************
  関数名： setRentalGoods
  概要： レンタルグッズのセット
  引数：　なし
  戻り値： なし
************************************/
function setRentalGoods() {
	var ren_gds_num;	// グッズの数量
	var enabled_check_flg;
	var exists_control_flg;
	if($("#car_model_sts").val()!="") {
		var car_class = objCarModel[$("#car_model_sts").val()]['class'];
	}

//	for (var i = 1; i <= Object.keys(objRentalGoods).length; i++) {
	Object.keys(objRentalGoods).forEach(function(key) {
		enabled_check_flg = $('#' + key).prop('checked');
		exists_control_flg = objRentalGoods[key]['set_exists'];

		// チェックON-OFFの判定
//		if ($('#' + objRentalGoods[key]).prop('checked')) { enabled_check_flg = true;}
		// 数量コントロールの存在判定
//		if($('#ren_gds_num' + i).length) { exists_control_flg = true;}

		if((exists_control_flg) && (enabled_check_flg)) {
			// 数量コントロールを選択できるようにする
	    	$("#" + key + "_num").prop('disabled', false);
	    	ren_gds_num = $("#" + key + "_num").val();
		} else if((exists_control_flg) && (!enabled_check_flg)) {
	    	ren_gds_num = 0;
	    	$("#" + key + "_num").prop('disabled', true);
		} else if((!exists_control_flg) && (enabled_check_flg)) {
			ren_gds_num = 1;
		} else if((!exists_control_flg) && (!enabled_check_flg)) {
			ren_gds_num = 0;
		} else {
			ren_gds_num = 1;
		}

		// 2018.2.23 安眠マットレスは1台につき2枚まで無料　3枚目以降は有料
		//************************************************************************************************
		// クラスによってレンタル料金が変わる処理　※2018.9.12までの処理
		//************************************************************************************************
		//if(chkRentalCd(i,"001") && chkPremiumClass()) {
		//	if(ren_gds_num > 2) {
		//		ren_gds_num = ren_gds_num - 2;
		//	} else {
		//		ren_gds_num = 0;
		//	}
		//}

		// 選択したグッズ料金をセットする
		objRentalGoods[key]["amt"]
			= parseInt(objRentalGoods[key]["price"][car_class],10) * ren_gds_num;
	});

//	}
}

/***********************************
  関数名： setRidershipNum
  概要： 乗車人数のセット 
  引数：　riderShip_div(1:小学生以上 2:6歳未満)
  戻り値： なし
************************************/
function setRidershipNum(riderShip_div) {
	switch (riderShip_div) {
		// 小学生以上	
		case 1:

//			if($("#car_model_sts").val()!="") {

				//if (objCarModel[$("#car_model_sts").val()]['class'] == "luxury" || objCarModel[$("#car_model_sts").val()]['class'] == "premium") {
				//	$("#adult_ridership_num").val(parseInt($('[name=adult2]').val(),10));
				//} else {
					$("#adult_ridership_num").val(parseInt($('[name=adult]').val(),10));
				//}

//			}

			break;
		// 	6歳未満
		case 2:
			$("#child_ridership_num").val(parseInt($('[name=child]').val(),10));
			break;
	}
}

/***********************************
  関数名： setDriverAge
  概要： ドライバー１の年齢セレクトボックスの選択を変更
  引数：　
  戻り値： なし
************************************/
function setDriverAge() {
	var age = calcAge($('#birth_year').val(),$('#birth_month').val(),$('#birth_day').val());
	if (age >= 20 && age <= 80) {
		$('select[name="driver1_age"] option[value="'+ age + '"]').prop('selected',true);
		$('driver1_age').val(age);
	} else{
		$('select[name="driver1_age"] option[value="'+ "" + '"]').prop('selected',true);
		$('driver1_age').val();
	}
}

/***********************************
  関数名： setHep 
  概要：  
  引数：　なし
  戻り値： なし
  備考：
************************************/
function setHep() {
	if($('input[name="hep"]:eq(0)').prop('checked')) {
		$("#price_hep_flg").val(1);
	} else {
		$("#price_hep_flg").val(0);
	}
}

function disp_chenge(start_dt,end_dt) {

		var start_dt = new Date(start_dt);
		var start_dt = new Date (start_dt.getFullYear() , start_dt.getMonth() , start_dt.getDate());
		var end_dt = new Date(end_dt);
		var end_dt = new Date (end_dt.getFullYear() , end_dt.getMonth() , end_dt.getDate());

		// console.log(start_dt);
		// console.log(end_dt);

		if(start_dt <= new Date(2019,5,30,0,0,0)) {

			$('#st_chitose_openmae input[name="place_go"]').prop('disabled', false);
			$('#st_chitose_opengo input[name="place_go"]').prop('disabled', true);

			$('#st_chitose_openmae .disable').removeClass('active');
			$('#st_chitose_opengo .disable').addClass('active');

		} else if(start_dt > new Date(2019,5,30,0,0,0)) {

			$('#st_chitose_openmae input[name="place_go"]').prop('disabled', true);
			$('#st_chitose_opengo input[name="place_go"]').prop('disabled', false);

			$('#st_chitose_openmae .disable').addClass('active');
			$('#st_chitose_opengo .disable').removeClass('active');


		}

		if(end_dt <= new Date(2019,5,30,0,0,0)) {

			$('#ed_chitose_openmae input[name="place_back"]').prop('disabled', false);
			$('#ed_chitose_opengo input[name="place_back"]').prop('disabled', true);

			$('#ed_chitose_openmae .disable').removeClass('active');
			$('#ed_chitose_opengo .disable').addClass('active');


		} else if(end_dt > new Date(2019,5,30,0,0,0)) {

			$('#ed_chitose_openmae input[name="place_back"]').prop('disabled', true);
			$('#ed_chitose_opengo input[name="place_back"]').prop('disabled', false);

			$('#ed_chitose_openmae .disable').addClass('active');
			$('#ed_chitose_opengo .disable').removeClass('active');

		}
}


/***********************************
  関数名： calcPrice
  概要：　料金を計算する
  引数：　なし
  戻り値： なし
************************************/
function calcPrice() {
	var total_basic_price;
	var sub_total_price;
	var tax_price;
	var start_dt = $("#start_dt").val();
	var end_dt = $("#end_dt").val();

	// 6月30日前後の画面制御
	disp_chenge(start_dt,end_dt);

	// レンタル日数の算出
	var rental_days = getDiff(start_dt,end_dt);

	// 基本料金の計算
	total_basic_price = calcBasicPrice(rental_days,start_dt);

	// 割引料金の計算
	calcDiscPrice(total_basic_price,rental_days);

	// 配送料の計算
	calcDispatch(start_dt,end_dt);

	// 免責補償料金の計算
	calcSafePrice(rental_days);

	// 安心パック料金の計算
	calcPackPrice(rental_days);

	// レンタルグッズ料金の計算
	calcRentalGoods(rental_days);

	// 清掃料の計算
	calcCleaningPrice();

	// 寝具クリーニング料金の計算
	calcBedCleaningPrice();

	// HEP料金の計算
	calcHepPrice(rental_days);

	//小計の計算
	sub_total_price = calcSubTotalPrice();

	//消費税の計算
	tax_price = calcTaxPrice(sub_total_price);

	//合計の計算
	calcTotalPrice(sub_total_price,tax_price);

	//高速料金コントロール制御
	chgControlHep(rental_days);

}

/***********************************
  関数名： calcBasicPrice
  概要：　基本料金の計算
  引数： rental_days(レンタル日数) start_dt(指定開始日)
  戻り値： total_price(基本料金) 
************************************/
function calcBasicPrice(rental_days,start_dt) {
	//基本料金の総計
	var total_basic_price = 0;
	//var car_model_sts = $("#car_model_sts").val();
	var go_time = $("#go_time").val();
	var back_time = $("#back_time").val();
	var go_hour = convertHour(go_time);	
	var back_hour = convertHour(back_time);
	var go_minute = convertMinute(go_time);
	var back_minute = convertMinute(back_time);

	var over_hour = 0;
	var total_over_hour = 0;
	var over_price = 0;
	var total_over_price = 0;

	// 2018.2.23 日数カウントクリア
	g_daysCnt = 0;

	// Excel用変数のクリア処理
	cleanExcelTxtPlan();

	// カウント用
	var counter_dt = new Date(start_dt);
	objPlanDetail = {
		//off_tsu:   {days: 0,over_hour: 0},
		free:      {days: 0,over_hour: 0},
		off_syuku: {days: 0,over_hour: 0},
		on_tsu:    {days: 0,over_hour: 0},
		on_syuku:  {days: 0,over_hour: 0},
		high:      {days: 0,over_hour: 0},
		golden:    {days: 0,over_hour: 0},
		winter:    {days: 0,over_hour: 0},
	};
	for (i = 1; i <= rental_days; i++) {
		var year = "year" + counter_dt.getFullYear();
		var month = "month" + (counter_dt.getMonth()+1);
		var day = "day" + counter_dt.getDate();
		var cal_event = g_cal_obj[year][month][day];
		var halfday_flg;

		// 車種クラスを取得
		var car_class = objCarModel[$("#car_model_sts").val()]['class'];

		// 半日チェック処理
		if(chkHalfDay(go_hour,go_minute,back_hour,back_minute,rental_days,i)) {
			if (objPriceList && objPriceList[car_class] && objPriceList[car_class][cal_event] && objPriceList[car_class][cal_event]['half_day']) {
				// 基本料金を加算(半日分)
				total_basic_price = total_basic_price + objPriceList[car_class][cal_event]['half_day'];
				// 営業外時間の加算
				over_hour = getOverTime(go_hour,back_hour,go_minute,back_minute,rental_days,i);
				//　営業時間外料金の加算
				over_price = over_hour * objPriceList[car_class][cal_event]['over_time'];
			} else {
				// wordpressカレンダーにイベント登録されていない場合はオフシーズン平日
				total_basic_price = total_basic_price + objPriceList[car_class]['free']['half_day'];
				// 営業外時間の加算
				over_hour = getOverTime(go_hour,back_hour,go_minute,back_minute,rental_days,i);
				//　営業時間外料金の加算
				over_price　= over_hour * objPriceList[car_class]['free']['over_time'];
			}
			// 半日の場合
			var halfday_flg = 1;
		} else {
			if (objPriceList && objPriceList[car_class] && objPriceList[car_class][cal_event] && objPriceList[car_class][cal_event]['one_day']) {
				// 基本料金を加算(1日分)
				total_basic_price = total_basic_price + objPriceList[car_class][cal_event]['one_day'];
				// 営業外時間の加算
				over_hour = getOverTime(go_hour,back_hour,go_minute,back_minute,rental_days,i);
				//　営業時間外料金の加算
				over_price = over_hour * objPriceList[car_class][cal_event]['over_time'];

			} else {
				// wordpressカレンダーにイベント登録されていない場合はオフシーズン平日 ※割引キャンペーン対応済み
				total_basic_price = total_basic_price + objPriceList[car_class]['free']['one_day'];
				// 営業外時間の加算
				over_hour = getOverTime(go_hour,back_hour,go_minute,back_minute,rental_days,i);
				//　営業時間外料金の加算
				over_price　= over_hour * objPriceList[car_class]['free']['over_time'];
			}
			// １日の場合
			halfday_flg = 0;
		}
		// 営業時間外の合計計算
		total_over_hour = total_over_hour + over_hour;
		// 営業時間外料金の合計計算
		total_over_price = total_over_price + over_price;

		// 2018.2.23 日数カウント（半日含む）
		setDaysCnt(halfday_flg);

		//　Excel出力の文字列生成（ご利用プラン）
		if(objPriceList && objPriceList[car_class] && objPriceList[car_class][cal_event]){
			createExcelTxtPlan(cal_event,halfday_flg,over_hour);
		} else {
			//createExcelTxtPlan('off_tsu',halfday_flg,over_hour);
			createExcelTxtPlan('free',halfday_flg,over_hour);
		}

		// 日付インクリメント
		counter_dt.setDate(counter_dt.getDate() + 1);
	}

	// 2019.1.21 基本料金と時間外料金を別々に算出する修正対応
	// total_basic_price = total_basic_price + total_over_price;

	// 基本料金の表示
	$("#price_basiccharge_num").html(getPriceSeparate(getPriceSeparate(total_basic_price)));
	$('#price_basiccharge_hidden').val(total_basic_price);
	$('#over_price_hidden').val(total_over_price);

	// 時間外料金の計算
	$("#price_over_num").html(getPriceSeparate(total_over_price));
	$('#price_over').val(total_over_price);
	if (total_over_price > 0) {
		// 時間外料金の表示
		$('#price_over').css('display','table');
	} else {
		// 時間外料金の非表示
		$('#price_over').css('display','none');
	}

	return total_basic_price + total_over_price;
}

/***********************************
  関数名： setDaysCnt
  概要： 日数カウント（半日含む）
  引数： halfday_flg
  戻り値： なし
  作成日：　2018/02/23(KEISUKE)
************************************/
function setDaysCnt(halfday_flg) {
	// 半日チェック
	if (halfday_flg==1) {
		var days = 0.5;
	} else {
		var days = 1;
	}
	//　グルーバル変数にセット
	g_daysCnt = g_daysCnt + days;

}

/***********************************
  関数名： createExcelTxtPlan
  概要： Excel出力用のテキスト生成　
  引数： 
  戻り値： なし
  作成日：　2017/06/18(KEISUKE)
************************************/
function createExcelTxtPlan(event,halfday_flg,over_hour) {

	if (halfday_flg==1) {
		var days = 0.5;
	} else {
		var days = 1;
	}

	var days_add;
	var over_hour_add;

	if (objPlanDetail[event]['days'] > 0) {
		days_add = objPlanDetail[event]['days'] + days;
	} else {
		days_add = days;
	}

	if (objPlanDetail[event]['over_hour'] > 0) {
		over_hour_add = objPlanDetail[event]['over_hour'] + over_hour;
	} else {
		over_hour_add = over_hour;
	}
	objPlanDetail[event]['days'] = days_add;
	objPlanDetail[event]['over_hour'] = over_hour_add;

	// <2017.06.18 KEISUKE Start> Excel出力用のデータセット（レンタルグッズ料金の内訳）
//	$('<input>').attr({type:'hidden',id:'plan_d_' + event,name:'plan_d_' + event,value:days_add}).appendTo('#form1');
//	$('<input>').attr({type:'hidden',id:'plan_o_' + event,name:'plan_o_' + event,value:over_hour_add}).appendTo('#form1');

	$('#plan_d_' + event).val(days_add);
	$('#plan_o_' + event).val(over_hour_add);

}

/***********************************
  関数名： cleanExcelTxtPlan()
  概要： Excel出力用のテキスト変数のクリア　
  引数： 
  戻り値： なし
  作成日：　2017/06/18(KEISUKE)
************************************/
function cleanExcelTxtPlan() {

	// var event = ['off_tsu','off_syuku','on_tsu','on_syuku','high','golden','winter'];
	 var event = ['free','off_syuku','on_tsu','on_syuku','high','golden','winter'];

	for (i = 0; i < event.length; i++) {

		$('#plan_d_' + event[i]).val(0);
		$('#plan_o_' + event[i]).val(0);

	}
}


/***********************************
  関数名： calcDiscPrice
  概要： 割引金額の計算 　
  引数： basic_price(基本料金) rental_days(レンタル日数) 
  戻り値： なし 
************************************/
function calcDiscPrice(basic_price,rental_days) {
	var disc_rate = 0;
	var disc_price;
	var disc_plan_div;

	// リピーター割引
	if(chkRepeater())　{
		disc_rate = 0.1;
		disc_plan_div = 4;
	}

	// 道民割引
	if(chkDoumin())　{
		disc_rate = 0.1;
		disc_plan_div = 5;
	}

	// シニア割引
	if(chkSenior()) {
		disc_rate = 0.1;
		disc_plan_div = 2;
	}

	// 2019.06.20　ノマドワーク割引削除
	// ノマドワーク割引(読書やお仕事で1名様でご利用時　１０％引き)
	// if(parseInt(getRidershipNum(1),10)==1 && parseInt(getRidershipNum(2),10)==0) {
	// 	disc_rate = 0.1;
	// 	disc_plan_div = 1;
	// }
	
	// console.log("g_dayCont : " + g_daysCnt);
	// console.log("rental_days : " + rental_days);

	// 長期割引
	if(g_daysCnt >= 7 && g_daysCnt <= 13.5) {
		// 10%割引
		disc_rate = 0.1;
		disc_plan_div = 3;
	} else if (g_daysCnt >= 14 && g_daysCnt <= 20.5) {
		// 15%割引
		disc_rate = 0.15;
		disc_plan_div = 3;
	} else if (g_daysCnt >= 21) {
		// 20%割引
		disc_rate = 0.2;
		disc_plan_div = 3;
	}

	// 割引額の算出
	disc_price = basic_price * disc_rate;
	if (disc_price > 0) {
		// 割引プランの表示
		$('#price_discount').css('display','table');
	} else {
		// 割引プランの非表示
		$('#price_discount').css('display','none');
	}
	$('#price_discount_num').html(getPriceSeparate(disc_price));
	$('#price_discount_hidden').val(disc_price);
	
	switch (disc_plan_div) {
		// 小学生以上
		case 1:
			// ノマドワーク割引
			$('#discount').html(objTextLang["discount_plan_nomad_name"][lang]);
			$('#disc_plan_name_hidden').val(objTextLang["discount_plan_nomad_name"][lang]);
			break;
		case 2:
			// シニア割引
			$('#discount').html(objTextLang["discount_plan_senior_name"][lang]);
			$('#disc_plan_name_hidden').val(objTextLang["discount_plan_senior_name"][lang]);
			break;
		case 3:
			// 長期割引
			$('#discount').html(objTextLang["discount_plan_longterm_name"][lang]);
			$('#disc_plan_name_hidden').val(objTextLang["discount_plan_longterm_name"][lang]);
			break;
		case 4:
			// リピーター割引
			$('#discount').html(objTextLang["discount_plan_repeater_name"][lang]);
			$('#disc_plan_name_hidden').val(objTextLang["discount_plan_repeater_name"][lang]);
			break;
		case 5:
			// 道民割引
			$('#discount').html("道民割引");
			$('#disc_plan_name_hidden').val("道民割引");
			break;

	}

}

/***********************************
  関数名： calcRentalGoods
  概要：　レンタルグッズ料金の計算
  引数： rental_days(レンタル日数)　
  戻り値： なし
  追加修正：　最高級プレミアムクラス（ジル５２０）無料（2018.2.23）
************************************/
function calcRentalGoods(rental_days) {

	var total_price = 0;
	var rental_price_detail;
	var rental_unit;
	var rental_num;
	// ６日目以降は５日目の料金とする
	if (rental_days > c_rental_goods_max_days) {
		var days = c_rental_goods_max_days;
	} else {
		var days = rental_days;
	}

	var aryRenGdsAmt = new Array(Object.keys(objRentalGoods).length);

	// for (var i = 1; i <= Object.keys(objRentalGoods).length; i++) {
	// 	aryRenGdsAmt[i] = parseInt(objRentalGoods["ren_gds" + i]["amt"],10);
	// }

	Object.keys(objRentalGoods).forEach(function(key) {
	//	aryRenGdsAmt[i] = parseInt(objRentalGoods[key]["amt"],10);
	});

//	for (var i = 1; i <= Object.keys(objRentalGoods).length; i++) {
	var i = 1;
	Object.keys(objRentalGoods).forEach(function(key) {

		//************************************************************************************************
		// クラスによってレンタル料金が変わる処理　※2018.9.12までの処理
		//************************************************************************************************
		// チェックON-OFFの判定 追加修正 高級クラスはレンタル料無料
		//if ($('#ren_gds' + i).prop('checked') && chkRentalCd(i,"001") && chkPremiumClass()) {
		// 高級クラス且つ、安眠マットレスの場合
		//	rental_price_detail = parseInt(aryRenGdsAmt[i],10) * parseInt(days,10);
		//	$('#ren_gds_disp' + i).val(1);
		//} else if ($('#ren_gds' + i).prop('checked') && !chkPremiumClass()) {
		// ハイクラスの場合は通常レンタル料金
		//	rental_price_detail = parseInt(aryRenGdsAmt[i],10) * parseInt(days,10);
		//	$('#ren_gds_disp' + i).val(1);
		//} else if ($('#ren_gds' + i).prop('checked') && chkPremiumClass()) {
		// 高級クラスの場合は無料
		//	rental_price_detail = 0;
		//	$('#ren_gds_disp' + i).val(1);
		//} else {
		// チェックない場合
		//	rental_price_detail = 0;
		//	$('#ren_gds_disp' + i).val(0);
		//}
		//************************************************************************************************

		// 2018.9.12 クラスに関係なくレンタル料金を計算するように変更
//		if ($('#ren_gds' + i).prop('checked')) {
		if ($("#" + key).prop('checked')) {
			rental_price_detail = parseInt(objRentalGoods[key]["amt"],10) * parseInt(days,10);
			$('#ren_gds_disp' + i).val(1);
		} else {
			// チェックない場合
			rental_price_detail = 0;
			$('#ren_gds_disp' + i).val(0);
		}

		// (１)レンタルグッズごとの合計額をセット
		$('#ren_gds_amt' + i).val(rental_price_detail);

		total_price = total_price + rental_price_detail;
		$('<input>').attr({type:'hidden',id:'ren_gds_name' + i,name:'ren_gds_name' + i,value:$('#' + key).val()}).appendTo('#form1');
		//$('#ren_gds_name' + i).val($('#ren_gds' + i).val());

		// S 2017.7.29 ADD 組数追加対応
		// 数量コントロール有無判定
//		if($('#ren_gds_num' + i).val()>0) {
		if(objRentalGoods[key]["set_exists"]) {
			//rental_unit = objRentalGoods["ren_gds" + i]["unit_name"];
			rental_unit = objRentalGoods[key]["unit_name"];
			//rental_num = $('#ren_gds_num' + i).val();
			rental_num = $("#" + key + "_num").val();
		} else {
			rental_unit = "";
			rental_num = 0;
		}
		$('<input>').attr({type:'hidden',id:'ren_gds_unit' + i,name:'ren_gds_unit' + i,value:rental_unit}).appendTo('#form1');
		//$('<input>').attr({type:'hidden',id:'rental_num' + i,name:'rental_num' + i,value:rental_num}).appendTo('#form1');
		// (２)レンタルグッズごとの組数セット
		$('#rental_num' + i).val(rental_num);
		// E 2017.7.29 ADD 組数追加対応

		i++;
	});

	// グッズのチェックボックスの数をセット
	$('<input>').attr({type:'hidden',id:'ren_gds_max',name:'ren_gds_max',value:Object.keys(objRentalGoods).length}).appendTo('#form1');

	// グッズ計算
	if (total_price > 0) {
		$('#price_goods').css('display','table');
	} else {
		$('#price_goods').css('display','none');
	}
	$('#price_goods_num').html(getPriceSeparate(total_price));
	$('#price_goods_hidden').val(total_price);

}


/***********************************
  関数名： chkStdDtDispatch
  概要： 2019年7月1日の配送仕様切り替えの判定処理
  引数：　日付
  戻り値： Trueは7月以降、falseは未満
************************************/
function chkStdDtDispatch(std_dt) {

		var std_dt = new Date(std_dt);
		var std_dt = new Date (std_dt.getFullYear() , std_dt.getMonth() , std_dt.getDate());
		// console.log(std_dt);

		if(std_dt <= new Date(2019,5,30,0,0,0)) {
			return false;
		} else {
			return true;
		}

}

/***********************************
  関数名： calcDispatch
  概要： 配送料の計算
  引数：　なし
  戻り値： なし
************************************/
function calcDispatch(start_dt,end_dt) {

	var total_price = 0;

	if (!chkPremiumClass()) {
		if((g_daysCnt - 0.5) > c_rental_dispatch_max_days) {
			total_price = 0;


			if(chkStdDtDispatch(start_dt)) {
				total_price = total_price + parseInt($("#price_go_dispatch").val(),10);
			} else {
				$("#price_go_dispatch").val(0);
			}

			if(chkStdDtDispatch(end_dt)) {
				total_price = total_price + parseInt($("#price_back_dispatch").val(),10);
			} else {
				$("#price_back_dispatch").val(0);
			}


		} else {
			total_price = parseInt($("#price_go_dispatch").val(),10) + parseInt($("#price_back_dispatch").val(),10);
		}

	} else {
	// 高級サルーンの場合
		if((g_daysCnt - 0.5) > c_premium_rental_dispatch_max_days) {
			total_price = 0;
			if(chkStdDtDispatch(start_dt)) {
				total_price = total_price + parseInt($("#price_go_dispatch").val(),10);
			} else {
				$("#price_go_dispatch").val(0);
			}

			if(chkStdDtDispatch(end_dt)) {
				total_price = total_price + parseInt($("#price_back_dispatch").val(),10);
			} else {
				$("#price_back_dispatch").val(0);
			}
		} else {
			total_price = parseInt($("#price_go_dispatch").val(),10) + parseInt($("#price_back_dispatch").val(),10);
		}
	}




	if (total_price > 0) {
		$('#price_dispatch').css('display','table');
	} else {
		$('#price_dispatch').css('display','none');
	}
	$('#price_dispatch_num').html(getPriceSeparate(total_price));
	$('#price_dispatch_hidden').val(total_price);
}

/***********************************
  関数名： calcCleaningPrice
  概要： 清掃料の計算
  引数：　なし
  戻り値： なし
************************************/
function calcCleaningPrice() {
	var price = $("#price_cleaning").val();
	if (price > 0) {
		$('#price_cleaning').css('display','table');
	} else {
		$('#price_cleaning').css('display','none');
	}
	$('#price_cleaning_num').html(getPriceSeparate(price));
	$('#price_cleaning_hidden').val(price);

}

/***********************************
  関数名： calcSafePrice
  概要： 免責補償料金の計算
  引数： rental_days(レンタル日数)　
  戻り値： なし
************************************/
function calcSafePrice(rental_days) {
	var price;

	// 指定日数以降は料金加算しない
	if (rental_days > c_safe_max_days) {
		var days = c_safe_max_days;
	} else {
		var days = rental_days;
	}

	if($("#price_menseki_flg").val()=="1"){

		// サンライトは料金変更
		if (objCarModel[$("#car_model_sts").val()]['class'] == "luxury") {
			price = parseInt(c_safe_luxury_price,10) * days;
		} else {
			price = parseInt(c_safe_price,10) * days;
		}

	} else {
		price = 0; 
	}
	if (price > 0) {
		$('#price_menseki').css('display','table');
	} else {
		$('#price_menseki').css('display','none');
	}
	$('#price_menseki_num').html(getPriceSeparate(price));
	$('#price_menseki_hidden').val(price);	
}

/***********************************
  関数名： calcPackPrice
  概要： 安心パック料金の計算
  引数： rental_days(レンタル日数)　　
  戻り値： なし
************************************/
function calcPackPrice(rental_days) {
	var price;

	// 指定日数以降は料金加算しない
	if (rental_days > c_pack_max_days) {
		var days = c_pack_max_days;
	} else {
		var days = rental_days;
	}

	if($("#price_pack_flg").val()=="1"){

		// サンライトは料金変更
		if (objCarModel[$("#car_model_sts").val()]['class'] == "luxury") {
			price = parseInt(c_pack_luxury_price,10) * days;
		} else {
			price = parseInt(c_pack_price,10) * days;
		}

	} else {
		price = 0; 
	}
	if (price > 0) {
		$('#price_safety').css('display','table');
	} else {
		$('#price_safety').css('display','none');
	}
	$('#price_safety_num').html(getPriceSeparate(price));
	$('#price_safety_hidden').val(price);
}

/***********************************
  関数名： calcBedCleaningPrice
  概要： 寝具クリーニング料金の計算
  引数： なし
  戻り値： なし
************************************/
function calcBedCleaningPrice() {

//	var total_price = parseInt(getRidershipNum(1),10) * parseInt(c_bedcleaning_price,10);
	var total_price = parseInt(getRidershipNum(0),10) * parseInt(c_bedcleaning_price,10);
	if (total_price > 0) {
		$('#price_bedding').css('display','table');
	} else {
		$('#price_bedding').css('display','none');
	}
	$('#price_bedding_num').html(getPriceSeparate(total_price));
	$('#price_bedding_hidden').val(total_price);
}


/***********************************
  関数名： calcHepPrice
  概要： 高速乗り放題料金の計算
  引数： rental_days(レンタル日数)　
  戻り値： なし
  備考： 14日以降は1日単価料金を加算する
************************************/
function calcHepPrice(rental_days) {
	var price = 0;
	var i = 1;
	var hep_no = 1;
	if($("#price_hep_flg").val()==1) {

		while (i <= rental_days) {
			price = price + objHepUnitPrice[hep_no];

			hep_no++;
			i++;

			if(hep_no==15) {
				hep_no = 1;
			}
		}

		// if(rental_days > 31) {
		// 	// 一日単価料金とレンタル日数から算出
		// 	price = c_hep_unit_pcice * rental_days;
		// } else{
		// 	// 料金表から算出
		// 	price =  parseInt(objHepPrice[rental_days],10);
		// }
	}

	if (rental_days < 2) {
		price = 0;
	}

	if (price > 0) {
		$('#price_hep').css('display','table');
	} else {
		$('#price_hep').css('display','none');
	}
	$('#price_hep_num').html(getPriceSeparate(price));
	$('#price_hep_hidden').val(price);
}

/***********************************
  関数名： calcSubTotalPrice
  概要： 小計金額の計算 
  引数： なし
  戻り値： total_price(小計) 
************************************/
function calcSubTotalPrice() {
	var total_price;
	total_price = 
		nvl(parseInt($('#price_basiccharge_hidden').val(),10)) +  
		nvl(parseInt($('#over_price_hidden').val(),10)) + 
		nvl(parseInt($('#price_dispatch_hidden').val(),10)) + 
		nvl(parseInt($('#price_cleaning_hidden').val(),10)) + 
		nvl(parseInt($('#price_menseki_hidden').val(),10)) + 
		nvl(parseInt($('#price_safety_hidden').val(),10)) + 
		nvl(parseInt($('#price_bedding_hidden').val(),10)) + 
		nvl(parseInt($('#price_goods_hidden').val(),10)) + 
		nvl(parseInt($('#price_hep_hidden').val(),10));

	total_price = total_price - nvl(parseInt($('#price_discount_hidden').val(),10));
	$('#price_subtotal_num').html(getPriceSeparate(total_price));
	$('#price_subtotal_hidden').val(total_price);

	return total_price; 
}

/***********************************
  関数名： calcTaxPrice
  概要： 消費税の計算
  引数： price(金額) 
  戻り値： tax_price(消費税額)
************************************/
function calcTaxPrice(price) {
	var tax_price = Math.floor(price * 0.01 * c_tax_rate);
	$('#price_tax_num').html(getPriceSeparate(tax_price));
	$('#price_tax_hidden').val(tax_price);

	return tax_price;
}

/***********************************
  関数名： calcTotalPrice
  概要： 合計金額の計算 
  引数： sub_total_price(小計) tax_price(消費税）
  戻り値： なし
************************************/
function calcTotalPrice(sub_total_price,tax_price) {
	var total_price = sub_total_price + tax_price;
//	$('#price_total_num').html(getPriceSeparate(total_price));
	calcCounter(total_price);
	$('#price_total_hidden').val(total_price);

}

/***********************************
  関数名： calcCounter
  概要： 合計金額の表示アニメーション 
  引数： 
  戻り値： なし
************************************/
function calcCounter(total_price) {
	var price = $('#price_total_hidden').val();
	$({count: price}).animate({count: total_price}, {
		duration: 1000,
		easing: 'swing',
		progress: function() { 
		    $('#price_total_num').html(getPriceSeparate(Math.ceil(this.count)));
		}
	});
}

/***********************************
  関数名： NVL関数
  概要： NULL値を0に変換する 
  引数： val(変換前の値) 
  戻り値： val(変換後の値)
************************************/
function nvl(val) {
	if(isNaN(val)) {
		return 0;
	}
	if(val=="" || val==null || (isNaN(val))){
		return 0
	} else {
		return val;
	}
}

/***********************************
  関数名： getRidershipNum
  概要： 乗車人数の取得
  引数： riderShip_div(0:乗車人数合計 1:小学生以上のみ 2:6歳未満のみ)
  戻り値： 乗車人数 
************************************/
function getRidershipNum(riderShip_div) {
	var riderShipNum;
	switch (riderShip_div) {
		// 乗車人数合計
		case 0:
			riderShipNum =  parseInt(nvl($("#adult_ridership_num").val(),10)) 
							+ parseInt(nvl($("#child_ridership_num").val(),10)) ;
			break;
		// 小学生以上	
		case 1:
			riderShipNum = parseInt(nvl($("#adult_ridership_num").val(),10));
			break;
		// 	6歳未満
		case 2:
			riderShipNum = parseInt(nvl($("#child_ridership_num").val(),10));
			break;
	}
	return riderShipNum;
}


/***********************************
  関数名： chkRepeater 
  概要： リピーターかどうかチェック
  引数： なし
  戻り値： 真理値(リピーターの場合true 違う場合はfalse） 
************************************/
function chkRepeater() {
	if($("#disc_repeater_hidden").val()==1) {
		return true;
	} else {
		return false;
	}
}

/***********************************
  関数名： chkDoumin
  概要： 道民かどうかチェック
  引数： なし
  戻り値： 真理値(道民の場合true 違う場合はfalse） 
************************************/
function chkDoumin() {
	if($("#disc_doumin_hidden").val()==1) {
		return true;
	} else {
		return false;
	}
}


/***********************************
  関数名： chkSenior 
  概要： シニア年齢かどうかチェッ/ク
  引数： なし
  戻り値： 真理値(シニア年齢の場合true 違う場合はfalse） 
************************************/
function chkSenior() {
	var age = calcAge($('#birth_year').val(),$('#birth_month').val(),$('#birth_day').val());
	if(age != "" && age >= c_senior_age) {
		return true;
	} else {
		return false;
	}
}

function paddingZero(num, digit) {
	return ('0000' + num).slice(-1 * digit);
} 

/***********************************
  関数名： calcAge 
  概要： 生年月日から年齢を計算	
  引数： なし
  戻り値： 
************************************/
function calcAge(year,month,day) {

	// 生年月日
	var birth = new Date(year, month, day);
	var y2 = paddingZero(birth.getFullYear(), 4);
	var m2 = paddingZero(birth.getMonth() + 1, 2);
	var d2 = paddingZero(birth.getDate(), 2);

	// 今日の日付
	var today = new Date();
	var y1 = paddingZero(today.getFullYear(), 4);
	var m1 = paddingZero(today.getMonth() + 1, 2);
	var d1 = paddingZero(today.getDate(), 2);

	return age = Math.floor((Number(y1 + m1 + d1) - Number(y2 + m2 + d2)) / 10000);

}

/***********************************
  関数名： changeAgeText
  概要： 年齢テキストボックスのイベント処理 
  引数： 
  戻り値： なし 
  !!!!!!削除予定の関数!!!!!!!
************************************/
function changeAgeText(keyCode, value) {
	// テキストが変更されていなかったら処理をしない
	if (textValue == value) { return; }
	// 再計算処理	
	onloadCalcPrice();
}

/***********************************
  関数名： chkHalfDay
  概要： 半日チェック 　
  引数： go_hour(貸出希望時間)
		go_minute(貸出希望分)
        back_hour(返却希望時間)
		back_minute(返却希望分)
        rental_days(レンタル日数)
        current_day(カレント日)
  戻り値： 真理値(半日の場合はtrue 違う場合はfalse)
************************************/
function chkHalfDay(go_hour,go_minute,back_hour,back_minute,rental_days,current_day) {
	var go_minute_conv = (go_minute == 30 ? 0.5 : 0);
	var back_minute_conv = (back_minute == 30 ? 0.5 : 0);
	var go_time = go_hour + go_minute_conv;
	var back_time = back_hour + back_minute_conv;

	if (rental_days==1) {
		// 当日レンタル
		if(back_time <= 12) { // ここ修正 20170617
//		if(back_hour < 12) { // ここ修正 20170617
			return true;
		} else if (go_time >= 12) {
			return true;
		}
	} else {
		// 2日以上のレンタル
		if ((current_day==1) && (go_time >= 12)) {
			return true;
		} else if ((current_day==rental_days) && (back_time <= 12)) {
			return true;
		}
	}
	return false;
}

/***********************************
  関数名： getOverTime
  概要： 時間外チェック	
  引数： go_hour(貸出希望時間)
        back_hour(返却希望時間)
        rental_days(レンタル日数)
        current_day(カレント日)
  戻り値： hour(営業外時間）
************************************/
function getOverTime(go_hour,back_hour,go_minute,back_minute,rental_days,current_day) {
	var over_time = 0;
	var go_minute_conv = (go_minute == 30 ? 0.5 : 0);
	var back_minute_conv = (back_minute == 30 ? 0.5 : 0);
	var go_time = go_hour + go_minute_conv;
	var back_time = back_hour + back_minute_conv;


	if (rental_days==1) {
		//***********
		// 当日レンタル
		//***********
		// 貸出時間が営業時間外

		if ((go_time < c_open_start_time) && (back_time < c_open_start_time)){
			over_time = Math.ceil(back_time - go_time);

		} else if (go_time < c_open_start_time) {
			over_time = Math.ceil(c_open_start_time - go_time);
		// Start 2017.10.14 Add  貸出時が18時以降の時間外を考慮
		} else if (go_time > c_open_end_time) {
			over_time = Math.ceil(go_time - c_open_end_time);
		}
		// End 2017.10.14 Add  貸出時が18時以降の時間外を考慮

		// 返却時間が営業時間外
		if ((go_time > c_open_end_time) && (back_time > c_open_end_time)){
			over_time = Math.ceil(back_time - go_time);
		} else if (back_time > c_open_end_time) {
			over_time = Math.ceil(over_time + (back_time - c_open_end_time));
		}

	} else {
		//***************
		// 2日以上のレンタル
		//***************
		// 貸出日が初日で貸出時間が営業時間外
		if(current_day==1 && (go_time < c_open_start_time)) {
			over_time = Math.ceil(c_open_start_time - go_time);
		// Start 2017.10.14 Add  貸出時が18時以降の時間外を考慮
		} else if (current_day==1 && (go_time > c_open_end_time)) {
			over_time = Math.ceil(go_time - c_open_end_time);
		}
		// End 2017.10.14 Add  貸出時が18時以降の時間外を考慮

		// 返却日が最終日で返却時間が営業時間外
		if(current_day==rental_days && (back_time > c_open_end_time)) {
			over_time = Math.ceil(over_time + (back_time - c_open_end_time));
		// Start 2017.10.14 Add  返却時が9時より前の時間外を考慮
		} else if (current_day==1 && (back_time < c_open_start_time)) {
			over_time = Math.ceil(over_time + (c_open_start_time - back_time));
		}
		// End 2017.10.14 Add  返却時が9時より前の時間外を考慮
	}

	return over_time;
}

/***********************************
  関数名： getDiff
  概要：　２つの日付の差を求める
  引数：　date1Str,date2Str
  戻り値： daysDiff(日付の差分)
************************************/
function getDiff(date1Str, date2Str) {
	var date1 = new Date(date1Str);
	var date2 = new Date(date2Str);
 
	// getTimeメソッドで経過ミリ秒を取得し、２つの日付の差を求める
	var msDiff = date2.getTime() - date1.getTime();
 
	// 求めた差分（ミリ秒）を日付へ変換（経過ミリ秒÷(1000ミリ秒×60秒×60分×24時間)。端数切り捨て）
	var daysDiff = Math.floor(msDiff / (1000 * 60 * 60 *24));
	// 差分へ1日分加算して返却
	return ++daysDiff;
}

/***********************************
  関数名： getNowYMD
  概要：　現在の日付を取得する
  引数：　
  戻り値： YYYY/MM/DD
************************************/
function getNowYMD(){
  var dt = new Date();
  var y = dt.getFullYear();
  var m = ("00" + (dt.getMonth()+1)).slice(-2);
  var d = ("00" + dt.getDate()).slice(-2);
  var result = y + "/" + m + "/" + d;
  return result;
}


/***********************************
  関数名： existsCheckVal
  概要：　必須項目の入力チェック
  引数：　なし
  戻り値： 真理値 
************************************/
function existsCheckVal() {

	if ($("#car_model_sts").val() == "" ||
		$("#start_dt").val() == "" ||
		$("#end_dt").val() == "" || 
		$("#go_time").val() == "" ||
		$("#back_time").val() == "") {
		return false;
	} else {
		return true;
	}
}

/***********************************
  関数名： convertHour
  概要：　時間文字列を数値に変換
　　　　  10：30の場合は10を返す
  引数： 変換前の値　
  戻り値： 変換後の値
************************************/
function convertHour(hour) {
	return parseInt(hour.substring(0,2).replace(/:/g,""),10);
}

/***********************************
  関数名： convertMinute
  概要：　時間文字列を数値に変換
　　　　  10：30の場合は30を返す
  引数： 変換前の値　
  戻り値： 変換後の値
************************************/
function convertMinute(minute) {
	return parseInt(minute.substring(5,2).replace(/:/g,""),10);
}

/***********************************
  関数名： getPriceSeparate
  概要：　金額をカンマ区切りにする
  引数： num(金額)　
  戻り値： カンマ区切りの金額
************************************/
function getPriceSeparate(num){
    return String(num).replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
}

/***********************************
  関数名： updateField
  概要：　申込者名から運転者を自動入力
  引数： 　
  戻り値： 
************************************/
function updateField(){
	$('input[name="driver1_name"]').val(document.getElementById("name").value); 
}

/***********************************
  関数名： lang
  概要：　URLパラメータから言語を取得する
  引数： 　
  戻り値： 
************************************/
function lang() {
	var string = location.search.match(/lang=(.*?)(&|$)/);
	if(string==undefined || string == null) {
		return "ja"; // デフォルト言語は日本語
	} else {
		return decodeURIComponent(location.search.match(/lang=(.*?)(&|$)/)[1]);;
	}
}

/***********************************
  関数名： chkPremiumClass
  概要：　最高級プレミアムクラス（ジル５２０）かどうか判断する
  引数： なし
  戻り値： true false
************************************/
function chkPremiumClass() {
//	var car_model = $("#car_model_sts").val();

	if(objCarModel[$("#car_model_sts").val()]['class']=="premium" || objCarModel[$("#car_model_sts").val()]['class']=="luxury") {
//	if(car_model==1 || car_model==8 || car_model==9 || car_model==10 || car_model==11 || car_model==12) {
		return true;
	} else {
		return false;
	}
}

/***********************************
  関数名： chkRentalCd
  概要： 配列連番から取得したレンタルコード（ユニークコード）と
  第二引数のコードを比較して一致したらTrue、不一致ならFalseを返す
  引数：　objRentalGoodsの配列連番
  　　　　　比較したいレンタルコード
  戻り値： 真偽値
************************************/
function chkRentalCd(i,ren_cd) {
	if (objRentalGoods["ren_gds" + i]["ren_cd"] == ren_cd) {
		return true;
	} else {
		return false;
	}
}