<section id="reserve">
<section class="calender">
			<h2 class="headline01 typesquare_tags">空車状況カレンダー</h2>
			<ul class="cf">

				<li id="c06_1">
					<h4 style="background-image: linear-gradient(135deg, #a79756, #faf8c5, #a79756);"> サンライトⅠ </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=1sd7d3li01rk0h2rgpn96m53ec%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>

				<li id="c05_1">
					<h4 class="yellow"> ジル520Ⅰ（高級サルーン）<img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_2; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=d7hnk3hgae5lbsr6fjsl1dc528%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>

				<li id="c05_2">
					<h4 class="yellow"> ジル520Ⅱ（高級サルーン）<img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_2; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=usicqfs5juf9ein6h4kg9a11h8%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_3">
					<h4 class="yellow"> ジル520Ⅲ（高級サルーン）<img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_3; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=6koghk8aujgv8c1fkam6kqp6qo%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_4">
					<h4 class="yellow"> ジル520Ⅳ（高級サルーン）<img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_4; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=kj1n0ran9pgnkr059sd57vr5n0%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>

				<li id="c05_10">
					<h4 class="yellow"> ジル520Ⅴ（高級サルーン）<img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_5; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe src="https://calendar.google.com/calendar/embed?src=8j9par6goclgnnc7hgf9j34jig%40group.calendar.google.com&ctz=Asia%2FTokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_11">
					<h4 class="yellow"> ジル520Ⅵ（高級サルーン） <img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_6; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe src="https://calendar.google.com/calendar/embed?src=naapfrefvdg5bbsqkqi8e03p10%40group.calendar.google.com&ctz=Asia%2FTokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>

				<li id="c05_12">
					<h4 class="yellow"> ジル520Ⅶ（高級サルーン） <img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="<?php echo $zil520_6; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe src="https://calendar.google.com/calendar/embed?src=ockjf9rqcej9ug3s5cb2am5us0%40group.calendar.google.com&ctz=Asia%2FTokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>

				<li id="c05_4">
					<h4 class="blue"> コルドバンクスⅠ <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="<?php echo $cordeBunks1; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=coh8o21ju2itn8uah33819p678%40group.calendar.google.com&ctz=asia/tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_5">
					<h4 class="blue"> コルドバンクスⅡ <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="<?php echo $cordeBunks2; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=kbsrmr4pml0afjgjsrg10svqn4%40group.calendar.google.com&ctz=asia/tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_6">
					<h4 class="blue"> コルドバンクスⅢ <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="<?php echo $cordeBunks3; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=l7ggnd4a1abiv704brbdu3vagg%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_7">
					<h4 class="green"> コルドリーブスⅠ <img src="<?php bloginfo('template_url'); ?>/images/car_pic002.png" alt="<?php echo $cordeLeaves1; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=nfota0r3s60uftr61e5nv94k1o%40group.calendar.google.com&ctz=asia/tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
				<li id="c05_8">
					<h4 class="green"> コルドリーブスⅡ <img src="<?php bloginfo('template_url'); ?>/images/car_pic002.png" alt="<?php echo $cordeLeaves2; ?>"/> </h4>
					<div class="ifrm-container">
						<iframe class="ifrm" src="https://calendar.google.com/calendar/embed?src=epn419eojg7fvr51guardhqr58%40group.calendar.google.com&ctz=Asia/Tokyo<?php echo $lang_cal;?>&showtitle=0&showPrint=0&showTabs=0&showCalendars=0&showTz=0" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
					</div>
				</li>
			</ul>
</div>
</div>