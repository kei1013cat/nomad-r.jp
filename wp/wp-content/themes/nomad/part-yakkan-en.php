<div id="yakkan">
<section>
		
		<h2 class="headline01">Lending rules</h2>
		<h4>Chapter 1 General agreement</h4>
		<h5>Section 1 Application of agreement</h5>
		<ul>
		  <li>Car rental is subject to the following agreement. Anything not mentioned here is in accordance with laws, regulations and standard practices.</li>
			<li>Custom contracts may be made, in which case the custom contract takes precedence.</li>
		</ul>
		<h4>Chapter 2 Reservation</h4>
		<h5>Section 2 Application</h5>
		<ul>
		  <li>The type of rental car, the rental period, pickup and drop-off locations, drivers and other options can be selected in advance (a price list is provided).</li>
			<li>It is not possible to rent more cars than our company has. A deposit is charged at the time of reservation.</li>
		</ul>
		<h5>Section 3 Changes</h5>
		<ul>
		  <li>If you want to change your reservation, please let us know in advance.</li>
		</ul>
		<h5>Section 4 Cancellation</h5>
		<ul>
		  <li>Reservations can be canceled.</li>
			<li>Your reservation will be canceled if a contract has not been made after one hour of the scheduled pickup time.</li>
			<li>In the event of this type of cancelation, a cancelation fee will be charged. The deposit will be refunded.</li>
			<li>If our company is responsible for a reservation cancelation or a failure to make a rental contract, we will refund your deposit.</li>
			<li>If car rental is not possible due to accident, theft, or any reason beyond our company's control, your reservation will be canceled and your deposit refunded. </li>
			<li>If we are unable to contact you by email or phone, your reservation may be canceled.</li>
		</ul>
		<h5>Section 5 Alternative cars </h5>
		<ul>
			<li>If we are unable to provide a rental car from the class you selected, you may select an alternative car from another class (hereafter referred to as &quot;alternative car&quot;). </li>
			<li>In such a case, with your agreement, the alternative car will be rented. All other conditions are identical to your reservation, There is no extra charge if the alternative car is more expensive than your original selection. However, if the alternative car is cheaper, you will pay the lower price. </li>
			<li>If there are no alternative cars to your liking, you can cancel your reservation. </li>
			<li>In such a case, your deposit will be refunded. </li>
		</ul>
		<h5>Section 6 Disclaimer </h5>
		<ul>
		  <li>If a reservation is canceled for a reason other than those specified in sections 3.4 and 3.5, you and we will not be charged. </li>
			<li>If car rental is not possible because of reasons beyond our control (such as a natural disaster), and if such an event inconveniences you, our company cannot be held responsible. </li>
		</ul>
		<h5>Section 7 Agents for reservations </h5>
		<ul>
		  <li>It is possible to make a reservation through an agent. </li>
			<li>In such cases, please talk to your agent about the reservation details. </li>
		</ul>
		
		<h4>Chapter 3 Rental </h4>
		<h5>Section 8 Creation of rental contract </h5>
		<ul>
		  <li>When you have selected the type of car and other options and this agreement and price list are provided, we will draw up a rental contract, unless your selected car is not available for your selected dates, or any of the items in Section 9 apply to you. </li>
			<li>When the contract has been made, you will pay the rental fee. The price is in section 11. </li>
			<li>We are required by Japanese law to register the driver's name and address, and to make a copy of the driver's license. If the renter and the driver are different people, we still need to make a copy of the driver's license. <br />Note 1: This regulation is called &quot;Rentakaa ni kansuru kihon tsuutatsu&quot;, by the Japanese Ministry of Land, Infrastructure, Transport and Tourism. <br />Note 2: &quot;Driver's license&quot; is that described in the Ordinance for the Enforcement of Road Traffic Law, section 19.14 of Article 92 of the Road Traffic Act. Furthermore, internaional driver's licenses and foreign driver's licenses will satisfy the conditions described in Article 107.2 of the Road Traffic Act. </li>
			<li>We may ask the renter and the driver to show identification (such as a passport) in addition to a driver's license, and make a copy. </li>
			<li>We may ask for a contact telephone number.(Cell phone) </li>
			<li>Payment is by cash or credit card. Other options may be possible. </li>
		</ul>
		<h5>Section 9 Refusal of rental contract </h5>
		<ul>
			<li>We are unable to rent you a car in the following situations: <br />（１）you fail to provide a valid driver's license.<br>
（２）you drive under the influence of alcohol. <br>
（３）you drive under the influence of illegal drugs such as marijuana, cocaine etc.<br>
（４）you intend to allow children to travel without a child safety seat. (It is mandatory for children under 6 years old to have a child safety seat).<br>
（５）you have connections with "bouryokudan" (Japanese yakuza organizations).<br>
</li>
			<li>&quot;We are unable to rent a car in the following situations: <br />（１）the driver is a different person from the one specified at the time of reservation.  <br />（２）if you have an experience of payment delayal. <br />（３）you have committed any of the acts detailed in section 17. <br />（４）you have committed the act described in section 18.6 or section 23.1 (whether with our or any other car rental company).<br /> （５）you cannot secure car insurance due to any instance of violating a car rental agreement or car insurance agreement.  <br />（６）you fail to meet other conditions specified here. <br />（７）you make unreasonable demands, or use violence or offensive language.<br />（８）you fail to conform to our company rules.&quot; </li>
			<li>If any of the above transgressions occur or are discovered subsequent to the reservation, the reservation will be cancelled and the deposit refunded. </li>
		</ul>
		<h5>Section 10 Validation of rental contract </h5>
		<ul>
		  <li>The contract is validated when the payment is made and the car is delivered. The payment is the rental fee minus the deposit fee. </li>
			<li>The car is delivered according to your reservation specifications. </li>
		</ul>
		<h5>Section 11 Rental fee </h5>
		<ul>
			<li>The rental fee is the sum of the fees below. The price table is provided. <br />（1） basic fee <br />（2） disclaimer compensation <br />（3） option fee <br />（4） &quot;Anshin Paku&quot; (Safety Pack) fee <br />（5） fuel fee <br />（6） delivery and pickup charge <br />（7） other charges </li>
			<li>The basic fee is reported to Hokkaido District Transport Bureau. </li>
			<li>If we revise our price after your reservation, the lower price will be adopted. </li>
		</ul>
		<h5>Section 12 Alterations </h5>
		<ul>
		  <li>If you want to change conditions after the contract has been made, please inform us, but you should be aware that not every change is possible. </li>
		  <li>We will do our best to accommodate you but not all requested changes can be made.</li>
		</ul>
		<h5>Section 13 Maintenance </h5>
		<ul>
		  <li>Our cars undergo regular safety inspections, as required by law. </li>
			<li>Please check that there are no problems with the car you have rented. </li>
			<li>If you notice a problem, we will immediately rectify it. </li>
	</ul>
	<h5>Section 14 Rental documents</h5>
		<ul>
		  <li>We provide you with a copy of the rental documents upon delivery of the car. </li>
			<li>Keep the rental documents with you while using the car. </li>
			<li>If you lose the rental documents, please call us immediately. </li>
			<li>Please return the rental documents when you return the car. </li>
		</ul>

		<h4>Chapter 4. Usage </h4>
		<h5>Section 15 Renter's responsibility in using the car </h5>
		<ul>
		  <li>When you rent a car from us, ensure that you drive responsibly, in accordance with the law and the following conditions. </li>
		</ul>
		<h5>Section 16 Daily maintenance </h5>
		<ul>
		  <li>Check the car on a daily basis. </li>
		</ul>

		<h5>Section 17 Prohibited acts </h5>
		<ul>
			<li> When you rent a car from us, the following acts are forbidden: <br />（１）using the car for commercial purposes, e.g. transporting goods or people for money. <br />（２）using the car for any purpose other than the intended use, or permitting anyone other than the driver specified on the rental document to drive. <br />（３）renting the car to other people, or using the car as a guarantee. <br />（４）forging the license plate or chassis number, or in any way modifying the car. <br />（５）using the car in demonstrations or competitions, or using the car to tow or push other cars. <br />（６）using the car illegally or recklessly. <br />（７）arranging your own rental car insurance without our permission. <br />（８）taking the car abroad. <br />（９）allowing pets in the car without our permission. <br />（１０）smoking, or consuming or cooking strong-smelling food or drink; using strong-smelling insecticide or air freshener; using mosquito coils. <br />（１１）driving the car on the beach, rough roads, in a river or anywhere that damages the car. <br />（１２）violating section 8.1 <br /></li>
		</ul>
		
		<h5>Section 18 Illegal parking </h5>
		<ul>
		  <li>If you get a parking ticket for illegal parking, go to the police station specified on the ticket and pay the fine. If there are other costs such as wrecker fees, you need to pay them. </li>
			<li>If the police contact us, we will contact you immediately. We will instruct you on how to move the car, how to retrieve the car from the police, and how to proceed with the transaction for the violation (including paying the fine by the deadline). Furthermore, if the car is impounded, we may retrieve the car at our discretion. </li>
			<li>We will check the status of the violation processing and contact you as and when it is completed. You will sign an acknowledgement letter attesting to the illegal parking and the transaction for the violation. </li>
			<li>We may be required to submit any necessary documents to the authorities, and you are required to agree to this. </li>
			<li>If you do not go to the police, and we are ordered to pay a fine instead of you, we will charge you the fee below. You will pay this fee by the date we specify. <br />（１）the fee of the fine in full.  <br />（２）any other illegal parking fine determined by the company. <br />（３）any reserching costs and any transport costs incurred by the company in vehicle reclamation. </li>
			<li>If you do not follow the order to go to the police station, pay the fine or sign the acknowledgement letter, we can charge you an extra violation fee that we stipulate. </li>
			<li>If, having paid the charges in section 5, any reimbursement is due to you (e.g. if a payment order from the police to us is canceled), we will reimburse you. </li>
		</ul>
		
		<h4>Chapter 5 Car return </h4>
		<h5>Section 19 Responsibility for returning </h5>
		<ul>
		  <li>You must return the car to us at the appointed place by the appointed time. </li>
			<li>If you cannot fulfill condition 1, you will compensate us for losses incurred. </li>
			<li>If you are unable to return the car by the appointed time due to unavoidable circumstances, contact us immediately and follow our instructions. There may be no penalty. </li>
		</ul>
		<h5>Section 20 Details to confirm when returning </h5>
		<ul>
		  <li>Car return is subject to inspection by us. The car must be returned in the same condition as when it was hired (apart from natural wear and tear, such as of tires). </li>
			<li>Make sure you do not leave any personal items in the car. We are not responsible for any lost items. </li>
		</ul>
		<h5>Section 21 Change of return time charge </h5>
		<ul>
		  <li>If you change the rental terms (section 12.1), there will be an extra charge when you return the car. </li>
		</ul>
		<h5>Section 22 Car return location </h5>
		<ul>
		  <li>If you change the return location (section 12.1) and there is an extra charge involved, you will pay the charge when you return the car. </li>
			<li>If you change the return location without our agreement, you will pay an extra charge (the cost of changing location x 250%). </li>
		</ul>
		<h5>Section 23 In the case of non-return </h5>
		<ul>
		  <li>If you don't return the car by the appointed time and follow our conditions for return, we will take legal action. </li>
			<li>In such a case, we will pursue the matter with your contacts such as your family or coworkers and activate a vehicle locating system. </li>
			<li>You will bear all costs involved in locating and retrieving the car. </li>
		</ul>
		<h4>Chapter 6 Breakdown , accident, theft </h4>
		<h5>Section 24 In the event of a breakdown </h5>
		<ul>
		  <li>If you experience any trouble with the car, pull over as soon as is safe, contact us and follow our instructions. </li>
			<li>If you cause the car to break down, the repair and pickup costs are paid by you. We also charge you for loss of income while the car is being repaired. <br />Non-Operation Charge ・Car inaction compensation <br />(１) If the car cannot be driven and requires a carrier, we charge you ￥50,000 as part of the cost. <br />(２) The daily charge for the car being inactive is ￥10,000. <br />(３) You cannot charge us for any losses you incur as a result of car breakdown.<br /> (４) You are not liable for the charges in 24.1 and 24.2 if you apply for an &quot;Anshin Paku&quot; (safety pack) at the time of contract. You can only apply for an &quot;Anshin Paku&quot; at the time of contract. </li>
		</ul>
		<h5>Section 25 In the event of an accident</h5>
		<ul>
			<li>If you have an accident while using the car, pull over as soon as it is safe. In addition to performing any actions required by law, take these actions below: <br />（１）contact us right away and follow our instructions. <br />（２）have the car repaired at the location specified by us. <br />（３）cooperate with the investigation by our insurance company, and submit any papers you are required to immediately. <br />（４）contact us before agreeing any private settlement with the other party involved in the accident. </li>
			<li>It is your responsibility to perform the above actions. Any costs incurred are borne by you. </li>
			<li>We will advise you on how to deal with an accident. </li>
	</ul>
		<h5>Section 26 In the event of car theft </h5>
		<ul>
			<li>If the car is stolen while you are renting it, take the following actions: <br />（１）contact the closest police station. <br />（２）contact us right away and follow our instructions. <br />（３）cooperate with us and our insurance company, responding quickly when requested by us or our insurance company. </li>
		</ul>
		<h5>Section 27 Contract cancellation due to car failure </h5>
		<ul>
		  <li>If the rental car is unusable due to theft or breakdown, the contract terminates. </li>
			<li>You bear any costs incurred for transport and repair of the car, except in the case of points 3 and 5 below. The rental fee is not refunded. </li>
			<li>If the car problem pre-dates the rental contract, we will make a new contract, offering you an alternative car (see section 5.2). </li>
			<li>If it is not possible to make a new contract, or we cannot provide an alternative car, the rental fee will be refunded. </li>
			<li>If the car problem is neither the renter's nor our company's fault, the daily rental fee will be calculated and refunded according to the number of days remaining until the scheduled return time. </li>
			<li>Our company cannot be charged for any loss not mentioned here. </li>
		</ul>
		<h4>Chapter 7  Compensation </h4>
		<h5>Section 28 Compensation and sales compensation </h5>
		<ul>
		  <li>If you cause damage to our company or a third party while renting a car, unless the damage is our responsibility, you will bear the compensation costs. </li>
			<li>Payment will be made according to the relevant price stipulated in the price table. </li>
		</ul>
		<h5>Section 29 Insurance and guarantee </h5>
		<ul>
			<li>The amounts for compensation paid by you accord with the limits specified in the details below: <br />（１）unlimited for body injury for each accident (excluding the amount due to mandatory car insurance). （２）unlimited for property damage for each accident (disclaimer price is up to 100,000 yen). <br />（３）market price for vehicle compensation for each accident (disclaimer price is up to 100,000 yen). <br />（４）50,000,000 yen per passenger for passenger compensation. </li>
			<li>The insurance money stipulated in paragraph 1 will not be paid if the case is covered by the disclaimer of an insurance agreement or compensation system. </li>
			<li>You are responsible for any reparations exceeding the above limitations. </li>
			<li>If our company pays compensation instead of you, you need to reimburse us the same amount of money immediately. </li>
			<li>The insurance charge is included in the rental fee. </li>
		</ul>
		<h4>Chapter 8 Termination of the rental contract </h4>
		<h5>Section 30 Termination of rental contract </h5>
		<ul>
		  <li>We will cancel the rental contract and demand the return of the car if you violate this agreement, or if any item of section 9.1 applies to you. The rental fee will not be refunded. </li>
		</ul>
		<h5>Section 31 Termination agreement </h5>
		<ul>
		  <li>You can terminate the contract with our agreement. There is a charge for contract cancellation. We will refund the cost of the days remaining until the scheduled return of the car. </li>
			<li>If you terminate the contract, you pay us the charge below. <br />Termination fee ＝ (Rental fee for the rental term) - (Rental fee for the term between when the car was rented and when the car is returned) × ５０％ </li>
		</ul>
		<h4>Chapter 9 Personal Information </h4>
		<h5>Section 32 Use of personal information </h5>
		<ul>
			<li>We use your personal information for the purposes below. <br />（１）To fulfill duties required by law and for making a rental document when we make a contract. <br />（２）To promote our services for you. <br />（３）To identify you and vet you to make the contract. <br />（４）In order to ask your cooperation with our service questionnaire. <br />（５）To compile statistical data (in which individuals are anonymous). </li>
			<li>If we ask for any information other than for the purposes above, we will explain the purpose to you in advance. </li>
		</ul>
		<h5>Section 33 Agreement for using personal information </h5>
		<ul>
			<li>Please consent to our use of your personal information in order to consult the rental contract:<br />（１）if we are ordered to pay any illegal parking fines for the car you rent. <br />（２）if you do not pay the illegal parking charge detailed in section 18.5. <br />（３）if you do not return the car, as detailed in section 23.</li>
		</ul>
		<h4>&quot;Chapter 10  Miscellaneous Provisions&quot; </h4>
		<h5>Section 34 Offset </h5>
		<ul>
		  <li>In the event of a debt between you and us, the debt can be offset at any time. </li>
		</ul>
		<h5>Section 35 Lateness Charge </h5>
		<ul>
		  <li>Should you or we fail to pay the other by an appointed date, both parties agree to pay a lateness charge at an annual interest rate of 10%. </li>
		</ul>
		<h5>Section 36 Bylaws </h5>
		<ul>
		  <li>It is possible to make other rules which are as valid and effective as this agreement. </li>
			<li>If we make any new rules or change existing rules, we will detail them in our offices, brochure and price tables. </li>
		</ul>
		<h5>Section 37 Agreed Appropriate Court </h5>
		<ul>
		  <li>In the event of a legal conflict, it will be brought before the judge who has jurisdiction over the area in which our head office is located. </li>
		</ul>
		<p>
	    This agreement is valid from April 15th 2017 <br />This revised version of the agreement is valid from June 20th 2017 </p>
</section>
</div>
