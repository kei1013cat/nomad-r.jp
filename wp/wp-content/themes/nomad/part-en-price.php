<?php require_once('./price_table.php'); ?>

<section class="cal" id="c01">
    <h2 class="headline01 typesquare_tags">Check price of each seasons on calender<span class="blue">!</span></h2>

    <ul class="cal_area cf"><?php dynamic_sidebar('Calendar'); ?></ul>
    <div class="season cf">
        <div class="cf">
            <dl class="cf">
                <dt class="color1"></dt>
                <dd>Off season(weekday)</dd>
            </dl>
            <dl class="cf">
                <dt class="color2"></dt>
                <dd>Off season(Friday, Saturday,Sunday and holidays)</dd>
            </dl>
            <dl class="cf">
                <dt class="color3"></dt>
                <dd>Weekday in second peak season</dd>
            </dl>
        </div>
        <div class="cf">
            <dl class="cf">
                <dt class="color4"></dt>
                <dd>Friday, Saturday, Sunday and Holidays in second peak season</dd>
            </dl>
            <dl class="cf">
                <dt class="color5"></dt>
                <dd>All day in peak season</dd>
            </dl>
            <dl class="cf">
                <dt class="color8"></dt>
                <dd>Winter season</dd>
            </dl>
        </div>
    </div>
    <!--season -->

    <table cellspacing="0" cellpadding="0" class="style01">
        <tr>
            <th>Off-season</th>
            <td>Oct 1st~Dec 20th<br />
                March 1st~ April 2th</td>
        </tr>
        <tr>
            <th>Second peak season</th>
            <td>
                May 7th~June 30th<br />
                Sep 1st~ Sep 30th</td>
        </tr>
        <tr>
            <th>Peak season</th>
            <td>July 1st~ Aug 31th</td>
        </tr>
        <!--         <tr>
            <th><span class="text">ハイシーズン特別日</span></th>
            <td><span class="text">お盆、年末年始、その他</span></td>
        </tr>
 -->
        <tr>
            <th><span class="text">Winter season</span></th>
            <td><span class="text">Dec 21th~ Feb 28th</span></td>
        </tr>
    </table>

</section>
<section class="pricetable" id="c02">
    <h2 class="headline01 typesquare_tags">Price list</h2>

    <h2 class="luxury">Luxury class</h2>
    <div class="box">
        <h3 id="c02_3" class="sunlight">SUNLIGHT
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic10.jpg" width="104" alt="サンライト">
        </h3>
    </div>
    <!-- box -->
    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['luxury']['free']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['luxury']['free']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['luxury']['free']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['free']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['luxury']['off_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['off_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['luxury']['on_tsu']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_tsu']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['luxury']['on_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['on_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['luxury']['high']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['luxury']['high']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['day']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['half']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['luxury']['free']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['day']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['half']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['luxury']['off_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['day']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['half']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['luxury']['on_tsu']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['day']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['half']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['luxury']['on_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><?php echo number_format($price_table['luxury']['high']['day']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['luxury']['high']['half']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['luxury']['high']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['luxury']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php endif; ?>

        <p>* Tax is excluded
        </p>
        <p>* We offer Nomad Car Rental Ansin Pack (RAP) for in case of an accident or damage. (Pay based option)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">About insurance</a></p>
        <p>*We recommend foreign customers to have CDW and RAP</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">Details</a> </p>
        <p>冬期間トイレ水道利用不可です。</p>
    </div>

    <h2 class="plmclass-2">Premium class</h2>
    <div class="box">
        <h3 id="c02_2" class="zil"><span>Luxury saloon</span>ZIL520<img src="<?php bloginfo('template_url'); ?>/images/price_pic03.jpg" alt="ジル520"></h3>
    </div>
    <!-- box -->
    <div class="scroll">

        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['premium']['free']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['free']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['premium']['off_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['off_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['premium']['on_tsu']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_tsu']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['premium']['on_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['on_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['premium']['high']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['premium']['high']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php else: ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['day']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['half']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['premium']['free']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['day']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['half']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['premium']['off_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['premium']['on_tsu']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['day']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['half']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['premium']['on_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['day']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['half']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['premium']['high']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['premium']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php endif; ?>

        <p>* Tax is excluded
        </p>
        <p>* We offer Nomad Car Rental Ansin Pack (RAP) for in case of an accident or damage. (Pay based option)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">About insurance</a></p>
        <p>*We recommend foreign customers to have CDW and RAP</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">Details</a> </p>
        <p>*AC ( available all the time) The latest flat model TV</p>
    </div>

    <h2 class="highclass-2">High class</h2>
    <div class="box">
        <h3 id="c02_1">CORDE BANKS<img src="<?php bloginfo('template_url'); ?>/images/price_pic01.jpg" width="97" alt="コルドバンクス"></h3>
        <h3>CORDE LEAVES<img src="<?php bloginfo('template_url'); ?>/images/price_pic02.jpg" width="104" alt="コルドリーブス"></h3><br class="pc">
    </div>

    <div class="scroll">
        <?php if(is_disc_camp()): ?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color1"><span class="price"><?php echo number_format($price_table['high']['free']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['free']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color2"><span class="price"><?php echo number_format($price_table['high']['off_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['off_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color3"><span class="price"><?php echo number_format($price_table['high']['on_tsu']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_tsu']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color4"><span class="price"><?php echo number_format($price_table['high']['on_syuku']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['on_syuku']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['day']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['day'] - $disc_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['half']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['half'] - $disc_half_amt); ?>JPY</span></td>
                <td class="color5"><span class="price"><?php echo number_format($price_table['high']['high']['time']); ?>JPY</span><span class="disc"><?php if(is_pc()): echo do_shortcode('[wp-svg-icons icon="arrow-right-2" wrap="i"]'); endif; ?>&nbsp;<?php echo number_format($price_table['high']['high']['time'] - $disc_time_amt); ?>JPY</span></td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php else:?>
        <table>
            <tr>
                <th colspan="2" class="h"></th>
                <td class="h">A day</td>
                <td class="h">Half day</td>
                <td class="h">Outside of our business hours<br> 1 hour extension</td>
            </tr>
            <tr>
                <th rowspan="2" class="color1">Off-season</th>
                <td class="color1">Weekday</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['day']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['half']); ?>JPY</td>
                <td class="color1"><?php echo number_format($price_table['high']['free']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color2">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['day']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['half']); ?>JPY</td>
                <td class="color2"><?php echo number_format($price_table['high']['off_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th rowspan="2" class="color3">Second peak season</th>
                <td class="color3">Weekday</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['half']); ?>JPY</td>
                <td class="color3"><?php echo number_format($price_table['high']['on_tsu']['time']); ?>JPY</td>
            </tr>
            <tr>
                <td class="color4">Friday,Saturday,<br>Sunday and Holidays</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['day']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['half']); ?>JPY</td>
                <td class="color4"><?php echo number_format($price_table['high']['on_syuku']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color5">Peak season All day</th>
                <td class="color5"><?php echo number_format($price_table['high']['high']['day']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['high']['high']['half']); ?>JPY</td>
                <td class="color5"><?php echo number_format($price_table['high']['high']['time']); ?>JPY</td>
            </tr>
            <tr>
                <th colspan="2" class="color8">Winter season</th>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['day']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['half']); ?>JPY</td>
                <td class="color8"><?php echo number_format($price_table['high']['winter']['time']); ?>JPY</td>
            </tr>
        </table>
        <?php endif; ?>
        <p>* Tax is excluded</p>
        <p>* We offer Nomad Car Rental Ansin Pack (RAP) for in case of an accident or damage. (Pay based option)</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">About insurance</a></p>
        <p>*We recommend foreign customers to have CDW and RAP</p>
        <p><a class="link" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c11">Details</a></p>
        <p>* Only dog can accompany on pet allowed vehicles. It costs \8,000/ one dog for cleaning fee. \10,000/ two dogs \12,000/ three dogs</p>
        <br>

    </div>

    <h4>Business hour : 9 - 18</h4>
    <p class="text">Half day is 9-12. Afternoon is 12-18. You need to return a car in this hours.<br />
        If you return 18:00-22:00, extension fee will be charged for every 1 hour.
    </p>
    <h4>Notices about late returning</h4>
    <p class="text">If your delay is over 5 hours and it affects preparations for renting next customer and we need to cancel the next booking because of your delay, we can charge you all cost of next booking and your extension fee. Please be careful of delay.</p>

    <h4>（Foreign Visitors to Japan Only）<br>For foreign customers</h4>
    <p class="text">We have Hokkaido Expressway Pass for only for foreign customers.<br>You can use Expressway free pass with ETC.<br>(Example: You can use expressway for \807 / day if you rent 14 days.)<br>*The cost for days of rental will be charged.<br>*If you have Japanese nationality, this pass is not available.<br><a class="link" href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">Details</a></p>

</section>
<section class="discount" id="c03">
    <h2 class="headline01 typesquare_tags">Discounts<span class="blue">!</span></h2>
    <table>
        <tr>
            <th>Long-term discount</th>
            <td class="pd2">10% off for 7 days of rental or more<br>
                15% off for 14 days of rental or more<br>
                20% off for 21 days of rental or more</td>
        </tr>
        <tr>
            <th>Senior discount</th>
            <td>10% off for over 55 years old</td>
        </tr>
        <tr>
            <th>Royal customer discount</th>
            <td>10% off if you have rent a car from us within 1 years.</td>
        </tr>

    </table>
    <div>
        <p>*Discount cannot be applied double. If you have two or more discount, we apply one which is higher late of discount.</p>
    </div>
</section>
<section class="example" id="c04">
    <h2 class="headline01 typesquare_tags">Examples<span class="blue">!</span></h2>

    <ul class="cf">
        <li>One day<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic04.jpg" width="83" alt="１日とは"></li>
        <li>Half day( AM )<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic05.jpg" width="83" alt="半日(午前)とは"></li>
        <li>Half day( PM )<br><img src="<?php bloginfo('template_url'); ?>/images/price_pic06.jpg" width="83" alt="半日（午後）とは"></li>
    </ul>
    </h3>
    <div class="item cf">
        <div class="left">
            <h4>Example 1</h4>
            <p> Car type : CORDE BUNKS<br>
                In case of the rent for one day is <?php echo number_format($price_table['high']['free']['day']); ?>JPY</p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic07.jpg" alt="コルドバンクス" /></p>
        </div>
        <!-- right -->
    </div><!-- item -->
    <table>
        <tr>
            <th>Day of start</th>
            <td>Day1</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['high']['free']['day']); ?>JPY</td>
            <td class="nobg"></td>
        </tr>
        <tr>
            <th>Day of return</th>
            <td>Day2</td>
            <td>～11：30</td>
            <td><?php echo number_format($price_table['high']['free']['half']); ?>JPY</td>
            <td class="nobg"></td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2" class="red"><span>Total</span><?php echo number_format($price_table['high']['free']['day'] + $price_table['high']['free']['half']); ?>JPY＋Tax</td>
            <td class="nobg"></td>
        </tr>
    </table>

    <div class="item cf">
        <div class="left">
            <h4>Example 2</h4>
            <p> Car type : CORDE LEAVES<br>
                In case of the rent for one day is <?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY </p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic08.jpg" alt="コルドリーブス" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>Day of start</th>
            <td>Day1</td>
            <td>13：00～</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['half']); ?>JPY</td>
        </tr>
        <tr>
            <th></th>
            <td>Day2</td>
            <td>Whole day rental</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY</td>
        </tr>
        <tr>
            <th></th>
            <td>Day3</td>
            <td>Whole day rental</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY</td>
        </tr>
        <tr>
            <th>Day of return</th>
            <td>Day4</td>
            <td>～17：30</td>
            <td><?php echo number_format($price_table['high']['on_tsu']['day']); ?>JPY</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>Total</span><?php echo number_format(($price_table['high']['on_tsu']['day'] * 3) + $price_table['high']['on_tsu']['half']); ?>JPY＋Tax</td>
        </tr>
    </table>
    <div class="item cf">
        <div class="left">
            <h4>Example 3</h4>
            <p> Car type : ZIL520<br>
                In case of the rent for one day is <?php echo number_format($price_table['premium']['on_tsu']['day']); ?>JPY </p>
        </div><!-- left -->
        <div class="right">
            <img src="<?php bloginfo('template_url'); ?>/images/price_pic09.jpg" alt="ジル520" />
        </div>
    </div><!-- item -->
    <table>
        <tr>
            <th>Day of start</th>
            <td>Day1</td>
            <td>9：00～</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>JPY</td>
        </tr>
        <tr>
            <th></th>
            <td>Day2</td>
            <td>Whole day rental</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['day']); ?>JPY</td>
        </tr>
        <tr>
            <th>Day of return</th>
            <td>Day3</td>
            <td>～12：00</td>
            <td><?php echo number_format($price_table['premium']['on_tsu']['half']); ?>JPY</td>
        </tr>
        <tr class="red">
            <th class="nobg"></th>
            <td class="nobg"></td>
            <td colspan="2"><span>Total</span><?php echo number_format(($price_table['premium']['on_tsu']['day'] * 2) + $price_table['premium']['on_tsu']['half'] ); ?>JPY＋Tax</td>
        </tr>
    </table>
</section>
<section class="empty" id="c05">
    <!--<h2><img src="<?php bloginfo('template_url'); ?>/images/price_tit05.jpg" alt="空車状況カレンダー"/></h2>
    <ul>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス1 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=coh8o21ju2itn8uah33819p678%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_2">
            <h4 class="blue"> コルドバンクス2 <img src="<?php bloginfo('template_url'); ?>/images/car_pic001.png" alt="コルドバンクス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=kbsrmr4pml0afjgjsrg10svqn4%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_3">
            <h4 class="green"> コルドリーブス <img src="<?php bloginfo('template_url'); ?>/images/car_pic002.png" alt="コルドリーブス"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=nfota0r3s60uftr61e5nv94k1o%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
        <li id="c05_1">
            <h4 class="yellow"> ジル520 <img src="<?php bloginfo('template_url'); ?>/images/car_pic003.png" alt="ジル520"/> </h4>
            <div>
                <iframe src="https://calendar.google.com/calendar/embed?src=d7hnk3hgae5lbsr6fjsl1dc528%40group.calendar.google.com&ctz=Asia/Tokyo&showPrint=0&showTabs=0&showCalendars=0&showTitle=0" style="border: 0" frameborder="0" scrolling="no"></iframe>
            </div>
        </li>
    </ul>
            <p class="text">ご予約は随時　メール　電話等により受け賜りますが　サイトへの表示には時間差があり、予約可能になっていても時間差で予約済みになってしまう場合もあります事をご了承くださいませ。</p><br class="sp">-->
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">Make a reservation here　＞</a></p>

</section>
