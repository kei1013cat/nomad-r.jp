<?php require_once('./lang/lang_rental.php'); ?>
<?php require('./price_table.php'); ?>

<section class="rental free" id="c01">
	<h2 class="headline01 typesquare_tags">寝具クリーニング代</h2>
	<div class="gallery_top">
		<ul class="cf">
	<li>
				<p class="photo"><a href="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" data-lightbox="charge"><img src="<?php bloginfo('template_url'); ?>/images/rental_charge_photo01.jpg" alt="タオルケット・ベッドシーツ・枕セット　５００円"></a>
				<p class="text">寝具セット【タオルケット（冬期間は厚手毛布）ベットシーツ・枕（枕カバー付き）】<br /><br />

1回のレンタルにつきご利用人数×500円をクリーニング代として頂戴しております。<br>
(安心・清潔な寝具をお使いいただけるよう毎回専門業者様にてクリーニングしております)</p>
				</a></li></ul>
				</div><!-- gallery -->
	<h2 class="headline01 typesquare_tags">有料レンタルグッズ</h2>	
	<h3>料金は1日あたりの税別金額です（半日も同額です）<br>
	最長5日目まで料金がかかります。<br>
	※6日目以降は5日間分の料金で継続ご利用いただけます。
	</h3>
	<div class="gallery">
		<ul class="cf">
			<?php
			$i = 1;
			if( have_rows('有料レンタルグッズ',$rental_post_id)):
			  while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
			  	<?php if(!get_sub_field('非表示')): ?>
				<?php $rental_img = get_sub_field('画像'); ?>
				<li class="fead<?php echo $i; ?>">
					<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
						<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?>"></p>
						<p class="text"><?php echo ${'rental_'.get_sub_field('レンタルグッズコード')}; ?></p>
					</a>

				</li>
				<?php endif; ?>
			<?php
			  $i++;
			  if($i==3) { $i=1; }
			  endwhile;
			endif;
			?>
		</ul>
		<p class="tax">価格はすべて税別です</p>
	</div>
	<!-- gallery -->
</section>
<section class="rental charge" id="c02">
	<h2 class="headline01 typesquare_tags">無料車内設置備品</h2>	

	<div class="gallery">
		<ul class="cf">

		<?php
		$i = 1;
		if( have_rows('無料車内設置備品',$rental_post_id)):
		  while( have_rows('無料車内設置備品',$rental_post_id) ): the_row(); ?>
		  	<?php if(!get_sub_field('非表示')): ?>
			<?php $rental_img = get_sub_field('画像'); ?>
			<li class="fead<?php echo $i; ?>">
				<a href="<?php echo $rental_img['sizes']['rental_photo'];?>" data-lightbox="charge">
					<p class="photo"><img src="<?php echo $rental_img['sizes']['rental_photo'];?>" alt="<?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?>"></p>
					<p class="text"><?php echo ${'rental_free_'.get_sub_field('備品コード')}; ?></p>
				</a>

			</li>
			<?php endif; ?>
		<?php
		  $i++;
		  if($i==3) { $i=1; }
		  endwhile;
		endif;
		?>

		</ul>
	</div>
	<!-- gallery -->
</section>
