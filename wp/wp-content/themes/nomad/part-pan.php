<?php if(isset($_GET['lang']) && $_GET['lang'] == 'zh' && (basename($_SERVER["PHP_SELF"]) == 'reservation.php' || basename($_SERVER["PHP_SELF"]) == 'mail.php')): ?>
<div id="pan">
	<div class="wrapper"><a href="http://zh.nomad-r.jp">首頁</a>&nbsp;&gt;&nbsp;預約單</div>
	<!-- wrapper --></div>
<?php elseif(isset($_GET['lang']) && $_GET['lang'] == 'th' && (basename($_SERVER["PHP_SELF"]) == 'reservation.php' || basename($_SERVER["PHP_SELF"]) == 'mail.php')): ?>
<div id="pan">
	<div class="wrapper"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>">หน้าแรก</a>&nbsp;&gt;&nbsp;แบบฟอร์มเช่ารถ</div>
	<!-- wrapper --></div>
<?php elseif(isset($_GET['lang']) && $_GET['lang'] == 'en' && (basename($_SERVER["PHP_SELF"]) == 'reservation.php' || basename($_SERVER["PHP_SELF"]) == 'mail.php')): ?>
<div id="pan">
	<div class="wrapper"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>">HOME</a>&nbsp;&gt;&nbsp;Price & Booking</div>
	<!-- wrapper --></div>
<?php else: ?>
<div id="pan">

<?php if(lang()=='ja'): ?>
	<div class="wrapper"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>">ホーム</a>&nbsp;&gt;&nbsp;<?php if( $post->post_type == "tabi"): ?><a href="<?php bloginfo('url'); ?>/?post_type=tabi">ノマド旅情報</a>&nbsp;&gt;&nbsp;<?php elseif(is_single()):?><a href="<?php bloginfo('url'); ?>/news/">お知らせ一覧</a>&nbsp;&gt;&nbsp;<?php endif;?><?php echo get_the_title() ?></div>
	<!-- wrapper --></div>
<?php endif; ?>

<?php if(lang()=='th'): ?>
	<div class="wrapper"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>">หน้าแรก</a>&nbsp;&gt;&nbsp;<?php if( $post->post_type == "tabi"): ?><a href="<?php bloginfo('url'); ?>/?post_type=tabi">ノマド旅情報</a>&nbsp;&gt;&nbsp;<?php elseif(is_single()):?><a href="<?php bloginfo('url'); ?>/news/">お知らせ一覧</a>&nbsp;&gt;&nbsp;<?php endif;?><?php echo get_the_title() ?></div>
	<!-- wrapper --></div>
<?php endif; ?>

<?php if(lang()=='en'): ?>
	<div class="wrapper"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>">HOME</a>&nbsp;&gt;&nbsp;<?php if( $post->post_type == "tabi"): ?><a href="<?php bloginfo('url'); ?>/?post_type=tabi">ノマド旅情報</a>&nbsp;&gt;&nbsp;<?php elseif(is_single()):?><a href="<?php bloginfo('url'); ?>/news/">お知らせ一覧</a>&nbsp;&gt;&nbsp;<?php endif;?><?php echo get_the_title() ?></div>
	<!-- wrapper --></div>
<?php endif; ?>


<!-- pan -->
<?php endif; ?>