<?php

//-------------------------------------------
//投稿部分表示カスタマイズ
//-------------------------------------------
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'お知らせ';
    $submenu['edit.php'][5][0] = 'お知らせ一覧';
    $submenu['edit.php'][10][0] = '新規作成';
    //$submenu['edit.php'][16][0] = 'タグ';
    //echo ”;
}
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'お知らせ';
    $labels->singular_name = 'お知らせ';
    $labels->add_new = _x('新規作成', '新着情報');
    $labels->add_new_item = '新しい情報';
    $labels->edit_item = '情報の編集';
    $labels->new_item = '新しい情報';
    $labels->view_item = '新着情報を表示';
    $labels->search_items = '新着情報検索';
    $labels->not_found = '新着情報が見つかりませんでした';
    $labels->not_found_in_trash = 'ゴミ箱の新着情報にも見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );


//-------------------------------------------
//カスタム投稿タイプ(ノマド旅情報)
//-------------------------------------------
add_action('init', 'my_custom_init');
function my_custom_init()
{
  $labels = array(
    'name' => _x('ノマド旅情報', 'post type general name'),
    'singular_name' => _x('ノマド旅情報一覧', 'post type singular name'),
    'add_new' => _x('新規作成', 'tabi'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('ノマド旅情報を編集'),
    'new_item' => __('新しいノマド旅情報'),
    'view_item' => __('ノマド旅情報一覧を見る'),
    'search_items' => __('ノマド旅情報を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => ''
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 5,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
    'supports' => array('title','editor','thumbnail'),
    'has_archive' => true
  );
  register_post_type('tabi',$args);
}

//-------------------------------------------
//  カスタム投稿タイプ(割引キャンペーン)
//-------------------------------------------
add_action('init', 'my_custom_init2');
function my_custom_init2()
{
  $labels = array(

    'name' => _x('割引・キャンペーン', 'post type general name'),
    'singular_name' => _x('割引・キャンペーン', 'post type singular name'),
    'add_new' => _x('新規作成', 'camp'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい投稿'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'camp' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => false
  );
  register_post_type('camp',$args);
}

//-------------------------------------------
//  カスタム投稿タイプ(車種)
//-------------------------------------------
add_action('init', 'my_custom_init3');
function my_custom_init3()
{
  $labels = array(

    'name' => _x('車種', 'post type general name'),
    'singular_name' => _x('車種', 'post type singular name'),
    'add_new' => _x('新規作成', 'carmodel'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい投稿'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'carmodel' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => false
  );
  register_post_type('carmodel',$args);
}

//-------------------------------------------
//  カスタム投稿タイプ(車種)
//-------------------------------------------
add_action('init', 'my_custom_init4');
function my_custom_init4()
{
  $labels = array(

    'name' => _x('料金', 'post type general name'),
    'singular_name' => _x('料金', 'post type singular name'),
    'add_new' => _x('新規作成', 'price'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい投稿'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'price' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => false
  );
  register_post_type('price',$args);
}

//-------------------------------------------
//  カスタム投稿タイプ(レンタルグッズ)
//-------------------------------------------
add_action('init', 'my_custom_init5');
function my_custom_init5()
{
  $labels = array(

    'name' => _x('レンタルグッズ', 'post type general name'),
    'singular_name' => _x('レンタルグッズ', 'post type singular name'),
    'add_new' => _x('新規作成', 'rental'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい投稿'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'rental' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => false
  );
  register_post_type('rental',$args);
}

//-------------------------------------------
//  カスタム投稿タイプ(おすすめスポット)
//-------------------------------------------
add_action('init', 'my_custom_init6');
function my_custom_init6
    ()
{
  $labels = array(

    'name' => _x('おすすめスポット', 'post type general name'),
    'singular_name' => _x('おすすめスポット', 'post type singular name'),
    'add_new' => _x('新規作成', 'spot'),
    'add_new_item' => __('新規作成'),
    'edit_item' => __('記事を編集'),
    'new_item' => __('新しい投稿'),
    'view_item' => __('記事を見る'),
    'search_items' => __('記事を探す'),
    'not_found' =>  __('記事はありません'),
    'not_found_in_trash' => __('ゴミ箱に記事はありません'),
    'parent_item_colon' => '',
  'enter_title_here' => _x( 'タイトルを入力', 'rental' ),
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,

    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => 6,
    //'supports' => array('title','editor','thumbnail','custom-fields','excerpt','author','trackbacks','comments','revisions','page-attributes'),
  'supports' => array('title','editor','thumbnail'),
    'has_archive' => false
  );
  register_post_type('spot',$args);
}

//-------------------------------------------
// アイチャッチ有効化
//-------------------------------------------
add_theme_support('post-thumbnails');


//-------------------------------------------
//サムネイルサイズを追加
//-------------------------------------------

function thumbAdd() {
add_theme_support( 'post-thumbnails' ); //テーマをサムネイル表示に対応させる
add_image_size( 'car_photo_cal', 120,84, true ); //予約ページのカレンダー上部
add_image_size( 'car_photo_form', 100,58, true ); //予約ページのフォーム部
add_image_size( 'rental_photo', 800,600, true ); //予約ページのフォーム部
add_image_size( 'spot_photo', 400,225, true ); //おすすめスポット
}
add_action( 'after_setup_theme', 'thumbAdd' );


//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
     $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
    global $is_mobile;
    return $is_mobile;
    //return false;
}
function is_pc(){
    if(is_mobile()){
        return false;
    }else{
        return true;
    }
}
function mobile_img(){
    if (is_mobile()) {
        echo "_sp";
    }
}

//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}


//-------------------------------------------
//言語判定
//-------------------------------------------
function lang() {

    $langlist = array('ja','en','zh','th');

    if(isset($_GET['lang']) && in_array($_GET['lang'], $langlist)){
        return $_GET['lang'];
    }
    foreach($langlist as $lang){
        // if ( strpos( $_SERVER["REQUEST_URI"], $lang ) !== false ) {
        //     return $lang;
        // }
        // 修正追加 keisuke
        if(substr( $_SERVER["REQUEST_URI"],0,1 ) == "/" && substr( $_SERVER["REQUEST_URI"],3,1 ) =="/") {
            if ( substr( $_SERVER["REQUEST_URI"],1,2 ) == $lang ) {
                return $lang;
            }

        }
    }
    return "ja";
}

//-------------------------------------------
//言語用URIの生成
//-------------------------------------------
function lang_uri() {

    $lang = lang();
    // 日本の場合はNULLで返す
    if ($lang=="ja") {
        $lang = "";
    } else {
        $lang =  $lang . "/";
    }
    return $lang;
}

function getMultiLang($en,$ja,$zh,$th) {
    $ret = array(
        'en' => $en,
        'ja' => $ja,
        'zh' => $zh,
        'th' => $th
    );
    return $ret[lang()];
}

function getCurrentLang(){

  if (isset($_POST['lang'])) {
      $lang = $_POST['lang'];
  }

  if(isset($_GET['lang'])) {
      $lang = $_GET['lang'];
  } else {
      $lang = 'ja'; // デフォルト
  }
  return $lang;
}

//-------------------------------------------
//引数で指定した日が割引キャンペーンの日かどうか判定
//-------------------------------------------
function is_days_disc_camp($std_dt) {
    global $disc_camp_start_dt,$disc_camp_end_dt;

    if($disc_camp_start_dt=="" || $disc_camp_end_dt=="") {
        return false;
    }

    if((date($std_dt) >= date($disc_camp_start_dt)) && (date($disc_camp_end_dt) >= date($std_dt))) {
        return true;
    } else {
        return false;
    }
}

//-------------------------------------------
//割引キャンペーンが有効かどうか判定
//-------------------------------------------
function is_disc_camp() {
    global $disc_camp_start_dt,$disc_camp_end_dt;
    if($disc_camp_start_dt=="" || $disc_camp_end_dt=="") {
        return false;
    } else {
        return true;
    }
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function delete_host_from_attachment_url( $url ) {
    $regex = '/^http(s)?:\/\/[^\/\s]+(.*)$/';
    if ( preg_match( $regex, $url, $m ) ) {
        $url = $m[2];
    }
    return $url;
}
add_filter( 'wp_get_attachment_url', 'delete_host_from_attachment_url' );
add_filter( 'attachment_link', 'delete_host_from_attachment_url' );

function replaceImagePath($arg) {
    $content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
    return $content;
}
add_action('the_content', 'replaceImagePath');
register_sidebar();


//-------------------------------------------
//抜粋に「続きを読む」リンクを追加したい
//-------------------------------------------

function new_excerpt_more($post) {
     return '...<p class="tar"><a href="'. get_permalink($post->ID) . '" class="more-link">' . '続きを読む' . '</a></p>';
}
add_filter('excerpt_more', 'new_excerpt_more');

//-------------------------------------------
// カレンダー表示
//-------------------------------------------
register_sidebar(array(
  'name' => 'Calendar'
));

//-------------------------------------------
// 固定ページにカテゴリーを表示
//-------------------------------------------
add_action('init','add_categories_for_pages');
function add_categories_for_pages(){
register_taxonomy_for_object_type('category', 'page');
}
add_action( 'pre_get_posts', 'nobita_merge_page_categories_at_category_archive' );
function nobita_merge_page_categories_at_category_archive( $query ) {
if ( $query->is_category== true && $query->is_main_query() ) {
$query->set('post_type', array( 'post', 'page', 'nav_menu_item'));
}
}



?>
