<?php
  $url = $_SERVER['REQUEST_URI'];
?>
<div id="page_title">
	<div class="wrapper">
		<?php if($post->post_type == "tabi"): ?>
			<h2 class="fead2">ノマド旅情報</h2>
		<?php elseif(is_single() || is_archive()): ?>
			<h2 class="fead2">お知らせ</h2>
		<?php elseif(lang()=='en' && ($post->post_name =='price' ||$post->post_name =='confirm' || $post->post_name =='thanks')): ?>
			<h2 class="fead2">Price & Booking</h2>
		<?php elseif(lang()=='zh' && ($post->post_name =='price' || $post->post_name =='confirm' || $post->post_name =='thanks')): ?>
			<h2 class="fead2">預約單</h2>
		<?php elseif(lang()=='th' && ($post->post_name =='price' || $post->post_name =='confirm' || $post->post_name =='thanks')): ?>
			<h2 class="fead2">แบบฟอร์มเช่ารถ</h2>
		<?php else: ?>
			<h2 class="fead2"><?=$post->post_title;?></h2>
		<?php endif; ?>
		
		<?php if(lang()!="en"): ?>
		<?php if(strstr($url,'price') == true): ?>
			<p class="fead4">Price</p>
		<?php endif; ?>

		<?php if((lang()=='ja' || lang()=='zh') && (strstr($url,'reservation.php') == true || strstr($url,'mail.php') == true || ($post->post_name =='confirm' || $post->post_name =='thanks'))): ?>
			<p class="fead4">Reservation</p>
		<?php endif; ?>

		<?php if($post->post_name =='yakkan'): ?>
		<p class="fead4">Agreement</p>
		<?php endif; ?>

		<?php if($post->post_name =='car'): ?>
		<p class="fead4">Introduce the car</p>
		<?php endif; ?>

		<?php if($post->post_name =='rental'): ?>
		<p class="fead4">Rental goods</p>
		<?php endif; ?>

		<?php if($post->post_name =='guide'): ?>
		<p class="fead4">Guide</p>
		<?php endif; ?>

		<?php if($post->post_name =='contact'): ?>
		<p class="fead4">Contact</p>
		<?php endif; ?>

		<?php if($post->post_name =='tourism'): ?>
		<p class="fead4">海外からの皆様へ</p>
		<?php endif; ?>

		<?php if(is_single() && $post->post_type != "tabi"): ?>
		<p class="fead4">News</p>
		<?php endif; ?>
		<?php endif; ?>

	</div>
	<!-- wrapper -->
</div>
<!-- page_title -->