<script>
    $(function() {
        $(document).on('click', '.open-options1', function(event) {
            event.preventDefault();
            $('.modal-options1').iziModal('open');
        });
        $('.modal-options1').iziModal({
            headerColor: '#0a8bcf', //ヘッダー部分の色
            width: 600, //横幅
            overlayColor: 'rgba(0, 0, 0, 0.5)', //モーダルの背景色
            fullscreen: true, //全画面表示
            transitionIn: 'fadeInUp', //表示される時のアニメーション
            transitionOut: 'fadeOutDown' //非表示になる時のアニメーション
        });
    });

</script>


<section class="reserve" id="c01">
    <h2 class="headline01">ご予約方法</h2>
    <h3 class="headline01 typesquare_tags"><span class="blue">簡単4ステップ</span>で、<br>すぐにキャンピングカーをレンタルできます。</h3>

    <p class="step"><img src="<?php bloginfo('template_url'); ?>/images/guide_reserve_step.jpg?v=201706" alt="STEP" /></p>
    <dl class="cf">
        <dt><span class="left">STEP01</span><span class="right">車種を選択　仮ご予約</span></dt>
        <dd>
            <p><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車カレンダー</a>をご確認の上、予約フォームより必要事項を記載の上送信してください。この時点では、まだ仮予約の状態です。<br />
                <!-- 予約内容を確認の上、-->2営業日以内に弊社よりお支払い合計金額と詳細をメールにてご返信させていただきます。<br />
                【仮予約】の日に関しては、1週間を目途にキャンセルが出る可能性がありますので、キャンセル待ちも受付可能です。<br>
                <br>
                ＊万が一、１営業日以内に弊社からの返信メールが行かない場合、迷惑メールフォルダに紛れている事も考えられます。
                <!-- その場合は、-->お手数ですが、info@nomad-r.jp までメールにてご連絡くださいませ。 </p>
        </dd>
    </dl>
    <dl class="cf">
        <dt><span class="left">STEP02</span><span class="right">1週間以内にご入金で正式予約完了</span></dt>
        <dd>
            <!--
            <p>合計金額等をご確認の上、1週間以内にご利用料金全額を銀行振込またはクレジットカードにてご入金ください。入金が確認された時点で正式ご予約となります。<span class="red">1週間を過ぎてご入金が確認できない場合は、自動キャンセルとなります。</span><br>
                クレジットカード払いのお客様には、メールにてご案内させていただきます。<br>
                <p class="border">【銀行振込お支払い口座】<br />
                    北洋銀行　札幌西支店<br>
                    普通　５３２６１１１<br>
                    口座名　ホッカイドウノマドレンタカー </p>
-->
            <p>合計金額等をご確認の上、1週間以内にご利用料金全額をクレジットカードにてご入金ください。入金が確認された時点で正式ご予約となります。1週間を過ぎてご入金が確認できない場合は、自動キャンセルとなります。<br>
                お支払い方法につきましては、メールにてご案内させていただきます。</p>
            <p>※詳しくは<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04">キャンセルポリシー</a>をご覧ください。</p>

    </dl>
    <dl class="cf">
        <dt><span class="left">STEP03</span><span class="right">ご出発</span></dt>
        <dd>
            <p>出発当日は、お約束の場所と時間に合わせお越し下さいませ。車輛設備の使用方法説明、貸渡し契約書のご確認をしていただきサインをいただきます。所要時間は約４０分～５０分ほどです。</p>
            <p>ご確認後、安全運転で思いっきり北海道の魅力を堪能する旅へご出発くださいませ。レンタル中、不安な事やわからない事、おススメスポットなどはいつでもご連絡くださいませ。最高の旅を味わっていただけるよう精一杯のおもてなしをさせていただきたいと思います。</p>
            <ul>
                <li>＊万が一遅れそうな場合は　スタッフの携帯電話までご連絡くださいませ。</li>
                <li>＊飛行機の遅延等で連絡できない場合を除き、事前連絡が無く、お約束の時間から３０分を越えてもお越しいただけない場合は自動キャンセルとさせていただき、規定のキャンセル料をお支払いただく事になりますのでご注意くださいませ。</li>
            </ul>
        </dd>
    </dl>
    <dl class="cf">
        <dt><span class="left">STEP04</span><span class="right">燃料満タンでご返却</span></dt>
        <dd>
            <p>返却場所より3㎞以内のスタンドで、燃料を満タンにしてください。<span class="red">油種確認のためにレシートをスタッフにお見せいただきますので、必ず領収証をお持ちください。</span></p>
            <ul>
                <li><span class="red">＊油種間違いの場合は保険や免責及び安心パックの加入に関わらず適用外となり修理代と休車補償全額ご請求となります</span>のでくれぐれもご注意ください。</li>
                <li>＊返却時間を変更される場合は、スタッフの携帯電話までご連絡くださいませ。</li>
                <li>＊延長の場合は、別途延長料がかかりますが、次の方への貸出に影響が出る場合はお断りさせていただく事もございます。</li>
            </ul>
        </dd>
    </dl>
</section>
<section class="cancel" id="c04">
    <h2 class="headline01">キャンセルポリシー</h2>
    <p>ご予約をやむを得ずキャンセルする場合は、キャンセル料がかかります。</p>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>正式ご予約～乗車の1ヶ月（31日前）まで</td>
            <td>10,000円</td>
        </tr>
        <tr>
            <td>乗車の1ヶ月（30日前）～1週間前（7日前）</td>
            <td>利用料金の50%</td>
        </tr>
        <tr>
            <td>乗車の1週間前（6日前）～前日（1日前）</td>
            <td>利用料金の80％</td>
        </tr>
        <tr>
            <td>乗車当日</td>
            <td>利用料金の100％</td>
        </tr>
    </table>
    <p>お子様の急な発熱やお仕事などでやむをえない場合もあると思いますので、半年以内に日程を変更後<br />
        予約をいただける場合は、キャンセル料は無料で変更させていただきます（１回限り）<br />
        ※料金はお預かりさせていただきます。</p>

    <h3>返金について</h3>
    <div class="indent">
        <p>キャンセル時の返金について、下記の通りご指定の口座へ返金いたします。</p>
        <!--
        <p>
            ＜口座振込みのお客様＞<br>
            キャンセル料を引いた残金を口座へお振込みいたします。（振込み手数料はお客様ご負担）</p>-->
        <!--
        <p>
            ＜クレジット決済のお客様＞</p>-->
        <p>
            キャンセル料とクレジット決済手数料（決済金の3.25%）を<br class="pc">差引いた残金を口座へお振込みいたします。（振込み手数料はお客様ご負担）</p>
    </div>
    <!--
    <h3>日程変更の場合（期間を短くする場合）</h3>

    <div class="ex">

        <div class="indent">
            <p>乗車日から起算し、キャンセルポリシーに則って返金対象とする。</p>
            <p class="bold">(例) 8/12～8/15でご利用予定のお客様が8/12～8/14に日程を変更する（1日短縮）場合</p>
            <dl>
                <dt>7/12までのご連絡　→　</dt>
                <dd>1日分の利用料金減算（バンクス＆リーブスの場合40,000円減算）</dd>
                <dt>7/13～8/5までのご連絡　→　</dt>
                <dd>1日分の利用料金の50％をキャンセル料として発生（バンクス＆リーブスの場合20,000円減算）</dd>
                <dt>8/6～8/11までのご連絡　→　</dt>
                <dd>1日分の利用料金の80％をキャンセル料として発生（バンクス＆リーブスの場合8,000円減算）</dd>
                <dt>8/12（出発日）のご連絡　→　</dt>
                <dd>1日分の利用料金の１００％をキャンセル料として発生（減算なし）</dd>
            </dl>
        </div>
    </div>

    <section id="c10">
        <h3>海外の方の場合</h3>
        <div class="indent">
            <p>海外の方がご利用時はCDW（免責補償）とRAP（レンタカー安心パック）の両方をご加入をおすすめします。<br>※お申し込みいただけない場合は出発前にデポジット（補償金）として50万円をカードでお預かりします。</p>
        </div>
    </section>

    <h4>海外の方限定　【デポジット】</h4>
    <div class="indent">
        <p>※出発時にデポジットを預かります。（クレジットでお預かり）<br>
            CDW免責補償+RAP安心パック保険加入者→50,000円<br>
            CDW免責補償のみ加入者→300,000円<br>
            CDW免責・RAP安心パック保険　未加入者→500,000円<br>
            <br>
            事故の際に使う免責分と交通違反時の反則金、車輌破損時の費用として実費分をお預かり金額の中からご精算させていただきます。
            <br><br>
            特に該当する事故・修理等が無い場合は、車輌返却後にお預かりした金額全額をクレジット返金処理にてお戻しいたします。</p>
    </div>-->
</section>
<section class="hoken" id="c02">
    <h2 class="headline01">保険・保障</h2>

    <h3>保険・保障制度について</h3>
    <section class="about">

        <h4>□基本保険・補償額<span class="small">（レンタル費用に含まれている分）</span></h4>
        <div class="indent line_b">
            <div class="scroll">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <th>&nbsp;</th>
                        <td>&nbsp;</td>
                        <td>補償額</td>
                        <td>
                            免責額（事故・破損の際にお客様が負担する免責金額）
                        </td>
                    </tr>
                    <tr>
                        <th>対人</th>
                        <td>１名限定額</td>
                        <td><span class="blue">無制限</span></td>
                        <td>無し</td>
                    </tr>
                    <tr>
                        <th>対物</th>
                        <td rowspan="2">１事故限定額</td>
                        <td><span class="blue">無制限</span></td>
                        <td><span class="red bold2">100,000円</span></td>
                    </tr>
                    <tr>
                        <th>車両</th>
                        <td><span class="blue">時　価</span></td>
                        <td><span class="red bold2">100,000円</span></td>
                    </tr>
                    <tr>
                        <th rowspan="2">人身傷害</th>
                        <td colspan="3" class="tal">※1名につき 50,000,000円まで </td>
                    </tr>
                    <tr>
                        <td colspan="3" class="tal">※搭乗者の自動車事故によるケガ （後遺障害を含みます） 及び死亡につき、運転者の過失割合に関わらず、 損害額を補償いたします。（限度額 50,000,000円 : 損害額認定は保険約款に基づき保険会社が実施）</td>
                    </tr>
                </table>
            </div>

            <p>キャンピングカーの修理代は高額です！<br>
                <span class="red bold">＊万が一事故・破損の際は、上記免責額の他、下記の費用もご負担いただきます</span></p>

            <div class="under_arrow"></div>

            <ul class="line_box">
                <li>●休車補償料 NOC（1日10,000円　最長20日間）・・・・<span class="red">200,000円（最長）</span><br>
                    <span class="small">万一当社の責任によらない事故･盗難･故障･汚損等が発生し、車両の修理･清掃等が必要となった場合、その期間中の｢営業補償｣としてご負担いただきます。（車内装備の損害、シートの焦げ穴等もＮＯＣの対象となります）</span>
                </li>
                <li>
                    ●フロントガラス破損・傷（飛び石含む）・・・・・・・・ <span class="red">80,000円</span>
                </li>
                <li>
                    ●タイヤバースト交換費用・・・・・・・・・・・・・・・ <span class="red">20,000円</span>
                </li>
                <li>
                    ●車内外設備破損・汚れ・修理代・・・・・・・・・・・・ <span class="red">実費請求</span>
                </li>
                <li>
                    ●車輌搬送費<br>
                    <span class="small">（当社指定工場まで）の、300,000円（税込）までは補償の範囲内となり、それを超える部分はお客さまのご負担となります。</span>
                </li>
            </ul>
            <div class="pt">
                <!--
                <h4 class="underline">このような方は弊社の追加補償に入らなくても大丈夫ですのでご確認ください</h4>
                <p>＊自家用車の自動車保険の多くにオプションで付いている<span class="red">「他車運転特約」</span>にご加入の方はご自身の保険を使う事ができますので、出発前までにご確認くださいませ。<br>
                </p>-->

                <h4>＊高額な修理代を気にしないで安心して旅を楽しみたい方は・・・・</h4>
                <div class="under_arrow"></div>
                <h4 class="blue">安心の追加補償オプション制度への加入をおすすめします</h4>
                <p>＊約90％のお客様が加入されます<br>
                </p>
            </div>

        </div>
        <section id="option">
            <h4>□追加補償オプション制度ご加入のご案内</h4>

            <p>キャンピングカーの修理代は高額ですので、追加補償の加入をオススメします<br>
                <span class="red bold">＊万が一事故の際は、下記の費用負担が免除されます。</span></p>

            <div class="indent line_b">
                <div class="scroll">
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <th rowspan="2"><span class="blue">＊追加補償料は最大15日分のお支払いで、<br>全貸出期間適用になります。</span></th>
                            <th rowspan="2">①基本保険</th>
                            <th rowspan="2">②CDW加入<br>3,000円/1日</th>
                            <th rowspan="2">③CDW+RAP加入<br>4,500円/1日</th>
                        </tr>
                        <tr> </tr>
                        <tr>
                            <th>事故時　免責額（対物）</th>
                            <td><span class="red">100,000円</span></td>
                            <td><span class="blue">0円</span></td>
                            <td><span class="blue">0円</span></td>
                        </tr>
                        <tr>
                            <th>事故時　免責額（車輌）</th>
                            <td><span class="red">100,000円</span></td>
                            <td><span class="blue">0円</span></td>
                            <td><span class="blue">0円</span></td>
                        </tr>
                        <tr>
                            <th>休車補償料　NOC<br>（1日10,000円　最長20日間）</th>
                            <td><span class="red">200,000円</span></td>
                            <td><span class="red">200,000円</span> </td>
                            <td><span class="blue">0円</span></td>
                        </tr>
                        <tr>
                            <th>フロントガラス破損・傷（飛び石）</th>
                            <td><span class="red">80,000円</span></td>
                            <td><span class="red">80,000円</span></td>
                            <td><span class="blue">0円</span></td>
                        </tr>
                        <tr>
                            <th>タイヤ破損時</th>
                            <td><span class="red">20,000円</span></td>
                            <td><span class="red">20,000円</span></td>
                            <td><span class="blue">0円</span></td>
                        </tr>
                        <tr>
                            <th>車内設備破損<br>（汚れ・キズ・破損傷修理代）</th>
                            <td><span class="red">実費請求</span></td>
                            <td><span class="red">実費請求</span></td>
                            <td><span class="blue">0円<br>（一般的な利用時）</span></td>
                        </tr>
                        <tr>
                            <th><span class="red"><span class="fsize_l">事故時のお客様負担最大費用</span><br>（返却時デポジットお支払い）</span></th>
                            <td><span class="red fsize_l">500,000円</span></td>
                            <td><span class="red fsize_l">300,000円</span></td>
                            <td><span class="blue fsize_l">0円</span></td>
                        </tr>
                    </table>
                </div>
                <p class="underline">＊返却時に、車輌にキズがあった場合は一度上記費用をお支払いいただき、修理後差額があれば返金となります</p>
            </div>

            <h4>□弊社の追加補償に入らず　自家用車の「<span class="red">他車運転特約保険</span>」をご利用いただく事もできます。</h4>
            <div class="indent">
                <h5>①<span class="red">500,000円</span>請求の事例（へこみ・変形・割れ）修理期間約20～90日間</h5>
                <img src="<?php bloginfo('template_url'); ?>/images/guide_tokuyaku_hoken1.jpg" />
                <h5 class="pt">②<span class="red">300,000円</span>請求の事例（変形が無く擦り傷のみ）修理期間約15～30日間</h5>
                <img src="<?php bloginfo('template_url'); ?>/images/guide_tokuyaku_hoken2.jpg" />
            </div>
        </section>

        <!--
        <h4>□保険・補償額</h4>
        <div class="indent line_b">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th>&nbsp;</th>
                    <td>&nbsp;</td>
                    <td>補償額</td>
                    <td>免責額（万が一の際お客様が負担する金額）</td>
                </tr>
                <tr>
                    <th>対人</th>
                    <td>１名限定額</td>
                    <td>無制限</td>
                    <td>無し</td>
                </tr>
                <tr>
                    <th>対物</th>
                    <td rowspan="2">１事故限定額</td>
                    <td>無制限</td>
                    <td><span class="red bold">100,000円</span></td>
                </tr>
                <tr>
                    <th>車両</th>
                    <td>時　価</td>
                    <td><span class="red bold">100,000円</span></td>
                </tr>
                <tr>
                    <th>人身傷害</th>
                    <td colspan="3" class="tal">※1 名につき 50,000,000円まで <br />
                        ※搭乗者の自動車事故によるケガ ( 後遺障害を含みます ) 及び死亡につき、運転者の過失割合に関わらず、 損害額を補償いたします。( 限度額 50,000,000円 : 損害額認定は保険約款に基づき保険会社が実施 )</td>
                </tr>
            </table>
        </div>
    </section>
    <section>
        <h4 class="big">□免責補償制度 (ＣＤＷ)　<span class="red">※加入をおススメします</span></h4>
        <div class="indent">
            <p>万一事故の際にお客様にご負担いただく対物・車輌補償の免責額のお支払いが免除される制度です。<br />
                <span class="comm red">※｢シートベルト着用｣を条件とします。</span></p>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <th scope="col">加入料 (1台につき 1日)</th>
                    <th scope="col">ご加入されると</th>
                </tr>
                <tr>
                    <td>
                        <p class="red bold"><span style="font-size:36px;">3,000</span>円 (税別)</p>
                    </td>
                    <td>
                        <table border="0" class="price_info">
                            <tr>
                                <td align="center">
                                    <p>対物<br />
                                        100,000円</p>
                                    <p>車両<br />
                                        100,000円</p>
                                </td>
                                <td><span class="arrow">→</span></td>
                                <td align="center">免責額<br />
                                    <span class="price">0</span><span class="en">円</span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p class="comm">※貸渡期間途中の加入、解約はできません<br />
                ※半日の貸出しでも1日分の料金がかかります。<br>
                ※15 日以上 1 ヶ月以内の貸渡契約については 15 日分の加入料とし､補償は貸渡契約期間とします。</p>
        </div>
    </section>
-->
    </section>
    <section class="hoken service">
        <h3>ロードサービスについて</h3>
        <section>
            <h4>□ロードサービスの内容・範囲</h4>
            <div class="indent">
                <h5>①車両搬送サービスについて</h5>
                <ul class="mark">
                    <li>1 回の事故等について 300,000円 ( 税込 ) を上限に補償します。</li>
                    <li>300,000円 ( 税込 ) を超える部分については、お客さまのご負担となります。</li>
                    <li>搬送先は当社の指定する工場先等になります。</li>
                </ul>
                <h5>②緊急時応急対応サービス</h5>
                <ul class="mark">
                    <li>ご契約のお車が、故障やバッテリー上がり等の車両自体に生じたトラブルにより走行ができなくなった場合に、30 分程度で対応可能な応急対応を行います。但し、サービスが適用されない作業等、又は補償の限度を超えた部分についてはお客さまのご負担となります。 </li>
                </ul>
                <div class="scroll">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th rowspan="9" scope="col">応急対応</th>
                            <td rowspan="4" scope="col">バッテリー等</td>
                            <td class="tal" scope="col">バッテリーのジャンピング</td>
                            <td scope="col">○</td>
                        </tr>
                        <tr>
                            <td class="tal">各種バルブ、ヒューズの取替え</td>
                            <td>○</td>
                        </tr>
                        <tr>
                            <td class="tal">30 分程度で可能な応急対応</td>
                            <td>○</td>
                        </tr>
                        <tr>
                            <td class="tal">冷却水の補充</td>
                            <td>○</td>
                        </tr>
                        <tr>
                            <td>カギ</td>
                            <td class="tal">インロック時のカギ開け ( 一般シリンダー錠 )※３</td>
                            <td>○</td>
                        </tr>
                        <tr>
                            <td rowspan="2">タイヤ</td>
                            <td class="tal">
                                <p>スペアタイヤとの交換作業<br />
                                    注 ) スペアタイヤを装備していない車両がパンクした際は、パンク修理キットを使用せずに最寄のガソリンスタンド等へ車両を搬送します。<br class="pc">
                                    その場合の搬送費用は無償です。</p>
                            </td>
                            <td>○</td>
                        </tr>
                        <tr>
                            <td class="tal">脱輪および落輪引き上げ (１ｍ以内 )　の補充</td>
                            <td>○</td>
                        </tr>
                    </table>
                </div>
                <!-- scroll -->
                <ul class="mark">
                    <li>損傷タイヤの修理代、又はタイヤ代はお客様のご負担となります。</li>
                </ul>
                <p>※２　ガス欠とは燃料切れによりエンジンが掛からない状態<br />
                    ※３　レンタカー貸渡証で本人確認ができる方のみ対応可能</p>
                <ul class="mark">
                    <li>貸渡証に記載されている運転者名と運転免許証の氏名が同一と確認できた場合</li>
                    <li>貸渡証に記載されている借受人名称の法人名と同一の名刺、社員証の提示及び、運転免許証で本人確認ができた場合</li>
                </ul>
                <ul class="comm">
                    <li>※一定の条件にあてはまらない場合は、お客さまのご負担となります。</li>
                    <li>※上記以外のサービスを受けられた場合は現地でのご精算になります。</li>
                    <li>※ご利用にあたっては事前に三井住友海上火災保険事故受付センター ｢TEL<span class="tel">0120-258-365</span> 又は、<br />
                        おクルマ QQ 隊（ロードサービス）TEL<span class="tel">0120-096-991</span>｣までご連絡ください。<br />
                        外国の方は、<span class="tel">0476-31-3643</span>までご連絡ください。</li>
                    <li>※英語は24時間 その他の言語は9時～22時まで対応可能です。<br>
                        事前のご連絡なく独自に手配されますと、各種の案内や手配を行うことができません。</li>
                </ul>
                <p>◎上記ロードサービスの一部は当社が締結する損害保険のサービスです。</p>
            </div>
        </section>
    </section>
    <section class="hoken about">
        <h3>お客様のご負担について</h3>
        <section>
            <!--
            <h4>□休車補償料 (ＮＯＣ)</h4>
            <p>1日10,000円 最長20日まで 最大200,000円（税込）</p>
            <div class="indent line_b">
                <p>万一当社の責任によらない事故･盗難･故障･汚損等が発生し、車両の修理･清掃等が必要となった場合、その期間中の｢営業補償｣の 一部として下記金額をご負担いただきます。( 車内装備の損害、シートの焦げ穴等もＮＯＣの対象となります )</p>
                <p>●自走して予定の返却場所に返還された場合<span class="pc">・・・・・ </span> <span class="red bold">20,000円 ( 税込 ) </span><br />
                    ●自走不可能な場合<span class="pc">・・・・・・・・・・・・・・・・</span> <span class="red bold">50,000円 ( 税込 ) </span><br />
                    注 ) ノンオペレーションチャージは免責補償制度ご加入の場合でもご負担いただきます。</p>
            </div>
            <h4>□車輌搬送費</h4>
            <div class="indent line_b">
                <p>　( 当社指定工場まで ) の、150,000円 ( 税込 ) までは補償の範囲内となり、それを超える部分はお客さまのご負担となります。</p>
            </div>
-->
            <h4>□保険適用以外の車輛設備の破損・汚れ・悪臭</h4>
            <div class="indent line_b">
                <p>全額お客様負担となります。 <br />
                    保険適用になる事故以外で、お客様による車輛設備の破損・汚損・悪臭発生時は修理代・現状回復費用全額をご負担いただきます。</p>
            </div>

            <h4>□ペット乗車可能車について</h4>
            <div class="indent line_b">
                <p>ペット乗車可能車は『犬』のみとさせていただきます。<br>別途清掃料として1匹8000円、2匹10000円、3匹以上12000円を頂戴します。<br>
                    但し、犬の引っかきキズやシートその他の破損、車内におしっこやうんちを漏らしてしまった場合や、悪臭が付いてしまった場合などは、原状回復費用として、免責補償・安心パックの加入の有無に関係なく、実費ご精算いただきます事を事前にご了承くださいませ。</p>
            </div>

            <p class="big">＊車内は禁煙・ペット専用車以外はペット不可とさせていただきます。 </p>
            <p class="big">＊キッチンのご利用について</p>
            <p class="pl2">車輛内にはガスコンロが付いておりますが、<span class="red bold">お湯を沸かす程度でのご利用でお願いします。 </span><br />
                焼きもの・炒め物・鍋料理などの匂いや煙が出る料理は禁止とさせていただきます。 その代わりに、別途ガスカセットコンロを無償でご用意させていただきますので、 車輛の外で調理をお願いします。（多くのお客様に快適にご利用いただくために何卒ご理解　ご協力のほどお願いします）</p>

            <p class="big">＊お子様でおねしょの心配がある方は、おねしょシーツをご用意しておりますので、お申し付けくださいませ。</p>
            <p class="big">＊免許取得3年以内のグリーン免許の方への貸出は不可とさせていただきます。</p>

            <!--
            <h4>□修理中の休車補償料</h4>
            <div class="indent line_b">
                <p><span class="bold f15">1日10,000円 ( 税別 ) 最大20日までかかります。</span><br />
                    事故及び保険適用以外の車輛設備の破損・汚れ・悪臭にて休車の場合1日 10,000 円 ( 税別 ) 最大20日までご負担いただきます。（修理は弊社指定工場での作業日数を摘要とさせていただきます。）</p>

            </div>
            <h4 id="c2rap"><span class="small">加入をオススメします！</span><span class="big">□レンタカー安心パック制度 (ＲＡＰ) </span> </h4>
            <div class="indent ">
                <p>ご利用されるお客様の9割以上の方がご加入されております。レンタカー安心パックは免責補償制度（CDW）にご加入の方のみ追加オプションとしてご加入いただけます<br />
                    ①事故の際にお客様にご負担いただくノンオペレーションチャージ (ＮＯＣ)のお支払いが免除。<br />
                    ②休車補償料 10,000 円 × 最大 20 日免除。<br />
                    ③パンクが発生した場合､損傷タイヤの修理代又はタイヤ代が無償(※1､※2) <br />
                    ④車内各設備の破損修理費も免除（規定の使用方法の範囲内でご利用時）</p>
                <div class="scroll">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <th scope="col">加入料 (1台につき 1日)</th>
                            <th scope="col">ご加入されると</th>
                        </tr>
                        <tr>
                            <td>
                                <p class="bold red"><span style="font-size:28px;">1,500 円</span> <span style="font-size:14px;">(税別)</span></p>
                            </td>
                            <td>
                                <table border="0" class="price_info">
                                    <tr>
                                        <td class="noc">・ノンオペレーションチャージ(NOC)<span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>20,000円～50,000円<br />
                                            ・休車補償料　最大200,000円<br />
                                            ・車内備品損傷修理代　実費<br>
                                            ・パンク・バースト時タイヤ代　実費
                                        </td>
                                        <td><span class="arrow">→</span></td>
                                        <td><span class="price">0</span><span class="en">円</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <ul class="comm">
                    <li>※貸渡し期間途中の加入･解約はできません。</li>
                    <li>※半日の貸出しでも1日分の料金がかかります。</li>
                    <li>※15 日以上 1 ヶ月以内の貸渡契約については 15 日分の加入料とし補償は貸渡期間とします。</li>
                    <li>※1 新しいタイヤの購入にあたっては原則、損傷タイヤと同等のもの《上限 20,000円 ( 税込 )》となります。</li>
                    <li>※2 お客さまに損傷タイヤの修理代又はタイヤ代を一旦お立替いただく場合があります。<br />
                        その際、帰着時に領収証と引き換えにご精算いたします。</li>
                </ul>
            </div>
            -->
            <!-- indent -->
        </section>
    </section>
    <section class="hoken about">
        <h3>注意事項</h3>
        <section>
            <h4>□保険･補償制度が適用できないケース</h4>
            <dl id="acMenu" class="small">
                <dt class="cf">詳しくみる<p class="accordion_icon"><span></span><span></span></p>
                </dt>
                <dd>
                    <div class="indent line_b">
                        <h5>①事故時に警察及び当社への連絡など所定の手続きが無かった場合</h5>
                        <h5>②貸渡約款に違反している場合</h5>
                        <p class="pl2">◆迷惑 ( 違法 ) 駐車に起因した損害　　◆飲酒及び酒気帯び運転<br />
                            ◆薬物使用　　◆無断延長　　◆契約書記載の運転者及び副運転者以外の運転<br />
                            ◆又貸し　　◆無免許運転 ( 運転免許停止期間中や運転できる自動車の種類に違反している場合を含む )<br />
                            ◆無断で示談した場合　　◆各種テスト･競技に使用し、又は他車のけん引･後押しに使用した場合<br />
                            ◆その他、レンタル約款 ( 貸渡約款 ) に定める免責事項に該当する事故　等</p>
                        <h5>③当社が締結する損害保険の保険約款の免責事項に該当する場合</h5>
                        <p class="pl2">◆故意による事故　　◆鍵の紛失･破損　　◆お客様の所有､使用､管理する財物の損害<br />
                            ◆天災 ( 地震､津波などによって生じた損害 ) 等</p>
                        <h5>④使用､管理上の落ち度があった場合</h5>
                        <p class="pl2">◆キーをつけたまま、又は施錠しないで駐車し盗難にあった場合<br />
                            ◆使用方法が劣悪なために生じた車体等の損傷や腐食の補修費<br />
                            ◆車内装備の汚損　　◆装備品の紛失･破損<br />
                            ◆タイヤチェーン･キャリア･チャイルドシートの取付及び､装着不備による損害<br />
                            ◆海岸､河川敷又は林間等車道以外で走行した場合の車両損害 ( 維持･管理された道路以外での事故 )<br />
                            ◆給油時の燃料種別の間違いにより生じた補修費</p>
                        <h5>⑤その他</h5>
                        <ul class="comm">
                            <li>※上記､保険･補償制度が適用されない損害､又補償の限度額を超えた損害については､
                                お客様の実費負担となります。</li>
                            <li>※相手不明の当て逃げ･車上荒らしによる損害について補償制度の適用を受ける為には､
                                警察への届出が必要です。</li>
                        </ul>
                    </div>
                </dd>
            </dl>
            <br>
            <h4>□オプション装備の装着確認について</h4>
            <div class="indent line_b">
                <p>オプションの装着不具合により生じた事故について､当社は責任を負いかねます｡お客様ご自身でご装着ください｡当社のスタッフが代わりに取付を行った場合であっても､最終的なご確認はお客様ご自身でお願いいたします。</p>
            </div>
            <h4>□カーナビ使用上のご注意について</h4>
            <div class="indent line_b">
                <p>カーナビゲーションは目的地までのルート案内を補助する機器であり、使用上で走行状態や走行場所などにより誤った表示や案内をする場合がございます。実際の交通規制･道路標識に従って運転してください。誤った表示や案内などによる金銭的な損害、所定時間の相違等による損害については責任を負いかねます。</p>
            </div>
            <h4>□ご返却時間変更の場合</h4>
            <div class="indent line_b">
                <p>ご返却は営業店の営業時間内にお願いいたします。ご予定の返却時間を変更される場合や返却時間に間に合わない場合は、必ず弊社までご連絡ください。無断で延長された場合には保険･補償制度が適用できませんのでご注意ください。</p>
            </div>
            <h4>□燃料は全車「軽油」（ディーゼル）です</h4>
            <div class="indent line_b">
                <p><span class="red">全車「軽油」ですのでお間違えなく。万が一「ガソリンを入れてしまった場合は、エンジンをかけずすぐに当社までご連絡の上、燃料タンク内洗浄して下さい。油種間違いによるエンジン破損　及び洗浄、修理作業費は免責補償制度（CDW）及びレンタカー安心パック制度（RAP）の加入に関係なく全額お客様のご負担となります。</span><br />
                    <br />
                    <!--給油機のノズルを奥まで差し込み、しっかりと握った状態で給油を行い、自動的に止まった時点を｢燃料満タン｣としております。<br />-->
                    ※ご都合により満タンで返却できない場合は、別に定める走行距離換算にて精算させていただきます。この場合実際の給油金額より割高となりますので予めご了承ください。なお、料金は営業店によって異なります。</p>
            </div>
            <h4>□駐車違反・スピード違反の場合は</h4>
            <div class="indent line_b">
                <!--
                <p><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig1.jpg" /></p>
                <p>※｢交通反則告知書｣及び、｢納付書･領収証書等｣をご提示いただけない場合 ( 反則金の納付が確認できない場合）は、下記の違約金 をお支払いいただきますので予めご了承ください。</p>
                <p>●違約金額 <span class="red bold">30,000円 ( 税込 ) </span></p>
                <p>※後日反則金を納付し、｢交通反則告知書｣及び｢納付書･領収証書等｣をご提示いただければ違約金をご返金させていただきます。 ご返金につきましては指定口座へのお振込となり､振込手数料はお客様のご負担となります。 <br />
                    ※反則金納付の確認がとれない場合及び違約金のお支払いをされなかった場合は、以後のレンタカーご利用をお断りする場合がございます。</p>
-->
                <p>貸出期間内に速やかに管轄の警察署に行き所定の手続き後、反則金または罰金をお支払いください。<br><span class="red">※万が一支払いをしなかった場合は入国禁止や財産差し押さえとなり処罰の対象となりますので、くれぐれもご注意ください。</span></p>
            </div>
            <h4>□事故･故障について</h4>
            <p class="big tac ">もしも事故･故障が発生したら･･･</p>
            <p class="tac"><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig2.jpg" /><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_fig3.jpg?v=201706" /></p>
            <div class="line_box">
                <p><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_tel1.jpg" /><img src="<?php bloginfo('template_url'); ?>/images/hoken_tyuui_tel2.jpg" /></p>
                <p>外国のお客様は<span class="tel"><span class="sp"><br></span>TEL：0476-31-3643</span>へお電話下さい</p>
                <ul class="comm">
                    <li><span class="red">※英語は24時間 その他の言語は午前9時～22時まで対応可能です。</span></li>
                    <li><span class="red">※｢駐車場で隣の車両のドアを傷つけてしまった｣｢バックの際、柱とぶつかりバンパーが凹んでしまった｣ たとえ､小さなキズ･ヘコミが自分の車両､又は相手の車両 ( 対象物 ) に発生した場合でも､事故手続きが必要になります。</span></li>
                    <li>※事故が発生した場合､キズ･ヘコミの大小や相手の有無にかかわらず警察及び当社への届出が必要になります。</li>
                    <li>※警察及び当社への届出など所定の手続きがなかった場合､保険･補償制度が適用されません。</li>
                    <li>※お客さまからいただいたお電話は､内容を正確に把握するため通話を録音させていただくことがあります｡ なお､個人情報の取り扱いについては､プライバシーポリシーに従うものといたします。</li>
                </ul>
            </div>
        </section>
    </section>
    <section class="access" id="c03">
        <h2 class="headline01">アクセス</h2>
        <h4 class="open">2019年7月1日　千歳店オープン！！</h4>
        <h5>千歳店ご予約車輛受渡場所</h5>
        <p><span class="bold">営業受付時間9時～18時（ご要望により時間外も対応可）</span><br>
            <span class="bold">住所 北海道千歳市青葉2丁目17－28</span><br>
            <span class="bold">電話 0123-21-8572</span>
        </p>

        <div id="map" class="map" style="margin-bottom:4%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d821.2001606345339!2d141.6583167716561!3d42.8216405600166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f7520a0b7d7681f%3A0xee5c1e842ff28da5!2z44CSMDY2LTAwMTUg5YyX5rW36YGT5Y2D5q2z5biC6Z2S6JGJ77yS5LiB55uu77yR77yX4oiS77yS77yY!5e0!3m2!1sja!2sjp!4v1560758527874!5m2!1sja!2sjp" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <p>「新千歳空港からお越しの場合」<br>
            タクシーで約10分です。</p>

        <div id="map" class="map route" style="margin-bottom:4%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d15187.56911169395!2d141.65382915818932!3d42.79249183169484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e0!4m5!1s0x5f7520a0b7d7681f%3A0xee5c1e842ff28da5!2z44CSMDY2LTAwMTUg5YyX5rW36YGT5Y2D5q2z5biC6Z2S6JGJ77yS5LiB55uu77yR77yX4oiS77yS77yY!3m2!1d42.8220374!2d141.6592139!4m5!1s0x5f752041be8717cd%3A0x6cf4a129de778931!2z5paw5Y2D5q2z56m65riv5Zu96Zqb57ea44K_44O844Of44OK44Or44CB44CSMDY2LTAwMTIg5YyX5rW36YGT5Y2D5q2z5biC576O44CF!3m2!1d42.78662!2d141.6766001!5e0!3m2!1sja!2sjp!4v1560758845312!5m2!1sja!2sjp" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <p>「お車でお越しの場合」
            カーナビで「千歳市青葉2丁目 17－28」で検索の上お越しください。
            お車は一台お預かりさせていただきます。<br>
            ※但し事故、盗難、破損については補償を致しかねますので、ご了承くださいませ。</p>

        <h5>清田店ご予約車輛受渡場所</h5>
        <p><span class="bold">営業受付時間9時～18時（ご要望により時間外も対応可）</span></p>
        <div id="map" class="map" style="margin-bottom:4%;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2918.822910965028!2d141.45739461562474!3d42.982003903589224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDLCsDU4JzU1LjIiTiAxNDHCsDI3JzM0LjUiRQ!5e0!3m2!1sja!2sjp!4v1512112465952" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <h5>「お車でお越しの場合」</h5>
        <p>カーナビで　「札幌市清田区美しが丘2条6丁目2」で検索の上お越しください。<br />
            道央道　北広島ICより3分(※国道36号線より1本南側の道で、里塚病院様の裏側になります。)<br />
            お車は一台お預かりさせていただきます。<br>
            ※但し事故、盗難、破損については補償を致しかねますので、ご了承くださいませ。<br />
        </p>
        <h5>「バスでお越しの場合」</h5>
        <dl id="acMenu">
            <dt class="cf">札幌市内からお越しの場合<p class="accordion_icon"><span></span><span></span></p>
            </dt>
            <dd>
                <ul>
                    <li>
                        <h5>①札幌市内　北海道中央バス札幌ターミナルからお越しの場合</h5>
                        <p>北海道中央バス　千歳線急行千歳駅前行きに乗車 「里塚南」で下車　バス停より徒歩３分<br>
                            注）降車バス停前にある株式会社ナッツRV札幌さんではありませんのでご注意ください、国道３６号線を渡って里塚病院の裏にございます。<br>
                            <a class="link" href="#map">ノマドレンタカーの場所はこちら</a><br>
                            〒004-0812　札幌市清田区美しが丘2条6丁目2</p>
                        <p>
                            <a class="link" href="http://www.chuo-bus.co.jp/city_route/" target="_blank">バス停マップはこちら</a><br>
                        </p>
                    </li>
                    <li>
                        <h5>②地下鉄東豊線福住駅からお越しの場合</h5>
                        <p>a.北海道中央バス　千歳線急行千歳駅前行きに乗車 「里塚南」で下車　バス停より徒歩３分<span class="pc"><br>
                                &nbsp;&nbsp;&nbsp;</span>注）降車バス停前にある株式会社ナッツRV札幌さんではありませんのでご注意<span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>ください、国道３６号線を渡って里塚病院の裏にございます。<br>
                            b.中央バス「福95」三井アウトレットパークor大曲工業団地行き乗車後、<span class="pc"><br>&nbsp;&nbsp;&nbsp;</span>「美しが丘3条6丁目」で下車徒歩8分<br>
                            <a class="link" href="#map">ノマドレンタカーの場所はこちら</a><br>
                            〒004-0812　札幌市清田区美しが丘2条6丁目2</p>
                        <p>
                            <a class="link" href="http://www.chuo-bus.co.jp/city_route/" target="_blank">バス停マップはこちら</a><br>
                        </p>
                    </li>
                </ul>
            </dd>
            <!--
            <dt class="cf">新千歳空港からお越しの場合<p class="accordion_icon"><span></span><span></span></p>
            </dt>
            <dd>
                <ul>
                    <li>
                        <p>新千歳空港国内線・国際線ターミナルバス乗り場より　札幌都心行き（北都交通・中央バス）にご乗車いただき　「里塚南」で下車　バス停より徒歩３分<br>
                            注）降車バス停前にある株式会社ナッツRV札幌さんではありませんのでご注意ください、国道３６号線を渡って里塚病院の裏にございます。<br>
                            <a class="link" href="#map">ノマドレンタカーの場所はこちら</a><br>
                            〒004-0812　札幌市清田区美しが丘2条6丁目2</p>
                        <p>
                            <a class="link" href="http://www.hokto.co.jp/a_chitose_sap_t.htm" target="_blank">空港連絡バスの時刻表はこちら</a><br>
                            <a class="link" href="http://www.hokto.co.jp/a_airport_index.htm#map-sapporo" target="_blank">バス停マップはこちら</a><br>
                            <a class="link" href="http://www.hokto.co.jp/english-timetable.html" target="_blank">英語ページはこちら（English)</a>
                        </p>
                    </li>
                </ul>
            </dd>-->
        </dl>

    </section>
    <section class="access" id="c05">

        <h2 class="headline01">送迎及び配車について</h2>

        <h5>《新千歳空港から千歳店へご来店のお客様》</h5>
        <p>
            空港到着後はタクシーにて千歳店へお越しください。<br>
            ※タクシー代はレシートを提示いただくと返金します。
        </p>
        <!--
    <h5>《送迎エリア》</h5>
    <p>新千歳空港 ⇔ 清田貸出ベース<br>札幌駅北口 ⇔ 清田貸出ベース<br>札幌市内中心部ホテル ⇔ 清田貸出ベース<br>
        【貸出時】指定の場所にてハイヤー運転手と待ち合わせの後、清田貸出ベースにて車両の貸出となります。<br>
        【返却時】清田貸出ベースにて車両を返却後、指定のハイヤーまたは送迎車に乗車頂きお帰りいただきます。</p>-->

        <h5>《有料送迎サービス料金》</h5>
        <p>
            ◆新千歳空港 ⇔ 千歳店 ： 送迎料無料<br>
            ◆札幌駅(市内中心部主要ホテル) ⇔ 千歳店 ： 送迎料 片道 8000 円(税抜)<br>
            ◆清田貸出ベース ： 送迎サービスなし（ご自身のお車などでご来店される方のみ）
        </p>
        <!--
    <img src="<?php bloginfo('template_url'); ?>/images/machiawase_sapporo<?php echo mobile_img(); ?>.png" />
    <p class="txt">新千歳空港では荷物受け取り後到着ロビー出口にて運転手がお客様のお名前を書いたプレートを持ってお待ちしております。</p>
    <ul class="cf airport">
        <li><img src="<?php bloginfo('template_url'); ?>/images/guide_regulations_photo1.jpg" /></li>
        <li><img src="<?php bloginfo('template_url'); ?>/images/guide_regulations_photo2.jpg" /></li>
    </ul>
    <p class="txt"><a class="open-options1" href="#">海外の方へ</a></p>

    <div class="modal-options1" data-izimodal-loop="" data-izimodal-title="Rules and Regulations">
        <p>*Please kindly note that this service is a direct transport between the airport and one designated location.<br>
            Passengers must have the same destination and origin. No other stops can be made between the place of origin and destination.<br>
            *Please check the pick-up location at the airport below:<br>
            Chitose Airport<br><br>
            Domestic Flight Arrival Hall (1F)<br>
            International Flight Arrival Hall (2F)<br><br>
            Your driver is waiting at the exit of the arrival lobby holding passenger's name written on the board.
        </p>
        <br>
        <ul>
            <li><img src="<?php bloginfo('template_url'); ?>/images/guide_regulations_photo1.jpg" /></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/guide_regulations_photo2.jpg" /></li>
        </ul>
        <?php if(is_pc()): ?>
        <br><br><br><br><br><br><br><br>
        <?php endif; ?>
    </div>
    <span class="red">※新千歳空港でお帰りになられる場合、遅くても飛行機出発の2～3時間前には返却する様にお願いいたします。<br>新千歳空港・札幌駅・市内中心部ホテル⇔当店間 無料送迎サービス<br>ジル520&nbsp;…&nbsp;ご利用料金計算上5日以上のご利用で無料送迎<br>その他&nbsp;&nbsp;&nbsp;…&nbsp;ご利用料金計算上6日以上のご利用で無料送迎</span>
    <p class="txt">【有料送迎サービス料金】<br>
        &nbsp;◆札幌駅（市内中心部主要ホテル）⇔清田貸出ベース　：送迎料　片道3,000円<br>
        &nbsp;◆新千歳空港⇔清田貸出ベース　：送迎料　片道5,000円<br>
        <span class="red">※ジル520以外の車輛を6日以下でご利用いただくお客様には、別途有料送迎サービスもご用意させていただいております。</span>
        <br><span class="red">※半日の場合は0.5日換算となります。丸々6日以上の貸出時に適用となります。</span>
    </p><br>
-->
        <h5>《配車エリア》～上記以外の場合</h5>
        <p>
            札幌駅及び中心部ホテル&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8,000円<br />
            小樽フェリーターミナル&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15,000円<br />
            苫小牧フェリーターミナル&nbsp;&nbsp;15,000円<br />
            旭川空港配車　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30,000円<br />
            ニセコ　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;35,000円<br />
            函館空港配車　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000円<br />
            女満別空港配車&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000円<br />
            釧路空港配車　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000円<br />
            中標津空港&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000円<br />
            稚内空港　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;50,000円<br />
            羽田空港・成田空港　&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;150,000円<br />
            ※配車料金については、長期利用割引は適用になりません<br />
            ※季節　場所等により随時配車費用をお見積りさせていただきます。<br />
            状況により対応できない場合もございますのでご了承くださいませ。</p>
    </section>
    <section class="pay" id="c09">
        <h2 class="headline01">お支払いについて</h2>
        <p>お申し込み後、1週間以内に、クレジットカード決済にてお支払いお願いいたします。<br>
            <span class="red">※銀行振込をご希望の方は備考欄に「銀行振込希望」と記載してください。</span></p>
    </section>
    <section class="cancel" id="c06">
        <h2 class="headline01">中途解約</h2>
        <p>レンタル開始後にお客様のご都合によりレンタルを中止し、途中解約される場合は、あらかじめご契約にて決めていた残りの期間分のレンタル基本料金の半額を中途解約手数料として頂戴します。 </p>
    </section>
    <section class="cancel" id="c08">
        <h2 class="headline01">事故・故障等により貸出不可能時の対応</h2>
        <p>弊社では、万が一の際に備え常に1台代替え用車輛を待機させておりますが・・・<br>
            万が一お客様がご予約いただいた車輛が、直前にご利用のお客様による事故・故障等により貸出不可能になる場合は、他に空車がある場合は代替え車輛に変更させていただきます。<br />
            但し、繁忙期等で代替え車輛の手配が不可能の場合は、あらかじめご入金いただきました代金を全額返金させていただき、ご契約自体を中止とさせていただきます。<br />
            <br />
            キャンピングカーという特性上、すぐに代替えのお車をご用意する事ができませんのでこのあたりを事前にご了承の上お申し込みくださいませ。<br />
            尚、貸出不可になった際には、あらかじめお約束しております貸渡し場所までの往復旅費、交通費、宿泊費等の補償は致しかねます事も事前にご了承の上お申し込みくださいませ。<br />
            せっかく楽しみにされていたご旅行ですので、弊社としては他のレンタカー会社の空車情報調査などできる限りの事は精一杯対応させていただきますが、夏の繁忙期等は厳しい場合もございます事を事前にご了承のほどお願いします。 </p>
    </section>
    <section class="faq" id="c07">
        <h2 class="headline01">よくあるご質問</h2>
        <h4>レンタル方法について</h4>
        <dl>
            <dt>Q、予約なしでも乗れますか？</dt>
            <dd>A、オフシーズン以外は、清掃等のスケジュールの都合がございますので、3日前までにご入金と同時にお申込ください。</dd>
        </dl>
        <dl>
            <dt>Q、レンタルする際に何が必要ですか。</dt>
            <dd>A、運転される方全員の運転免許証が必要です。<br>
                ＊免許取得後３年以内のグリーン免許の方には貸出できません。（条件により特例有り＊要ご相談ください）</dd>
        </dl>
        <dl>
            <dt>Q、支払にクレジットカードは使えますか。</dt>
            <dd>A、VISA、マスター、アメックス、JCB、ダイナーズが使用可能です。</dd>
        </dl>
        <dl>
            <dt>Q、レンタル中、自分の車は駐車できますか。</dt>
            <dd>A、キャンピングカーと入れ替えで１台分駐車できます。但し保管中の事故や盗難等に関しては弊社では一切責任を取れませんので、貴重品等はお持ちください。</dd>
        </dl>
        <dl>
            <dt>Q、満タン返しですか？</dt>
            <dd>A、燃料は<span class="bold">全車軽油です　満タン状態</span>で返却してください。<br>
                都合により給油できない場合は、弊社規定によりメモリ４分の１につき3,000円を頂戴いたします。<br>
                ご自身で給油されるより割高になりますので、返却前に軽油満タンにされるようお願いします。</dd>
        </dl>
        <dl>
            <dt>Q、自宅やホテルまで配車又は送迎はしてくれますか</dt>
            <dd>A、札幌駅（市内中心部主要ホテル）その他の場所への配車までは、専用車両またはハイヤーでの送迎となります。（有料）新千歳空港への送迎は無料です。<br>
                <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c05">詳しくはこちら</a></dd>
        </dl>
        <section id="c11">
            <dl>
                <dt>Q、国際免許証で借りることはできますか。</dt>
                <dd>A、海外の方は、日本国内で運転可能な国際免許証とパスポートをお持ちください。<br>
                    なお、海外の方は、あらかじめレンタカーの修理費用も出る海外旅行保険にご加入、または追加補償の免責補償CDW（１日3,000円）とレンタカー安心パックRAP（１日1,500円）の加入をおすすめします。<br>
                    ※事故・車輛破損時には別途デポジットをお預かりさせていただきます。<br>
                    <!--
                    <h4>海外の方限定　【デポジット】</h4>
                    ※出発時にデポジットを預かります。（クレジットでお預かり）<br>
                    CDW免責補償+RAP安心パック保険加入者→50,000円<br>
                    CDW免責補償のみ加入者→300,000円<br>
                    CDW免責・RAP安心パック保険　未加入者→500,000円<br><br>
                    事故の際に使う免責分と交通違反時の反則金、車輌破損時の費用として実費分をお預かり金額の中からご精算させていただきます。<br><br>
                    特に該当する事故・修理等が無い場合は、車輌返却後にお預かりした金額全額をクレジット返金処理にてお戻しいたします。
                    <span class="red">※万が一の事故や車輛破損時は、返却時に上記費用をデポジットとしてクレジットカードで精算いただきます。</span>
-->
                </dd>
            </dl>
        </section>
        <dl>
            <dt>Q、あらかじめスーツケースなどの荷物を送って置く事はできますか？</dt>
            <dd>A、はい大丈夫です。事前に宅配便でお送りいただけましたら、当日までお預かり保管させていただきます。キャンピングカー内部はスーツケースなどの大きな荷物を積むと空間が狭くなりますので、ご旅行中もお預かりできます。</dd>
        </dl>
        <h4>料金について </h4>
        <dl>
            <dt>Q、料金はいくらですか？</dt>
            <dd>A、時期によって金額が変わりますので<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c01">年間料金カレンダーと料金表</a>をご覧くださいませ。<br>
                お得な割引サービスもご用意しております。<br>
                長期ご利用の場合は別途ご相談くださいませ。</dd>
        </dl>
        <dl>
            <dt>Q、追加料金はかかりますか？</dt>
            <dd>A、営業時間外の早朝貸出や延長した場合に別途１時間単位で追加料金が発生致します。　延長に関しては次のお客様への貸出に影響が出る場合はお断りさせていただく事もございます。</dd>
        </dl>
        <dl>
            <dt>Q、他に料金がかかりますか？</dt>
            <dd>A、その他の費用は、お客様の選択にて　<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">①免責保険料</a>　<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c2rap">②レンタカー安心パック</a>　<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c01">③各種オプション貸出グッズ代</a>などがございます。お客様のご要望に応じ、できる限りのオプションやサービスをご提供させていただきたいと思いますので、ご要望をお知らせください。</dd>
        </dl>
        <dl>
            <dt>Q、キャンセル料金はいつから発生しますか？</dt>
            <dd>A、正式予約成立後より発生いたします。<br>
                ※お子様の急な発熱等もございますので、6ヶ月以内に再度予約を入れていただける場合、１回に限りキャンセル料金は不要でございます。<br>
                ※予約した日時を短くする場合も短縮した時間に対し<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c04">キャンセル料金</a>がかかりますのでご注意下さい。</dd>
        </dl>
        <dl>
            <dt>Q、レンタル開始後の中途キャンセル時は返金されますか？</dt>
            <dd>A、レンタル開始後にお客様のご都合によりレンタルを中止し、途中解約される場合は、あらかじめご契約にて決めていた残りの期間分のレンタル基本料金の<span class="bold">半額を中途解約手数料</span>として頂戴します。</dd>
        </dl>
        <h4>保険・補償について</h4>
        <dl>
            <dt>Q、事故の場合の自己負担はどうなりますか。</dt>
            <dd>A、万一事故をおこした場合は弊社にて車輛が加入している対人・対物保険を適用する事ができます。<br>
                但し、免責金額　車両100,000円　対物100,000円はお客様にご負担いただきます。<br>
                その他、保険の補償を超える補償額、休車補償料1日10,000円（最大200,000円まで）がかかります。<br>
                <br>
                ≪日本人の方≫<br>
                ご自身が所有する車輛の任意保険にオプションで加入する「他車運転補償特約」が付いている方はご自身の保険を適用することで追加補償は不要となりますので、保険内容をお確かめください。<br><br>
                ≪海外の方≫<br>
                レンタカー車輛破損時にも補償される海外旅行保険に加入の方は、追加補償への加入は不要でございます。条件はお客様ご自身でご確認ください。<br>
                <br>
                事故の際は上記費用が高額になるため、弊社では別途ご用意しております免責金額が0円になる<br>
                <span class="bold"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c02">＊免責補償制度</a>（１日あたり税別3,000円）のご加入をおススメしております。</span><br>
                <br>
                さらにノンオペレーションチャージ＋休車補償料＋通常の使用状況での車内備品の損傷も免除になる。<br>
                <span class="bold">＊<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c2rap">レンタカー安心パック</a>（１日あたり税別1,500円）のオプション追加もご用意しております。キャンピングカーは部品が高額なため、こちらもご加入をおススメしております。</span><br>
                <br>
                ＊油種間違いによるエンジン破損、修理・休業補償　規約以外の使い方による破損・自己等は対象外となります。</dd>
        </dl>
        <dl>
            <dt>Q、車内の備品を壊したときはどうなりますか。</dt>
            <dd>A、備品の修理費用、交換費用の全額をご負担いただきます。キャンピングカーの備品は輸入製品が多く高額な部品ですので扱いは丁寧にお願いします。<br>
                <span class="bold">＊<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/#c2rap">レンタカー安心パック</a>（１日あたり税別1,500円）のオプション加入</span>の方は通常の使用状況での<span class="bold">車内備品の損傷も免除</span>になります。※免責補償制度にご加入の方のみ追加オプションとして加入できます。</dd>
        </dl>
        <h4>車両・運転について</h4>
        <dl>
            <dt>Q、普通免許証で運転できますか？</dt>
            <dd>A、普通免許証で運転できます。</dd>
        </dl>
        <dl>
            <dd>AT車なのでAT限定免許でも運転できます。<br>
                ＊残念ながら<span class="bold">免許取得後３年以内のグリーン免許の方の運転は基本的に不可</span>とさせていただいております。<br>
                ＊ただし、グリーン免許の方でも、普段毎日のようにお車を運転される方や、お仕事で毎日運転されており、運転に自信がある方につきましては、免責補償料（１日あたり3,000円）とレンタカー安心パック（１日あたり1,500円）の２つにご加入が条件で、貸出も可能ですので、ご相談くださいませ。</dd>
        </dl>
        <dl>
            <dt>Q、何人まで乗れますか？また何人寝れますか？</dt>
            <dd>A、コルドバンクス・コルドリーブスは乗車定員７名 就寝定員５名、ジル520は乗車定員６名 就寝定員５名、サンライトは乗車定員６名 就寝定員６名となります。バンテック製の車輛は室内が広々しており快適にお過ごしいただけると思います。</dd>
        </dl>
        <dl>
            <dt>Q、チャイルドシート・ジュニアシートはありますか？</dt>
            <dd>A、チャイルドシート・ジュニアシートは<span class="bold">無料貸出し</span>ております。３点式シートベルトです。<br>
                チャイルドシートは5歳以下のお子様には装着義務がありますのでご予約時に人数とお子様の年齢を教えてください</dd>
        </dl>
        <dl>
            <dt>Q、ペットの同乗はできますか？</dt>
            <dd>A、一部ペット乗車可能車両（ジル520・コルドバンクス・コルドリーブスの一部）のみペット乗車可能です。<br />
                ペット就寝時はペット専用スペース内でゲージ等に入れるなどして、座席シートやベッド上には乗せないようにお願いします。
                ペット乗車の際は、1レンタルごとに別途清掃料を頂戴いたしております。<br>
                但し、犬の引っかきキズやシートその他の破損、車内におしっこやうんちを漏らしてしてしまった場合や、悪臭が付いてしまった場合などは、原状回復費用として、免責補償・安心パックの加入の有無に関係なく、実費ご精算いただきます事を事前にご了承くださいませ。<br>
                ペット乗車後の悪臭に関しては、返却時に弊社スタッフが嗅いで判断させていただく事も事前にご了承くださいませ。<br>
                また、ペットは犬のみとさせていただき、ネコやその他の動物はお断りさせていただきます。
            </dd>
        </dl>
        <dl>
            <dt>Q、燃料は何ですか？</dt>
            <dd>A、全車ディーゼルエンジンですので「軽油」です。<br>
                油種間違いにはくれぐれもご注意願います。万が一間違って給油された場合は絶対にエンジンを掛けずに弊社までご連絡の上、専門業者にて修理となります。　その際の修理費用、休業期間中の補償等は全額お客様ご負担となってしまいますので、くれぐれもご注意お願いします。</dd>
        </dl>
        <dl>
            <dt>Q、燃費はどれぐらいですか？</dt>
            <dd>A、3000ＣＣディーゼルターボエンジンのためパワフルで燃費が良く、１リッターあたり８～１２kmほど走りますので、さらに軽油はガソリン車に比べて安いので、広い北海道を走るにはとても経済的です。</dd>
        </dl>
        <dl>
            <dt>Q、カーナビはついていますか？</dt>
            <dd>A、全車４か国語対応簡単操作ができるカーナビがついています。<br>また、Googleマップ等をお使いの方に便利なスマートフォンホルダーも付いてます。</dd>
        </dl>
        <dl>
            <dt>Q、ETCは搭載していますか？</dt>
            <dd>A、ETC装置搭載しています。ETCカードはご持参ください。</dd>
        </dl>
        <dl>
            <dt>Q、サイドオーニングは付いてますか？</dt>
            <dd>A、サイドオーニングの利用については故障の原因となりますので、レンタルでは利用禁止です。</dd>
        </dl>
        <dl>
            <dt>Q、ラジオや音楽は聞けますか？</dt>
            <dd>A、全車ナビ一体型のオーディオ装備しております。ＣＤは使用できませんが、ラジオについてBluetooth接続可能な車種はリーブス１号車とバンクス１号車のみとなります。その他はUSBやイヤフォンジャックでの接続となります。</dd>
        </dl>
        <dl>
            <dt>Q、雪道の走行は可能ですか。</dt>
            <dd>A、全車ＡＴフルタイム４ＷＤで、11月～４月末頃までスタッドレスタイヤに履き替えます。<br>
                スキーやボードでキャンピングカーは大活躍します！　ニセコや富良野　大雪山のパウダースノーを満喫するツアーは最高ですよ。　エンジンを切っても使えるベバストＦＦヒーター付きで、真冬でも車内はポカポカ
                で過ごせます。<br>
                <span class="bold">＊但し、いつも雪道を走行していない方は、止めた方が良いです。キャンピングカーは重量もあり、アイスバーンでの運転は相当な運転歴とテクニックが無いと運転できません。</span></dd>
        </dl>
        <dl>
            <dt>Q、キャンピングカーならではの習慣はありますか？</dt>
            <dd>A、これはお乗りいただくとわかるのですが、北海道ならではの習慣で、ツーリングのバイクと同様にキャンピングカー同士がすれ違う時はお互いが手を上げて挨拶をします。　ぜひ恥ずかしがらずに同じキャンピングカー仲間として合図してあげてください。とても気持ちが良いものですよ。</dd>
        </dl>
        <dl>
            <dt>Q、車内の見学をしたいのですが</dt>
            <dd>A、車が貸出しされていない日でしたらＯＫです。担当者は配車やお客様対応にて外出している事が多いので、必ず事前に見学希望日時をご連絡くださいませ。</dd>
        </dl>
        <h4>内装について</h4>
        <dl>
            <dt>Q、トイレは付いていますか？</dt>
            <dd>A、全車にトイレシート利用のカセット式ポータブルトイレが付いております。郊外でトイレが無い場所、夜中や渋滞時などの緊急時に使用する程度でお考えいただいても良いかもしれません。ガソリンスタンド・スーパー・コンビニ・道の駅・キャンプ場をおすすめいたします。</dd>
        </dl>

        <dl>
            <dt>Q、水道は付いてますか？</dt>
            <dd>A、はい、全車２０リッター以上の水道とシンクが付いてます。飲用可ですが、ペットボトル等のお水を飲まれる事をオススメします。<br>＊冬期間は凍結するため使用できません。</dd>
        </dl>


        <dl>
            <dt>Q、喫煙できますか？</dt>
            <dd>A、車内は全車禁煙となっております。お煙草を吸われる場合は車外でお願い致します。<br>
                ＊ご返却時に「少しでもたばこ臭がした際」、または「少量でも車内にたばこの灰が発見された際」には、別途清掃消臭作業費として一律20,000円をいただきます。（たばこ臭についてはたばこを吸わない弊社スタッフの判断とさせていただきます事をあらかじめご了承ください。）　　また消臭作業が追い付かず、次の方のキャンセルが発生した際にはキャンセル費用実費をご負担いただきますので、ご注意ください。<br>
                多くの方がご利用されますので、ご理解くださいませ。</dd>
        </dl>
        <dl>
            <dt>Q、車内で調理はできますか？</dt>
            <dd>A、残念ながら、レンタカーで多くの方がご利用されますので、<span class="bold">車内で匂いが出る調理、焼き物・炒め物・焼肉などはお断りしております。</span>キッチンにはガスコンロがございますが、お湯を沸かす程度のご利用でお願いします。　本格的な匂いや煙が出る調理は別途無料で貸出しております　屋外調理用カセットコンロ（ボンベも無料）を車外にてご利用ください。（多くのお客様がご利用されますので、どうかご理解のほどお願いします。）<br>
                ＊ご返却時に「不快と思われる匂いがした場合」は、　別途清掃消臭作業費一律20,000円をいただきます。（匂いにつきましては弊社スタッフの判断とさせていただきます事をご了承ください）　　また消臭作業が追い付かず、次の方のキャンセルが発生した際にはキャンセル費用実費をご負担いただきますので、ご注意ください。　多くの方がご利用されますので、ご理解くださいませ。</dd>
        </dl>
        <dl>
            <dt>Q、家庭用100Ｖコンセントは使えますか？</dt>
            <dd>A、外部から家庭用AC100V電源を引き込めば、車内で家庭用コンセントが使用できます。<br>
                室内エアコンも外部電源供給時のみご使用できます。<br>
                12Ｖシガーソケットからは常時携帯充電などができるＵＳＢソケットプラグは装備しておりますので、携帯・スマートフォンの充電ができます。（充電用ＵＳＢケーブルは各自ご持参ください）<br>
                <br>
                また、消費電力が少ないノートパソコンは１２Ｖから１００Ｖへの変換インバーターをご使用いただく事で、使用可能です。　ノートパソコンでお仕事をされる方は、インバーターを無料貸し出ししますので、予約時にお申し付けください。（高ワット数の電気機器は焼き付きますのでご使用できません）</dd>
            <dt>Q、寝具はどうするのですか？</dt>
            <dd>A、衛生上の観点から、ベットやシートの上に直接寝ることをご遠慮いただいております。別途有料レンタル品にございます<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c01">夏はタオルケット・冬は毛布・ベッドシーツ・枕セット</a>（１日500円）をご利用ください。　寝具は毎回専門業者によるクリーニングに出しており清潔な状態にしておりますので、ご安心ください。</dd>
        </dl>
        <dl>
            <dt>Q、冷蔵庫はどのくらいの時間使えますか？</dt>
            <dd>A、貸出時は室内で使用するサブバッテリーをフル充電状態にしておりますが、エンジンOFF状態で半日～1日でバッテリーは無くなります。（使い方次第）<br>
                走行状態で変わってきますので、冷蔵庫のスイッチはなるべく走行時に全開で冷やし、停車時はなるべく節約してご使用いただく事をおススメします。<br>
                ＊万が一バッテリー残が無くなっても、走行用バッテリーは別系統ですので、エンジンはかかりますのでご安心ください。<br>
                ＊室内バッテリーが無くなると、ＦＦヒーターや室内照明含むすべての電気用品が動かなくなりますので、オートキャンプ場等に宿泊し１００Ｖの電源を確保するか、エンジンをかけて充電してください。　<br>
                ＊まる一日走行すると充電が増えます。</dd>
        </dl>
        <dl>
            <dt>Q、ＦＦヒーターはどのくらいの時間使えますか？</dt>
            <dd>A、燃料は軽油タンクから自動的に流れますので、別途給油する必要はございません。全開でつけても１日で3リットルも使いませんので、気兼ねなくお使いください。　　送風ファンはサブバッテリーの電力を使います。貸出時はサブバッテリーをフル充電状態にしておりますが、エンジンOFF状態で1日～２日です。<br>
                走行状態で変わってきますので、就寝時はなるべく消してご使用いただく事をおススメします。<br>
                ＊<span class="bold">使用時はダイヤルを全開に近くまで回してご利用ください</span>、温度を低く設定してしまうと、サーモスタットが頻繁に作動し　バッテリーが無くなってしまいます。<br>
                ＊万が一バッテリー残が無くなっても、走行用バッテリーは別系統ですので、エンジンはかかりますのでご安心ください。</dd>
        </dl>
        <dl>
            <dt>Q、室内エアコンはいつ使えますか？</dt>
            <dd>A、<span class="bold">コルドリーブス・ジル５２０
                    <!--・コルドランディ-->には　室内エアコンが設置されておりますが、１００Ｖの外部電源をつないでいる時のみ動作いたします。</span>　またエアコンは冷房送風のみご使用ください。　暖房になるような温度にはしないでください。　暖房にはＦＦヒーターを使ってください。<br>
                <span class="bold">室内エアコンで暖房を使うと破損の原因となります</span>（車輛搭載仕様のため）<br>
                <br>
                <span class="bold">＜その他、バッテリーに関して注意が必要なこと＞</span><br>
                ・電気ポットやドライヤーはワット数が大きいので使えません。<br>
                但し、キャンプ場など電源がある場所で外部コンセント使用時は使用可能です。<br>
                ・扇風機などモーターを使用する機器は瞬間的に大きなワット数が掛かり、ヒューズが飛ぶ場合があります。<br>
                ※キャンピングカーには天井に扇風機として使える大型換気扇があります。</dd>
        </dl>
        <dl>
            <dt>Q、備え付けの備品は何がありますか？</dt>
            <dd>A、無料車内設置備品・・・清掃セット、ガイド本類、スマホ充電、ＵＳＢソケット、車輛グッズ、屋外調理用カセットコンロ、チャイルドシート、ジュニアシートなどなど。　詳細はこちらをご覧ください→　<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c02">無料車内設置備品</a><br>
                その他は有料レンタルグッズからお選びくださいませ。　詳細はこちらをご覧ください→　<a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/#c01">有料レンタルグッズ</a></dd>
        </dl>
        <dl>
            <h4>宿泊地について</h4>
            <dt>Q、キャンピングカーはどこに止めてキャンプすれば良いですか</dt>
            <dd>A、キャンピングカーはどこでも好きなところへ止めてキャンプできるのが最大の魅力ですが、初心者の方はキャンプ場や道の駅、また日帰り温泉施設駐車場などに許可をもらって止めると良いでしょう。<br>
                キャンプ場を利用すると別途お金がかかりますが、食事の用意や後片付け、ごみの処理、お風呂やトイレに困らないのでより快適なキャンプ生活が楽しめます。また、キャンプ場から100Vの外部電源を接続すれば車内で気がねなく電気製品を使えます。<br>
                ＊道の駅やパーキングでイスやタープを広げての宴会等はマナー違反ですので、お止めください。<br>
                ＊また北海道はヒグマや野生生物がたくさんおりますので、森の中での宿泊等はお気を付けください。<br>
                <br>
                今後は一日１組限定の　景色が良く快適に過ごせる　「ノマドレンタカー専用　宿泊地」をご用意して参りますので、お楽しみに！　</dd>
        </dl>
    </section>
