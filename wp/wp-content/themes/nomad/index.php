<?php get_header(); ?>

<div id="main_image" class="<?php echo lang(); ?>">
    <div id="slider" class="flexslider">
        <ul class="slides <?php echo lang(); ?>">

            <li id="slider_bg10">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>国内最高級ラグジュアリーサルーン<br>サンライトで最高の旅を！</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg11">
                <div class="wrapper">
                    <div class="catch right">
                        <p>無料送迎！！</p>
                        <h3>「JR千歳駅から3分」<br>「新千歳空港から7分」</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg1">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg2">
                <div class="wrapper">
                    <div class="catch right">
                        <p>ノマドのキャンピングカーは、全てディーゼル４ＷＤ、ＦＦヒーター付き<br class="pc">
                            北海道の真冬でも室内は温かく快適に過ごせるバンテック社製の車輛です</p>
                        <h3>北海道のパウダースノーを遊び尽くそう！<br class="pc">
                            ニセコ～富良野～大雪　キャンピングカーならゲレンデ一番乗り！
                        </h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg3">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg4">
                <div class="wrapper">
                    <div class="catch right">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg5">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg6">
                <div class="wrapper">
                    <div class="catch right">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg7">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg8">
                <div class="wrapper">
                    <div class="catch right">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>

            <li id="slider_bg9">
                <div class="wrapper">
                    <div class="catch left">
                        <p>2019年7月より千歳店OPEN!!<br>ますます便利に</p>
                        <h3>北海道No.1 ラグジュアリークラス<br>キャンピングカーレンタル専門店</h3>
                        <p class="btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況・ご予約はこちら　＞</a></p>
                    </div>
                    <!-- catch -->
                </div>
                <!-- wrapper -->
            </li>
        </ul>
    </div>
    <div id="carousel" class="flexslider cf">
        <ul class="slides">
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide10_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide11_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide01_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide09_thumb.jpg?v=20190826" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide03_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide04_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide05_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide06_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide07_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide08_thumb.jpg" class="alpha60" alt="" /></a> </li>
            <li> <a href="javascript:;"><img src="<?php bloginfo('template_url'); ?>/images/main_slide02_thumb.jpg?v=20190826" class="alpha60" alt="" /></a> </li>
        </ul>
    </div>
</div>
<!-- main_image -->

<section class="topcar <?php echo lang(); ?>">
    <div class="wrapper1000">
        <p class="topmsg">北海道No.1 ラグジュアリークラスキャンピングカーレンタル専門店</p>
    </div>
    <!--         <h2><span class="big">キャンピングカー</span>で<span class="big">北海道</span>の隠れた魅力発見の<span class="blue">旅</span>へ!</h2>
        <p class="catch">全車バンテック社製最新型車輌導入！<br />
      高級感のある清潔・安心な車輌をご提供</p>
      <p class="wifi">
      ＼&nbsp;&nbsp;<img src="<?php bloginfo('template_url'); ?>/images/wifi.jpg" width="45" height="45" alt="WI-FI">全車WI-FI完備&nbsp;&nbsp;／</p> -->

    <div class="movie_link">
        <ul class="cf carusel">
            <li>
                <a href="https://www.youtube.com/watch?v=XRp0ZH2O4Cs" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img4.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.1</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=TvyD6Gu8INI" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img6.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.2</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=4oIJviKiNxw" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img1.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.3</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=vGsgDr1oaVY" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img3.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.4</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=ustY5Um00Uo&list=UUOPpdk9YO5CE6g9XBDCYdvg&index=4" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img5.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.5</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=P4YUBXS_nQU" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/index_movie_img2.jpg?v=<?php echo date('YmdHis'); ?>" /><img src="" alt=""></a>
                <p>Nomad movie.6</p>
            </li>
            <li>
                <a href="https://www.youtube.com/watch?v=CTDOHjWnJbo&list=UUOPpdk9YO5CE6g9XBDCYdvg&index=2" target="_blank">
                    <img src="<?php bloginfo('template_url'); ?>/images/index_movie_img7.jpg?v=<?php echo date('YmdHis'); ?>" alt=""></a>
                <p>Nomad movie.7</p>
            </li>
        </ul>
        <div id="arrows">
            <div class="slick-next">
                <img src="<?php bloginfo('template_url'); ?>/images/nextbtn.svg" alt="→">
            </div>
            <div class="slick-prev">
                <img src="<?php bloginfo('template_url'); ?>/images/prevbtn.svg" alt="←">
            </div>
        </div>
    </div>

    <section class="topcontent">
        <div class="bg_black">
            <div class="wrapper1000">

                <div class="premium_class enter-bottom">
                    <h2 class="plmclass1">上質な空間で優雅な北海道の旅へ</h2>

                    <?php if(is_pc()): ?>
                    <div class="toptitle cf pc">
                        <div class="left">
                            <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo1.jpg?v=20190516" />
                        </div>
                        <div class="center">
                            <h3><span>新型車両</span><br>最高級ラグジュアリークラス</h3>
                            <img src="<?php bloginfo('template_url'); ?>/images/plmclass_underline.png" />
                            <h4>SUNLIGHT<span>サンライト</span></h4>
                        </div>
                        <div class="right">
                            <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo2.jpg?v=20190516" />
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="toptitle cf">
                        <div class="top">
                            <img src="<?php bloginfo('template_url'); ?>/images/plmclass_photo_sp.jpg" />

                        </div>

                        <div class="bottom">
                            <h3><span>新型車両</span><br>最高級ラグジュアリークラス</h3>
                            <img src="<?php bloginfo('template_url'); ?>/images/plmclass_underline.png" />
                            <h4>SUNLIGHT<span>サンライト</span></h4>
                        </div>

                    </div>


                    <?php endif; ?>


                    <div class="top_inner">
                        <div class="car_outer cf">
                            <div class="car_left cf">
                                <h2 class="car_title pc">SUNLIGHT<span>- 最高級ラグジュアリークラス -</span></h2>
                                <div class="car_photo">
                                    <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_sunlight.jpg" /></a>
                                </div>
                                <div class="car_inside pc">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=05#camera-sunlignt"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang();?>.png">
                                            <div class="mask">
                                                <div class="caption">360度カメラで車内を見る</div>
                                            </div>
                                            <!-- mask -->
                                        </a>
                                        <!--
                                        <ul class="mark cf">
                                            <li>
                                                <img class="wifi" src="<?php bloginfo('template_url'); ?>/images/wifi.jpg" />
                                            </li>
                                        </ul>
-->

                                    </div>
                                    <!-- inside -->
                                </div>
                                <!-- car_inside -->
                            </div>
                            <!-- car_left -->
                            <div class="car_right">
                                <!--
                                <h3>空港・市内ホテル5日以上の<br>ご利用で送迎無料<br>
                                </h3>
-->
                                <br><br>
                                <h4>１日／ 56,000円～</h4>

                                <div class="car_center sp">
                                    <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=05#camera-sunlignt"><img src="<?php bloginfo('template_url'); ?>/images/carinside_sunlight_<?php echo lang();?>.png" alt="ZIL">
                                        </a>
                                    </div>
                                    <!-- inside -->
                                    <div class="caption"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=05#camera-sunlignt">360度カメラで車内を見る</a></div>
                                </div>
                                <!-- car_center -->

                                <dl class="cf">
                                    <dt>定員</dt>
                                    <dd>乗車6名（就寝5名）</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>燃料</dt>
                                    <dd>軽油（ディーゼルターボ）</dd>
                                </dl>
                                <dl class="cf">
                                    <dt>駆動方式</dt>
                                    <dd>FF 2WD</dd>
                                </dl>
                                <div class="link_btn">
                                    <ol class="plemclass_btn cf">
                                        <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_3">料金</a></li>
                                        <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況</a></li>
                                    </ol>
                                </div>
                                <!-- link_btn -->
                            </div>
                            <!-- car_right -->
                        </div>
                        <!-- car_outer -->
                    </div>
                    <!-- top_inner -->
                </div>
                <!-- premium_class -->
            </div>
            <!-- wrapper1000 -->
        </div>
        <!-- bg_black -->
    </section>
    <!-- topcontent -->

    <section class="btmcontent">
        <div class="wrapper1000">
            <ul>
                <li class="car_cont enter-bottom">
                    <div class="car_outer cf">
                        <div class="car_left cf">
                            <h2 class="car_title">ZIL520<span>- 最高級プレミアムクラス -</span></h2>
                            <div class="car_photo">
                                <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_zil.jpg" /></a>
                                <p>豪華装備で優雅な旅を</p>
                            </div>
                            <div class="car_inside pc">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png">
                                        <div class="mask">
                                            <div class="caption">360度カメラで車内を見る</div>
                                        </div>
                                        <!-- mask -->
                                    </a>
                                    <ul class="mark cf">
                                        <li>
                                            <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                        </li>
                                    </ul>

                                </div>
                                <!-- inside -->
                            </div>
                            <!-- car_inside -->
                        </div>
                        <!-- car_left -->
                        <div class="car_right">
                            <!--
                            <h3>空港・市内ホテル5日以上の<br>ご利用で送迎無料<br>
                            </h3>
-->
                            <br><br>
                            <h4>１日／ 30,000円～</h4>

                            <div class="car_center sp">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil"><img src="<?php bloginfo('template_url'); ?>/images/carinside_zil_<?php echo lang(); ?>.png" alt="ZIL">
                                    </a>
                                </div>
                                <!-- inside -->
                                <div class="caption"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=03#camera-zil">360度カメラで車内を見る</a></div>

                            </div>
                            <!-- car_center -->

                            <dl class="cf">
                                <dt>定員</dt>
                                <dd>乗車6名（就寝5名）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>燃料</dt>
                                <dd>軽油（ディーゼルターボ）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>駆動方式</dt>
                                <dd>4WD／4速AT</dd>
                            </dl>
                            <div class="link_btn">
                                <ol class="plemclass_btn cf">
                                    <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_2">料金</a></li>
                                    <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況</a></li>
                                </ol>
                            </div>
                            <!-- link_btn -->
                        </div>
                        <!-- car_right -->
                    </div>
                    <!-- car_outer -->
                </li>

                <li class="car_cont enter-bottom">
                    <div class="car_outer cf">
                        <div class="car_left cf">
                            <h2 class="car_title">コルドバンクス<span>- ハイクラス -</span></h2>
                            <div class="car_photo">
                                <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_bunks.jpg" /></a>
                                <p>ご家族・グループでのご利用にオススメ</p>
                            </div>
                            <div class="car_inside pc">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=01#camera-bunks"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png">
                                        <div class="mask">
                                            <div class="caption">360度カメラで車内を見る</div>
                                        </div>
                                        <!-- mask -->
                                    </a>
                                    <ul class="mark cf">
                                        <li>
                                            <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                        </li>
                                    </ul>

                                </div>
                                <!-- inside -->
                            </div>
                            <!-- car_inside -->
                        </div>
                        <!-- car_left -->
                        <div class="car_right">
                            <!--
                            <h3>空港・市内ホテル6日以上の<br>ご利用で送迎無料<br>
                            </h3>
-->
                            <br><br>
                            <h4>１日／ 22,000円～</h4>

                            <div class="car_center sp">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=01#camera-bunks"><img src="<?php bloginfo('template_url'); ?>/images/carinside_bunks_<?php echo lang(); ?>.png">
                                    </a>
                                </div>
                                <!-- inside -->
                                <div class="caption"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=01#camera-bunks">360度カメラで車内を見る</a></div>
                            </div>
                            <!-- car_center -->

                            <dl class="cf">
                                <dt>定員</dt>
                                <dd>乗車7名（就寝5名）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>燃料</dt>
                                <dd>軽油（ディーゼルターボ）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>駆動方式</dt>
                                <dd>4WD／4速AT</dd>
                            </dl>
                            <div class="link_btn">
                                <ol class="plemclass_btn cf">
                                    <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">料金</a></li>
                                    <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況</a></li>
                                </ol>
                            </div>
                            <!-- link_btn -->
                        </div>
                        <!-- car_right -->
                    </div>
                    <!-- car_outer -->
                </li>

                <li class="car_cont enter-bottom">
                    <div class="car_outer cf">
                        <div class="car_left cf">
                            <h2 class="car_title">コルドリーブス<span>- ハイクラス -</span></h2>
                            <div class="car_photo">
                                <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_leaves.jpg" /></a>
                                <p>ご夫婦お二人でゆったりと</p>
                            </div>
                            <div class="car_inside pc">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=02#camera-leaves"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png">
                                        <div class="mask">
                                            <div class="caption">360度カメラで車内を見る</div>
                                        </div>
                                        <!-- mask -->
                                    </a>
                                    <ul class="mark cf">
                                        <li>
                                            <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                        </li>
                                    </ul>

                                </div>
                                <!-- inside -->
                            </div>
                            <!-- car_inside -->
                        </div>
                        <!-- car_left -->
                        <div class="car_right">
                            <!--
                            <h3>空港・市内ホテル6日以上の<br>ご利用で送迎無料<br>
                            </h3>-->
                            <br><br>
                            <h4>１日／ 22,000円～</h4>

                            <div class="car_center sp">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=02#camera-leaves"><img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png">
                                    </a>
                                </div>
                                <!-- inside -->
                                <div class="caption"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=02#camera-leaves">360度カメラで車内を見る</a></div>
                            </div>
                            <!-- car_center -->

                            <dl class="cf">
                                <dt>定員</dt>
                                <dd>乗車7名（就寝5名）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>燃料</dt>
                                <dd>軽油（ディーゼルターボ）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>駆動方式</dt>
                                <dd>4WD／4速AT</dd>
                            </dl>
                            <div class="link_btn">
                                <ol class="plemclass_btn cf">
                                    <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">料金</a></li>
                                    <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況</a></li>
                                </ol>
                            </div>
                            <!-- link_btn -->
                        </div>
                        <!-- car_right -->
                    </div>
                    <!-- car_outer -->
                </li>


                <li class="car_cont enter-bottom">
                    <div class="coming-soon">
                        <div class="inner">只今準備中です。申し訳ありませんが、もうしばらくお待ち下さい</div>
                    </div>
                    <div class="car_outer cf">
                        <div class="car_left cf">
                            <h2 class="car_title">コルドランディ<span>- ハイクラス -</span></h2>
                            <div class="car_photo">
                                <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04"><img src="<?php bloginfo('template_url'); ?>/images/car_pic_rundy.jpg" /></a>
                                <p>愛犬と共にゆったりと</p>
                            </div>
                            <div class="car_inside pc">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=04#camera-rundy"><img class="inside_photo" src="<?php bloginfo('template_url'); ?>/images/carinside_rundy_<?php echo lang(); ?>.jpg?v=20190926">
                                        <div class="mask">
                                            <div class="caption">360度カメラで車内を見る</div>
                                        </div>
                                        <!-- mask -->
                                    </a>
                                    <ul class="mark cf">
                                        <li>
                                            <img class="pet" src="<?php bloginfo('template_url'); ?>/images/pets_logo.png">
                                        </li>
                                    </ul>

                                </div>
                                <!-- inside -->
                            </div>
                            <!-- car_inside -->
                        </div>
                        <!-- car_left -->
                        <div class="car_right">
                            <!--
                            <h3>空港・市内ホテル6日以上の<br>ご利用で送迎無料<br>
                            </h3>-->
                            <br><br>
                            <h4>１日／ 22,000円～</h4>

                            <div class="car_center sp">
                                <div class="inside"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=04#camera-rundy"><img src="<?php bloginfo('template_url'); ?>/images/carinside_leaves_<?php echo lang(); ?>.png">
                                    </a>
                                </div>
                                <!-- inside -->
                                <div class="caption"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/?type=04#camera-rundy">360度カメラで車内を見る</a></div>
                            </div>
                            <!-- car_center -->

                            <dl class="cf">
                                <dt>定員</dt>
                                <dd>乗車7名（就寝5名）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>燃料</dt>
                                <dd>軽油（ディーゼルターボ）</dd>
                            </dl>
                            <dl class="cf">
                                <dt>駆動方式</dt>
                                <dd>4WD／4速AT</dd>
                            </dl>
                            <div class="link_btn">
                                <ol class="plemclass_btn cf">
                                    <li class="btn2"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02_1">料金</a></li>
                                    <li class="btn3"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空車状況</a></li>
                                </ol>
                            </div>
                            <!-- link_btn -->
                        </div>
                        <!-- car_right -->
                    </div>
                    <!-- car_outer -->
                </li>


            </ul>
            <!-- btncontent -->
            <p class="comment enter-bottom">
                <span>※スーツケースなど荷物を持ち込みのお客様は、1台につき4名様までの乗車をお勧めします。(全車共通)<br>
                    ※プレミアムクラスの「ジル520」、ハイクラスの「コルドバンクス」、「コルドリーブス」はペット乗車専用の車輌がございます。ペット乗車専用の車輌は基本料金の他に「ペット清掃料金」がかかります。</span>
            </p>

            <p class="form_btn enter-bottom"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">簡単見積りシミュレーション</a></p>
        </div>
        <!-- wrapper -->
    </section>
    <!-- btmcontent -->
</section>

<section class="point">
    <div class="wrapper">
        <h2 class="headline01"><span class="small2">北海道No1のラグジュアリーな旅の提供を目指す</span><br>北海道ノマドレンタカーが選ばれる<span class="blue">9</span>つの理由</h2>

        <ul class="cf">
            <li class="fead1">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic01.jpg" alt="全て平成28年登録の新車"></p>
                <h3>最新型のキャンピングカーをご提供<br class="pc"><br class="pc"></h3>
                <p class="text">高級感あふれるラグジュアリーな旅と安心をご提供させていただきます。全車最新型モデルの車輛です。</p>
            </li>
            <li class="fead2">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic02.jpg" alt="清潔で快適な車内"></p>
                <h3>女性も安心してご利用いただける、いつも清潔で快適な車内</h3>
                <p class="text">安全・安心・清潔を最優先に考え、いつもお客様に快適にご利用いただけるお車をご提供するためにオゾン殺菌装置も導入し、毎回専門スタッフによる清掃・除菌・消臭を実施します。</p>
            </li>
            <li class="fead3">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic03.jpg" alt="冬も暖かいＦＦヒーター付き"></p>
                <h3>冬も暖かいＦＦヒーター付き<br class="pc"><br class="pc"></h3>
                <p class="text">全車AT車　クリーンディーゼル４ＷＤ・ＦＦヒーター付きで冬のスキー・スノーボードのお客様にも長期で安心してご利用いただけます。</p>
            </li>
        </ul>
        <ul class="cf">
            <li class="fead1">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic04.jpg" alt="半日5千円～得々ノマド割引"></p>
                <h3>外国語対応スタッフ　訪日外国人限定高速料金優遇サービス</h3>
                <p class="text">英語・台湾語・中国語・韓国語対応スタッフがおりますので、ご安心ください。当社は<a href="<?php bloginfo('template_url'); ?>/images/expressway.pdf" target="_blank">[訪日外国人限定]Hokkaido Expressway Pass</a>取り扱い店です。</p>
            </li>
            <li class="fead2">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic05.jpg" alt="24時間安心のロードサービス"></p>
                <h3>24時間安心のロードサービスとGPS即位システム搭載<br class="pc"></h3>
                <p class="text">２４時間３６５日　安心のロードサービスで万が一の時のフォロー体制、レンタル中　困った事は、いつでもご遠慮なくお電話ください。GPS即位システム搭載により、もし道に迷った時もご安心ください。現在位置から目的地まで誘導させていただきます。これで広い北海道も安心ドライブ</p>
            </li>
            <li class="fead3">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic06.jpg?v=201808" alt=""></p>
                <h3>厳選されたキャンピングカー専用車中泊スポットをご紹介</h3>
                <p class="text">道の駅やキャンプ場などの混雑とは無縁のキャンピングカー専用の特別な宿泊ベースをご紹介(別途有料)。<br />一日一組限定のラグジュアリーな絶景車中泊スポットもございます。<br />ご予約はこちら（当サイトとは別にご予約ください）<br />→　<a href="https://shachuoo.com" target="_blank">車中泊スポットshachuoo!</a></p>
            </li>
        </ul>
        <ul class="cf">
            <li class="fead1">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic07.jpg" alt="充実のアクティビティレンタル"></p>
                <h3>充実のアクティビティレンタル</h3>
                <p class="text">最高の思い出を体験していただくため、充実のアクティビティレンタルグッズと無料アメニティをご用意。ご要望に応じ別途手配もいたします。</p>
            </li>
            <li class="fead2">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic08.jpg" alt="配車サービス"></p>
                <h3>新千歳空港⇔千歳店 送迎無料</h3>
                <p class="text">2019年7月1日にオープンする千歳店は、空港からタクシーで約10分。
                    千歳店へ到着の際にレシートのご提示で、タクシー代金をご返金致します。送迎料金が実質無料でご利用いただけます。</p>
            </li>
            <li class="fead3">

                <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/top_pic09.jpg" alt="急な日程変更にも対応"></p>
                <h3>急な日程変更にも対応</h3>
                <p class="text">お子さまの発熱やお仕事などやむをえない場合の変更は、車輛に空きがある場合に限り、1回まで無料で対応致します。<br />2回目以降は、1回につき5,000円(税別)の手数料を頂戴します。<br>
                    ※ご予約金はそのままお預かりいたします。</p>
            </li>
        </ul>
        <p class="form_btn"><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>">空席状況・ご予約はこちら　＞</a></p>
    </div>
    <!-- wrapper -->
</section>

<!--
<section class="area">
    <div class="wrapper">
        <h2 class="headline01">キャンピングカーで旅をしよう！<br>ノマドおすすめ車中泊スポット紹介</h2>
    </div>

    <ul class="cf">

        <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area1.jpg') no-repeat;">
            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=douou">
                <h3>道央エリア</h3>
                <div class="hover"></div>
            </a>
        </li>
        <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area3.jpg') no-repeat;">
            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=douhoku">
                <h3>道北エリア</h3>
                <div class="hover"></div>
            </a>
        </li>
        <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area4.jpg') no-repeat;">
            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=doutou">
                <h3>道東エリア</h3>
                <div class="hover"></div>
            </a>
        </li>
        <li class="photo effect-ming" style="background:url('<?php bloginfo('template_url'); ?>/images/index_area2.jpg') no-repeat;">
            <a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/?area=dounan">
                <h3>道南エリア</h3>
                <div class="hover"></div>
            </a>
        </li>
    </ul>
    <p class="form_btn"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>/spot/">すべて見る　＞</a></p>
</section>
-->
<!-- area -->


<p class="catch2"><span>忙しい都会の喧騒から離れてノマドライフ</span>
    <a class="btn" href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>tourism/">Welcome to HOKKAIDO TOURISM<br>海外からの皆様へ</a>
</p>

<div class="wrapper info cf">
    <?php
                $wp_query = new WP_Query();
                $param = array(
                    'posts_per_page' => '5', //表示件数。-1なら全件表示
                    'post_status' => 'publish',
                    'orderby' => 'date', //ID順に並び替え
                    'order' => 'DESC'
                );
                $wp_query->query($param);?>
    <?php if($wp_query->have_posts()):?>
    <section class="news post">
        <h2 class="headline01 typesquare_tags">新着情報</h2>

        <?php while($wp_query->have_posts()) :?>
        <?php $wp_query->the_post(); ?>
        <dl class="cf">
            <dt>
                <?php the_time('Y.m.d'); ?>
            </dt>
            <dd> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                    <?php the_title(); ?>
                </a> </dd>
        </dl>
        <?php endwhile; ?>
    </section>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
    <section class="blog post">
        <?php
                $wp_query = new WP_Query();
                $param = array(
                    'posts_per_page' => '10', //表示件数。-1なら全件表示
                    'post_type' => 'tabi',
                    'post_status' => 'publish',
                    'orderby' => 'date', //ID順に並び替え
                    'order' => 'DESC'
                );
                $wp_query->query($param);?>
        <?php if($wp_query->have_posts()):?>
        <div class="post_list white">
            <h2 class="headline01 typesquare_tags">ノマド旅情報</h2>

            <?php while($wp_query->have_posts()) : $wp_query->the_post();
                        $link ="";
                        $link = get_post_meta( $post->ID, 'link', true);
                        ?>
            <dl class="cf">
                <dt>
                    <p class="thumb"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php if(has_post_thumbnail()):?><?php echo get_the_post_thumbnail($post->ID, 'medium'); ?><?php else:?><img src="<?php bloginfo('template_url'); ?>/images/nophoto_thumb.jpg"><?php endif; ?></a></p>
                </dt>
                <dd>
                    <?php if($link != "") : ?>
                    <a href="<?php echo $link; ?>">
                        <?php the_title(); ?>
                    </a>
                    <?php else: ?>
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                        <?php the_title(); ?>
                    </a>
                    <?php endif; ?>
                </dd>
            </dl>
            <?php endwhile; ?>

        </div>
        <!-- post_list -->
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </section>
</div>
<!-- wrapper -->

<?php get_footer(); ?>
