<?php require_once('./lang/lang_footer.php'); ?>
<?php if(isset($_GET['lang']) && $_GET['lang'] == 'zh'): ?>
<?php include('http://zh.nomad-r.jp/footer.php'); ?>
<?php else: ?>

<footer id="footer" class="cf">
    <div class="wrapper">
        <div class="left">
            <nav>
                <ul>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>"><?php echo $footer_top; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car/"><?php echo $footer_car; ?></a>
                        <ul>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=05"><?php echo $footer_car_type05; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=03"><?php echo $footer_car_type03; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=01"><?php echo $footer_car_type01; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=02"><?php echo $footer_car_type02; ?></a></li>
                            <!--                        <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>car?type=04"><?php echo $footer_car_type04; ?></a></li>-->
                            <!--<li><a href="<?php bloginfo('url'); ?>/car/">ZIL(ジル)520</a></li>-->
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/"><?php echo $footer_price; ?></a>
                        <ul>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c02"><?php echo $footer_price_02; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>price/#c03"><?php echo $footer_price_03; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>reservation.php"><?php echo $footer_reservation1; ?></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/reservation.php?lang=<?php echo lang();?>"><?php echo $footer_reservation2; ?></a></li>
                        </ul>
                    </li>

                </ul>
                <ul>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>rental/"><?php echo $footer_rental; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>guide/"><?php echo $footer_guide; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan/"><?php echo $footer_yakkan; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-en/"><?php echo $footer_yakkan_en; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>yakkan-zh/"><?php echo $footer_yakkan_zh; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/"><?php echo $footer_contact; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>tokusyou/"><?php echo $footer_tokusyou; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>tourism/"><?php echo $footer_tourism; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/reservation.php?lang=ja"><?php echo $footer_reservation_ja; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/reservation.php?lang=en"><?php echo $footer_reservation_en; ?></a></li>
                    <li><a href="<?php bloginfo('url'); ?>/reservation.php?lang=zh"><?php echo $footer_reservation_zh; ?></a></li>

                </ul>
            </nav>
            <?php if(lang()=='ja'): ?>
            <p class="contact_link">御来店の際は、<br class="sp"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/">お問い合わせフォーム</a>よりご連絡ください。</p>
            <?php endif; ?>
            <?php if(lang()=='th'): ?>
            <p class="contact_link"><a href="<?php bloginfo('url'); ?>/<?php echo lang_uri();?>contact/">กรณีที่ลูกค้าต้องการมาที่บริษัท กรุณากรอกแบบฟอร์มติดต่อสอบถาม</a></p>
            <?php endif; ?>


            <div class="address">
                <h2><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png" alt="北海道ノマドレンタカー" /></h2>
                <address>

                    <?php if(lang()=='ja'): ?>
                    千歳店（車輛受渡場所）<br>
                    〒066-0015<br>
                    千歳市青葉2丁目17-28<br>
                    <?php endif; ?>
                    <?php if(lang()=='en'): ?>
                    千歳店（車輛受渡場所）<br>
                    〒066-0015<br>
                    千歳市青葉2丁目17-28<br>
                    <?php endif; ?>
                    <?php if(lang()=='th'): ?>
                    千歳店（車輛受渡場所）<br>
                    〒066-0015<br>
                    千歳市青葉2丁目17-28<br>
                    <?php endif; ?>


                    <div class="address_photo">
                        <div class="tape"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"></div>
                        <img class="main" src="<?php bloginfo('template_url'); ?>/images/footer_chitose.jpg">
                    </div>

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d821.2001606345339!2d141.6583167716561!3d42.8216405600166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f7520a0b7d7681f%3A0xee5c1e842ff28da5!2z44CSMDY2LTAwMTUg5YyX5rW36YGT5Y2D5q2z5biC6Z2S6JGJ77yS5LiB55uu77yR77yX4oiS77yS77yY!5e0!3m2!1sja!2sjp!4v1560758527874!5m2!1sja!2sjp" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                </address>
                <br><br>

                <address>

                    <?php if(lang()=='ja'): ?>
                    清田店（車輛受渡場所）<br>
                    〒004-0812<br>
                    札幌市清田区美しが丘2条6丁目2<br>
                    <?php endif; ?>
                    <?php if(lang()=='en'): ?>
                    Place for pick-up the EV you've reserved(Office at Kiyota District)<br>
                    Postcode 004-0812<br>
                    6 Chome-2 Utsukushigaoka 2 Jō Kiyota-ku, Sapporo-shi, Hokkaidō <br>
                    <?php endif; ?>
                    <?php if(lang()=='th'): ?>
                    สถานที่รับและส่งคืนรถ (kiyota rental space)<br>
                    Post code 004-0812<br>
                    Sapporo, Kiyota-ku, Utsukushigaoka 2-6-2<br>
                    <?php endif; ?>

                    <div class="address_photo">
                        <div class="tape"><img src="<?php bloginfo('template_url'); ?>/images/tape.svg"></div>
                        <img class="main" src="<?php bloginfo('template_url'); ?>/images/footer_kiyota.jpg">
                    </div>

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2918.822910965028!2d141.45739461562474!3d42.982003903589224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNDLCsDU4JzU1LjIiTiAxNDHCsDI3JzM0LjUiRQ!5e0!3m2!1sja!2sjp!4v1512112465952" frameborder="0" style="border:0" allowfullscreen></iframe>
                </address>
                <br>
                <div class="lay-bg">
                    <p>
                        <?php if(lang()=='ja'): ?>
                        お問合せ・ご連絡<br>
                        info@nomad-r.jp<br>
                        TEL 0123-21-8572<br>
                        （配車等で不在の場合があるためメールをおすすめいたします）<br>
                        営業受付時間 9時～18時（配車・返却時間はご相談くださいませ）<br>
                        ※スタッフは配車等で不在の場合がございますので、現車をご覧になりたい方は、必ず事前にご連絡頂き調整のうえご来店をお願い致します。<br>
                        <?php endif; ?>
                        <?php if(lang()=='en'): ?>
                        お問合せ・ご連絡<br>
                        info@nomad-r.jp<br>
                        TEL 011-839-4867<br>
                        （配車等で不在の場合があるためメールをおすすめいたします）<br>
                        営業受付時間 9時～18時（配車・返却時間はご相談くださいませ）<br>
                        ※スタッフは配車等で不在の場合がございますので、現車をご覧になりたい方は、必ず事前にご連絡頂き調整のうえご来店をお願い致します。<br>
                        <?php endif; ?>
                        <?php if(lang()=='th'): ?>
                        お問合せ・ご連絡<br>
                        info@nomad-r.jp<br>
                        TEL 011-839-4867<br>
                        （配車等で不在の場合があるためメールをおすすめいたします）<br>
                        営業受付時間 9時～18時（配車・返却時間はご相談くださいませ）<br>
                        ※スタッフは配車等で不在の場合がございますので、現車をご覧になりたい方は、必ず事前にご連絡頂き調整のうえご来店をお願い致します。<br>
                        <?php endif; ?>
                    </p>
                    <br>
                    <p>
                        <?php echo $footer_receiv_address; ?>
                    </p>
                    <p>
                        <?php echo $footer_delivery1; ?><br>
                        <?php echo $footer_delivery2; ?>
                    </p>
                    <br>

                    <!--
                <p><?php echo $footer_menkyo; ?></p><br>
                <p>
                    <?php echo $footer_pay1; ?><br>
                    <?php echo $footer_pay2; ?><br>
                    <?php echo $footer_pay3; ?><br>
                    <?php echo $footer_pay4; ?><br><br>
                </p>
-->

                </div>
            </div>
            <!-- address -->
        </div><!-- left -->
        <div class="right">
            <p class="instagram-btn"><a href="https://www.instagram.com/nomad.car.rental/" target="_blank"><i class="fa fa-instagram fa-lg"></i><span>Instagram</span></a></p>
            <div class="fb">

                <!--<h2 class="facebook">facebookで最新情報</h2>    -->


                <?php if(!is_mobile()){ ?>
                <div class="fb-page" data-href="https://www.facebook.com/hokkaido.nomad/" data-tabs="timeline" data-width="304" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/hokkaido.nomad/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/hokkaido.nomad/">北海道ノマドレンタカー　</a></blockquote>
                </div>
                <?php } ?>
                <?php if(is_mobile()){ ?>
                <div class="fb-page" data-href="https://www.facebook.com/hokkaido.nomad/" data-tabs="timeline" data-width="500" data-height="328" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/hokkaido.nomad/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/hokkaido.nomad/">北海道ノマドレンタカー　</a></blockquote>
                </div>
                <?php } ?>
                <p class="bnr"><a href="http://www.rental-camper.jp/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/footer_bnr_portal.jpg" alt="レンタルキャンピングカーネット" /></a><br />
                    <?php echo $footer_shokai; ?></p>
                <p class="travel"><a href="http://www.motor-home.net" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/travel_depot.jpg" alt="Trabel Depot" /></a></p>
                <p class="lgbt"><a href="<?php bloginfo('url'); ?>/tokusyou/#lgbt"><img src="<?php bloginfo('template_url'); ?>/images/lgbt_title.jpg" alt="北海道ノマドレンタカーLGBTフレンドリー宣言" /></a></p>
                <div class="oneplanet cf"><a href="http://www.patagonia.jp/one-percent-for-the-planet.html" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/oneplanet.jpg" /></a>
                    <p><?php echo $footer_kihu; ?></p>
                </div>
            </div>
            <!-- fb -->

            <p class="copy">Copyright &copy; NomadCarRental. All Rights Reserved.</p>
        </div><!-- right -->
    </div>
    <!-- wrapper -->
</footer>
<!-- ページトップ -->
<p id="page-top"><a href="#top">TOP</a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php if(is_mobile()): ?>
<script>
    $(function() {

        $("#openMenu").click(function() {
            $("#openMenu").fadeOut("fast");
            $("#closeMenu").fadeIn("fast");
            $("#layerMenu").fadeIn("fast");
            $("#overray").fadeIn("fast");
        });
        $("#closeMenu").click(function() {
            $("#closeMenu").fadeOut("fast");
            $("#openMenu").fadeIn("fast");
            $("#layerMenu").fadeOut("fast");
            $("#overray").fadeOut("fast");
        });
        $("#layerMenu a").click(function() {
            $("#closeMenu").fadeOut("fast");
            $("#openMenu").fadeIn("fast");
            $("#layerMenu").fadeOut("fast");
            $("#overray").fadeOut("fast");
        });
        $("#overray").click(function() {
            $("#closeMenu").fadeOut("fast");
            $("#openMenu").fadeIn("fast");
            $("#layerMenu").fadeOut("fast");
            $("#overray").fadeOut("fast");
        });
    });

</script>
<?php endif; ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/lightbox.js"></script>
<?php wp_footer(); ?>
<?php endif;?>
</div>
<!-- outer -->
</body>

</html>
