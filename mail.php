<?php
ini_set("display_errors", On);
error_reporting(E_ALL);

$debug_mode = 0;

?>
<?php header("Content-Type:text/html;charset=utf-8"); ?>
<?php require('./wp/wp-load.php'); ?>
<?php require('./lang/lang_mail.php'); ?>
<?php //error_reporting(E_ALL | E_STRICT);
##-----------------------------------------------------------------------------------------------------------------##
#
#  PHPメールプログラム【Mail05】　ファイル添付版　要PHP5以上
#　改造や改変は自己責任で行ってください。
#
#  今のところ特に問題点はありませんが、不具合等がありましたら下記までご連絡ください。
#  MailAddress: info@php-factory.net
#  name: K.Numata
#  HP: http://www.php-factory.net/
#
#  重要！！サイトでチェックボックスを使用する場合のみですが。。。
#  チェックボックスを使用する場合はinputタグに記述するname属性の値を必ず配列の形にしてください。
#  例　name="当サイトをしったきっかけ[]"  として下さい。
#  nameの値の最後に[と]を付ける。じゃないと複数の値を取得できません！
#
##-----------------------------------------------------------------------------------------------------------------##
if (version_compare(PHP_VERSION, '5.1.0', '>=')) {//PHP5.1.0以上の場合のみタイムゾーンを定義
    date_default_timezone_set('Asia/Tokyo');//タイムゾーンの設定（日本以外の場合には適宜設定ください）
}

/*-------------------------------------------------------------------------------------------------------------------
* ★以下設定時の注意点　
* ・値（=の後）は数字以外の文字列（一部を除く）はダブルクオーテーション「"」、または「'」で囲んでいます。
* ・これをを外したり削除したりしないでください。後ろのセミコロン「;」も削除しないください。
* ・また先頭に「$」が付いた文字列は変更しないでください。数字の1または0で設定しているものは必ず半角数字で設定下さい。
* ・メールアドレスのname属性の値が「Email」ではない場合、以下必須設定箇所の「$Email」の値も変更下さい。
* ・name属性の値に半角スペースは使用できません。
*以上のことを間違えてしまうとプログラムが動作しなくなりますので注意下さい。
-------------------------------------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------------------------------------
* Excel出力用のイベント設定
* KEISUKE(2017.06.18)
-------------------------------------------------------------------------------------------------------------------*/
//$event = array("off_tsu","off_syuku","on_tsu","on_syuku","high","golden","winter");
$event = array("free","off_syuku","on_tsu","on_syuku","high","golden","winter");

$excel_file_name = "nomad_";
$excel_file_dir = "/../../data/";


if(!isset($_POST['lang'])) {
    $lang = "ja";
} else {
    $lang = $_POST['lang'];
}

/*-----------------*/
// 確認画面　表示リスト
/*-----------------*/
$disp_list = array(
        'car',
        'goDate',
        'backDate',
        'place_go',
        'place_back',
        'name',
        'name_furigana',
        'tel',
        'mailadd',
        'zip',
        'country',
        'address',
        'birth',
        'emagency',
        'adult',
        'child',
        'child_seat',
        'junior_seat',
        'driver1_name',
        'driver1_no',
        'driver1_age',
        'driver2_name',
        'driver2_no',
        'driver2_tel',
        'driver2_age',
        'driver3_name',
        'driver3_no',
        'driver3_tel',
        'driver3_age',
        'driver4_name',
        'driver4_no',
        'driver4_tel',
        'driver4_age',
        'hep',
        'payout',
        'main_comment',
        'question',
        'rental',
        'accident',
        'yakkan',
        'safe',
        'pack',
        'rental',
        'child_seat_age',
        'junior_seat_age',
        'pet',
        'pet_num',
        'cdw',
        'shokai',
        'group1',
        );

// ペット乗車無しの場合は確認画面から除外
if(!isset($_POST['pet'])) {
    array_splice($disp_list,40,2);
    $pet_mail_text = $pet_none;
} else {
    $pet_mail_text = $_POST['pet']."／".$_POST['pet_num'].$pet_unit;
}


/*-----------------*/
// フォームのname値を確認画面用にテキストに変換
/*-----------------*/

$object_name = array(
                'car' => array('ja' => '車種','en' => 'Type of Vehicle','zh' => '請確認車種','th' => 'รุ่นรถ'),
                'goDate' => array('ja' => 'ご出発希望日','en' => 'Departure Date and Time','zh' => '欲出發日','th' => 'วันที่ต้องการออกเดินทาง'),
                'goTime' => array('ja' => 'ご出発希望時間','en' => '','zh' => '欲出發時間','th' => 'เวลาที่ต้องการออกเดินทาง'),
                'backDate' => array('ja' => 'ご返却希望日','en' => 'Return Date and Time','zh' => '欲還車日','th' => 'วันที่ต้องการคืนรถ'),
                'backTime' => array('ja' => 'ご返却希望時間','en' => '','zh' => '欲還車時間','th' => 'เวลาที่ต้องการคืนรถ'),
                'place_go' => array('ja' => 'ご出発希望場所','en' => 'Departure Location','zh' => '欲出發地點','th' => 'สถานที่ที่ต้องการออกเดินทาง'),
                'go' => array('ja' => 'ご出発希望場所(詳細)','en' => '','zh' => '欲出發地點(詳細)','th' => 'สถานที่ที่ต้องการออกเดินทาง(รายละเอียด)'),
                'place_back' => array('ja' => 'ご返却希望場所','en' => 'Return Location','zh' => '欲還車地點','th' => 'สถานที่ที่ต้องการคืนรถ'),
                'back' => array('ja' => 'ご返却希望場所(詳細)','en' => '','zh' => '欲還車地點(詳細)','th' => 'สถานที่ที่ต้องการคืนรถ(รายละเอียด)'),
                'name' => array('ja' => 'お申込者名','en' => 'Applicant','zh' => '申請人','th' => 'ชื่อผู้จอง'),
                'name_furigana' => array('ja' => 'フリガナ','en' => 'フリガナ','zh' => '英文名稱','th' => 'คำอ่าน'),
                'mailadd' => array('ja' => 'メールアドレス','en' => 'Email address','zh' => 'Email','th' => 'อีเมล์'),
                'tel' => array('ja' => '電話番号','en' => 'Telephone number','zh' => '電話號碼','th' => 'เบอร์โทรศัพท์'),
                'zip' => array('ja' => '郵便番号','en' => 'Zip code','zh' => '郵遞區號','th' => 'รหัสไปรษณีย์'),
                'address' => array('ja' => 'ご住所','en' => 'Home address','zh' => '地址','th' => 'ที่อยู่'),
                'birth' => array('ja' => '生年月日','en' => 'Date of birth','zh' => '出生年月日','th' => 'วันเดือนปีเกิด'),
                'emagency' => array('ja' => '緊急時連絡先','en' => 'Emergency contact','zh' => '緊急聯絡人','th' => 'เบอร์ติดต่อฉุกเฉิน'),
                'adult' => array('ja' => 'ご利用人数(小学生以上)','en' => 'Number of passengers(more than 6 years old)','zh' => '搭乘人數(6歲以上)','th' => 'จำนวนผู้ใช้รถ(เด็กโตกว่าระดับประถม)'),
                'child' => array('ja' => 'ご利用人数(6歳未満のお子様)','en' => 'Number of passengers(Child less than 6 years old)','zh' => '搭乘人數(未滿6歲的小孩)','th' => 'จำนวนผู้ใช้รถ(เด็กอายุไม่เกิน6ปี)'),
                'pet' => array('ja' => 'ペットの乗車','en' => 'Pet ride','zh' => '寵物共乘','th' => 'รถเพื่อสัตว์เลี้ยง'),
                'pet_num' => array('ja' => 'ペット数','en' => 'Number of pets','zh' => '寵物數量','th' => 'จำนวนสัตว์เลี้ยง'),
                'child_seat' => array('ja' => 'チャイルドシート','en' => 'Child seat','zh' => '嬰兒座椅數','th' => 'เบาะเสริมสำหรับเด็ก'),
                'junior_seat' => array('ja' => 'ジュニアシート','en' => 'Junior seat','zh' => '兒童座椅數','th' => 'ที่รองนั่งสำหรับเด็ก'),
                'child_seat_age' => array('ja' => 'チャイルドシート(年齢)','en' => 'Child seat(age)','zh' => '嬰兒座椅數(年齢)','th' => 'เบาะเสริมสำหรับเด็ก(อายุ)'),
                'junior_seat_age' => array('ja' => 'ジュニアシート(年齢)','en' => 'Junior seat(age)','zh' => '兒童座椅數(年齢)','th' => 'ที่รองนั่งสำหรับเด็ก(อายุ)'),
                'driver1' => array('ja' => '運転される方1','en' => 'Driver #1','zh' => '駕駛人1','th' => 'ผู้ขับรถคนที่1'),
                'driver2' => array('ja' => '運転される方2','en' => 'Driver #2','zh' => '駕駛人2','th' => 'ผู้ขับรถคนที่2'),
                'driver3' => array('ja' => '運転される方3','en' => 'Driver #3','zh' => '駕駛人3','th' => 'ผู้ขับรถคนที่3'),
                'driver4' => array('ja' => '運転される方4','en' => 'Driver #4','zh' => '駕駛人4','th' => 'ผู้ขับรถคนที่4'),
                'plan' => array('ja' => '割引プラン','en' => 'Discount options','zh' => '割引プラン','th' => 'แผนส่วนลด'),
                'payout' => array('ja' => 'お支払方法','en' => 'Payment method','zh' => '付款方式','th' => 'วิธรการชำระเงิน'),
                //'safe' => array('ja' => '免責補償制度','en' => 'Collision Damage Waiver','zh' => '免責補償制度','th' => ''),
                //'pack' => array('ja' => '安心パック','en' => 'ANSHIN PACKAGE','zh' => '安心方案','th' => ''),
                'rental' => array('ja' => 'レンタルグッズ','en' => 'Rental items','zh' => '租借用品','th' => 'อุปกรณ์เช่าเสริม'),
                'hep' => array('ja' => '高速乗り放題パス','en' => 'Hokkaido Expressway Pass','zh' => '行駛高速公路吃到飽','th' => 'บัตรผ่านทางด่วนแบบใช้ไม่อั้น'),
                'main_comment' => array('ja' => '今回のご旅行の主人公は？','en' => 'Who is the main person for this trip?','zh' => '這次旅行的主角是那位？','th' => 'จุดประสงค์หลักในการท่องเที่ยวครั้งนี้'),

                'cdw' => array('ja' => '事故でも安心補償パック','en' => 'Anshin guarantee pack','zh' => '發生事故也能放心的安心補償方案','th' => 'แผนประกันสบายใจแม้มีอุบัติเหตุ'),
                'country' => array('ja' => '国名','en' => 'Country','zh' => '國名','th' => 'ประเทศ'),

                'question' => array('ja' => 'ご質問・ご要望','en' => 'Question/Request','zh' => '詢問・要求。','th' => 'คำถาม ข้อเสนอแนะ'),
                'accident' => array('ja' => '事故・故障等により貸出不可能時の対応','en' => 'Coping with accidents or failures reasons that cannot be lend','zh' => '因發生事故・故障等狀況而無法租借的對應措施','th' => 'การดูแลเรื่องอุบัติเหตุและความขัดข้องในกรณีที่ไม่สามารถให้เช่าได้'),
                'yakkan' => array('ja' => '貸渡約款','en' => 'Rental agreement','zh' => '租賃契約','th' => 'ข้อกำหนดในการเช่ารถ'),
                'price_bedding' => array('ja' => '寝具クリーニング代','en' => '‘Bedding’ cleaning fee','zh' => '寝具清潔費','th' => 'ค่าทำความสะอาดเครื่องนอน'),
                'currency' => array('ja' => '￥','en' => 'JPY','zh' => '日幣','th' => 'เยน'),
                'driver1_name' => array('ja' => 'お名前（運転される方1）','en' => 'Name(Driver #1)','zh' => '你的名字(駕駛人1)','th' => 'ชื่อ (ผู้ขับที่1)'),
                'driver1_no' => array('ja' => '免許証番号（運転される方1）','en' => 'Driver license number(Driver #1)','zh' => '駕照號碼(駕駛人1)','th' => 'หมายเลขใบขับขี่ (ผู้ขับที่1)'),
                'driver1_age' => array('ja' => '年齢（運転される方1）','en' => 'Age(Driver #1)','zh' => '年齢(駕駛人1)','th' => 'อายุ (ผู้ขับที่1)'),
                'driver2_name' => array('ja' => 'お名前（運転される方2）','en' => 'Name(Driver #2)','zh' => '你的名字(駕駛人2)','th' => 'หมายเลขใบขับขี่ (ผู้ขับที่2)'),
                'driver2_no' => array('ja' => '免許証番号（運転される方2）','en' => 'Driver license number(Driver #2)','zh' => '駕照號碼(駕駛人2)','th' => 'อายุ (ผู้ขับที่2)'),
                'driver2_tel' => array('ja' => '電話番号（運転される方2）','en' => 'Telephone number(Driver #2)','zh' => '電話號碼(駕駛人2)','th' => 'driver2_tel'),
                'driver2_age' => array('ja' => '年齢（運転される方2）','en' => 'Age(Driver #2)','zh' => '年齢(駕駛人2)','th' => 'อายุ (ผู้ขับที่2)'),
                'driver3_name' => array('ja' => 'お名前（運転される方3）','en' => 'Name(Driver #3)','zh' => '你的名字(駕駛人3)','th' => 'ชื่อ (ผู้ขับที่3)'),
                'driver3_no' => array('ja' => '免許証番号（運転される方3）','en' => 'Driver license number(Driver #3)','zh' => '駕照號碼(駕駛人3)','th' => 'หมายเลขใบขับขี่ (ผู้ขับที่3)'),
                'driver3_tel' => array('ja' => '電話番号（運転される方3）','en' => 'Telephone number(Driver #3)','zh' => '電話號碼(駕駛人3)','th' => 'driver3_tel'),
                'driver3_age' => array('ja' => '年齢（運転される方3）','en' => 'Age(Driver #3)','zh' => '年齢(駕駛人3)','th' => 'อายุ (ผู้ขับที่3)'),
                'driver4_name' => array('ja' => 'お名前（運転される方4）','en' => 'Name(Driver #4)','zh' => '你的名字(駕駛人4)','th' => 'ชื่อ (ผู้ขับที่4)'),
                'driver4_no' => array('ja' => '免許証番号（運転される方4）','en' => 'Driver license number(Driver #4)','zh' => '駕照號碼(駕駛人4)','th' => 'หมายเลขใบขับขี่ (ผู้ขับที่4)'),
                'driver4_tel' => array('ja' => '電話番号（運転される方4）','en' => 'Telephone number(Driver #4)','zh' => '電話號碼(駕駛人4)','th' => 'driver4_tel'),
                'driver4_age' => array('ja' => '年齢（運転される方4）','en' => 'Age(Driver #4)','zh' => '年齢(駕駛人4)','th' => 'อายุ (ผู้ขับที่4)'),
                'age' => array('ja' => '年齢','en' => 'Age','zh' => '年齢','th' => 'อายุ'),
                'price_basiccharge' => array('ja' => '基本料金','en' => 'Basic price','zh' => '基本費用','th' => 'ราคาพื้นฐาน'),
                'over_price' => array('ja' => '延長料金','en' => 'Extension Fee','zh' => '延長租金','th' => 'ค่าต่อเวลา'),
                'price_discount' => array('ja' => '割引料金','en' => 'Discount amount','zh' => '割引料金','th' => 'ราคาส่วนลด'),
                'price_dispatch' => array('ja' => '配車料','en' => 'Vehicle delivery fee','zh' => '派車費','th' => 'ราคาจัดส่งรถ'),
                'price_menseki' => array('ja' => '免責補償制度','en' => 'CDW','zh' => '免責補償制度','th' => 'แผนประกันยกเว้นค่าเสียหาย'),
                'price_safety' => array('ja' => '安心パック','en' => 'RAP','zh' => '安心方案','th' => 'แผนประกันขับขี่สบายใจ'),
                'price_cleaning' => array('ja' => '清掃料','en' => 'Cleaning fee','zh' => '清潔費','th' => 'ค่าทำความสะอาด'),
                'price_goods' => array('ja' => 'レンタルグッズ','en' => 'Rental items','zh' => '租借用品','th' => 'อุปกรณ์เช่าเสริม'),
                'price_hep' => array('ja' => '高速乗り放題パス','en' => 'Hokkaido Expressway Pass','zh' => '行駛高速公路吃到飽','th' => 'บัตรผ่านทางด่วนแบบใช้ไม่อั้น'),
                'price_subtotal' => array('ja' => '小計','en' => 'Subtotal','zh' => '小計','th' => 'ยอดรวม'),
                'price_tax' => array('ja' => '消費税','en' => 'Tax','zh' => '消費稅','th' => 'ภาษี'),
                'price_total' => array('ja' => '合計','en' => 'Total','zh' => '合計','th' => 'ยอดรวมทั้งหมด'),
                'price_estimation' => array('ja' => 'お見積もり金額','en' => 'Estimation price','zh' => '報價金額','th' => 'ราคาค่าใช้จ่ายโดยประมาณ'),
                'correct_btn' => array('ja' => '修正する','en' => 'Correct','zh' => '糾正','th' => 'แก้ไข'),
                'send_btn' => array('ja' => '送信する','en' => 'Send','zh' => '發送','th' => 'ส่ง'),
                'mail_headline1' => array('ja' => '※このメールは自動返信メールです。','en' => '※This mail is an automatic reply mail.','zh' => '※本封Email為自動回信。','th' => 'นี่เป็นอีเมล์ที่ถูกส่งโดยอัตโนมัติจาก 「info@nomad-r.jp」'),
                'mail_headline2' => array('ja' => 'この度は、ノマドレンタカーに仮ご予約いただき誠にありがとうございます。下記のとおり受付致しました。','en' => 'Thank you for reserving to Nomad Car Rental. We accepted as follows.','zh' => '非常感謝您本次暫定預約北海道逍遙遊露營車。如下列受理了。','th' => 'ขอบพระคุณที่ใช้บริการรถเช่าโนแมดในครั้งนี้ ได้รับข้อมูลเรียบร้อยแล้ว'),
                'mail_title' => array('ja' => $_POST['name'].'様','en' => 'Dear, Mr./Ms. '.$_POST['name'].':','zh' => $_POST['name'].'先生/小姐您好','th' => 'ท่าน'),
                'mail_subject' => array('ja' => '北海道ノマドレンタカーご予約見積もり確認メール', 'en' => 'Hokkaido Nomad Car Rental Reservation Request Confirmation Confirmation Email','zh' => '北海道逍遙遊露營車預約報價確認信','th' => 'อีเมล์ยืนยันการจองรถเช่าฮอกไกโดโนแมด'),
                'excel_filenm' => array('ja' => 'template.xlsx','en' => 'template_en.xlsx','zh' => 'template_zh.xlsx','th' => 'template_th.xlsx'),
                'shokai' => array('ja' => 'ご紹介者様','en' => 'Introducer','zh' => '介紹人','th' => 'ผู้แนะนำ'),
                'group1' => array('ja' => '海外からの方へ','en' => 'To customers from overseas','zh' => '給國外客戶','th' => 'เรียนลูกค้าต่างชาติ'),


            );
//---------------------------　必須設定　必ず設定してください　-----------------------

//サイトのトップページのURL　※デフォルトでは送信完了後に「トップページへ戻る」ボタンが表示されますので
$site_top = "http://www.php-factory.net/";


//フォームのメールアドレス入力箇所のname属性の値（name="○○"　の○○部分）
$Email = "メールアドレス";

/*------------------------------------------------------------------------------------------------
以下スパム防止のための設定　
※有効にするにはこのファイルとフォームページが同一ドメイン内にある必要があります
------------------------------------------------------------------------------------------------*/

//スパム防止のためのリファラチェック（フォームページが同一ドメインであるかどうかのチェック）(する=1, しない=0)
$Referer_check = 0;

//リファラチェックを「する」場合のドメイン ※以下例を参考に設置するサイトのドメインを指定して下さい。
$Referer_check_domain = "php-factory.net";

//---------------------------　必須設定　ここまで　------------------------------------


//---------------------- 任意設定　以下は必要に応じて設定してください ------------------------

// 管理者宛のメールで差出人を送信者のメールアドレスにする(する=1, しない=0)
// する場合は、メール入力欄のname属性の値を「$Email」で指定した値にしてください。
//メーラーなどで返信する場合に便利なので「する」がおすすめです。
$userMail = 1;

// Bccで送るメールアドレス(複数指定する場合は「,」で区切ってください 例 $BccMail = "aa@aa.aa,bb@bb.bb";)
$BccMail = "";


// 送信確認画面の表示(する=1, しない=0)
$confirmDsp = 1;

// 送信完了後に自動的に指定のページ(サンクスページなど)に移動する(する=1, しない=0)
// CV率を解析したい場合などはサンクスページを別途用意し、URLをこの下の項目で指定してください。
// 0にすると、デフォルトの送信完了画面が表示されます。
$jumpPage = 1;

// 送信完了後に表示するページURL（上記で1を設定した場合のみ）※httpから始まるURLで指定ください。
if ($lang=='ja') {
    $thanksPage = get_bloginfo('url').'/thanks/';
}else {
    $thanksPage = get_bloginfo('url').'/'.$lang.'/thanks/';
}
// 必須入力項目を設定する(する=1, しない=0)
$requireCheck = 1;
//$requireCheck = 0;

/* 必須入力項目(入力フォームで指定したname属性の値を指定してください。（上記で1を設定した場合のみ）
値はシングルクォーテーションで囲み、複数の場合はカンマで区切ってください。フォーム側と順番を合わせると良いです。
配列の形「name="○○[]"」の場合には必ず後ろの[]を取ったものを指定して下さい。*/
if ($lang=='ja') {

    $require = array(
                    'car',
                    'goDate',
                    'goTime',
                    'backDate',
                    'backTime',
                    'place_go',
                    'place_back',
                    'name',
                    'name_furigana',
                    'tel',
                    'mailadd',
                    'zip',
                    'address',
                    'birth',
                    'emagency',
                    'adult',
                    'driver1_name',
                    'driver1_no',
                    'payout',
    //                'main_comment',
    //                'question',
                    'yakkan',
                    'accident',
                    'country',
                    'cdw',
    );
}
if ($lang=='en') {

    $require = array(
                    'car',
                    'goDate',
                    'goTime',
                    'backDate',
                    'backTime',
                    'place_go',
                    'place_back',
                    'name',
                    //'name_furigana',
                    'tel',
                    'mailadd',
                    'zip',
                    'address',
                    'birth',
                    'emagency',
                    'adult',
                    'driver1_name',
                    'driver1_no',
                    'payout',
    //                'main_comment',
    //                'question',
                    'yakkan',
                    'accident',
                    'country',
                    'cdw',
    );
}

if ($lang=='zh') {

    $require = array(
                    'car',
                    'goDate',
                    'goTime',
                    'backDate',
                    'backTime',
                    'place_go',
                    'place_back',
                    'name',
                    'name_furigana',
                    'tel',
                    'mailadd',
                    'zip',
                    'address',
                    'birth',
                    'emagency',
                    'adult',
                    'driver1_name',
                    'driver1_no',
                    'payout',
                    //'yakkan',
                    //'accident',
                    'country',
                    'cdw',
    );
}

if ($lang=='th') {

    $require = array(
                    'car',
                    'goDate',
                    'goTime',
                    'backDate',
                    'backTime',
                    'place_go',
                    'place_back',
                    'name',
                    'name_furigana',
                    'tel',
                    'mailadd',
                    'zip',
                    'address',
                    'birth',
                    'emagency',
                    'adult',
                    'driver1_name',
                    'driver1_no',
                    'payout',
    //                'main_comment',
    //                'question',
                    'yakkan',
                    'accident',
                    'country',
                    'cdw',
    );
}

// メールテキスト本文の生成　（レンタルグッズ）
$rental_gds_text = "";
for ($i = 1; $i <= number_format((int)$_POST['ren_gds_max']); $i++) {
    if($_POST['ren_gds_disp'.(string)$i] == 1) {

        if($lang=="en") {

            if($_POST['rental_num'.(string)$i] > 0) {
                $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$_POST['ren_gds_amt'.(string)$i].$object_name['currency'][$lang]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
            } else {
                $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$_POST['ren_gds_amt'.(string)$i].$object_name['currency'][$lang]."\n";
            }

        } else {

            if($_POST['rental_num'.(string)$i] > 0) {
                $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
            } else {
                $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]."\n";
            }

        }

        // switch ($lang) {
        //     case 'ja':
        //     if($_POST['rental_num'.(string)$i] > 0) {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
        //     } else {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]."\n";
        //     }
        //     break;
        // case 'en':
        //     if($_POST['rental_num'.(string)$i] > 0) {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$_POST['ren_gds_amt'.(string)$i].$object_name['currency'][$lang]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
        //     } else {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$_POST['ren_gds_amt'.(string)$i].$object_name['currency'][$lang]."\n";
        //     }
        //     break;
        // case 'zh':
        //     if($_POST['rental_num'.(string)$i] > 0) {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
        //     } else {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]."\n";
        //     }
        //     break;
        // case 'th':
        //     if($_POST['rental_num'.(string)$i] > 0) {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]." / ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]."\n";
        //     } else {
        //         $rental_gds_text = $rental_gds_text.$_POST['ren_gds_name'.(string)$i]." ".$object_name['currency'][$lang].$_POST['ren_gds_amt'.(string)$i]."\n";
        //     }
        //     break;
        // }
    }
}

// switch ($lang) {
// case 'ja':
//     // お見積り金額のセット
//     $price_total = "￥".number_format($_POST['price_total']);
//     $price_basiccharge = "￥".number_format($_POST['price_basiccharge']);
//     $price_discount = "-￥".number_format($_POST['price_discount']);
//     $price_dispatch = "￥".number_format($_POST['price_dispatch']);
//     $price_cleaning = "￥".number_format($_POST['price_cleaning']);
//     $price_menseki = "￥".number_format($_POST['price_menseki']);
//     $price_safety = "￥".number_format($_POST['price_safety']);
//     $price_bedding = "￥".number_format($_POST['price_bedding']);
//     $price_goods = "￥".number_format($_POST['price_goods']);
//     $price_hep = "￥".number_format($_POST['price_hep']);
//     $price_subtotal = "￥".number_format($_POST['price_subtotal']);
//     $price_tax = "￥".number_format($_POST['price_tax']);
//     break;
// case 'en':
//     // お見積り金額のセット
//     $price_total = number_format($_POST['price_total'])." JPY";
//     $price_basiccharge = number_format($_POST['price_basiccharge'])." JPY";
//     $price_discount = "-".number_format($_POST['price_discount'])." JPY";
//     $price_dispatch = number_format($_POST['price_dispatch'])." JPY";
//     $price_cleaning = number_format($_POST['price_cleaning'])." JPY";
//     $price_menseki = number_format($_POST['price_menseki'])." JPY";
//     $price_safety = number_format($_POST['price_safety'])." JPY";
//     $price_bedding = number_format($_POST['price_bedding'])." JPY";
//     $price_goods = number_format($_POST['price_goods'])." JPY";
//     $price_hep = number_format($_POST['price_hep'])." JPY";
//     $price_subtotal = number_format($_POST['price_subtotal'])." JPY";
//     $price_tax = number_format($_POST['price_tax'])." JPY";
//     break;
// case 'zh':
//     // お見積り金額のセット
//     $price_total = "日幣".number_format($_POST['price_total']);
//     $price_basiccharge = "日幣".number_format($_POST['price_basiccharge']);
//     $price_discount = "-日幣".number_format($_POST['price_discount']);
//     $price_dispatch = "日幣".number_format($_POST['price_dispatch']);
//     $price_cleaning = "日幣".number_format($_POST['price_cleaning']);
//     $price_menseki = "日幣".number_format($_POST['price_menseki']);
//     $price_safety = "日幣".number_format($_POST['price_safety']);
//     $price_bedding = "日幣".number_format($_POST['price_bedding']);
//     $price_goods = "日幣".number_format($_POST['price_goods']);
//     $price_hep = "日幣".number_format($_POST['price_hep']);
//     $price_subtotal = "日幣".number_format($_POST['price_subtotal']);
//     $price_tax = "日幣".number_format($_POST['price_tax']);
//     break;
// case 'th':
//     // お見積り金額のセット
//     $price_total = number_format($_POST['price_total'])." เยน";
//     $price_basiccharge = number_format($_POST['price_basiccharge'])." เยน";
//     $price_discount = "-".number_format($_POST['price_discount'])." เยน";
//     $price_dispatch = number_format($_POST['price_dispatch'])." เยน";
//     $price_cleaning = number_format($_POST['price_cleaning'])." เยน";
//     $price_menseki = number_format($_POST['price_menseki'])." เยน";
//     $price_safety = number_format($_POST['price_safety'])." เยน";
//     $price_bedding = number_format($_POST['price_bedding'])." เยน";
//     $price_goods = number_format($_POST['price_goods'])." เยน";
//     $price_hep = number_format($_POST['price_hep'])." เยน";
//     $price_subtotal = number_format($_POST['price_subtotal'])." เยน";
//     $price_tax = number_format($_POST['price_tax'])." เยน";
//     break;
// }

// お見積り金額のセット
$price_total = $price_prefix.number_format($_POST['price_total']).$price_suffix;
$price_basiccharge = $price_prefix.number_format($_POST['price_basiccharge']).$price_suffix;
$over_price = $price_prefix.number_format($_POST['over_price']).$price_suffix;
$price_discount = "-".$price_prefix.number_format($_POST['price_discount']).$price_suffix;
$price_dispatch = $price_prefix.number_format($_POST['price_dispatch']).$price_suffix;
$price_cleaning = $price_prefix.number_format($_POST['price_cleaning']).$price_suffix;
$price_menseki = $price_prefix.number_format($_POST['price_menseki']).$price_suffix;
$price_safety = $price_prefix.number_format($_POST['price_safety']).$price_suffix;
$price_bedding = $price_prefix.number_format($_POST['price_bedding']).$price_suffix;
$price_goods = $price_prefix.number_format($_POST['price_goods']).$price_suffix;
$price_hep = $price_prefix.number_format($_POST['price_hep']).$price_suffix;
$price_subtotal = $price_prefix.number_format($_POST['price_subtotal']).$price_suffix;
$price_tax = $price_prefix.number_format($_POST['price_tax']).$price_suffix;



//----------------------------------------------------------------------
//  管理者宛メール設定(START)
//----------------------------------------------------------------------
$admin_m_from = "info@nomad-r.jp";

if($debug_mode==1){
//$admin_m_to = "k-matsumura@alma-rhythm.com,abe@nissindou.co.jp";
$admin_m_to = "k-matsumura@alma-rhythm.com";
} else {
    if ($lang=='zh') {
        $admin_m_to = "info@nomad-r.jp,abe@nomad-r.jp,info-zh@nomad-r.jp,k-matsumura@alma-rhythm.com";
    } elseif ($lang=='th') {
        $admin_m_to = "info@nomad-r.jp,abe@nomad-r.jp,info-th@nomad-r.jp,k-matsumura@alma-rhythm.com";
    } else {
        $admin_m_to = "info@nomad-r.jp,abe@nomad-r.jp,k-matsumura@alma-rhythm.com";
    }
}

$admin_m_subject = "ノマドレンタカーのご予約見積もりが届きました";
$admin_m_text = <<<EOT
ノマドレンタカーホームページより、仮ご予約を頂きました。
内容は下記のとおりです。

-------------------------------------------------------------------------
{$object_name['car'][$lang]} ：{$_POST['car']}
{$object_name['goDate'][$lang]} ：{$_POST['goDate']}
{$object_name['backDate'][$lang]} ：{$_POST['backDate']}
{$object_name['place_go'][$lang]} ：{$_POST['place_go']}
{$object_name['place_back'][$lang]} ：{$_POST['place_back']}
{$object_name['name'][$lang]} ：{$_POST['name']}
{$object_name['name_furigana'][$lang]} ：{$_POST['name_furigana']}
{$object_name['tel'][$lang]} ：{$_POST['tel']}
{$object_name['mailadd'][$lang]} ：{$_POST['mailadd']}
{$object_name['zip'][$lang]} ：{$_POST['zip']}
{$object_name['country'][$lang]} ：{$_POST['country']}
{$object_name['address'][$lang]} ：{$_POST['address']}
{$object_name['birth'][$lang]} ：{$_POST['birth']}
{$object_name['emagency'][$lang]} ：{$_POST['emagency']}
{$object_name['adult'][$lang]} ：{$_POST['adult']}
{$object_name['child'][$lang]} ：{$_POST['child']}
{$object_name['child_seat'][$lang]} ：{$_POST['child_seat']} / {$object_name['age'][$lang]} :{$_POST['child_seat_age']}
{$object_name['junior_seat'][$lang]} ：{$_POST['junior_seat']} / {$object_name['age'][$lang]} :{$_POST['junior_seat_age']}
{$object_name['pet'][$lang]} ：{$pet_mail_text}
{$object_name['driver1'][$lang]} ：{$_POST['driver1']} ／ {$_POST['driver1_age']}歳
{$object_name['driver2'][$lang]} ：{$_POST['driver2']} ／ {$_POST['driver2_age']}歳
{$object_name['driver3'][$lang]} ：{$_POST['driver3']} ／ {$_POST['driver3_age']}歳
{$object_name['driver4'][$lang]} ：{$_POST['driver4']} ／ {$_POST['driver4_age']}歳
{$object_name['plan'][$lang]} ：{$_POST['disc_plan_name']}
{$object_name['hep'][$lang]} ：{$_POST['hep']}
{$object_name['payout'][$lang]} ：{$_POST['payout']}
{$object_name['cdw'][$lang]} ：{$_POST['cdw']}
{$object_name['price_bedding'][$lang]} ：{$object_name['currency'][$lang]}{$_POST['price_bedding']}
{$object_name['rental'][$lang]} ：
$rental_gds_text
{$object_name['main_comment'][$lang]} ：
{$_POST['main_comment']}
{$object_name['shokai'][$lang]} ：{$_POST['shokai']}
{$object_name['question'][$lang]} ：
{$_POST['question']}
{$object_name['accident'][$lang]} ：{$_POST['accident']}
{$object_name['yakkan'][$lang]} ：{$_POST['yakkan']}
{$object_name['group1'][$lang]} ：{$_POST['group1']}

<{$object_name['price_estimation'][$lang]}>
{$object_name['price_basiccharge'][$lang]} ：{$price_basiccharge}
{$object_name['over_price'][$lang]} ：{$over_price}
{$object_name['price_discount'][$lang]} ：{$price_discount}
{$object_name['price_dispatch'][$lang]} ：{$price_dispatch}
{$object_name['price_cleaning'][$lang]} ：{$price_cleaning}
{$object_name['price_menseki'][$lang]} ：{$price_menseki}
{$object_name['price_safety'][$lang]} ：{$price_safety}
{$object_name['price_bedding'][$lang]} ：{$price_bedding}
{$object_name['price_goods'][$lang]} ：{$price_goods}
{$object_name['price_hep'][$lang]} ：{$price_hep}
--------------------------------------
{$object_name['price_subtotal'][$lang]} ：{$price_subtotal}
{$object_name['price_tax'][$lang]} ：{$price_tax}
{$object_name['price_total'][$lang]} ：{$price_total}

-------------------------------------------------------------------------
北海道ノマドレンタカー
本社
〒062－0936
北海道札幌市豊平区平岸6条12丁目11－2
千歳店
〒066-0015
千歳市青葉2丁目17－28
URL : http://nomad-r.jp/
mail : info@nomad-r.jp
営業受付時間 9時～18時 (配車・返却時間はご相談くださいませ)
-------------------------------------------------------------------------
EOT;


//----------------------------------------------------------------------
//  管理者宛メール設定(END)
//----------------------------------------------------------------------



if ($lang=='en') {

$shomei = <<<EOT

We are going to send you rental documents in 2 business days. Please confirm it.
Please par your rent fee in 1 week after your checking total amount on the rental documents vie bank transfer or credit card. Your reservation will be completed when we check the fee is paid.

* If we cannot check the fee is paid within 1 week. Your reservation will be automatically canceled. Please understand.

Please check our cancellation policy here.
　　　↓　　↓
http://nomad-r.jp/guide/


【Bank account】
North　Pacific Bank
Saving account 5326111
Account holder  ホッカイドウノマドレンタカー　( Hokkaido Nomad Car Rental)

*If you will pay with your credit card, we will inform you how to by e-mail.

-------------------------------------------------------------------------
〒004-0812
2　Utsukushigaoka 2 jo 6 chome Kiyota-ku Sapporo, Hokkaido, Japan
URL : http://nomad-r.jp/
mail : info@nomad-r.jp
Business hours 9:00AM~6:00PM (pick-up and return times are negotiable)
-------------------------------------------------------------------------
EOT;

}

if ($lang=='ja' || $lang=='zh') {

$shomei = <<<EOT
上記日程にて「仮ご予約」を受け賜りました。

このあと弊社より２営業日以内に、貸渡書、及びクレジットカード決済についてのご案内をメールにて返信させていただきますので内容をご確認ください。
貸渡書の合計金額等をご確認の上、1週間以内にご利用料金全額をクレジットカードにてご入金ください。
入金が確認された時点で正式ご予約となります。

＊本メールに記載している金額はシステムにより自動計算された見積金額のため、貸渡書をもちまして正式な金額とさせていただきます。

＊1週間を過ぎてご入金が確認できない場合は、自動キャンセルとなりますことをあらかじめご了承くださいませ。

キャンセルポリシーはこちらをご覧ください
　　　↓　　↓
http://nomad-r.jp/guide/

-------------------------------------------------------------------------
北海道ノマドレンタカー
本社
〒062－0936
北海道札幌市豊平区平岸6条12丁目11－2
千歳店
〒066-0015
千歳市青葉2丁目17－28
URL : http://nomad-r.jp/
mail : info@nomad-r.jp
営業受付時間 9時～18時 (配車・返却時間はご相談くださいませ)
-------------------------------------------------------------------------
EOT;

}


if ($lang=='th') {

$shomei = <<<EOT
ขอบพระคุณสำหรับการจองรถเช่าของโนแมดในครั้งนี้

หลังจากนั้นทางเราจะส่งเมล์รายละเอียดการเช่ารถกลับภายใน2วันทำการ กรุณาตรวจสอบเนื้อหา
หลังจากตรวจสอบค่าใช้จ่ายตามรายละเอียดการเช่ารถแล้ว กรุณาโอนเงินหรือชำระเงินด้วยเครดิตการ์ดภายใน1สัปดาห์ จึงจะถือว่าการจองเสร็จสมบูรณ์

＊ในกรณีที่ไม่สามารถยืนยันการชำระเงินภายใน1สัปดาห์ จะยกเลิกการจองโดยอัตโนมัติ

เงื่อนไขในการยกเลิกการจอง อ่านที่นี่
　　　↓　　↓
http://nomad-r.jp/th/guide/


【บัญชีธนาคารในการชำระเงิน】
North Pacific Bank - Sapporo-Nishi Branch
เลขที่ 5326111
ชื่อบัญชี Hokkaido Nomad Car Rental

※ลูกค้าที่ชำระเงินด้วยเครดิตการ์ดจะได้รับวิธีการชำระเงินอีกอีเมล์หนึ่ง

-------------------------------------------------------------------------
รหัสไปรษณีย์ 062-0933
Hokkaido, Sapporo, Toyohira-ku, Hiragishi 3 Article 6-chome, 3-15
เว็บ  : http://nomad-r.jp/th/
อีเมล์ : info@nomad-r.jp
เวลาทำการ 09:00-18:00 (เวลาส่งรถและคืนรถกรุณาติดต่อสอบถามเพิ่มเติ่ม)
-------------------------------------------------------------------------
EOT;

}


//----------------------------------------------------------------------
//  自動返信メール設定(START)
//----------------------------------------------------------------------

$re_m_from = "info@nomad-r.jp";
$test_mail = "formtest@nomad-r.jp";
$re_m_to = $_POST['mailadd'];
$re_m_subject = $object_name['mail_subject'][$lang];
$re_m_text = <<<EOT
{$object_name['mail_title'][$lang]}

{$object_name['mail_headline1'][$lang]}
{$object_name['mail_headline2'][$lang]}

-------------------------------------------------------------------------

{$object_name['car'][$lang]} ：{$_POST['car']}
{$object_name['goDate'][$lang]} ：{$_POST['goDate']}
{$object_name['backDate'][$lang]} ：{$_POST['backDate']}
{$object_name['place_go'][$lang]} ：{$_POST['place_go']}
{$object_name['place_back'][$lang]} ：{$_POST['place_back']}
{$object_name['name'][$lang]} ：{$_POST['name']}
{$object_name['name_furigana'][$lang]} ：{$_POST['name_furigana']}
{$object_name['tel'][$lang]} ：{$_POST['tel']}
{$object_name['mailadd'][$lang]} ：{$_POST['mailadd']}
{$object_name['zip'][$lang]} ：{$_POST['zip']}
{$object_name['country'][$lang]} ：{$_POST['country']}
{$object_name['address'][$lang]} ：{$_POST['address']}
{$object_name['birth'][$lang]} ：{$_POST['birth']}
{$object_name['emagency'][$lang]} ：{$_POST['emagency']}
{$object_name['adult'][$lang]} ：{$_POST['adult']}
{$object_name['child'][$lang]} ：{$_POST['child']}
{$object_name['child_seat'][$lang]} ：{$_POST['child_seat']} ／ {$object_name['age'][$lang]} :{$_POST['child_seat_age']}
{$object_name['junior_seat'][$lang]} ：{$_POST['junior_seat']} ／ {$object_name['age'][$lang]} :{$_POST['junior_seat_age']}
{$object_name['pet'][$lang]} ：{$pet_mail_text}
{$object_name['driver1'][$lang]} ：{$_POST['driver1']} ／ {$_POST['driver1_age']}歳
{$object_name['driver2'][$lang]} ：{$_POST['driver2']} ／ {$_POST['driver2_age']}歳
{$object_name['driver3'][$lang]} ：{$_POST['driver3']} ／ {$_POST['driver3_age']}歳
{$object_name['driver4'][$lang]} ：{$_POST['driver4']} ／ {$_POST['driver4_age']}歳
{$object_name['plan'][$lang]} ：{$_POST['disc_plan_name']}
{$object_name['hep'][$lang]} ：{$_POST['hep_name']}
{$object_name['payout'][$lang]} ：{$_POST['payout']}
{$object_name['cdw'][$lang]} ：{$_POST['cdw']}
{$object_name['price_bedding'][$lang]} ：{$object_name['currency'][$lang]}{$_POST['price_bedding']}
{$object_name['rental'][$lang]} ：
$rental_gds_text
{$object_name['main_comment'][$lang]} ：
{$_POST['main_comment']}
{$object_name['shokai'][$lang]} ：{$_POST['shokai']}
{$object_name['question'][$lang]} ：
{$_POST['question']}
{$object_name['accident'][$lang]} ：{$_POST['accident']}
{$object_name['yakkan'][$lang]} ：{$_POST['yakkan']}
{$object_name['group1'][$lang]} ：{$_POST['group1']}

<{$object_name['price_estimation'][$lang]}>
{$object_name['price_basiccharge'][$lang]} ：{$price_basiccharge}
{$object_name['over_price'][$lang]} ：{$over_price}
{$object_name['price_discount'][$lang]} ：{$price_discount}
{$object_name['price_dispatch'][$lang]} ：{$price_dispatch}
{$object_name['price_cleaning'][$lang]} ：{$price_cleaning}
{$object_name['price_menseki'][$lang]} ：{$price_menseki}
{$object_name['price_safety'][$lang]} ：{$price_safety}
{$object_name['price_bedding'][$lang]} ：{$price_bedding}
{$object_name['price_goods'][$lang]} ：{$price_goods}
{$object_name['price_hep'][$lang]} ：{$price_hep}
--------------------------------------
{$object_name['price_subtotal'][$lang]} ：{$price_subtotal}
{$object_name['price_tax'][$lang]} ：{$price_tax}
{$object_name['price_total'][$lang]} ：{$price_total}

{$shomei}

EOT;


//メールアドレスの形式チェックを行うかどうか。(する=1, しない=0)
//※デフォルトは「する」。特に理由がなければ変更しないで下さい。メール入力欄のname属性の値が上記「$Email」で指定した値である必要があります。
$mail_check = 1;

//全角英数字→半角変換を行うかどうか。(する=1, しない=0)
$hankaku = 0;

//全角英数字→半角変換を行う項目のname属性の値（name="○○"の「○○」部分）
//※複数の場合にはカンマで区切って下さい。（上記で「1」を指定した場合のみ有効）
//配列の形「name="○○[]"」の場合には必ず後ろの[]を取ったものを指定して下さい。
//$require = array('車種','ご出発希望日時','ご返却希望日時','ご出発希望場所','ご返却場所','お申し込み者名','フリガナ','メールアドレス','電話番号','ご住所','メールアドレス','事故・故障等により貸出不可能時の対応','貸渡約款');

//----------------------------------------------------------------------
//  添付ファイル処理用設定(BEGIN)
//----------------------------------------------------------------------
//ファイル添付機能を使用する場合は一時ファイルを保存する必要があるため確認画面の表示が必須になります。
$confirmDsp = 1;//確認画面を表示 ※変更不可

/* ----- 重要 ------*/
//ファイルアップ部分のnameの値は必ず配列の形　例　upfile[]　としてください。※添付ファイルが1つでも
//添付ファイルは複数も可能です。

//例1 添付ファイルが1つの場合　
//添付ファイル <input type="file" name="upfile[]" />

//例2 添付ファイルが複数の場合　
//添付ファイル1：<input type="file" name="upfile[]" /> 添付ファイル2：<input type="file" name="upfile[]" />

//添付ファイルのMAXファイルサイズ　※単位バイト　デフォルトは5MB（ただしサーバーのphp.iniの設定による）
$maxImgSize = 5024000;

//添付ファイル一時保存用ディレクトリ ※書き込み可能なパーミッション（777等※サーバによる）にしてください
//$tmp_dir_name = './tmp/';
$tmp_dir_name = './tmp/';

//添付許可ファイル（拡張子）
//※大文字、小文字は区別されません（同じ扱い）のでここには小文字だけでOKです（拡張子を大文字で送信してもマッチします）
$permission_file = array('jpg','jpeg','gif','png','pdf','txt','xls','xlsx','zip','lzh','doc');

//フォームのファイル添付箇所のname属性の値 <input type="file" name="upfile[]" />の「upfile」部
//$upfile_key = 'upfile';
$upfile_key = 'upfile';

//サーバー上の一時ファイルを削除する(する=1, しない=0)　※バックアップ目的で保存させておきたい場合など
//添付ファイルは確認画面表示時にtmpディレクトリに一旦保存されますが、それを送信時に削除するかどうか。（残す場合サーバー容量に余裕がある場合のみ推奨）
//もちろん手動での削除も可能です。
$tempFileDel = 1;//デフォルトは削除する

//確認画面→戻る→確認画面のページ遷移では最初の一時ファイルはサーバ上に残りますが、1時間後以降の最初の送信時に自動で削除されます。


//メールソフトで添付ファイル名が文字化けする場合には「1」にしてみてください。（ThuderBirdで日本語ファイル名文字化け対策）
//「1」にすると    添付ファイル名が0～の連番になります。
$rename = 1;//(0 or 1)


//サーバーのphp.iniの「mail.add_x_header」がONかOFFかチェックを行う(する=1, しない=0)　※PHP5.3以降
//「する」場合、mail.add_x_headerがONの場合確認画面でメッセージが表示されます。
//mail.add_x_headerがONの場合、添付ファイルが正常に添付できない可能性が非常に高いためのチェックです。
//mail.add_x_headerはデフォルトは「OFF」ですが、サーバーによっては稀に「ON」になっているためです。
//mail.add_x_headerがONの場合でも正常に添付できていればこちらは「0」として下さい。メッセージは非表示となります。
$iniAddX = 1;

//----------------------------------------------------------------------
//  添付ファイル処理用設定(END)
//----------------------------------------------------------------------


//------------------------------- 任意設定ここまで ---------------------------------------------


// 以下の変更は知識のある方のみ自己責任でお願いします。


//----------------------------------------------------------------------
//  関数実行、変数初期化
//----------------------------------------------------------------------
$encode = "UTF-8";//このファイルの文字コード定義（変更不可）

if(isset($_GET)) $_GET = sanitize($_GET);//NULLバイト除去//
if(isset($_POST)) $_POST = sanitize($_POST);//NULLバイト除去//
if(isset($_COOKIE)) $_COOKIE = sanitize($_COOKIE);//NULLバイト除去//
if($encode == 'SJIS') $_POST = sjisReplace($_POST,$encode);//Shift-JISの場合に誤変換文字の置換実行
$funcRefererCheck = refererCheck($Referer_check,$Referer_check_domain);//リファラチェック実行

//変数初期化
$sendmail = 0;
$empty_flag = 0;
$post_mail = '';
$errm ='';
$header ='';

//----------------------------------------------------------------------
//  添付ファイル処理(BEGIN)
//----------------------------------------------------------------------

if(isset($_FILES[$upfile_key])){
    $file_count = count($_FILES[$upfile_key]["tmp_name"]);
    for ($i=0;$i<$file_count;$i++) {

        if (@is_uploaded_file($_FILES[$upfile_key]["tmp_name"][$i])) {
            if ($_FILES[$upfile_key]["size"][$i] < $maxImgSize) {

                //許可拡張子チェック
                $upfile_name_check = '';
                $upfile_name_array[$i] = explode('.',$_FILES[$upfile_key]['name'][$i]);
                $upfile_name_array_extension[$i] = strtolower(end($upfile_name_array[$i]));
                foreach($permission_file as $permission_val){
                  if($upfile_name_array_extension[$i] == $permission_val){
                      $upfile_name_check = 'checkOK';
                  }
                }
                if($upfile_name_check != 'checkOK'){
                  $errm .= "<p class=\"error_messe\">許可されていない拡張子です。</p>\n";
                  $empty_flag = 1;
                }else{

                      $temp_file_name[$i] = $_FILES[$upfile_key]["name"][$i];
                      $temp_file_name_array[$i] =  explode('.',$temp_file_name[$i]);

                      if(count($temp_file_name_array[$i]) < 2){
                        $errm .= "<p class=\"error_messe\">ファイルに拡張子がありません。</p>\n";
                        $empty_flag = 1;
                      }else{
                        $extension = end($temp_file_name_array[$i]);

                          if(function_exists('uniqid')){
                              if(!file_exists($tmp_dir_name) || !is_writable($tmp_dir_name)){
                              exit("（重大なエラー）添付ファイル一時保存用のディレクトリが無いかパーミッションが正しくありません。{$tmp_dir_name}ディレクトリが存在するか、または{$tmp_dir_name}ディレクトリのパーミッションを書き込み可能（777等※サーバによる）にしてください");
                              }
                          $upFileName[$i] = uniqid('temp_file_').mt_rand(10000,99999).'.'.$extension;
                          $upFilePath[$i] = $tmp_dir_name.$upFileName[$i];

                          }else{
                              exit('（重大なエラー）添付ﾌｧｲﾙ一時ﾌｧｲﾙ用のﾕﾆｰｸIDを生成するuniqid関数が存在しません。<br>PHPのﾊﾞｰｼﾞｮﾝが極端に低い（PHP4未満）ようです。<br>PHPをﾊﾞｰｼﾞｮﾝｱｯﾌﾟするか配布元に相談ください');
                          }
                          move_uploaded_file($_FILES[$upfile_key]['tmp_name'][$i],$upFilePath[$i]);
                          @chmod($upFilePath[$i], 0666);
                      }
                }
            }else{
                  $errm .= "<p class=\"error_messe\">ファイルサイズが大きすぎます。</p>\n";
                  $empty_flag = 1;
            }
        }
    }
}
//----------------------------------------------------------------------
//  添付ファイル処理(END)
//----------------------------------------------------------------------

if($requireCheck == 1) {

    $requireResArray = requireCheck($require);//必須チェック実行し返り値を受け取る
    $errm .= $requireResArray['errm'];
    if($requireResArray['empty_flag'] == 1) $empty_flag = $requireResArray['empty_flag'];
}
//メールアドレスチェック
if(empty($errm)){
    foreach($_POST as $key=>$val) {
        if($val == "confirm_submit") $sendmail = 1;
        if($key == $Email) $post_mail = h($val);
        if($key == $Email && $mail_check == 1 && !empty($val)){
            if(!checkMail($val)){
                $errm .= "<p class=\"error_messe\">【".$key."】はメールアドレスの形式が正しくありません。</p>\n";
                $empty_flag = 1;
            }
        }
    }
}

/*************************************************************
 // 送信時
**************************************************************/

foreach($_POST as $idx => $val) {
    error_log("$idx = $val,", 0);
}

if(($confirmDsp == 0 || $sendmail == 1) && $empty_flag != 1){


    if ($lang=='th') {
        //テストフォーム送信
        error_log('▼▼▼▼　Start テストフォーム送信 ▼▼▼▼', 0);
        send_attached_mail_th($excel_file,$re_m_from,$test_mail,$re_m_subject,$re_m_text,0);
        error_log('▲▲▲▲　End テストフォーム送信 ▲▲▲▲', 0);

        //自動返信メール送信
        error_log('▼▼▼▼　Start 自動返信メール送信 ▼▼▼▼', 0);
        send_attached_mail_th($excel_file,$re_m_from,$re_m_to,$re_m_subject,$re_m_text,0);
        error_log('▲▲▲▲　End 自動返信メール送信 ▲▲▲▲', 0);
    } else {

        //テストフォーム送信
        error_log('▼▼▼▼　Start テストフォーム送信 ▼▼▼▼', 0);
        send_attached_mail($excel_file,$re_m_from,$test_mail,$re_m_subject,$re_m_text,0);
        error_log('▲▲▲▲　End テストフォーム送信 ▲▲▲▲', 0);

        //自動返信メール送信
        error_log('▼▼▼▼　Start 自動返信メール送信 ▼▼▼▼', 0);
        send_attached_mail($excel_file,$re_m_from,$re_m_to,$re_m_subject,$re_m_text,0);
        error_log('▲▲▲▲　End 自動返信メール送信 ▲▲▲▲', 0);

    }
    // PHPExcel
    include_once ( dirname(__FILE__) . '/lib/PHPExcel.php');
    include_once ( dirname(__FILE__) . '/lib/PHPExcel/IOFactory.php');

    // Excel97-2003形式(xls)テンプレートの読み込み
    $reader = PHPExcel_IOFactory::createReader('Excel2007');
//    $excel = $reader->load(dirname(__FILE__) .'/../../data/template.xlsx');
//    $excel = $reader->load(dirname(__FILE__) .'/data/template.xlsx');


    $excel = $reader->load(dirname(__FILE__) .'/../../data/'.$object_name['excel_filenm'][$lang]);

/* Excel出力処理 */

    error_log('Excel出力処理1', 0);

    // レンタルグッズ内訳
    $mainSheet = $excel->getSheetByName("貸渡証");
    $goodsStartRow = 13; // 開始行
    for ($i = 1; $i <= number_format((int)$_POST['ren_gds_max']); $i++) {

        //if(number_format((int)$_POST['ren_gds_amt'.(string)$i]) > 0) {
        if($_POST['ren_gds_disp'.(string)$i] == 1) {

            if($_POST['rental_num'.(string)$i] > 0) {
                $mainSheet->setCellValue('T'.(string)($goodsStartRow), $_POST['ren_gds_name'.(string)$i]." ".$_POST['rental_num'.(string)$i].$_POST['ren_gds_unit'.(string)$i]);
            } else {
                $mainSheet->setCellValue('T'.(string)($goodsStartRow), $_POST['ren_gds_name'.(string)$i]);
            }

            $mainSheet->setCellValue('AC'.(string)($goodsStartRow), $_POST['ren_gds_amt'.(string)$i]);
            $goodsStartRow++;
        }
    }


    error_log('Excel出力処理2', 0);

    // ご契約日
    $today = date("Ymd");
    $mainSheet->setCellValue('AR1', date("Y"));
    $mainSheet->setCellValue('AV1', date("m"));
    $mainSheet->setCellValue('AY1', date("d"));

    error_log('Excel出力処理3', 0);

    // ご利用プラン
    $configSheet = $excel->getSheetByName("config");
    $eventStartRow = 2; // 開始行

    foreach ($event as $val) {
        $configSheet->setCellValue('D'.(string)($eventStartRow),$_POST['plan_d_'.$val] ? $_POST['plan_d_'.$val] : 0);
        $configSheet->setCellValue('E'.(string)($eventStartRow),$_POST['plan_o_'.$val] ? $_POST['plan_o_'.$val] : 0);
        $eventStartRow++;
    }

    error_log('Excel出力処理4', 0);

    $configSheet->setCellValue('B1', $_POST['price_basiccharge']);
    $configSheet->setCellValue('B2', $_POST['disc_plan_name']);
    $configSheet->setCellValue('B3', $_POST['price_discount']);
    $configSheet->setCellValue('B4', $_POST['price_cleaning']);
    $configSheet->setCellValue('B5', $_POST['price_menseki']);
    $configSheet->setCellValue('B6', $_POST['price_safety']);
//    $configSheet->setCellValue('B7', $_POST['price_bedding']);
//    $configSheet->setCellValue('B8', $_POST['price_dispatch']);
    $configSheet->setCellValue('B9', $_POST['price_go_dispatch']);
    $configSheet->setCellValue('B10', $_POST['price_back_dispatch']);
    $configSheet->setCellValue('B11', $_POST['goDate']);
    $configSheet->setCellValue('B12', $_POST['backDate']);
    $configSheet->setCellValue('B13', $_POST['place_go']);
    $configSheet->setCellValue('B14', $_POST['place_back']);
    $configSheet->setCellValue('B15', $_POST['name']);
    $configSheet->setCellValue('B16', $_POST['name_furigana']);
    $configSheet->setCellValue('B17', $_POST['zip']);
    $configSheet->setCellValue('B18', $_POST['address']);
    $configSheet->setCellValue('B19', $_POST['tel']);
    $configSheet->setCellValue('B20', $_POST['mailadd']);
    $configSheet->setCellValue('B21', $_POST['birth']);
    $configSheet->setCellValue('B22', $_POST['driver1_no']);
    $configSheet->setCellValue('B23', $_POST['driver2_name']);
    $configSheet->setCellValue('B24', $_POST['driver2_tel']);
    $configSheet->setCellValue('B25', $_POST['driver2_no']);
    $configSheet->setCellValue('B26', $_POST['driver3_name']);
    $configSheet->setCellValue('B27', $_POST['driver3_tel']);
    $configSheet->setCellValue('B28', $_POST['driver3_no']);
    $configSheet->setCellValue('B29', $_POST['driver4_name']);
    $configSheet->setCellValue('B30', $_POST['driver4_tel']);
    $configSheet->setCellValue('B31', $_POST['driver4_no']);
    $configSheet->setCellValue('B32', $_POST['adult']);
    $configSheet->setCellValue('B33', $_POST['child']);
    $configSheet->setCellValue('B34', $_POST['child_seat']);
    $configSheet->setCellValue('B35', $_POST['junior_seat']);
    $configSheet->setCellValue('B36', $_POST['car']);
    $configSheet->setCellValue('B37', $_POST['payout']);
    $configSheet->setCellValue('B38', $_POST['main_comment']);
    $configSheet->setCellValue('B39', $_POST['question']);
    $configSheet->setCellValue('B40', $_POST['driver1_age']);
    $configSheet->setCellValue('B41', $_POST['driver2_age']);
    $configSheet->setCellValue('B42', $_POST['driver3_age']);
    $configSheet->setCellValue('B43', $_POST['driver4_age']);
    $configSheet->setCellValue('B44', $_POST['price_hep']);
    $configSheet->setCellValue('B45', $_POST['country']);
    $configSheet->setCellValue('B46', $_POST['shokai']);
    $configSheet->setCellValue('B47', $_POST['over_price']);
    $configSheet->setCellValue('B48', $_POST['group1']);

    $paySheet = $excel->getSheetByName("入金管理表");
    $paySheet->setCellValue('A2' , $_POST['name']."様");
    $paySheet->setCellValue('B2' , $_POST['car']);
    $paySheet->setCellValue('C2' , $_POST['go_date_excel']);
    $paySheet->setCellValue('D2' , $_POST['back_date_excel']);
    $paySheet->setCellValue('F2' , $_POST['price_total']);

    error_log('Excel出力処理5', 0);

    // Excel97-2003形式(xls)で出力する
//    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
//    $writer->save(dirname(__FILE__) .'/data/template_out'.date('YmdHis').'.xlsx');
    $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $excel_file = $excel_file_dir . $excel_file_name .date('YmdHis').'.xlsx';
    $writer->save(dirname(__FILE__) . $excel_file);


    if ($lang=='th') {
        //管理者メール送信
        send_attached_mail_th($excel_file,$admin_m_from,$admin_m_to,$admin_m_subject,$admin_m_text,1);
    } else {
        //管理者メール送信
        send_attached_mail($excel_file,$admin_m_from,$admin_m_to,$admin_m_subject,$admin_m_text,1);
    }

    //サーバ上の一時ファイルを削除
    $dir = rtrim($tmp_dir_name,'/');
    deleteFile($dir,$tempFileDel);

if($remail == 1)
    //mail($post_mail,$re_subject,$userBody,$reheader);
    $a = 1;
}
else if($confirmDsp == 1){


/*--------------------------------------------*/
// 確認画面
/*--------------------------------------------*/
if(isset($_POST['goDate']) && isset($_POST['goTime'])){
    $_POST['go_date_excel'] = $_POST['goDate'];
    $_POST['goDate'] = $_POST['goDate'].' '.$_POST['goTime'];
}
if(isset($_POST['backDate']) && isset($_POST['backTime'])){
    $_POST['back_date_excel'] = $_POST['backDate'];
    $_POST['backDate'] = $_POST['backDate'].' '.$_POST['backTime'];
}

if(isset($_POST['place_go_text']) && !empty($_POST['place_go_text'])){
    var_dump($_POST['place_go']);
    $_POST['place_go'] = $_POST['place_go'].'／'.$_POST['place_go_text'];
}
if(isset($_POST['place_back_text']) && !empty($_POST['place_back_text'])){
    var_dump($_POST['place_back']);
    $_POST['place_back'] = $_POST['place_back'].'／'.$_POST['place_back_text'];
}

// driver結合
for($i=1;$i<=4;$i++){
    if(isset($_POST['driver'.$i.'_name']) && !empty($_POST['driver'.$i.'_name'])){
        $val = $_POST['driver'.$i.'_name'];
        if(isset($_POST['driver'.$i.'_tel']) && !empty($_POST['driver'.$i.'_tel'])){
            $val = $val.'／'.$_POST['driver'.$i.'_tel'];
        }
        if(isset($_POST['driver'.$i.'_no']) && !empty($_POST['driver'.$i.'_no'])){
            $val = $val.'／'.$_POST['driver'.$i.'_no'];
        }
        $_POST['driver'.$i.''] = $val;
    }
}



/*　▼▼▼送信確認画面のレイアウト※編集可　オリジナルのデザインも適用可能▼▼▼　*/
?>
<?php get_header(); ?>

<div id="reserve" class="subpage">

    <?php include(TEMPLATEPATH.'/part-title.php'); ?>
    <?php include(TEMPLATEPATH.'/part-pan.php'); ?>
    <div id="main_contents" class="wrapper cf">
        <div id="contents">
            <?php if(is_mobile()): ?>
            <div id="price">
                <?php endif; ?>

                <section class="form">

                    <?php if($empty_flag == 1){ ?>
                    <div align="center">
                        <p class="tac">
                            <?php echo $err_msg1 ?>;
                        </p>
                        <?php echo $errm; ?><br />
                        <br />
                        <input type="button" value=" <?php echo $return_btn ?> " class="backbtn" onClick="history.back()">
                    </div>
                    <?php }else{ ?>
                    <p class="war">
                        <?php echo $war_msg1; ?>
                    </p>

                    <p class="tac mb20">
                        <?php echo $war_msg2; ?>
                    </p>

                    <?php iniGetAddMailXHeader($iniAddX);//php.ini設定チェック?>
                    <form action="<?php echo h($_SERVER['SCRIPT_NAME']); ?>" method="POST">
                        <table class="formTable">
                            <?php echo confirmOutput($_POST);//入力内容を表示?>
                        </table>
                        <p align="center">
                            <input type="hidden" name="mail_set" value="confirm_submit">
                            <input type="hidden" name="httpReferer" value="<?php echo h($_SERVER['HTTP_REFERER']) ;?>">
                            <?php
if(isset($_FILES[$upfile_key]["tmp_name"])){
    $file_count = count($_FILES[$upfile_key]["tmp_name"]);
    for ($i=0;$i<$file_count;$i++) {
        if(!empty($_FILES[$upfile_key]["tmp_name"][$i])){
?>
                            <input type="hidden" name="upfilePath[]" value="<?php echo h($upFilePath[$i]);?>">
                            <input type="hidden" name="upfileType[]" value="<?php echo h($_FILES[$upfile_key]['type'][$i]);?>">
                            <input type="hidden" name="upfileOriginName[]" value="<?php echo h($_FILES[$upfile_key]['name'][$i]);?>">
                            <?php
        }
    }
}
?>
                            <input type="submit" value="<?php echo $object_name['send_btn'][$lang]; ?>">
                            <input type="button" value="<?php echo $object_name['correct_btn'][$lang]; ?>" class="backbtn" onClick="history.back()">
                        </p>
                    </form>
                    <?php } ?>

                    <?php echo $_POST['price_kei'];?>
                </section>

                <?php if(is_mobile()): ?>
            </div>
            <!-- price -->
            <?php endif; ?>
        </div>
        <!-- contents -->
        <div id="sidebar">
            <?php include('./sidebar-price.php'); ?>
        </div>
        <!-- sidebar -->
    </div>
    <!-- reserve -->
</div>
<!-- main_contents -->

<?php get_footer(); ?>
<?php
/* ▲▲▲送信確認画面のレイアウト　※オリジナルのデザインも適用可能▲▲▲　*/
}

if(($jumpPage == 0 && $sendmail == 1) || ($jumpPage == 0 && ($confirmDsp == 0 && $sendmail == 0))) {

/* ▼▼▼送信完了画面のレイアウト　編集可 ※送信完了後に指定のページに移動しない場合のみ表示▼▼▼　*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>完了画面</title>
</head>

<body>
    <div align="center">
        <?php if($empty_flag == 1){ ?>
        <h4>入力にエラーがあります。下記をご確認の上「戻る」ボタンにて修正をお願い致します。</h4>
        <div style="color:red"><?php echo $errm; ?></div>
        <br />
        <br />
        <input type="button" value=" 前画面に戻る " onClick="history.back()">
    </div>
</body>

</html><?php }else{ ?>
送信ありがとうございました。<br />
送信は正常に完了しました。<br />
<br />
<a href="<?php echo $site_top ;?>">トップページへ戻る&raquo;</a>
</div>
<?php copyright(); ?>
<!--  CV率を計測する場合ここにAnalyticsコードを貼り付け -->
</body>

</html>
<?php
/* ▲▲▲送信完了画面のレイアウト 編集可 ※送信完了後に指定のページに移動しない場合のみ表示▲▲▲　*/
  }
}
//確認画面無しの場合の表示、指定のページに移動する設定の場合、エラーチェックで問題が無ければ指定ページヘリダイレクト
else if(($jumpPage == 1 && $sendmail == 1) || $confirmDsp == 0) {
    if($empty_flag == 1){ ?>
<div align="center">
    <h4>入力にエラーがあります。下記をご確認の上「戻る」ボタンにて修正をお願い致します。</h4>
    <div style="color:red"><?php echo $errm; ?></div>
    <br />
    <br />
    <input type="button" value=" 前画面に戻る " onClick="history.back()">
</div>
<?php
    }else{
        header("Location: ".$thanksPage); }
}

// 以下の変更は知識のある方のみ自己責任でお願いします。

//----------------------------------------------------------------------
//  関数定義(START)
//----------------------------------------------------------------------
function checkMail($str){
    $mailaddress_array = explode('@',$str);
    if(preg_match("/^[\.!#%&\-_0-9a-zA-Z\?\/\+]+\@[!#%&\-_0-9a-z]+(\.[!#%&\-_0-9a-z]+)+$/", "$str") && count($mailaddress_array) ==2){
        return true;
    }else{
        return false;
    }
}
function h($string) {
    global $encode;
    return htmlspecialchars($string, ENT_QUOTES,$encode);
}
function sanitize($arr){
    if(is_array($arr)){
        return array_map('sanitize',$arr);
    }
    return str_replace("\0","",$arr);
}
//Shift-JISの場合に誤変換文字の置換関数
function sjisReplace($arr,$encode){
    foreach($arr as $key => $val){
        $key = str_replace('＼','ー',$key);
        $resArray[$key] = $val;
    }
    return $resArray;
}

function send_attached_mail($filePath,$mailFrom,$mailTo,$subject,$mailText,$attachFlg){
    global $lang;

    //カレントの言語を日本語にする
    mb_language("ja");

    //内部文字エンコードを設定する
    mb_internal_encoding("UTF-8");

    if($lang=='ja' || $lang=='en' ) {
        //文字コード変換
        $subject = mb_convert_encoding($subject,"ISO-2022-JP-MS","UTF-8");
        $message = mb_convert_encoding($msg,"ISO-2022-JP-MS","UTF-8");
    }
    if($lang=='zh' || $lang=='th' ) {
        //文字コード変換
        $subject = mb_convert_encoding($subject,"utf-8","auto");
        $message = mb_convert_encoding($msg,"utf-8","auto");
    }

    $file = basename($filePath);

    $header = array();
    $header[] = "From: $mailFrom";
    $header[] = "MIME-Version: 1.0";
    $header[] = "Content-Type: multipart/mixed; boundary=\"__PHPRECIPE__\"";
    $additional_headers = implode("\r\n", $header);

    $body  = "--__PHPRECIPE__\r\n";
    $body .= "Content-Type: text/plain; charset=\"ISO-2022-JP\"\r\n";
    $body .= "\r\n";
    $body .= $mailText . "\r\n";
    $body .= "--__PHPRECIPE__\r\n";


    if ($attachFlg==1) {
        // 添付ファイルへの処理
        $handle = fopen('.' . $filePath, 'r');
        $attachFile = fread($handle, filesize('.' . $filePath));
        fclose($handle);
        $attachEncode = base64_encode($attachFile);

        $body .= "Content-Type: application/vnd.ms-excel; name=\"$file\"\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n";
        $body .= "Content-Disposition: attachment; filename=\"$file\"\r\n";
        $body .= "\r\n";
        $body .= chunk_split($attachEncode) . "\r\n";
        $body .= "--__PHPRECIPE__--\r\n";
    }
    //メール送信
    mb_send_mail($mailTo, $subject, $body, $additional_headers);

}

function send_attached_mail_th($filePath,$mailFrom,$mailTo,$subject,$mailText,$attachFlg){
    global $lang;

    //文字コード変換
    $subject = mb_convert_encoding($subject,"utf-8","auto");
    $mailText = mb_convert_encoding($mailText,"utf-8","auto");

    $file = basename($filePath);

    $header = array();
    $header[] = "From: $mailFrom";
    $header[] = "MIME-Version: 1.0";
    $header[] = "Content-Type: multipart/mixed; boundary=\"__PHPRECIPE__\"";
    $additional_headers = implode("\r\n", $header);

    $body  = "--__PHPRECIPE__\r\n";
    $body .= "Content-Type: text/plain; charset=\"utf-8\"\r\n";
    $body .= "\r\n";
    $body .= $mailText . "\r\n";
    $body .= "--__PHPRECIPE__\r\n";


    if ($attachFlg==1) {
        // 添付ファイルへの処理
        $handle = fopen('.' . $filePath, 'r');
        $attachFile = fread($handle, filesize('.' . $filePath));
        fclose($handle);
        $attachEncode = base64_encode($attachFile);

        $body .= "Content-Type: application/vnd.ms-excel; name=\"$file\"\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n";
        $body .= "Content-Disposition: attachment; filename=\"$file\"\r\n";
        $body .= "\r\n";
        $body .= chunk_split($attachEncode) . "\r\n";
        $body .= "--__PHPRECIPE__--\r\n";
    }
    //メール送信
    mail($mailTo, $subject, $body, $additional_headers);

}

/************************************************
//送信メールにPOSTデータをセットする関数
**************************************************/
function postToMail($arr){
    global $hankaku,$hankaku_array;
    $resArray = '';
    foreach($arr as $key => $val){

        if(strpos($key,'price_') !== false){
            continue;
        }
        $out = '';
        if(is_array($val)){
            foreach($val as $key02 => $item){
                //連結項目の処理
                if(is_array($item)){
                    $out .= connect2val($item);
                }else{
                    $out .= $item . ', ';
                }
            }
            $out = rtrim($out,', ');

        }else{ $out = $val; }//チェックボックス（配列）追記ここまで
        if(get_magic_quotes_gpc()) { $out = stripslashes($out); }

        //全角→半角変換
        if($hankaku == 1){
            $out = zenkaku2hankaku($key,$out,$hankaku_array);
        }

        if($out != "confirm_submit" && $key != "httpReferer" && $key != "upfilePath" && $key != "upfileType") {

            if($key == "upfileOriginName" && $out !=''){
                $key = '添付ファイル';
            }elseif($key == "upfileOriginName" && $out ==''){
                continue;
            }

            $resArray .= "【 ".$key." 】 ".$out."\n";
        }
    }
    return $resArray;
}

//確認画面の入力内容出力用関数
function confirmOutput($arr){
    global $upFilePath,$upfile_key,$encode,$hankaku,$hankaku_array,$lang,$object_name,$disp_list;
    $html = '';


    foreach($arr as $key => $val) {

        $out = '';
        if(is_array($val)){
            foreach($val as $key02 => $item){
                //連結項目の処理
                if(is_array($item)){

                    $out .= connect2val($item);
                }else{
                    $out .= $item . ', ';
                }
            }
            $out = rtrim($out,', ');

        }else{ $out = $val; }//チェックボックス（配列）追記ここまで
        if(get_magic_quotes_gpc()) { $out = stripslashes($out); }
        //$out = nl2br(h($out));//※追記 改行コードを<br>タグに変換
        $key = h($key);

        //全角→半角変換
        if($hankaku == 1){
            $out = zenkaku2hankaku($key,$out,$hankaku_array);
        }


        // 必須項目ではない項目でNULLの場合は確認画面には表示しない
        if($out!="") {

            // nameが「price_」なら表示せずhiddenに入れる
            if(in_array($key,$disp_list) ){
                $html .= "<tr><th>".$object_name[$key][$lang]."</th><td>".mb_convert_kana($out,"K", $encode);
                $html .= '<input type="hidden" name="'.$key.'" value="'.str_replace(array("<br />","<br>"),"",mb_convert_kana($out,"K", $encode)).'" />';
                $html .= "</td></tr>\n";
            }else{
                $html .= '<input type="hidden" name="'.$key.'" value="'.str_replace(array("<br />","<br>"),"",mb_convert_kana($out,"K", $encode)).'" />';
            }

        } else {
                $html .= '<input type="hidden" name="'.$key.'" value="'.str_replace(array("<br />","<br>"),"",mb_convert_kana($out,"K", $encode)).'" />';
        }

    }

    //添付ファイル表示処理
    if(isset($_FILES[$upfile_key]["tmp_name"])){
        $file_count = count($_FILES[$upfile_key]["tmp_name"]);
        $j = 1;
        for($i=0;$i<$file_count;$i++,$j++) {
            //添付があったらファイル名表示
            if(!empty($upFilePath[$i])){
              $html .= "<tr><th>添付ファイル名{$j}.</th><td>{$_FILES[$upfile_key]['name'][$i]}</td></tr>\n";
            }
        }
    }

    return $html;
}
//全角→半角変換
function zenkaku2hankaku($key,$out,$hankaku_array){
    global $encode;
    if(is_array($hankaku_array) && function_exists('mb_convert_kana')){
        foreach($hankaku_array as $hankaku_array_val){
            if($key == $hankaku_array_val){
                $out = mb_convert_kana($out,'a',$encode);
            }
        }
    }
    return $out;
}
//配列連結の処理
function connect2val($arr){
    $out = '';
    foreach($arr as $key => $val){
        if($key === 0 || $val == ''){//配列が未記入（0）、または内容が空のの場合には連結文字を付加しない（型まで調べる必要あり）
            $key = '';
        }elseif(strpos($key,"円") !== false && $val != '' && preg_match("/^[0-9]+$/",$val)){
            $val = number_format($val);//金額の場合には3桁ごとにカンマを追加
        }

        $key = str_replace("br","<br>", $key);
        $out .= $val . $key;

    }
    return $out;
}
//管理者宛送信メールヘッダ
function adminHeader($userMail,$post_mail,$BccMail,$to){
    $header = '';

    //メールで日本語使用するための設定
    mb_language("Ja") ;
    mb_internal_encoding("utf-8");

    if($userMail == 1 && !empty($post_mail)) {
        $header="From: $post_mail\n";
        if($BccMail != '') {
          $header.="Bcc: $BccMail\n";
        }
        $header.="Reply-To: ".$post_mail."\n";
    }else {
        if($BccMail != '') {
          $header="Bcc: $BccMail\n";
        }
        $header.="Reply-To: ".$to."\n";
    }

    //----------------------------------------------------------------------
    //  添付ファイル処理(START)
    //----------------------------------------------------------------------
    if(isset($_POST['upfilePath'])){
        $header .= "MIME-Version: 1.0\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"__PHPFACTORY__\"\n";
    }else{
        $header.="Content-Type:text/plain;charset=iso-2022-jp\nX-Mailer: PHP/".phpversion();
    }

    return $header;
}

//管理者宛送信メールボディ
function mailToAdmin($arr,$subject,$mailFooterDsp,$mailSignature,$encode,$confirmDsp){
    global $rename;
    $adminBody = '';
    //----------------------------------------------------------------------
    //  添付ファイル処理(START)
    //----------------------------------------------------------------------
    if(isset($_POST['upfilePath'])){
        $adminBody .= "--__PHPFACTORY__\n";
        $adminBody .= "Content-Type: text/plain; charset=\"ISO-2022-JP\"\n";
        //$adminBody .= "\n";
    }

    //----------------------------------------------------------------------
    //  添付ファイル処理(END)
    //----------------------------------------------------------------------

    $adminBody .="「".$subject."」からメールが届きました \n\n";
    $adminBody .="＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
    $adminBody .= postToMail($arr);//POSTデータを関数からセット
    $adminBody .="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n";
    $adminBody .="送信された日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
    $adminBody .="送信者のIPアドレス：".@$_SERVER["REMOTE_ADDR"]."\n";
    $adminBody .="送信者のホスト名：".getHostByAddr(getenv('REMOTE_ADDR'))."\n";
    if($confirmDsp != 1){
        $adminBody.="問い合わせのページURL：".@h($_SERVER['HTTP_REFERER'])."\n";
    }else{
        $adminBody.="問い合わせのページURL：".@$arr['httpReferer']."\n";
    }
    if($mailFooterDsp == 1) $adminBody.= $mailSignature."\n";

//----------------------------------------------------------------------
//  添付ファイル処理(START)
//----------------------------------------------------------------------

//if(isset($_POST['upfilePath'])){
if(1==1){
    $default_internal_encode = mb_internal_encoding();
    if($default_internal_encode != $encode){
        mb_internal_encoding($encode);
    }

    //$file_count = ccouount($_POST['upfilePath']);
    $file_count = 1;

    for ($i=0;$i<$file_count;$i++) {

        if(1==1){

        $adminBody .= "--__PHPFACTORY__\n";
//        $filePath = h("./data/");//ファイルパスを指定
//        $fileName = h("template.xlsx");
        $imgType = h("application/vnd.ms-excel");

        //ファイル名が文字化けする場合には連番ファイル名とする
    //    if($rename == 1){
    //        $fileNameArray = explode(".",$fileName);
    //        $fileName = $i.'.'.end($fileNameArray);
    //    }


        # 添付ファイルへの処理をします。
    //    $handle = @fopen($filePath, 'r');
    //    $attachFile = @fread($handle, filesize($filePath));
    //    @fclose($handle);
    //    $attachEncode = base64_encode($attachFile);

    //    $handle = @fopen($filePath, 'r');


// 添付するファイル
//$dir = './data/';
//$file = 'template.xlsx';
//$fileName    = $dir.$file;

    // 添付ファイルへの処理をします。
//    $handle = fopen($fileName, 'r');
//    $attachFile = fread($handle, filesize($fileName));
//    fclose($handle);
//    $attachEncode = base64_encode($attachFile);

$fileName="template.png";
$file ="template.png";

//        $adminBody .= "Content-Type: {$imgType}; name=\"$fileName\"\n";
        $adminBody .= "Content-Type: image/png; name=\"$fileName\"\n";
        //$adminBody .= "Content-Type: application/vnd.ms-excel; name=\"$fileName\"\n";
        $adminBody .= "Content-Transfer-Encoding: base64\n";
        $adminBody .= "Content-Disposition: attachment; filename=\"$fileName\"\n";
        $adminBody .= "\n";
        $adminBody .= chunk_split($attachEncode) . "\n";
        }
    }
        $adminBody .= "--__PHPFACTORY__--\n";
}
//----------------------------------------------------------------------
//  添付ファイル処理(END)
//----------------------------------------------------------------------

    return mb_convert_encoding($adminBody,"JIS",$encode);
    //return $adminBody;
}
//ユーザ宛送信メールヘッダ
function userHeader($refrom_name,$to,$encode){
    $reheader = "From: ";
    if(!empty($refrom_name)){
        $default_internal_encode = mb_internal_encoding();
        if($default_internal_encode != $encode){
            mb_internal_encoding($encode);
        }
        $reheader .= mb_encode_mimeheader($refrom_name)." <".$to.">\nReply-To: ".$to;
    }else{
        $reheader .= "$to\nReply-To: ".$to;
    }
    $reheader .= "\nContent-Type: text/plain;charset=iso-2022-jp\nX-Mailer: PHP/".phpversion();
    return $reheader;
}
//ユーザ宛送信メールボディ
function mailToUser($arr,$dsp_name,$remail_text,$mailFooterDsp,$mailSignature,$encode){
    $userBody = '';
    if(isset($arr[$dsp_name])) $userBody = h($arr[$dsp_name]). " 様\n";
    $userBody.= $remail_text;
    $userBody.="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
    $userBody.= postToMail($arr);//POSTデータを関数からセット
    $userBody.="\n＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝\n\n";
    $userBody.="送信日時：".date( "Y/m/d (D) H:i:s", time() )."\n";
    if($mailFooterDsp == 1) $userBody.= $mailSignature;
    return mb_convert_encoding($userBody,"JIS",$encode);
}
//必須チェック関数
function requireCheck($require){
    global $upFilePath,$upfile_key,$encode,$hankaku,$hankaku_array,$lang,$object_name,$disp_list;
    $res['errm'] = '';
    $res['empty_flag'] = 0;
    foreach($require as $requireVal){
        $existsFalg = '';
        foreach($_POST as $key => $val) {
            if($key == $requireVal) {

                //連結指定の項目（配列）のための必須チェック
                if(is_array($val)){
                    $connectEmpty = 0;
                    foreach($val as $kk => $vv){
                        if(is_array($vv)){
                            foreach($vv as $kk02 => $vv02){
                                if($vv02 == ''){
                                    $connectEmpty++;
                                }
                            }
                        }

                    }
                    if($connectEmpty > 0){
                        $res['errm'] .= "<p class=\"error_messe\">【".h($object_name[$key][$lang])."】は必須項目です。</p>\n";
                        $res['empty_flag'] = 1;
                    }
                }
                //デフォルト必須チェック
                elseif($val == ''){
                    $res['errm'] .= "<p class=\"error_messe\">【".h($object_name[$key][$lang])."】は必須項目です。</p>\n";
                    $res['empty_flag'] = 1;
                }

                $existsFalg = 1;
                break;
            }

        }
        if($existsFalg != 1){
                $res['errm'] .= "<p class=\"error_messe\">【".h($object_name[$requireVal][$lang])."】が未選択です。</p>\n";
                $res['empty_flag'] = 1;
        }
    }

    return $res;
}


//リファラチェック
function refererCheck($Referer_check,$Referer_check_domain){
    if($Referer_check == 1 && !empty($Referer_check_domain)){
        if(strpos(h($_SERVER['HTTP_REFERER']),$Referer_check_domain) === false){
            return exit('<p align="center">リファラチェックエラー。フォームページのドメインとこのファイルのドメインが一致しません</p>');
        }
    }
}
function copyright(){
    echo '';
}
//ファイル添付用一時ファイルの削除
function deleteFile($dir,$tempFileDel){
    global $permission_file;

    if($tempFileDel == 1){
        if(isset($_POST['upfilePath'])){
            foreach($_POST['upfilePath'] as $key => $val){

                foreach($permission_file as $permission_file_val){
                    if(strpos(strtolower($val),$permission_file_val) !== false && file_exists($val)){
                        if(strpos($val,'htaccess') !== false) exit();
                        unlink($val);
                        break;
                    }
                }

            }
        }

        //ゴミファイルの削除（1時間経過したもののみ）※確認画面→戻る→確認画面の場合、先の一時ファイルが残るため
        if(file_exists($dir) && !empty($dir)){
        $handle = opendir($dir);
          while($temp_filename = readdir($handle)){
            if(strpos($temp_filename,'temp_file_') !== false ){
                if( strtotime(date("Y-m-d H:i:s",filemtime($dir."/".$temp_filename))) < strtotime(date("Y-m-d H:i:s",strtotime("-1 hour"))) ){
                    @unlink("$dir/$temp_filename");
                }
            }
          }
        }
    }
}
//php.iniのmail.add_x_headerのチェック
function iniGetAddMailXHeader($iniAddX){
    if($iniAddX == 1){
        if(@ini_get('mail.add_x_header') == 1) echo '<p style="color:red">php.iniの「mail.add_x_header」がONになっています。添付がうまくいかない可能性が高いです。htaccessファイルかphp.iniファイルで設定を変更してOFFに設定下さい。サーバーにより設定方法は異なります。詳しくはサーバーマニュアル等、またはサーバー会社にお問い合わせ下さい。正常に添付できていればOKです。このメーッセージはmail.php内のオプションで非表示可能です</p>';
    }
}

//トラバーサル対策
function traversalCheck($tmp_dir_name){
    if(isset($_POST['upfilePath']) && is_array($_POST['upfilePath'])){
        foreach($_POST['upfilePath'] as $val){
            if(strpos($val,$tmp_dir_name) === false || strpos($val,'temp_file_') === false) exit('Warning!! you are wrong..1');//ルール違反は強制終了
            if(substr_count($tmp_dir_name,'/') != substr_count($val,'/') ) exit('Warning!! you are wrong..2');//ルール違反は強制終了
            if(strpos($val,'htaccess') !== false) exit('Warning!! you are wrong..3');
            if(!file_exists($val)) exit('Warning!! you are wrong..4');
            if(strpos(str_replace($tmp_dir_name,'',$val),'..') !== false)  exit('Warning!! you are wrong..5');
        }
    }
}

//----------------------------------------------------------------------
//  関数定義(END)
//----------------------------------------------------------------------
?>
