<?php require_once('./lang/lang.php'); ?>
<div class="price_area">
	<h3><?php echo (isset($_POST['estimationPrice'])) ? $_POST['estimationPrice'] : $estimationPrice; ?></h3>
	<div class="box border">
		<dl class="cf" id="price_basiccharge">
			<dt><?php echo (isset($_POST['basicPrice'])) ? $_POST['basicPrice'] : $basicPrice; ?></dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_basiccharge_num"><?php echo (isset($_POST['price_basiccharge'])) ? number_format($_POST['price_basiccharge']) : 0;?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?></dd>
		</dl>

		 <dl class="cf" id="price_over" <?php if(!isset($_POST['over_price']) || $_POST['over_price']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt><?php echo $extension_fee; ?></dt>
			<dd> 
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
                <span id="price_over_num"><?php if(isset($_POST['over_price'])) echo number_format($_POST['over_price']);?>
                </span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
             </dd>
		</dl> 
        
		<?php //var_dump($_POST); ?>
		<dl class="cf" id="price_discount" <?php if(!isset($_POST['price_discount']) || $_POST['price_discount']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt id="discount"><?php echo $_POST['disc_plan_name'] ?></dt>
			<dd class="price waribiki">- 
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_discount_num"><?php if(isset($_POST['price_discount'])) echo number_format($_POST['price_discount']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>

<!-- 一旦ここに移動-->

	</div>
	<div class="box">
		<dl class="cf" id="price_dispatch" <?php if(!isset($_POST['price_dispatch']) || $_POST['price_dispatch']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt><?php echo (isset($_POST['vehicleDeliveryFee'])) ? $_POST['vehicleDeliveryFee'] : $vehicleDeliveryFee; ?></dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_dispatch_num"><?php if(isset($_POST['price_dispatch'])) echo number_format($_POST['price_dispatch']);?></span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf" id="price_cleaning" <?php if(!isset($_POST['price_cleaning']) || $_POST['price_cleaning']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt><?php echo (isset($_POST['cleaningFee'])) ? $_POST['cleaningFee'] : $cleaningFee; ?></dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_cleaning_num"><?php if(isset($_POST['price_cleaning'])) echo number_format($_POST['price_cleaning']);?></span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>

			</dd>
		</dl>
		<dl class="cf" id="price_menseki" <?php if(!isset($_POST['price_menseki']) || $_POST['price_menseki']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt><?php echo (isset($_POST['menseki'])) ? $_POST['menseki'] : $menseki; ?></dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_menseki_num"><?php if(isset($_POST['price_menseki'])) echo number_format($_POST['price_menseki']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>

			</dd>
		</dl>
		<dl class="cf" id="price_safety" <?php if(!isset($_POST['price_safety']) || $_POST['price_safety']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt><?php echo (isset($_POST['anshinPack'])) ? $_POST['anshinPack'] : $anshinPack; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_safety_num"><?php if(isset($_POST['price_safety'])) echo number_format($_POST['price_safety']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf" id="price_bedding" <?php if(!isset($_POST['price_bedding']) || $_POST['price_bedding']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt>
				<?php echo (isset($_POST['bedClean'])) ? $_POST['bedClean'] : $bedClean; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_bedding_num"><?php if(isset($_POST['price_bedding'])) echo number_format($_POST['price_bedding']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf" id="price_goods" <?php if(!isset($_POST['price_goods']) || $_POST['price_goods']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt>
				<?php echo (isset($_POST['rentalItem'])) ? $_POST['rentalItem'] : $rentalItem; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_goods_num"><?php if(isset($_POST['price_goods'])) echo number_format($_POST['price_goods']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf" id="price_hep" <?php if(!isset($_POST['price_hep']) || $_POST['price_hep']==0) : ?> style="display:none;" <?php endif; ?>>
			<dt>
				<?php echo (isset($_POST['hep_name'])) ? $_POST['hep_name'] : $hep; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_hep_num"><?php if(isset($_POST['price_hep'])) echo number_format($_POST['price_hep']);?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
<!-- 一旦ここに移動-->
	</div>


	<div class="box kei">
		<dl class="cf" id="price_subtotal">
			<dt>
				<?php echo (isset($_POST['subtotal'])) ? $_POST['subtotal'] : $subtotal; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_subtotal_num"><?php echo (isset($_POST['price_subtotal'])) ? number_format($_POST['price_subtotal']) : 0;?>
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf" id="price_tax">
			<dt>
				<?php echo (isset($_POST['tax'])) ? $_POST['tax'] : $tax; ?>
			</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_tax_num"><?php echo (isset($_POST['price_tax'])) ? number_format($_POST['price_tax']) : 0;?>	
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
		<dl class="cf sum" id="price_total">
			<dt>
				<?php echo (isset($_POST['totalPrice'])) ? $_POST['totalPrice'] : $totalPrice; ?>
				</dt>
			<dd>
				<?php if(lang()=='ja'): ?>&yen;<?php endif; ?>
				<?php if(lang()=='zh'): ?>日幣<?php endif; ?>
				<span id="price_total_num"><?php echo (isset($_POST['price_total'])) ? number_format($_POST['price_total']) : 0;?>	
				</span>
				<?php if(lang()=='en'): ?>JPY<?php endif; ?>
				<?php if(lang()=='th'): ?>เยน<?php endif; ?>
			</dd>
		</dl>
	</div>
	<!-- box -->
	<div id="log">

	</div>
</div>
<!-- price_area -->
<?php if(lang()=='ja'): ?>
<p style="margin-top:5px;margin-left:10px;">※For English, please move to another <a style="color:#33F;text-decoration:underline;" href="<?php bloginfo('url'); ?>/reservation.php?lang=en">webpage</a>.</p>
<?php endif; ?>

