<?php
$carmodel_post_id = 288; // 車種のカスタム投稿ID
$price_post_id = 315; // 料金のカスタム投稿ID
$rental_post_id = 325;

while( have_rows('最高級ラグジュアリークラス',$price_post_id) ): the_row();
$price_table['luxury'][get_sub_field('シーズン区分')] = array('day' => get_sub_field('一日料金'),'half' => get_sub_field('半日料金'),'time' => get_sub_field('営業時間外_1時間延長'));
endwhile;

while( have_rows('プレミアムクラス',$price_post_id) ): the_row();
$price_table['premium'][get_sub_field('シーズン区分')] = array('day' => get_sub_field('一日料金'),'half' => get_sub_field('半日料金'),'time' => get_sub_field('営業時間外_1時間延長'));
endwhile;

while( have_rows('ハイクラス',$price_post_id) ): the_row();
$price_table['high'][get_sub_field('シーズン区分')] = array('day' => get_sub_field('一日料金'),'half' => get_sub_field('半日料金'),'time' => get_sub_field('営業時間外_1時間延長'));
endwhile;

//モーダルのカラーで使用してます
$modal_setting = array (
'free'        =>    array('background' => '#ffffff','color' => '#000','subtitle' => 'オフシーズン平日'),
'off_syuku' =>    array('background' => '#cccccc','color' => '#000','subtitle' => 'オフシーズン 金土日・祝日'),
'on_tsu'     =>    array('background' => '#95fcf8','color' => '#000','subtitle' => 'オンシーズン 平日'),
'on_syuku'     =>    array('background' => '#00a0e9','color' => '#000','subtitle' => 'オンシーズン 金土日・祝日'),
'high'         =>    array('background' => '#76f94e','color' => '#000','subtitle' => 'ハイシーズン 全日'),
'winter'     =>    array('background' => '#ff9827','color' => '#000','subtitle' => 'ウィンターシーズン')
);

?>
<script>
    <?php $i = 0; ?>
    var objCarModel = {
        <?php while( have_rows('車種',$carmodel_post_id) ): the_row(); ?>
        <?php if($i!==0): echo ","; endif; ?>
        <?php echo get_sub_field('車種コード'); ?>: {
            class: "<?php echo get_sub_field('車種クラス'); ?>",
            pet: <?php if(get_sub_field('ペット乗車可能')): echo "true"; else: echo "false"; endif; ?>
        }
        <?php $i++ ?>
        <?php endwhile; ?>
    };

    var objPriceList = {
        <?php $i = 0; ?>
        luxury: {
            <?php while( have_rows('最高級ラグジュアリークラス',$price_post_id) ): the_row(); ?>
            <?php if($i!==0): echo ","; endif; ?>
            <?php echo get_sub_field('シーズン区分'); ?>: {
                one_day: <?php echo get_sub_field('一日料金'); ?>,
                half_day: <?php echo get_sub_field('半日料金'); ?>,
                over_time: <?php echo get_sub_field('営業時間外_1時間延長'); ?>
            }
            <?php $i++ ?>
            <?php endwhile; ?>
        },
        <?php $i = 0; ?>
        premium: {
            <?php while( have_rows('プレミアムクラス',$price_post_id) ): the_row(); ?>
            <?php if($i!==0): echo ","; endif; ?>
            <?php echo get_sub_field('シーズン区分'); ?>: {
                one_day: <?php echo get_sub_field('一日料金'); ?>,
                half_day: <?php echo get_sub_field('半日料金'); ?>,
                over_time: <?php echo get_sub_field('営業時間外_1時間延長'); ?>
            }
            <?php $i++ ?>
            <?php endwhile; ?>
        },
        <?php $i = 0; ?>
        high: {
            <?php while( have_rows('ハイクラス',$price_post_id) ): the_row(); ?>
            <?php if($i!==0): echo ","; endif; ?>
            <?php echo get_sub_field('シーズン区分'); ?>: {
                one_day: <?php echo get_sub_field('一日料金'); ?>,
                half_day: <?php echo get_sub_field('半日料金'); ?>,
                over_time: <?php echo get_sub_field('営業時間外_1時間延長'); ?>
            }
            <?php $i++ ?>
            <?php endwhile; ?>
        }
    };

    var objRentalGoods = {
        <?php $i = 0; ?>
        <?php while( have_rows('有料レンタルグッズ',$rental_post_id) ): the_row(); ?>
        <?php if(!get_sub_field('非表示')): ?>
        <?php if($i!==0): echo ","; endif; ?>
        <?php echo get_sub_field('レンタルグッズコード'); ?>: {
            amt: 0,
            set_exists: <?php if(get_sub_field('セット貸出有')): echo "true"; else: echo "false"; endif; ?>,
            unit_name: "<?php echo get_sub_field('助数詞'); ?>",
            price: {
                luxury: <?php echo get_sub_field('ラグジュアリークラス料金'); ?>,
                premium: <?php echo get_sub_field('プレミアムクラス料金'); ?>,
                high: <?php echo get_sub_field('ハイクラス料金'); ?>
            }
        }
        <?php $i++ ?>
        <?php endif; ?>
        <?php endwhile; ?>
    };
    // Object.keys(objRentalGoods).forEach(function(key) {
    // console.log(key, objRentalGoods[key]);
    // });

</script>
